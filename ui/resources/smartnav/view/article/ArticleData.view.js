sap.ui.jsview("view.article.ArticleData", {

	getControllerName : function() {
		return "view.article.ArticleData";
	},

	createContent : function(oController) {
		var parent = this;
		
		var nameLbl = new sap.ui.commons.Label({text: "Title:"}).addStyleClass("docLabel");
		var nameTF = new sap.ui.commons.TextField({
			width: "100%"
		});
		
		var authorLbl = new sap.ui.commons.Label({text: "Author:"}).addStyleClass("docLabel");
		var authorTF = new sap.ui.commons.TextField({
			width: "100%"
		});

		var dateLbl = new sap.ui.commons.Label({text: "Date:"}).addStyleClass("docLabel");
		var datePicker = new sap.ui.commons.DatePicker()
		
		var content = new sap.ui.commons.TextArea({width: "100%", rows: 12, editable: false}).addStyleClass("articleDisplay");
		content.setValue(this.getViewData().content);
		var contentCell = new sap.ui.commons.layout.MatrixLayoutCell({colSpan: 2, content: content});
		
		
		var eventLbl = new sap.ui.commons.Label({text: "Event Data"}).addStyleClass("dividerLbl");
		var divider = new sap.ui.commons.HorizontalDivider({width: "100%"}).addStyleClass("divider");
		var dividerLayout = new sap.ui.layout.VerticalLayout({
			width: "100%",
			content: [eventLbl, divider]
		});

		
		var oItemTemplate = new sap.ui.core.ListItem();
		oItemTemplate.bindProperty("text", "name");
		oItemTemplate.bindProperty("key", "name");
			
		var eventTypeLbl = new sap.ui.commons.Label({text: "Event Type:"}).addStyleClass("eventLabel");
		var eventTypeDD = new sap.ui.commons.DropdownBox({
			width: "100%",
			items: {
				path: "/eventtypes",
				template: oItemTemplate
			},
		});

		var actor1Lbl = new sap.ui.commons.Label({text: "Actor 1:"}).addStyleClass("eventLabel");
		var actor1TF = new sap.ui.commons.TextField({
			width: "100%"
		});
		
		var actor2Lbl = new sap.ui.commons.Label({text: "Actor 2:"}).addStyleClass("eventLabel");
		var actor2TF = new sap.ui.commons.TextField({
			width: "100%"
		});
		
		var eventDateLbl = new sap.ui.commons.Label({text: "Event Date:"}).addStyleClass("eventLabel");
		var eventDatePicker = new sap.ui.commons.DatePicker();
		
		var oDocLayout = new sap.ui.commons.layout.MatrixLayout({width: "100%",
			widths : [ "75px", ""]});
		oDocLayout.createRow(nameLbl, nameTF);
		oDocLayout.createRow(authorLbl, authorTF);
		oDocLayout.createRow(dateLbl, datePicker);
		oDocLayout.createRow(contentCell);
		
		
		var oEventLayout = new sap.ui.commons.layout.MatrixLayout({width: "100%",
			widths : [ "75px", ""]});
		oEventLayout.createRow(eventTypeLbl, eventTypeDD);
		oEventLayout.createRow(actor1Lbl, actor1TF);
		oEventLayout.createRow(actor2Lbl, actor2TF);
		oEventLayout.createRow(eventDateLbl, eventDatePicker);
		
		var saveButton = new sap.ui.commons.Button({
			text : "Save",
			press : function() {
				var oDialog = parent.getViewData().dialog;
				var doc = {name: nameTF.getValue(), author: authorTF.getValue(), date: datePicker.getValue()};
				var event = {type: eventTypeDD.getValue(), actor1: actor1TF.getValue(), actor2: actor2TF.getValue(), date: eventDatePicker.getValue()};
				oController.save(parent.getViewData().id, doc, event);
				oDialog.destroy();
			}
		});
		
		var oButtonLayout = new sap.ui.layout.HorizontalLayout({
			content: [saveButton]
		}).addStyleClass("buttonLayout");
		
		var oVLayout = new sap.ui.layout.VerticalLayout({
			content: [oDocLayout, dividerLayout, oEventLayout, oButtonLayout]
		});
		
		return oVLayout;
	}
});