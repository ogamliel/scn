sap.ui.controller("view.article.EventUpload", {

	onInit : function() {
		var oModel = new sap.ui.model.json.JSONModel();	
		this.getView().setModel(oModel);
		$.ajax({
		    url: "/smartnavxs/article/eventTypes.xsjs",
		    type: 'GET',
		    async: false,
		    success: function(data) {
		    	// add empty value to the top of drop down
				data.eventtypes.unshift({name: ''});
				oModel.setData(data, false);
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}			    
		});	
		$.ajax({
		    url: "/smartnavxs/article/interactionTypes.xsjs",
		    type: 'GET',
		    async: false,
		    success: function(data) {
		    	// add empty value to the top of drop down
				data.interactiontypes.unshift({name: ''});
				oModel.setData(data, true);
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}			    
		});	
		$.ajax({
		    url: "/smartnavxs/article/countries.xsjs",
		    type: 'GET',
		    async: false,
		    success: function(data) {
		    	// add empty value to the top of drop down
				data.countries.unshift({name: ''});
				oModel.setSizeLimit(2000);
				oModel.setData(data, true);
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}			    
		});	
	},
	
	saveEvent : function (event, statusLbl)  {
		var opts = {
	  			  lines: 13 // The number of lines to draw
	  			, length: 20 // The length of each line
	  			, width: 14 // The line thickness
	  			, radius: 42 // The radius of the inner circle
	  			, scale: 1 // Scales overall size of the spinner
	  			, corners: 1 // Corner roundness (0..1)
	  			, color: '#000' // #rgb or #rrggbb or array of colors
	  			, opacity: 0.25 // Opacity of the lines
	  			, rotate: 0 // The rotation offset
	  			, direction: 1 // 1: clockwise, -1: counterclockwise
	  			, speed: 1 // Rounds per second
	  			, trail: 60 // Afterglow percentage
	  			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	  			, zIndex: 2e9 // The z-index (defaults to 2000000000)
	  			, className: 'spinner' // The CSS class to assign to the spinner
	  			, top: '50%' // Top position relative to parent
	  			, left: '50%' // Left position relative to parent
	  			, shadow: false // Whether to render a shadow
	  			, hwaccel: false // Whether to use hardware acceleration
	  			, position: 'absolute' // Element positioning
	  			}
		
		//$('#el').spin();
		var resp = $.ajax({
	        url: "/smartnavxs/article/insertEvent.xsjs",
	        type: "GET",
	        async: false,
	        data: {"type": event.type,
	        		"typeId": event.typeId,
	        		"actor1": event.actor1,
					"actor2": event.actor2,
					"date": event.date,
					"countryIso3": event.countryIso3,
					"country": event.country,
					"location": event.location,
					"lat": event.lat,
					"lng": event.lng,
					"fatalities" : event.fatalities,
					"comments": encodeURIComponent(event.comments),
					"interactionId": event.interactionId},
	        error: function(xhr, textStatus, errorThrown){
	            console.log(xhr.responseText);
	        	//spinner.stop();
	        },
	        success : function (data)  {
	        	//spinner.stop();	  
	        	statusLbl.setText('Event ' + data.eventId + ' saved.');
	        }
	    });
		return resp.responseJSON.eventId; 
	},
	
	upload : function(eventId, eventType, eventLat, eventLng, eventLoc, eventCountry)  {	
		var parent = this;
    	var reader, name, type;
		var x = document.getElementById("fileUploader");
		var files = x.files;
		var reader;
		
		if (files) {
	        for (var i=0, f; f=files[i]; i++) {
	        	reader = new FileReader();
	        	
	            reader.onload = (function(f) {
	                return function(e) {
	                    var contents = e.target.result;
	                    console.log("Got file: " + f.name + " type: " + f.type + " size: " + f.size + " bytes");
	                    var type = f.type;
	                    if (type === '')  {
	                    	type = parent.getMimeType(f.name);
	                    }
	                    if (type === 'application/pdf')  {
	                    	contents = parent.getPdfText(contents);
	                    	console.log(contents);
	                    	if (contents)  {
	                    		parent.saveDocument(contents, eventId, type, eventType, eventLat, eventLng, eventLoc, eventCountry);
	                    	}
	                    	else  {
	                    		alert ('PDF conversion error.  Document not saved.')
	                    	}
	    			    }
	                    else if (type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')  {
	                    	contents = parent.getDocxText(contents);
	                    	console.log(contents)
	                    	if (contents)  {
	                    		parent.saveDocument(contents, eventId, type, eventType, eventLat, eventLng, eventLoc, eventCountry);
	                    	}
	                    	else  {
	                    		alert ('Docx conversion error.  Document not saved.')
	                    	}
	                    }
	                    else  {
	                    	parent.arrayBufferToString(contents, eventId, type, parent, eventType, eventLat, eventLng, eventLoc, eventCountry);
	                    }	                    
	                };
	            })(f);
	            
	            reader.onloadend = function(event) {
	                var contents = event.target.result,
	                    error    = event.target.error;
	                
	                if (error != null) {
	                    switch (error.code) {
	                        case error.ENCODING_ERR:
	                            console.error("Encoding error!");
	                            break;

	                        case error.NOT_FOUND_ERR:
	                            console.error("File not found!");
	                            break;

	                        case error.NOT_READABLE_ERR:
	                            console.error("File could not be read!");
	                            break;

	                        case error.SECURITY_ERR:
	                            console.error("Security issue with file!");
	                            break;

	                        default:
	                            console.error("I have no idea what's wrong!");
	                    }
	                }
	                
	            }

	            reader.readAsArrayBuffer(f);

	        }   
	    } else {
		      console.log("Failed to load files"); 
	    }
	},
	
	arrayBufferToString : function(buf, eventId, type, parent, eventType, eventLat, eventLng, eventLoc, eventCountry) {
		var contents='';
		var bb = new Blob([new Uint8Array(buf)]);
		  var f = new FileReader();
		  f.onload = function(e) {
			  parent.saveDocument(e.target.result, eventId, type, eventType, eventLat, eventLng, eventLoc, eventCountry);
		  };
		  f.readAsText(bb);
		},
		
	saveDocument : function (contents, eventId, docType, eventType, eventLat, eventLng, eventLoc, eventCountry)  {
		// 1.  Upload the document
		// 2.  Geotag the document using metacarta node.js service
		// 3.  Insert any geotags returned from metacarta into the db
		
	    $.ajax({
	        url: "/smartnavxs/article/upload.xsjs",
	        type: "POST",
	        contentType: docType,
	        data: JSON.stringify({"contents": encodeURIComponent(contents), "eventId": eventId, "eventType": eventType, "lat": eventLat, "lng": eventLng}),
	        dataType: "json",
	        async: false,
	        success:  function(result)  {
	        	if (result)  {
		    	    $.ajax({
		    	        url: "/metacarta/geotagger",
		    	        async: false,
		    	        type: "POST",
		    	        contentType: 'application/json',
		    	        data: JSON.stringify({contents: encodeURIComponent(contents)}),
		    	        success: function(resp)  {
		    	        	if (resp)  {
			    	        	$.ajax({
					    	        url: "/smartnavxs/article/insertGeotags.xsjs",
					    	        async: true,
					    	        type: "POST",
					    	        contentType: 'application/json',
					    	        data: JSON.stringify({geotags: resp, docId: result.id}),
					    	        success: function(resp)  {
					    	        	//console.log(resp);
					    	        },
					    	        error: function(xhr, textStatus, errorThrown){
					    	        	console.log(textStatus)
					    	        	console.log(errorThrown)
					    	            console.log(xhr.responseText);
					    	        }
					    	    });
		    	        	}
		    	        },
		    	        error: function(xhr, textStatus, errorThrown){
		    	        	console.log(textStatus)
		    	        	console.log(errorThrown)
		    	            console.log(xhr.responseText);
		    	        }
		    	    });
				}
	        },
	        error: function(xhr, textStatus, errorThrown){
	            console.log(xhr.responseText);
	        }
	    });
	    
	},
	
	saveTextDocument : function (contents, eventId, type, parent)  {
		parent.saveDocument(contents, eventId, type);
	},
	
	getPdfText : function (buffer)  {
		var resp = $.ajax({
	        url: "/docparser/textract",
	        async: false,
	        type: "POST",
	        processData: false,
	        contentType: 'application/pdf',
	        data: buffer,
	        crossDomain: true,
	        error: function(xhr, textStatus, errorThrown){
	        	console.log(textStatus)
	        	console.log(errorThrown)
	            console.log(xhr.responseText);
	        }
	    });
		return resp.responseText;
	},
	
	getDocxText : function (buffer)  {
		var resp = $.ajax({
	        //url: "https://sgihanar01:64017/mammoth",
	        url: "/docparser/mammoth",
	        async: false,
	        type: "POST",
	        contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	        processData: false,
	        data: buffer,
	        crossDomain: true,
	        error: function(xhr, textStatus, errorThrown){
	            console.log(xhr.responseText);
	        }
	    });
		return resp.responseText;	
	},
	
	getMimeType : function (docName)  {
		var ext = docName.substring(docName.indexOf('.'));
		if (ext === '.docx')  {
			return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
		}
		if (ext === '.xls')  {
			return 'application/vnd.ms-excel';
		}
		if (ext === '.pdf')  {
			return 'application/pdf';
		}
		if (ext === '.csv')  {
			return 'text/csv';
		}
		return 'text/plain';
	},
	
	openDialog : function()  {
		var dialog = new sap.ui.commons.Dialog({
			title  : "Upload Event",
			modal  : true,
			styled: false,
			showCloseButton : false,
			width  : "600px",
			height : "550px"
		});
		var uploadView = sap.ui.view({
			viewName : "view.article.EventUpload",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : {
			dialog : dialog
			}
		});
		dialog.addContent(uploadView);
		dialog.open();	
	}
		
});	