sap.ui.controller("view.article.ArticleData", {

	onInit : function() {
		var oModel = new sap.ui.model.json.JSONModel();	
		this.getView().setModel(oModel);

		$.ajax({
		    url: "/smartnavxs/article/eventTypes.xsjs",
		    type: 'GET',
		    async: false,
		    success: function(data) {
		    	// add empty value to the top of drop down
				data.eventtypes.unshift({name: ''});
				oModel.setData(data, false);
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}			    
		});	
	},
		
	save : function (docId, doc, event)  {
		$.ajax({
		    url: "/smartnavxs/article/updateDoc.xsjs",
		    data: doc,
		    type: 'GET',
		    async: true,
		    success: function(data) {
		    	
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}			    
		});

		if (event && event.type)  {
			$.ajax({
			    url: "/smartnavxs/article/insertEvent.xsjs",
			    data: event,
			    type: 'GET',
			    async: true,
			    success: function(data) {
			    	
			    },
				error: function (xhr, ajaxOptions, thrownError) {
				    console.log(xhr.status);
				    console.log(thrownError);			    
				}			    
			});
		}
	},
		
});	