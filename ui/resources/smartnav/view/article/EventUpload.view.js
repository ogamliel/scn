sap.ui.jsview("view.article.EventUpload", {

	getControllerName : function() {
		return "view.article.EventUpload";
	},

	createContent : function(oController) {
		var parent = this;
		
		var oItemTemplate = new sap.ui.core.ListItem();
		oItemTemplate.bindProperty("text", "name");
		oItemTemplate.bindProperty("key", "id");
			
		var eventLbl = new sap.ui.commons.Label({text: "Event Type:"}).addStyleClass("eventLabel");
		var eventDD = new sap.ui.commons.DropdownBox({
			width: "100%",
			items: {
				path: "/eventtypes",
				template: oItemTemplate
			},
			//selectedKey: "101"
		}).addStyleClass("eventInput");
		eventLbl.setLabelFor(eventDD);

		var interactionLbl = new sap.ui.commons.Label({text: "Interaction Type:"}).addStyleClass("eventLabel");
		var interactionDD = new sap.ui.commons.DropdownBox({
			width: "100%",
			items: {
				path: "/interactiontypes",
				template: oItemTemplate
			},
		}).addStyleClass("eventInput");
		interactionLbl.setLabelFor(interactionDD);

		var actor1Lbl = new sap.ui.commons.Label({text: "Actor 1:"}).addStyleClass("eventLabel");
		var actor1TF = new sap.ui.commons.TextField({
			width: "100%"
		}).addStyleClass("eventInput");
		actor1Lbl.setLabelFor(actor1TF);
		
		var actor2Lbl = new sap.ui.commons.Label({text: "Actor 2:"}).addStyleClass("eventLabel");
		var actor2TF = new sap.ui.commons.TextField({
			width: "100%"
		}).addStyleClass("eventInput");
		actor2Lbl.setLabelFor(actor2TF);

		var dateLbl = new sap.ui.commons.Label({text: "Event Date:"}).addStyleClass("eventLabel");
		var datePicker = new sap.ui.commons.DatePicker({value: {
			path: "/date",
			type: new sap.ui.model.type.Date({pattern: "yyyyMMdd", strictParsing: true})
		}}).addStyleClass("eventInput");
		dateLbl.setLabelFor(datePicker);
		
		var eventLayout = new sap.ui.commons.layout.MatrixLayout({
			width: "100%",
			widths : [ "75px", "350px"]
		});

		var countryLbl = new sap.ui.commons.Label({text: "Country:"}).addStyleClass("eventLabel");
		var countryDD = new sap.ui.commons.DropdownBox({
			width: "100%",
			items: {
				path: "/countries",
				template: oItemTemplate
			},
			change : function(e) {
				locationTF.setValue("");
				locationLookupTF.setValue("");
				latTF.setValue("");
				lngTF.setValue("");
			}
		}).addStyleClass("eventInput");
		countryLbl.setLabelFor(countryDD);

		var locationLbl = new sap.ui.commons.Label({text: "Location:"}).addStyleClass("eventLabel");
		var locationTF = new sap.ui.commons.TextField({
			width: "92%"
		}).addStyleClass("eventInput");
		locationLbl.setLabelFor(locationTF);
		
		var locationButton = new sap.ui.commons.Button({
			icon:'sap-icon://world',
			tooltip: 'Lat/Lng Lookup',  
			press:function() {
				var city = locationTF.getValue();
				var country = countryDD.getLiveValue();
				$.ajax({
				    //url: "http://nominatim.openstreetmap.org/search/" + city + "%20" + country + "?&format=json&addressdetails=1&limit=1&polygon_svg=1",
					url: "/metacarta/locationFinder?query=" + city,
				    async: true,
				    //dataType: 'jsonp',
				    type: 'GET',
				    success: function(data) {
				    	if (data)  {
				    		var locs = data.Locations;
				    		if (locs.length > 0)  {
				    			var loc = locs[0];
				    			var admin = loc.Paths.Administrative;
				    			var locString = loc.Name;
				    			if (admin.length > 0)  {
				    				locString = admin.join();
				    			}
				    			latTF.setValue(loc.Centroid.Latitude);
				    			lngTF.setValue(loc.Centroid.Longitude);
				    			locationLookupTF.setValue(locString);
				    		}
				    	}

				    	/*if (data)  {
				    		latTF.setValue(data[0].lat);
				    		lngTF.setValue(data[0].lon);
				    		locationLookupTF.setValue(data[0].display_name);
				    	}*/
				    },
					error: function (xhr, ajaxOptions, thrownError) {
					    console.log(xhr.status);
					    console.log(thrownError);			    
					}
				});
				
			}
		}).addStyleClass("uploadButton");
		
		var c = sap.ui.commons.layout;
		var oLocationCell = new c.MatrixLayoutCell({
			hAlign : c.HAlign.Begin, 
			vAlign : c.VAlign.Top, 
			colspan: 2,
			content:[locationTF, locationButton]});	

		var locationLookupTF = new sap.ui.commons.TextField({
			width: "100%",
			enabled: false
		}).addStyleClass("eventInput");

		var latLbl = new sap.ui.commons.Label({text: "Latitude:"}).addStyleClass("eventLabel");
		var latTF = new sap.ui.commons.TextField({
			width: "100%"
		}).addStyleClass("eventInput");
		latLbl.setLabelFor(latTF);

		var lngLbl = new sap.ui.commons.Label({text: "Longitude:"}).addStyleClass("eventLabel");
		var lngTF = new sap.ui.commons.TextField({
			width: "100%"
		}).addStyleClass("eventInput");
		lngLbl.setLabelFor(lngTF);

		var fatalitiesLbl = new sap.ui.commons.Label({text: "Fatalities:"}).addStyleClass("eventLabel");
		var fatalitiesTF = new sap.ui.commons.TextField({
			width: "100%"
		}).addStyleClass("eventInput");
		fatalitiesLbl.setLabelFor(fatalitiesTF);

		var commentsLbl = new sap.ui.commons.Label({text: "Comments:"}).addStyleClass("eventLabel");
		var commentsTA = new sap.ui.commons.TextArea({
			width: "100%", 
			rows: 4
		}).addStyleClass("eventInput");
		commentsLbl.setLabelFor(commentsTA);

		eventLayout.createRow(eventLbl,eventDD);
		eventLayout.createRow(interactionLbl,interactionDD);
		eventLayout.createRow(actor1Lbl,actor1TF);
		eventLayout.createRow(actor2Lbl,actor2TF);
		eventLayout.createRow(dateLbl, datePicker);
		eventLayout.createRow(countryLbl, countryDD);
		//eventLayout.createRow(locationLbl, locationTF, locationButton);
		eventLayout.createRow(locationLbl, oLocationCell);
		eventLayout.createRow("", locationLookupTF);
		eventLayout.createRow(latLbl, latTF);
		eventLayout.createRow(lngLbl, lngTF);
		eventLayout.createRow(fatalitiesLbl, fatalitiesTF);
		eventLayout.createRow(commentsLbl, commentsTA);

		var htmlUploader = new sap.ui.core.HTML( {
			content:
				//"<input id=\"fileUploader\" type=\"file\" multiple=\"multiple\" onChange=\"getFileMetadata()\" class=\"article\">",
				"<input id=\"fileUploader\" type=\"file\" multiple=\"multiple\" class=\"article\">",
			preferDOM : false
		});
		
		/*var htmlFiles = new sap.ui.core.HTML( {
			content:
				"<div id=\"fileGallery\"></div>",
			preferDOM : false
		});*/
		
		var oFileUploader = new sap.ui.commons.FileUploader({
			uploadUrl : "/FFA/smartnavxs/rest/article/uploadUI5.xsjs",   // URL to submit the form to
			name: "uploader",          // name of the input type=file element within the form
			uploadOnChange: false,            // immediately upload the file after selection
			width: "400px",
			multiple: true,			
		});
		
		var oUploadButton = new sap.ui.commons.Button({
			text:'Upload',
			press:function() {	    		
				var obj = {typeId: eventDD.getSelectedKey(),
							type: eventDD.getLiveValue(),
							actor1: actor1TF.getValue(),
							actor2: actor2TF.getValue(),
							date: datePicker.getValue(),
							countryIso3: countryDD.getSelectedKey(),
							country: countryDD.getLiveValue(),
							location: locationTF.getValue(),
							lat: latTF.getValue(),
							lng: lngTF.getValue(),
							fatalities : fatalitiesTF.getValue(),
							comments: commentsTA.getValue(),
							interactionId: interactionDD.getSelectedKey()
						}
				var eventId = oController.saveEvent(obj, statusLbl);
				oController.upload(eventId, eventDD.getLiveValue(), latTF.getValue(), 
						lngTF.getValue(), locationTF.getValue(), countryDD.getSelectedKey());
				//parent.getViewData().dialog.close();
			}
		}).addStyleClass("uploadButton");

		var oCancelButton = new sap.ui.commons.Button({
			text:'Cancel',
			press:function() {
				parent.getViewData().dialog.close();
			}
		});
		
		var spinner = new sap.ui.core.HTML({
			content: "<div id='progress'><div id='progress-bar'></div></div>"
		});
		
		var statusLbl = new sap.ui.commons.Label();

		var oButtonLayout = new sap.ui.layout.HorizontalLayout({
			content: [spinner, oUploadButton, oCancelButton]
		}).addStyleClass("buttonLayout");
		
		var oVLayout = new sap.ui.layout.VerticalLayout({
			content: [eventLayout, htmlUploader, statusLbl, oButtonLayout]
		});
		
		return oVLayout;
	}
});