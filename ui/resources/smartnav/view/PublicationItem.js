sap.ui.core.Control.extend("view.PublicationItem", {
	metadata : {
		properties : {
			pdfId : "string",
			journal : "string",
			articleDate : "string",
			//articleType : "string",
			cpnyName : "string",
			extractDate : "string",
			//year : "string",
			sitetype : "string",
			objectType: "string",
			language: "string"
		},
		aggregations : {
			"authors" : {
				type : "sap.ui.core.Control",
				multiple : true,
				singularName : "author"
			},
			"title" : {
				type : "sap.ui.core.Control",
				multiple : false
			},
			"image" : {
				type : "sap.ui.commons.Image",
				multiple : false
			},
			//"language" : {
			//	type : "sap.ui.commons.Image",
			//	multiple : false
			//},
			"content" : {
				type : "sap.ui.core.Control",
				multiple : false
			},
		}
	},

	renderer : function(rm, ctrl) {
		rm.writePublicationAttribute = function(attribute, cssclass) {
			if (attribute) {
				return this.write("<span")
				.writeAttribute("class", cssclass || "")
				.write(">")
				.write(attribute)
				.write("</span>");
			} else {
				return this;
			}
		};
		var titletext = ctrl.getTitle().getText();
		rm.write("<section");
		rm.writeControlData(ctrl);
		rm.writeAttribute("class", "PublicationItemLayout");
		rm.writeAttribute("title", titletext);
		rm.write("\"><div");
		rm.writeAttribute("class", "PublicationItemLayoutInner");
		rm.write(">");
		rm.write("<header");
		rm.writeAttribute("class", "PublicationItemLayoutTitle");
		rm.write(">");
		rm.renderControl(ctrl.getImage());
		rm.write("<h2>");
		if (titletext) { 
			rm.write("<a>",titletext,"</a>");
			//rm.renderControl(ctrl.getTitle());
		} else {
//			rm.writePublicationAttribute(ctrl.getSitetype() + ": " + ctrl.getJournal(), "PublicationJournal");
			rm.writePublicationAttribute(ctrl.getSitetype(), "PublicationJournal");
		} 
		rm.write("</h2>");
		//rm.renderControl(ctrl.getLanguage());
		rm.write("<p><span");
		rm.writeAttribute("class", "PublicationAuthors");
		rm.write(">");
		jQuery.each(ctrl.getAuthors(), function() { rm.renderControl(this); });
		rm.write("</span></p><p>");
		if (titletext) {
//			rm.writePublicationAttribute(ctrl.getSitetype() + ": " + ctrl.getJournal(), "PublicationJournal");
			rm.writePublicationAttribute(ctrl.getSitetype(), "PublicationJournal");
		}
		//rm.writePublicationAttribute(ctrl.getYear(), "PublicationYear");
		rm.writePublicationAttribute(ctrl.getArticleDate(), "PublicationDate");
		//rm.writePublicationAttribute(ctrl.getArticleType(), "PublicationType");
		rm.writePublicationAttribute(ctrl.getCpnyName(), "PublicationCpnyName");
		rm.writePublicationAttribute(ctrl.getExtractDate(), "PublicationExtractDate");
		rm.writePublicationAttribute(ctrl.getLanguage(), "PublicationLanguage");
		rm.write("</p>");
		rm.write("</header><article");
		rm.writeAttribute("class", "PublicationItemLayoutCntnt");
		rm.write(">");
		rm.renderControl(ctrl.getContent());
		rm.write("</article>");
		rm.write("<footer>");
		rm.write("</footer></div></section>");
	},
	
	onBeforeRendering : function() {
		if (this.resizeTimer) {
			clearTimeout(this.resizeTimer);
			this.resizeTimer = null;
		}
	},

	onAfterRendering : function() {
		var $This = this.$();
		if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
			this._resize();
		} else {
			$This.addClass("PublicationItemLayoutSmall");
		}
	},

	_resize : function() {
		if (!this.getDomRef()) {
			return;
		}
		var $This = this.$();
		if ($This.outerWidth() >= 440) {
			$This.removeClass("PublicationItemLayoutSmall").addClass(
					"PublicationItemLayoutLarge");
		} else {
			$This.removeClass("PublicationItemLayoutLarge").addClass(
					"PublicationItemLayoutSmall");
		}
		setTimeout(jQuery.proxy(this._resize, this), 300);
	}
});
