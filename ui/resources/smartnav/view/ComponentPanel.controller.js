sap.ui.controller("view.ComponentPanel", {

	onInit : function() {
		var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		var oModelDemo = new sap.ui.model.json.JSONModel();

		oModelDemo.setData(this.getView().getViewData());
		this.getView().setModel(oModelDemo);

		this.getView().bindContext("/" + globalModel.viewContext.viewType);

		sap.ui.getCore().getEventBus().subscribe(constants.events.CHANGE_VIEW, this.onChangeView, this);
		
	},
	
	onChangeView : function(sChannelId, sEventId, oData) {
		this.getView().bindContext("/" + oData.viewType);
	},

	setStatus : function(component, status) {
		var components = this.getView().getBindingContext().getProperty("components");
		
		$.each(components, function(index, value) {
			if (value.name === 'view.components.' + component) {
				value.active = status;
			}
		});
	},
	
	onExit : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.CHANGE_VIEW, this.onUpdateView, this);
	}
});