sap.ui.core.HTML.extend("view.components.YearHTMLChart", {
                                              
    metadata : {         
    	properties : {
    		"minYear" : "int",
    		"maxYear" : "int",
    		"min" : "int",
    		"max" : "int",
    		"years" : ""
    	}
    },
    
    setMinYear: function(param) {
    	if (param) {
    		this.setProperty("minYear", param, true);
    		this.updateGraph();
    	}
    	return this;
    },
    
    setMaxYear: function(param) {
    	if (param) {
    		this.setProperty("maxYear", param, true);
    		this.updateGraph();
    	}
    	return this;
    },    
    setMin: function(param) {
    	if (param) {
    		this.setProperty("min", param, true);    	
    		this.updateGraph();
    	}
    	return this;
    },
    
    setMax: function(param) {
    	if (param) {
    		this.setProperty("max", param, true);
    		this.updateGraph();
    	}
    	return this;
    },
    
    setYears: function(param) {
    	if (param) {
    		this.getModel().setProperty("/series",param);
    		this.updateGraph();
    	}
 	    return this;
    },
    
    updateGraph: function() {
    	var min = this.getProperty("min");
    	var max = this.getProperty("max");
    	var minYear = this.getProperty("minYear");
    	var maxYear = this.getProperty("maxYear");
    	var years = this.getModel().getProperty("/years");
    	if (min && max && minYear && maxYear && years) {
    		var html = jQuery("#"+this.sId);
			//html.css("background-color","yellow");
			html.css('position', 'relative');
			html.empty();
			var width = html.innerWidth();
			var height = html.height();
			
			var opacity = "0.5";
			op  = function(currentYear) {
				if (currentYear >= minYear && currentYear <= maxYear)
					opacity = "1.0";
				else
					opacity = "0.3";
				return opacity;
			};
			var maxHeight = 0;
			for (elem in years)
			{
				if (years[elem].count > maxHeight)
					maxHeight = years[elem].count;
			}
			percent = function(currentCount) {
				return currentCount*100/maxHeight;
			};
			var range = max - min;
			
			var barWidth = 1 + (width-range) / range;
			barWidth = Math.floor(barWidth);
			var paintWidth = (barWidth > 5) ? barWidth-4 : barWidth;
			if (barWidth > 20)
				paintWidth = 18;
			var offset = 0;
			if (paintWidth < barWidth)
				offset = 4;
			//console.log("barWidth: "+barWidth+ " range:"+range+ "  width:"+width+" min:"+min+ "  paintWidth:"+paintWidth);
			var left = 0;
			var x = "";
			//console.log("maxHeight: "+maxHeight);
			
			var lastYear = min;
			for (elem in years)
			{
				while (years[elem].year > lastYear)  // actually this is not necessary, except we want to show "empty" bars...
				{
					left = ((lastYear-min) *(width-barWidth)) / range;
					html.append(
						
						'<div title="'+lastYear+': 0" style="position:absolute; left:'+(left+offset)+'px; width:'+paintWidth+'px;" class="yearChartBar empty">' +
							'<div style="width: 100%; position: absolute; bottom: 0; height:0px; opacity:'+op(lastYear)+';" />' +
						'</div></a>'
					);
				
					lastYear += 1;					
				}
				
				left = ((years[elem].year-min) *(width-barWidth)) / range;
				
				x = years[elem];
				
				var month = "";
				if (years[elem].year == "1"){
					month = "Jan";
				} else if (years[elem].year == "2"){
					month = "Feb";
				} else if (years[elem].year == "3"){
					month = "Mar";
				} else if (years[elem].year == "4"){
					month = "Apr";
				} else if (years[elem].year == "5"){
					month = "May";
				} else if (years[elem].year == "6"){
					month = "Jun";
				} else if (years[elem].year == "7"){
					month = "Jul";
				} else if (years[elem].year == "8"){
					month = "Aug";
				} else if (years[elem].year == "9"){
					month = "Sep";
				} else if (years[elem].year == "10"){
					month = "Oct";
				} else if (years[elem].year == "11"){
					month = "Nov";
				} else if (years[elem].year == "12"){
					month = "Dec";
				} else {
					month = years[elem].year;
				}
				html.append(
						'<a onclick=\'sap.ui.getCore().getEventBus().publish(constants.events.YEAR_TO_MONTH, { yearToMonth:'+ x.year +'});\'>' +
						'<div title="'+(x.title ? x.title : month)+': '+x.count+'" style="position:absolute; left:'+(left+offset)+'px; width:'+paintWidth+'px;" class="yearChartBar'+(x.empty ? " empty" : "")+'">' +
							'<div style="width: 100%; position: absolute; bottom: 0; height:'+percent(x.count)+'%; opacity:'+op(x.year)+';" />' +
						'</div></a>'
					);
				lastYear += 1;
			}
			while (lastYear < max+1)  // actually this is not necessary, except we want to show "empty" bars...
			{
				left = ((lastYear-min) *(width-barWidth)) / range;
				html.append(
					'<div title="'+lastYear+': 0" style="position:absolute; left:'+(left+offset)+'px; width:'+paintWidth+'px;" class="yearChartBar empty">' +
						'<div style="width: 100%; position: absolute; bottom: 0; height:0px; opacity:'+op(lastYear)+';" />' +
					'</div>'
				);
			
				lastYear += 1;			
			}

    	}
    },
    
    renderer : "sap.ui.core.HTMLRenderer"
    	
});