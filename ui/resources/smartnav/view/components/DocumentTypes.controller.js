/*jQuery.sap.require("smartnav.components.AbstractFilterController");
smartnav.components.AbstractFilterController.extend("smartnav.components.DocumentTypes", {
	
	path : "contentTypes",
	
	attrName : "contentTypes",
	
	restCall: constants.appRoot + "/rest/article/documentTypes.xsjs"
});*/

sap.ui.core.mvc.Controller.extend("view.components.DocumentTypes", { 
       
       showLimit: 5,
       attrName : "contentTypes",
       
       setActive : function() {
              var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
              this.onUpdateView(null, null, oData.viewContext);

              sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
       },
       
       setInactive : function() {
              sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
       },

       onInit : function() {
              this.getView().setModel(new sap.ui.model.json.JSONModel());
              this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
       },
       
       onItemSelected : function(item, path, category) {
    	   if (path ==='contentTypes')  {
    		   var siteSource = category + ': ' + item;
    		   var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;

    		   if (!viewContext.filters[path]) 
    			   viewContext.filters[path] = [];
 
		       // already in array
	           if ($.inArray(siteSource, viewContext.filters[path]) != -1) {
                     viewContext.filters[path].splice( $.inArray(siteSource, viewContext.filters[path]), 1 );
	           } else {
                     viewContext.filters[path].push(siteSource);
	           }
	           
	           // avoid self notification
	           sap.ui.getCore().getEventBus()
	               .unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this)
	               .publish(constants.events.UPDATE_VIEW, viewContext);
	           sap.ui.getCore().getEventBus()
	           	.subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
    	   }
           
    	   else  {
              this.toggleCategory(item);
    	   }
           
       },
       
 
       onUpdateView : function(sChannelId, sEventId, oData) {
              this.loadData(oData.searchTerm, oData.filters);
       },

       onDataLoaded : function(event, test) {
              var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
              
              this.getView().getModel().oData.showButton = undefined;
              
              if (this.onShowButtonPressed == this.showAll || this.onShowButtonPressed == undefined)
                     this.showLess();
              else
                     this.showAll();
              
       },
       
       toggleCategory : function(category) {
              var oData = this.getView().getModel().oData;
              $.each(oData.contentTypes, function(idx, val){
                 if (val.category === category)  {
                       if (val.isSelected)  {
                              val.isSelected = false;
                              val.styleClass = 'notSelected';
                              var itemsDiv = $('div#DocumentTypes').find('div#' + category.replace(' ', ''));
                              $(itemsDiv).removeClass('selected');
                              $(itemsDiv).addClass('notSelected');
                       }
                       else  {
                              val.isSelected = true;
                              val.styleClass = 'selected';
                              var itemsDiv = $('div#DocumentTypes').find('div#' + category.replace(' ', ''));
                              $(itemsDiv).removeClass('notSelected');
                              $(itemsDiv).addClass('selected');
                       }
                 }
              });
       },

       onShowButtonPressed : undefined, //showAll or showLess
       
       showAll : function() {
              var oData = this.getView().getModel().oData;
              if (oData[this.path] && oData[this.path].length > this.showLimit)
                     oData.showButton = "Show Less";
              
              this.forEachItem(function(index, value) {
                     value.isVisible = true; 
              });
              this.onShowButtonPressed = this.showLess;
       },
       
       showLess : function() {
              var oData = this.getView().getModel().oData;
              var visibleItems = 0;
              
              this.forEachItem(function(index, value) {
                     visibleItems += value.isSelected ? 1 : 0;
              }, this, false);
              
              if (oData[this.path] && oData[this.path].length > this.showLimit)
                     oData.showButton = "Show All";
              
              this.forEachItem(function(index, value) {
                     value.isVisible = (visibleItems < this.showLimit) ? true : false;
                     visibleItems += (value.isVisible && !value.isSelected) ? 1 : 0;
              }, this);
              
              this.onShowButtonPressed = this.showAll;
       },
       
       selectItems : function(itemssToSelect) {
              if (itemssToSelect) {
                     for (var i = 0; i < itemssToSelect.length; i++) {
                           
                           var found = false;
                           this.forEachItem(function(index, value) {
                                  if (itemssToSelect[i] === value[this.attrName]) 
                                         found = true;                            
                           }, this, false);
                           
                           if (!found) {
                                  var obj = {   };
                                  obj.count = 0;
                                  obj.total = 0;
                                  obj[this.attrName] = itemssToSelect[i];
                                  
                                  this.getView().getModel().oData[this.path].push(obj);
                           }
                     }
              }
              
              this.forEachItem(function(index, value) {
                     value.isSelected = $.inArray(value[this.attrName], itemssToSelect) != -1 ? true : false;
                     
                     if (!value.isSelected && value.count == 0)
                           this.getView().getModel().oData[this.path].splice(index, 1);
              }, this);
       },
       
       forEachItem : function(callback, obj, updateBinding) {
              var oData = this.getView().getModel().oData;
              
              if (oData[this.path]) 
                     $.each(oData[this.path], function(index, value) {
                           callback.call(obj, index, value);
                     });
              
              if (updateBinding == undefined || updateBinding)
                     this.getView().getModel().updateBindings(true);
       },
       
       onExit : function() {
              this.setInactive();
              this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
       },
       
       loadData : function(searchTerm, filters) {
                     this.getView().getModel().fetchData("/smartnavxs/article/documentTypes.xsjs",
                                  {
                                         "search": searchTerm,
                                         "from": filters && filters.year ? filters.year.from : '',
                                         "to": filters && filters.year ? filters.year.to : '',
                                         "publishers": filters.publishers,
                                         "sitetype": filters.sitetype,
                                         "subjects": filters.subjects,
                                         "persons": filters.persons,
                                         "orgs": filters.orgs,
                                         "localities": filters.localities,
                                         "sentimentStart": (filters.sentiment) ? filters.sentiment.start : '',
                                         "sentimentEnd": (filters.sentiment) ? filters.sentiment.end : '',
                             			"interactionTypes": filters.interactionTypes
                                  }, true, "GET", true);
       }
       
 });
