jQuery.sap.require("view.components.DrilldownItem");

sap.ui.jsview("view.components.InteractionTypes", {

    getControllerName : function() {
        return "view.components.InteractionTypes";
    },

    createContent : function(oController) {
        var oTemplate = new view.components.DrilldownItem({
            title : "{interactionType}",
            count : "{count}",
            total : "{total}",
            selected : "{isSelected}",
            press : [ function(oEvent) {
                this.onItemSelected(oEvent.getSource().getTitle(), 'Interaction Type');
            }, oController ]
        });
        
        this.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/interactionTypes",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, true) ]
            }
        })).addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/interactionTypes",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, false),
                    new sap.ui.model.Filter("isVisible", sap.ui.model.FilterOperator.EQ, true) ]
            }
        })).addContent(new sap.ui.commons.Link({
            text : "{/showButton}",
            press : [ function() {
                this.onShowButtonPressed();
            }, oController ]
        }));
    }
});

