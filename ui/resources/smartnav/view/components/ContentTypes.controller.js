jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.ContentTypes", {
	
	path : "contentTypes",
	
	attrName : "contentTypes",
	
	restCall: "/smartnavxs/article/contentTypes.xsjs"
});
