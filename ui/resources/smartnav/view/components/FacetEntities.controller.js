sap.ui.core.mvc.Controller.extend("view.components.FacetEntities", { 
		
    //path : "entities", 
    attrName : "entities",
    
    setActive : function() {
           var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
           this.onUpdateView(null, null, oData.viewContext);

           sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
    },
    
    setInactive : function() {
           sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
    },

    onInit : function() {
           this.getView().setModel(new sap.ui.model.json.JSONModel());
           this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
    },
    
    onItemSelected : function(item, path, category) {      
 	   if (path ==='entities')  {
 		   var catItem = category + ': ' + item;
 		   var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;

 		   if (!viewContext.filters[path]) 
 			   viewContext.filters[path] = [];

		       // already in array
	           if ($.inArray(catItem, viewContext.filters[path]) != -1) {
                  viewContext.filters[path].splice( $.inArray(catItem, viewContext.filters[path]), 1 );
	           } else {
                  viewContext.filters[path].push(catItem);
	           }

	          // this.loadData(sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.searchTerm, 
	        	//	   sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.filters);
	           
	           // avoid self notification
	           sap.ui.getCore().getEventBus()
	               .unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this)
	               .publish(constants.events.UPDATE_VIEW, viewContext);
	           sap.ui.getCore().getEventBus()
	           	.subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
 	   }
        
 	   else  {
           this.toggleCategory(item);
 	   }
        
    },
    
    onUpdateView : function(sChannelId, sEventId, oData) {
           this.loadData(oData.searchTerm, oData.filters);
    },

    onDataLoaded : function(event, test) {
       // var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
       // var oData = this.getView().getModel().oData;
        this.getView().getModel().updateBindings(true);
    	
    },
    
    toggleCategory : function(category) {
           var oData = this.getView().getModel().oData;
           $.each(oData.facets, function(idx, val){
              if (val.category === category)  {
                    if (val.isSelected)  {
                           val.isSelected = false;
                           val.styleClass = 'notSelected';
                           var itemsDiv = $('div#FacetEntities').find('div#' + category);
                           $(itemsDiv).removeClass('selected');
                           $(itemsDiv).addClass('notSelected');
                    }
                    else  {
                           val.isSelected = true;
                           val.styleClass = 'selected';
                           var itemsDiv = $('div#FacetEntities').find('div#' + category);
                           $(itemsDiv).removeClass('notSelected');
                           $(itemsDiv).addClass('selected');
                    }
              }
           });
    },
    
    onExit : function() {
           this.setInactive();
           this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
    },
    
    loadData : function(searchTerm, filters) {
                  this.getView().getModel().fetchData("/smartnavxs/rest/entities/facet.xsjs",
                               {
                                      "search": searchTerm,
                                      "from": filters && filters.year ? filters.year.from : '',
                                      "to": filters && filters.year ? filters.year.to : '',
                                      "publishers": filters && filters.publishers ? filters.publishers : '',
                                      "sitetype": filters && filters.sitetype ? filters.sitetype : '',
                                      "subjects": filters && filters.subjects ? filters.subjects : '',
                                      "entities": filters && filters.entities ? filters.entities : ''
                               }, true, "GET", true);
    }
    
});