jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.EntityLocality", {
	path : "localities",
	attrName : "locality",
	restCall: "/smartnavxs/entities/localities.xsjs"
});
