sap.ui.controller("view.components.Month", {
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		this.onUpdateView(null, null, oData.viewContext);
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.YEAR_TO_MONTH, this.loadData, this);
	},
	
	setInactive : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.YEAR_TO_MONTH, this.loadData, this);
	},

   onInit: function() {
	   this.getView().setModel(new sap.ui.model.json.JSONModel());
	   this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);

   },
   
	onExit : function()  {
	},
   
   onUpdateView : function(sChannelId, sEventId, oData) {
       this.loadData(oData);
   },

   onDataLoaded : function(event) {
	   	var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel();
		
		model.setProperty("searchTerm", oData.searchTerm);
		model.setProperty("publishers", oData.filters.publishers ? jQuery.merge([], oData.filters.publishers) : undefined);
		model.setProperty("subjects", oData.filters.subjects ? jQuery.merge([], oData.filters.subjects) : undefined);
		this.calcModel();
		this.setSelection(oData.filters.year2);
   },
	
	
	setSelection : function(year2) {
	    if (year2) {
	       var model = this.getView().getModel();
	 	   var series = model.getProperty("/years");
	 	   var newCount = 0;
	 	   for (var elem=0; elem < series.length; elem++)
	 	   {
	            if (series[elem].year >= year2.from && series[elem].year <= year2.to)
	         	   newCount += series[elem].count;
	            console.log("newCount: "+newCount);
	 	   }
	 	   model.setProperty("/count",newCount);
	 	   model.setProperty("/minSelected","JAN");
	 	   model.setProperty("/maxSelected",year2.to);
	 	   model.updateBindings(true);
		}
	},
	calcModel : function() {
		var model = this.getView().getModel();
		var series = model.getProperty("/years");
		var newCount = 0;
		var minYear = 9999;
		var maxYear = 0;
		var labels = []; 
		for (var elem=0; elem < series.length; elem++)
		{
			newCount += series[elem].count;
			if (series[elem].year < minYear)
				minYear = series[elem].year;
			if (series[elem].year > maxYear)
				maxYear = series[elem].year;
			labels.push("");
			
		}
		
		labels[0] = "Jan";
		labels[labels.length-1] = "Dec";
		model.setProperty("/count",newCount);
		model.setProperty("/minYear",minYear);
		model.setProperty("/maxYear",maxYear);
		model.setProperty("/labels",labels);
		model.setProperty("/minSelected",minYear);
		model.setProperty("/maxSelected",maxYear);
		model.setProperty("/yearDistance", ((maxYear - minYear) > 4 ? 4 : (maxYear-minYear > 1) ? (maxYear-minYear) : 1));
		model.setProperty("/series",series);
	},
	
	loadData : function(sChannelId, sEventId, oData) {
		//var yearSpan = $('body').children('#year'); 
		//var year = yearSpan.attr("class");
		
		console.log(oData);
		
		this.getView().getModel().fetchData("/smartnavxs/year/byKeywordMonthSlider.xsjs",
				{
					"year": year,
					"search": oData.searchTerm,
					"publishers": oData.filters.publishers,
					"subjects": oData.filters.subjects,
	 			});
	},
	
	
	onExit : function() {
		this.setInactive();
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	}   
   
});
