sap.ui.jsview("view.components.Map", {

	getControllerName : function() {
		return "view.components.Map";
	},

	createContent : function(oController) {
		var oMap = new sap.ui.core.HTML({
			content: "<div id='searchmap' style='width: 100px; height: 200px;'></div>"
		});
		
		return oMap;
	}
});
