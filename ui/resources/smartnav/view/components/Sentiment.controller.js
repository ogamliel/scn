sap.ui.controller("view.components.Sentiment", {
	path : "sentiment",
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		
	},
	
	setInactive : function() {
	},

   onInit: function() {
	   this.getView().setModel(new sap.ui.model.json.JSONModel());
	   var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
	   var sentiment = viewContext.filters[this.path];
	   if (sentiment)  {
		   var start = sentiment.start;
		   var end = sentiment.end;
		   this.getView().getModel().setProperty("/sentimentStart", parseInt(start));
		   this.getView().getModel().setProperty("/sentimentEnd", parseInt(end));
	   }
	   else  {
		   this.getView().getModel().setProperty("/sentimentStart", -1000);
		   this.getView().getModel().setProperty("/sentimentEnd", 1000);
	   }
  },
   
   onUpdateView : function(sChannelId, sEventId, oData) {
       //this.loadData(oData);
   },

   onDataLoaded : function(event) {
	   	var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel();
		

   },
   
   changeSentiment : function (val1, val2)  {
	   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		viewContext.filters[this.path] = {start: parseInt(val1), end: parseInt(val2)};		
		viewContext.lastFilter = this.path;

		sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
   },
	
	onExit : function() {
		this.setInactive();
		//this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	}   
   
});
