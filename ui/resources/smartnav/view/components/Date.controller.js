sap.ui.controller("view.components.Date", {
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		this.onUpdateView(null, null, oData.viewContext);
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},
	
	setInactive : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},

   onInit: function() {
	   this.getView().setModel(new sap.ui.model.json.JSONModel());
	   this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
  
   },
   
   onUpdateView : function(sChannelId, sEventId, oData) {
       this.loadData(oData);
   },

   onDataLoaded : function(event) {
	   	var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel();
		
		var startDate = new Date();
		startDate.setMonth(startDate.getMonth() - 6);
		
		if (oData.filters.year){
			var to = oData.filters.year.to;
			var from = oData.filters.year.from;

			var toDate = new Date(to);
			toDate.setDate(toDate.getDate()+1);
			
			var fromDate = new Date(from);
			fromDate.setDate(fromDate.getDate()+1);
			
			model.setData({
				dateValue:toDate,
				dateValue2 : fromDate
			});
		}
			
		else {
		model.setData({
			dateValue: new Date(),
			dateValue2: startDate
		}); 
   }
		
 		
		model.setProperty("searchTerm", oData.searchTerm);
		model.setProperty("publishers", oData.filters.publishers ? jQuery.merge([], oData.filters.publishers) : undefined);
		model.setProperty("subjects", oData.filters.subjects ? jQuery.merge([], oData.filters.subjects) : undefined);
		this.setSelection(oData.filters.year);
   },
	
	
	setSelection : function(year) {
	    if (year) {
	       var model = this.getView().getModel();
	 	   var series = model.getProperty("/years");
	 	   var newCount = 0;
	 	   
	 	var from1 = year.from;
	 	var fromDate1 = new Date(from1);
	 	fromDate1.setDate(fromDate1.getDate()+1);
	 	
	 	var to1 = year.to;
	 	var toDate1 = new Date(to1);
	 	toDate1.setDate(toDate1.getDate()+1);
	 	
	    model.setProperty("/count",newCount);
	    model.setProperty("/dateValue2",fromDate1);
	    model.setProperty("/dateValue",toDate1);
	    
	 	model.updateBindings(true);
		}
	},
	
	loadData : function(oData) {
		/*this.getView().getModel().fetchData("/smartnavxs/year/byKeywordsAndDate.xsjs",
				{
					"search": oData.searchTerm,
					"publishers": oData.filters.publishers,
					"subjects": oData.filters.subjects
				});*/
	},
	
   changeDate : function(start, end) {
	   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
  	
	   	if (!viewContext.filters.year) 
  			viewContext.filters.year = {from:start, to:end};
  		else {
  			viewContext.filters.year.from = start;
  			viewContext.filters.year.to = end;
  		}

        // avoid self notification
        sap.ui.getCore().getEventBus()
            .unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this)
            .publish(constants.events.UPDATE_VIEW, viewContext);
        sap.ui.getCore().getEventBus()
            .subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
        this.setSelection(viewContext.filters.year);
   },
   
   changeLiveDate : function(start, end) {
	   this.setSelection({from: start, to: end});
   },
	
	onExit : function() {
		this.setInactive();
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	}   
   
});
