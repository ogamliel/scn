sap.ui.core.Control.extend("view.components.Placeholder", {

	metadata : {
		properties : {
			"text" : {type : "string", defaultValue : "Click me to add Widget!"},
			"popupPosition" : {type : "string[]", defaultValue: [sap.ui.core.Popup.Dock.BeginTop, sap.ui.core.Popup.Dock.EndCenter]}
		},
		aggregations: {
			"components" : {type : "sap.ui.commons.Button", multiple : true}
	    }
	},
	
	renderer : function(oRm, oControl) {
    	var oImage = new sap.ui.commons.Image({
    		src : "images/toolbarButton-zoomIn.png"
    	});
    	
    	var oTextView = new sap.ui.commons.TextView({
    		text : oControl.getText()
    	});
    	
    	var oLayout = new sap.ui.commons.layout.VerticalLayout({
    		width: "100%",
    		content: [oImage, oTextView]
    	});
    	oLayout.addStyleClass("placeholder");
    	
    	oLayout.attachBrowserEvent("click", function(event){
    		event.stopPropagation();
    		if (tp3.isOpen()) {
                tp3.close();
            } else {
            	tp3.open(oControl.getPopupPosition()[0], oControl.getPopupPosition()[1]);
            }
		});
    	
    	var tp3 = new sap.ui.ux3.ToolPopup({
            content : new sap.ui.commons.layout.VerticalLayout({
                content: oControl.getComponents()
            }),
            opener : oLayout
		});
    	tp3.addStyleClass("placeholderPopup");
    	
    	$('*').click(function(event) {
    		if (tp3.isOpen()) 
                tp3.close();
    	});
    	
		oRm.renderControl(oLayout);
	}
});