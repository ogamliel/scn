sap.ui.jsview("view.components.FileUpload", {

    getControllerName : function() {
        return "view.components.FileUpload";
    },

    createContent : function(oController) {
    	var html = new sap.ui.core.HTML( {
			content: "<div id=\"drop_zone\" ondrop=\"dropHandler(event);\" ondragover=\"dragoverHandler(event);\" ondragend=\"dragendHandler(event);\"> " +
					"<img src=\"images/upload32.png\"/> " +
					"<input type=\"file\" name=\"files[]\" id=\"file\" class=\"box_file\" data-multiple-caption=\"{count} files selected\" multiple onchange=\"browseUpload(this);\" /> " +
					"<label for=\"file\"><strong>Choose a file</strong><span class=\"box_dragndrop\"> or drag it here</span>.</label> " +
					"<div id=\"list\" class=\"list\"></div>" +
					"<div id=\"errors\" class=\"errors\"></div>" +
					"</div>",
			preferDOM : false
		});
		
		return html;
		
    }
});

