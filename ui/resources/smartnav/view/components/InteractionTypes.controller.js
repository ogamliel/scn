jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.InteractionTypes", {
	path : "interactionTypes",
	attrName : "interactionType",
	restCall: "/smartnavxs/article/interactionTypesFilter.xsjs"
});
