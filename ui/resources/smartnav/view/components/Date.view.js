jQuery.sap.require("view.components.DateHTMLChart");
sap.ui.jsview("view.components.Date", {

	getControllerName : function() {
		return "view.components.Date";
	},

	createContent : function(oController) {
		var oLayout = new sap.ui.commons.layout.VerticalLayout({
			layoutFixed : false,
			width: "100%",
			columns : 2,
	        widths : [ '40%','60%'],
		});
		
		var oLabel1 = new sap.ui.commons.TextView({
			text: "Start Date:"}).addStyleClass("dateLabel1");
		
		var oDatePicker1 = new sap.ui.commons.DatePicker({
			id : 'date1',
			width: "8em",
	        value: {
                path: "/dateValue2",
                type: new sap.ui.model.type.Date({pattern: "yyyy-MM-dd"})
	        },
			change : function(oEvent){
	             //oController.changeDate(oDatePicker1.getValue(), oDatePicker2.getValue());
				oController.changeDate(oDatePicker1.getLiveValue(), oDatePicker2.getLiveValue());
	        },
			liveChange : function(oEvent){
	             //oController.changeLiveDates(oDatePicker1.getValue(), oDatePicker2.getValue());
				if (oDatePicker1.getLiveValue().length === 10)  {
					oController.changeDate(oDatePicker1.getLiveValue(), oDatePicker2.getLiveValue());
				}
	        }
		}).addStyleClass("datePickerStyle");;
		
		var startDate = new sap.ui.commons.layout.HorizontalLayout({
			content: [oLabel1, oDatePicker1]
		});
		oLayout.addContent(startDate);
		
		var oLabel2 = new sap.ui.commons.TextView({
			text: "  End Date: "}).addStyleClass("dateLabel2");

		var oDatePicker2 = new sap.ui.commons.DatePicker({
			id : 'date2',
			width: "8em",
			value: {
                path: "/dateValue",
                type: new sap.ui.model.type.Date({pattern: "yyyy-MM-dd"})
	        },
			change : function(){
	             //oController.changeDate(oDatePicker1.getValue(), oDatePicker2.getValue());
				oController.changeDate(oDatePicker1.getLiveValue(), oDatePicker2.getLiveValue());
	        },
			liveChange : function(){
	            //oController.changeLiveDates(oDatePicker1.getValue(), oDatePicker2.getValue());
				if (oDatePicker2.getLiveValue().length === 10)  {
					oController.changeDate(oDatePicker1.getLiveValue(), oDatePicker2.getLiveValue());
				}
	        }
		}).addStyleClass("datePickerStyle");
		
		var endDate = new sap.ui.commons.layout.HorizontalLayout({
			content: [oLabel2, oDatePicker2]
		});
		oLayout.addContent(endDate);
	
		return oLayout;
	}
});
