sap.ui.core.mvc.Controller.extend("view.components.AbstractFilterController", { 
	
	showLimit: 5,
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		this.onUpdateView(null, null, oData.viewContext);
		
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},
	
	setInactive : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},

	onInit : function() {
		this.getView().setModel(new sap.ui.model.json.JSONModel());
		this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
	},
	
	onItemSelected : function(item, category) {		
		if (category)  {
			item = category + ': ' + item;
		}
		
	   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		if (!viewContext.filters[this.path]) 
			viewContext.filters[this.path] = [];
		
		if ($.inArray(item, viewContext.filters[this.path]) != -1) {
			viewContext.filters[this.path].splice( $.inArray(item, viewContext.filters[this.path]), 1 );
		} else {
			viewContext.filters[this.path].push(item);
		}
		viewContext.lastFilter = this.path;

		// avoid self notification
		sap.ui.getCore().getEventBus()
		    .unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this)
		    .publish(constants.events.UPDATE_VIEW, viewContext);
	    sap.ui.getCore().getEventBus()
            .subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
        //this.selectItems(viewContext.filters[this.path]);
        if (this.onShowButtonPressed == this.showAll)
            this.showLess();
	},

	onUpdateView : function(sChannelId, sEventId, oData) {
		this.loadData(oData.searchTerm, oData.filters);
	},

	onDataLoaded : function(event, test) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;

		this.getView().getModel().oData.showButton = undefined;
		this.getView().getModel().oData.searchButton = 'Search...';
		
		this.selectItems(viewContext.filters[this.path]);
		if (this.onShowButtonPressed == this.showAll || this.onShowButtonPressed == undefined)
			this.showLess();
		else
			this.showAll();
	},
	
	onSearchButtonPressed : function (type)  {
		var parentThis = this;
		
		var oDialog = new sap.ui.commons.Dialog({
			title : type + " Search",
			modal : true,
			showCloseButton : false,
			width : "600px",
			height : "220px"
		});

		// list box used to store search results
		var oListBox = new sap.ui.commons.ListBox("entitySearchLB", {visibleItems:6, width:"300px"});

		var oSearchField = new sap.ui.commons.SearchField("search", {
			enableListSuggest: false,
			width: "270px",
			search: function(oEvent){
				var searchTerm = oEvent.getParameter("query");
				$.ajax({
			        url : "/smartnavxs/entities/suggest.xsjs?type=" + type + "&token=" + searchTerm,
			        dataType: "json",
			        success : function (data) {
			        	//Destroy all existing items first
			        	oListBox.destroyItems();
			        	
			        	if(data.tokens.length > 0)  {
				        	// add matching tokens to list box
							$.each(data.tokens, function(i, field){
				                oListBox.addItem(new sap.ui.core.ListItem({text:field.token}));
				       	    });
							addFilterButton.setEnabled(true);
			        	}
			        	else  {
			        		oListBox.addItem(new sap.ui.core.ListItem({text:'No Results Found'}));
			        		addFilterButton.setEnabled(false);
			        	}
			        },
			        error: function (xhr, ajaxOptions, thrownError) {
			            console.log(xhr.status);
			            console.log(thrownError);
			          }
			    });
			}
		});
		
		oSearchField.setTooltip("Enter " + type + " Search Value");
		oSearchField.focus();
		oSearchField.addStyleClass("entitySearchField");
				
		var searchLayout = new sap.ui.commons.layout.HorizontalLayout({
			content: [oSearchField, 
			          oListBox]
		});

		var addFilterButton = new sap.ui.commons.Button({
			text : "Add Filter",
			enabled : false,
			press : function() {
				if (oSearchField.getValue())  {
					parentThis.onItemSelected(oListBox.getSelectedItem().getText(), type);
				}
				oDialog.close();
				oDialog.destroy();
			}
		});
		
		oDialog.addContent(searchLayout);
		oDialog.addStyleClass("conceptDialog"); 
		oDialog.addButton(addFilterButton);
		oDialog.addButton(new sap.ui.commons.Button({
			text : "Cancel",
			press : function() {
				oDialog.close();
				oDialog.destroy();
			}
		}));
		oDialog.open();
	},
	
	onShowButtonPressed : undefined, //showAll or showLess
	
	showAll : function() {
		var oData = this.getView().getModel().oData;
		if (oData[this.path] && oData[this.path].length > this.showLimit)
			oData.showButton = "Show Less";
		
		this.forEachItem(function(index, value) {
			value.isVisible = true; 
		});
		this.onShowButtonPressed = this.showLess;
	},
	
	showLess : function() {
		var oData = this.getView().getModel().oData;
		var visibleItems = 0;
		
		this.forEachItem(function(index, value) {
			visibleItems += value.isSelected ? 1 : 0;
		}, this, false);
		
		if (oData[this.path] && oData[this.path].length > this.showLimit)
			oData.showButton = "Show All";
		
		this.forEachItem(function(index, value) {
			value.isVisible = (visibleItems < this.showLimit) ? true : false;
			visibleItems += (value.isVisible && !value.isSelected) ? 1 : 0;
		}, this);
		
		this.onShowButtonPressed = this.showAll;
	},
	
	selectItems : function(itemssToSelect) {
		var items = [];
		
		if (itemssToSelect) {
			for (var i=0; i<itemssToSelect.length; i++)  {
				var idx = itemssToSelect[i].indexOf(':');
				if (idx != -1)  {
					items.push(itemssToSelect[i].substring(itemssToSelect[i].indexOf(':')+2));
				}
				else  {
					items.push(itemssToSelect[i]);
				}
			}

			for (var i = 0; i < items.length; i++) {				
				var found = false;
				this.forEachItem(function(index, value) {
					//if (itemssToSelect[i] === value[this.attrName]) 
					if (items[i] === value[this.attrName]) 
						found = true;					
				}, this, false);
				
				if (!found) {
					var obj = {	};
					obj.count = 0;
					obj.total = 0;
					obj[this.attrName] = items[i];
					
					this.getView().getModel().oData[this.path].push(obj);
				}
			}
		}
		
		this.forEachItem(function(index, value) {
			value.isSelected = $.inArray(value[this.attrName], items) != -1 ? true : false;
			
			if (!value.isSelected && value.count == 0)
				this.getView().getModel().oData[this.path].splice(index, 1);
		}, this);
	},
	
	forEachItem : function(callback, obj, updateBinding) {
		var oData = this.getView().getModel().oData;
		
		if (oData[this.path]) 
			$.each(oData[this.path], function(index, value) {
				callback.call(obj, index, value);
			});
		
		if (updateBinding == undefined || updateBinding)
			this.getView().getModel().updateBindings(true);
	},
	
	onExit : function() {
		this.setInactive();
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	},
	
	loadData : function(searchTerm, filters) {	
		this.getView().getModel().fetchData(this.restCall,
				{
					"search": searchTerm,
					"from": filters && filters.year ? filters.year.from : '',
					"to": filters && filters.year ? filters.year.to : '',
					"publishers": filters.publishers,
					"subjects": filters.subjects,
					"persons": filters.persons,
					"orgs": filters.orgs,
					"localities": filters.localities,
					"type": this.type,
					"contentTypes": filters.contentTypes,
					"sentimentStart": (filters.sentiment) ? filters.sentiment.start : '',
					"sentimentEnd": (filters.sentiment) ? filters.sentiment.end : '',
					"interactionTypes": filters.interactionTypes
				}, true, "GET", true);
	},
	
	parseItem : function (val)  {
		 var item = val.substring(val.indexOf(':')+2);
		 return item;
	}

});