sap.ui.controller("view.components.Recommendations", {

       /**
       * Called when a controller is instantiated and its View controls (if
       * available) are already created. Can be used to modify the View before it
       * is displayed, to bind event handlers and do other one-time
       * initialization.
       */
       showLimit : 5,
       
       onInit : function() {
              this.getView().setModel(new sap.ui.model.json.JSONModel());
       },

       onExit : function() {
              this.setInactive();        
       },

       onUpdateView : function(sChannelId, sEventId, oData) {
	   	  var parent = this;
    	   
        $.ajax({
  		    url: "/smartnavxs/document/callGetRelatedDocuments.xsjs?&id=" + oData.pdfId,
  		    type: 'GET',
  		    async: true,
  		    success: function(data) {
  		    	//var keys=[];
  		    	//data.RESULTS_TBL.forEach(function(result) {
  		    	//    keys.push("'" + result.ENTITYID + "'");
  		    	//});
  		    	if (data.length > 0)  {
  		    		parent.getView().getModel().fetchData("/smartnavxs/article/getRecommended.xsjs", { keys: data.map(x=>`'${x}'`).join(',') });
  		    	}
  		    },
  			error: function (xhr, ajaxOptions, thrownError) {
  			    console.log(xhr.status);
  			    console.log(thrownError);			    
  			}			    
  		});
       },

       onFilter : function(oEvent) {
              var sQuery = oEvent.getParameter("query");
              var oBinding = this.getBinding("items");
              oBinding.filter(!sQuery ? [] : [new sap.ui.model.Filter("title", sap.ui.model.FilterOperator.Contains, sQuery)]);
              oDataSet.setLeadSelection(-1);
       },
       
   	onSelect : function(oEvent) {
		var leadSelectedIndex = oEvent.getParameter("newLeadSelectedIndex");
		if(leadSelectedIndex == -1) return;
	
		if(leadSelectedIndex === 0)
			{
			var oItem = this.getItems()[oEvent.getParameter("newLeadSelectedIndex")];
			var pdfid = this.getModel().getProperty("pdfid", oItem.getBindingContext());
			var pdfTitle = this.getModel().getProperty("title", oItem.getBindingContext());
			var authors = this.getModel().getProperty("authors", oItem.getBindingContext());
			var articleDate = this.getModel().getProperty("articleDate", oItem.getBindingContext());
			var journal = this.getModel().getProperty("journal", oItem.getBindingContext());
			var type = this.getModel().getProperty("type", oItem.getBindingContext());
			var siteType = this.getModel().getProperty("siteType", oItem.getBindingContext());
			var objectType = this.getModel().getProperty("objectType", oItem.getBindingContext());
			
			sap.ui.getCore().getEventBus().publish(constants.events.OPEN_TEXT, { pdfId: pdfid, 
																				 pdfTitle: pdfTitle,
																				 authors: authors,
																				 articleDate: articleDate,
																				 journal: journal,
																				 type: type,
																				 siteType: siteType,
																				 objectType: objectType});
			
			}else
	      	{ 
				if (!leadSelectedIndex) {  	  
		      		return ;
		      	}
		       else {
					var oItem = this.getItems()[oEvent.getParameter("newLeadSelectedIndex")];
					var pdfid = this.getModel().getProperty("pdfid", oItem.getBindingContext());
					var pdfTitle = this.getModel().getProperty("title", oItem.getBindingContext());
					var authors = this.getModel().getProperty("authors", oItem.getBindingContext());
					var articleDate = this.getModel().getProperty("articleDate", oItem.getBindingContext());
					var journal = this.getModel().getProperty("journal", oItem.getBindingContext());
					var type = this.getModel().getProperty("type", oItem.getBindingContext());
					var siteType = this.getModel().getProperty("siteType", oItem.getBindingContext());
					var objectType = this.getModel().getProperty("objectType", oItem.getBindingContext());
					
					sap.ui.getCore().getEventBus().publish(constants.events.OPEN_TEXT, { pdfId: pdfid, 
																						 pdfTitle: pdfTitle,
																						 authors: authors,
																						 articleDate: articleDate,
																						 journal: journal,
																						 type: type,
																						 siteType: siteType,
																						 objectType: objectType});
	            }
	      	}
	},
	   
       setActive : function() {
              sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
              this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
              
              this.onUpdateView(null, null, sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"));
       },
       
       setInactive : function() {
              this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
              sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
       },
       onShowButtonPressed : undefined, //showAll or showLess

       onDataLoaded : function(event, test) {

              if (!this.onShowButtonPressed || this.onShowButtonPressed == this.showAll)
              {      this.showLess();
              }
              else {
                     this.showAll();
              }
       },
       
       showAll : function() {
              this.getView().getModel().oData.showButton = "Show Less";
              this.forEachItem(function(index, value) {
                     value.isVisible = true; 
              });
              this.onShowButtonPressed = this.showLess;
       },
       
       showLess : function() {
              var oData = this.getView().getModel().oData;
              var visibleItems = 0;
              
              if (oData.recommendations && oData.recommendations.length > this.showLimit)
                     oData.showButton = "Show All";
              
              this.forEachItem(function(index, value) {
                     value.isVisible = (visibleItems < this.showLimit) ? true : false;
                     visibleItems += value.isVisible ? 1 : 0;
              }, this);
              
              this.onShowButtonPressed = this.showAll;
       },
       
       forEachItem : function(callback, obj, updateBinding) {
              var oData = this.getView().getModel().oData;
              
              if (oData.recommendations) 
                     $.each(oData.recommendations, function(index, value) {
                           callback.call(obj, index, value);
                     });
              
              if (updateBinding === undefined || updateBinding)
                     this.getView().getModel().updateBindings(true);
       }      
       
});


