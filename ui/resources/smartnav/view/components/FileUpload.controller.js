sap.ui.controller("view.components.FileUpload", {

	onInit: function() {
   },
   
	setActive : function() {
	},

	setInactive : function() {
	},
	
	dropHandler : function (event)  {
		event.preventDefault();
		var dt = event.dataTransfer;
		var files = dt.files;
		this.upload(files);
	},
	
	dragoverHandler : function (event)  {
		event.preventDefault();
	},

	dragendHandler : function (event)  {
		event.preventDefault();
	},
	
	browseUpload : function (event)  {
		this.upload(event.files);
	},
	
	upload : function(files)  {
		this.docIds.length = 0;
		var list = $("#list");
		list.html("<label for=\"list\"><strong>Uploading " + files.length + " file(s)...</strong></label>");
		var filesArr = jQuery.makeArray(files); // convert list object to array
		this.readFiles(filesArr);
	},
	
	docIds: [],
	uploadErrs: [],
	
	readFiles : function(files)  {
		var parent = this;
		var reader = new FileReader();
		if (files.length > 0) { // if we still have files left
    		var file = files.shift(); // remove first from queue and store in file
    		reader.onloadend = function (e) { // when finished reading file, call recursive readFiles function
		    	var params = { 
					content : encodeURIComponent(e.target.result)
				};
				var type = file.type;
				if (type === "text/plain")  {
		    		$.ajax({
					    url: "/smartnavxs/article/insertDocument.xsjs",
					    async: false,
					    type: "POST",
					    data: JSON.stringify(params),
					    contentType: file.type,
					    success: function (data)  {
							parent.docIds.push(data.id);
						},
						error: function (xhr, ajaxOptions, thrownError) {
						    console.log(xhr.status);
						    console.log(thrownError);
						    // display error msg
				    		var errors = $("#errors");
						    errors.html("<strong>Upload Error!</strong>");
						}
					});
				}
				else  {
					parent.uploadErrs.push(type);
				}
				
        		parent.readFiles(files);
    		}
    		reader.readAsText(file);
		} 
		else {
    		// no more files to read.  show success msg
			// display uploaded documents in results table
    		var list = $("#list");
			list.html("<strong>" + parent.docIds.length + " document(s) uploaded.</strong>");
			$("#list").css("display", "block");
			$("#list").fadeOut(20000);
			
			if (parent.uploadErrs.length > 0)  {
	    		var errors = $("#errors");
    			errors.html("<strong>" + parent.uploadErrs.length + " Error(s):  Only text files allowed.</strong>");
    			$("#errors").css("display", "block");
				$("#errors").fadeOut(20000);
			}
			else  {
				var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
				sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
			}
				
			parent.docIds.length = 0; // reset docIds array
			parent.uploadErrs.length = 0;
		}
	}


});
