sap.ui.core.HTML.extend("view.components.MonthHTMLChart", {
                                              
    metadata : {         
    	properties : {
    		"minYear" : "int",
    		"maxYear" : "int",
    		"min" : "int",
    		"max" : "int",
    		"years" : ""
    	}
    },
    
    setMinYear: function(param) {
    	if (param) {
    		this.setProperty("minYear", param, true);
    		this.updateGraph();
    	}
    	return this;
    },
    
    setMaxYear: function(param) {
    	if (param) {
    		this.setProperty("maxYear", param, true);
    		this.updateGraph();
    	}
    	return this;
    },    
    setMin: function(param) {
    	if (param) {
    		this.setProperty("min", param, true);    	
    		this.updateGraph();
    	}
    	return this;
    },
    
    setMax: function(param) {
    	if (param) {
    		this.setProperty("max", param, true);
    		this.updateGraph();
    	}
    	return this;
    },
    
    setYears: function(param) {
    	if (param) {
    		this.getModel().setProperty("/series",param);
    		this.updateGraph();
    	}
 	    return this;
    },
    
    updateGraph: function() {
    	var min = this.getProperty("min");
    	var max = this.getProperty("max");
    	var minYear = this.getProperty("minYear");
    	var maxYear = this.getProperty("maxYear");
    	var years = this.getModel().getProperty("/years");
    	if (min && max && minYear && maxYear && years) {
    		var html = jQuery("#"+this.sId);
			html.css('position', 'relative');
			html.empty();
			var width = html.innerWidth();
			var height = html.height();
			
			var opacity = "0.5";
			op  = function(currentYear) {
				if (currentYear >= minYear && currentYear <= maxYear)
					opacity = "1.0";
				else
					opacity = "0.3";
				return opacity;
			};
			var maxHeight = 0;
			for (elem in years)
			{
				if (years[elem].count > maxHeight)
					maxHeight = years[elem].count;
			}
			percent = function(currentCount) {
				return currentCount*100/maxHeight;
			};
			
		
			var left = 8;
			var x = "";
			var month = "";
			var paintWidth = 14;
		
			var lastYear = min;
			var offset = 0;
			for (elem in years)
			{
				
				x = years[elem];
				if (years[elem].year == "1"){
					month = "Jan";
				} else if (years[elem].year == "2"){
					month = "Feb";
				} else if (years[elem].year == "3"){
					month = "Mar";
				} else if (years[elem].year == "4"){
					month = "Apr";
				} else if (years[elem].year == "5"){
					month = "May";
				} else if (years[elem].year == "6"){
					month = "Jun";
				} else if (years[elem].year == "7"){
					month = "Jul";
				} else if (years[elem].year == "8"){
					month = "Aug";
				} else if (years[elem].year == "9"){
					month = "Sep";
				} else if (years[elem].year == "10"){
					month = "Oct";
				} else if (years[elem].year == "11"){
					month = "Nov";
				} else if (years[elem].year == "12"){
					month = "Dec";
				}
				
				html.append(
						'<a href=# onclick=Hide()>' +
						'<div title="'+(x.title ? x.title : month)+': '+x.count+'" style="position:absolute; left:'+(left+offset)+'px; width:'+paintWidth+'px;" class="yearChartBar'+(x.empty ? " empty" : "")+'">' +
							'<div style="width: 100%; position: absolute; bottom: 0; height:'+percent(x.count)+'%; opacity:'+op(x.year)+';" />' +
						'</div>'
					);
				lastYear += 1;
				left+= paintWidth+5;
			}

    	}
    },
    
    renderer : "sap.ui.core.HTMLRenderer"
    	
});