jQuery.sap.require("view.components.DrilldownItem");
jQuery.sap.require("view.components.DrilldownItemCategory");

sap.ui.jsview("view.components.DocumentTypes", {

    getControllerName : function() {
        return "view.components.DocumentTypes";
    },

    createContent : function(oController) {
        var oTemplate = new view.components.DrilldownItemCategory({
            title : "{category}",
            catTotal : "{catTotal}",
            selected : "{isSelected}",
            styleClass : "{styleClass}",
            items : {
              path: "items",
                template:  new view.components.DrilldownItem({
                     title : "{subject}", 
                     count : "{count}", 
                     total : "{total}",
                    press : [ function(oEvent) {
                    	var parent = $('span:contains(' + oEvent.getSource().getTitle() + ')').closest('div.selected').parent();
                    	var cat = $(parent).children('span.drilldownCategoryTitle').text();
                    	this.onItemSelected(oEvent.getSource().getTitle(), 'contentTypes', cat);                           
                     }, oController ]
                }),
                templateShareable : true,
            }, 
            press : [ function(oEvent) {
            	this.onItemSelected(oEvent.getSource().getTitle(), 'contentCat', null);
            }, oController ]
        });
        
        this.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/contentTypes",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true)//,
            }
        }));
    }
});

