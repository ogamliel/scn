jQuery.sap.require("view.components.DrilldownItem");

sap.ui.commons.Button.extend("view.components.DrilldownItemCategory", {
    metadata : {
        properties : {
            title : {
                type : "string",
                defaultValue : "Unknown"
            },
            catTotal : "int",
            selected : {
                type : "boolean",
                defaultValue : false
            },
            styleClass :  "string"
        },
        aggregations: {
                 items : {type : "view.components.DrilldownItem", multiple : true, singularName : "item"} 
        }
    },

    renderer : function(oRm, oControl) {
        var oNumberFormat = sap.ui.core.format.NumberFormat.getIntegerInstance(constants.NUMBER_FORMAT);

        oRm.write("<div");
        oRm.writeControlData(oControl);
        oRm.writeStyles();
        oRm.addClass("drilldowncategory");
        oRm.writeClasses();
        oRm.write(">");
        oRm.write("<span class='drilldownCategoryTitle'>");
        oRm.write(oControl.getTitle());
        oRm.write("</span>");
        if (oControl.getCatTotal())  {
	        oRm.write("<span class='drilldownCount'>");
	        oRm.write(oNumberFormat.format(oControl.getCatTotal()));
	        oRm.write("</span>");
        }
        oRm.write("<div style=\"clear:both;\"></div>");

        oRm.write("<div id=\"" + oControl.getTitle().replace(' ', '') + "\" ");
        if (oControl.getStyleClass()) {
            oRm.write(" class='" + oControl.getStyleClass() + "' ");
        }
        oRm.write(" >");
        
        // output the source publication items
        var items = oControl.getItems();
        $.each(items, function(idx, val){
              oRm.renderControl(val);
        });   
        oRm.write("</div>");
        
        oRm.write("</div>");

    }
});
