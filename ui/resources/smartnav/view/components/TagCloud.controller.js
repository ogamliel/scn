sap.ui.controller("view.components.TagCloud", {
	
	setActive : function() {
		// load initial data and manually trigger AJAX
		var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
		this.onUpdateView(null, null, globalModel.getData().viewContext);
	
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},
	
	setInactive : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},
	
	onInit: function() {
		var model = new sap.ui.model.json.JSONModel();	   
		this.getView().setModel(model);
		this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);

	},

	onExit : function() {
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
		this.setInactive();
	},
	
	onUpdateView : function(sChannelId, sEventId, oData) {
		if (oData.isLiveSearch) {
			return;
		}

		var model = this.getView().getModel();
		if (model.oData.searchTerm != oData.searchTerm || model.oData.year != oData.filters.year
				|| model.oData.publishersCurrent != oData.filters.publishers || model.oData.subjectsCurrent != oData.filters.subjects) {
			this.loadData(oData.searchTerm, oData.filters);
			
			model.oData.searchTerm = oData.searchTerm;
			model.oData.publishersCurrent = oData.filters.publishers ? jQuery.extend(true, {}, oData.filters.publishers) : undefined;
			model.oData.subjectsCurrent = oData.filters.subjects ? jQuery.extend(true, {}, oData.filters.subjects) : undefined;
			if (oData.filters.year)
				model.oData.year = {from: oData.filters.year.from, to: oData.filters.year.to};
		}
	},
	
	onDataLoaded : function(event) {
		this.showTagCloud();
	},
	
	loadData : function(searchTerm, filters) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;		
		var query = { "search" : searchTerm };
		if (filters) {
			if  (filters.year) {
				query.from = filters.year.from;
				query.to = filters.year.to;				
			}
			if (filters.subjects) {
				query.subjects = filters.subjects;
			}
			if (filters.sentiment)  {
				query.sentimentStart = filters.sentiment.start;
				query.sentimentEnd = filters.sentiment.end;
			}
		}
		if (viewContext)  {
			query.lastFilter = viewContext.lastFilter;
		}
		
		this.getView().getModel().fetchData("/smartnavxs/tagcloud/relatedtags.xsjs", query);
	},

	showTagCloud: function() {
		var data = this.getView().getModel().getData();
		this.d3Cloud(this.normalize(data));
	},
	
	normalize : function(data) {
		var max = Number.MIN_VALUE;
		var min2 = Number.MAX_VALUE;
		var min = Number.MAX_VALUE;
		for (var i = 0; i < data.length; ++i) {
			if (data[i].count > max) {
				max = data[i].count;
				if (max < min2) {
					min2 = max;
				}
			}
			if (data[i].count < min) {
				min2 = min;
				min = data[i].count;
			}
		}
		// calc size in percent w.r.t. max value
		var tmp = [];		
		for (var i = 0; i < data.length; ++i) {
			tmp.push({ text : data[i].tag, percent:  data[i].count});// Math.ceil(100 * data[i].count / max)});
		}

		return {
			min : Math.floor(100 * min / max),
			min2 : Math.floor(100 * min2 / max),
			max : 100,
			data : tmp
		};		
	},

	d3Cloud : function(data) {
		var sample = [
		              "Hello", "world", "normally", "you", "want", "more", "words",
		              "than", "this"].map(
		            		  function(d) {
		            			  return {text: d, size: 10 + Math.random() * 90};
		            		  });
		
		var width = $("#tagcloud").width();
	 	var height = width;
		
		for (var i = 0; i < data.data.length; ++i) {
			data.data[i].size = Math.max(13, (Math.floor(data.data[i].percent / 10) * 2) + 8);
			data.data[i].rotate = 0;
		}

		d3.layout.cloud().size([width, height])
			.words(data.data)
			.rotate(function(d) { return d.rotate; })
			.font("Impact")
			.fontSize(function(d) { return d.size; })
			.on("end", this.d3CloudDraw)
			.start();		 
	},
	
	 d3CloudDraw : function(words) {
		 var width = $("#TagCloud").width();
		 var height = width;
		 var fill = d3.scale.category20();

		 d3.select("#tagcloud").select("svg").remove();
	     d3.select("#tagcloud").attr("height", height).append("svg")
		        .attr("width", width)
		        .attr("height", height)
		      .append("g")
		        .attr("transform", "translate(" + Math.floor(width / 2) + "," + Math.floor(height / 2) + ")")
		      .selectAll("text")
		        .data(words)
		      .enter().append("text")
		        .style("font-size", function(d) { return d.size + "px"; })
		        .style("font-family", "Tahoma")
		        .style("font-weight", "normal")
		        .style("cursor", "pointer")
		        .style("fill", function(d, i) { return constants.tagcloud.COLORS[Math.floor(Math.random()*constants.tagcloud.COLORS.length)];}) // fill(i); })
		        .attr("text-anchor", "middle")
		        .attr("id", function(d, i) {
		        	setTimeout(function() {
		    			$("#tagcloud_word_" + i).click(
		    					function() {
		    						var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		    						var currSearchTerm = viewContext.searchTerm;
		    						//console.debug("TagCloud word '" + d.text + "' was clicked");
		    						sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST,
    								{
    									searchTerm: (currSearchTerm) ? currSearchTerm  + ' ' + d.text : d.text,
    								});
		    					});
		    		}, 12);		        	
		        	return "tagcloud_word_" + i;
		        } )
		        .attr("transform", function(d) {
		        	//console.debug(d);
			          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
		        })
		        .text(function(d) { return d.text; })
		        // tooltip
		        .append("title").text(function(d) {return d.text;});
	 }
});
