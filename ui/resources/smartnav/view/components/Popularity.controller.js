sap.ui.core.mvc.Controller.extend("view.components.Popularity", { 
	
	monthNames : [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],

	onInit : function() {
		this.getView().setModel(new sap.ui.model.json.JSONModel());
	},

	onExit : function() {
		this.setInactive();
	},
	
	onUpdateView : function(sChannelId, sEventId, oData) {
		var model = this.getView().getModel();
		
		if (model.oData.pdfId != oData.pdfId) {
			model.setData({pdfId : oData.pdfId});
			this.loadData(oData);
		}
	},

	onDataLoaded : function(event) {
		var model = this.getView().getModel();
		var series = model.getProperty("/accesscounts");
		var d = new Date();
		var yearSeries = [];
		var count = 0;
		
		var month = null;
		for (var elem=0; elem < series.length; elem++) {
			count += parseInt(series[elem].count);
			month = parseInt(series[elem].month);
		/*	monthyr = parseInt(series[elem].year); */
			month = month > d.getMonth()+1 ? month-d.getMonth()-1 : 11-d.getMonth()+month; 
	/*		yearSeries.push({year: month, count: parseInt(series[elem].count), title: this.monthNames[series[elem].month-1]}); */
			yearSeries.push({year: month, count: parseInt(series[elem].count), title: this.monthNames[series[elem].month-1]});
		}
		
		var lastMonth = 1;
		for (var i=0; i<yearSeries.length; i++) {
			if (yearSeries[i].year > lastMonth) {
				month = lastMonth + d.getMonth();
				month = month > 11 ? month - 12 : month;
				yearSeries.splice(i, 0, {year: lastMonth, count: 0, title: this.monthNames[month], empty: true});
			}
			lastMonth += 1;
		}
		while (lastMonth <= 12) {
			month = lastMonth + d.getMonth();
			month = month > 11 ? month - 12 : month;
			yearSeries.push({year: lastMonth, count: 0, title: this.monthNames[month], empty: true});	
			lastMonth += 1;			
		}
		
		model.oData.count = count;
		model.oData.minMonth = 1;
		model.oData.maxMonth = 24;
		model.oData.minSelected = 1;
		model.oData.maxSelected = 24;
		model.oData.years = yearSeries;
		model.oData.firstMonth = yearSeries[0].title;
		model.oData.lastMonth = yearSeries[yearSeries.length-1].title;
		model.oData.lastYear = d.getFullYear();
	//	model.oData.firstYear = d.getMonth() == 12 ? model.oData.lastYear : model.oData.lastYear - 1;
		model.oData.firstYear = d.getMonth() == 12 ? model.oData.lastYear : model.oData.lastYear - 2;
		   
		setTimeout(function() {
			model.updateBindings(true);
		}, 0);
	},
	
	loadData : function(oData) {
		this.getView().getModel().fetchData("/smartnavxs/popularity/popularity.xsjs", { pdfid : oData.pdfId }, true, "GET", true);
	},
	
	setActive : function() {
		this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
		this.onUpdateView(null, null, sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext);
	},

	setInactive : function() {
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	}
});