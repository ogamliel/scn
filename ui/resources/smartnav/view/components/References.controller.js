sap.ui.controller("view.components.References", {

	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 */
	showLimit : 5,
	
	onInit : function() {
		this.getView().setModel(new sap.ui.model.json.JSONModel());
		var oData = this.getView().getModel().oData;
		this.getView().getModel().fetchData("/smartnavxs/links/references.xsjs", { ref: oData.pdfId });
	},

	onExit : function() {
		this.setInactive();	
	},

	onUpdateView : function(sChannelId, sEventId, oData) {
		this.getView().getModel().fetchData("/smartnavxs/links/references.xsjs", { ref: oData.pdfId });
	},

	onFilter : function(oEvent) {
		var sQuery = oEvent.getParameter("query");
		var oBinding = this.getBinding("items");
		oBinding.filter(!sQuery ? [] : [new sap.ui.model.Filter("title", sap.ui.model.FilterOperator.Contains, sQuery)]);
		oDataSet.setLeadSelection(-1);
	},
	
	onSelect : function(oEvent) {
		var oItem = this.getItems()[oEvent.getParameter("newLeadSelectedIndex")];
		var viewname = this.getModel().getProperty("viewname", oItem.getBindingContext());
		var title = this.getModel().getProperty("title", oItem.getBindingContext());

		if (viewname === 'BokoHaramTargets')  {
			window.open('/smartnavxs/bokoHaramTargets.html', 'targets', 'height=550,width=900,location=no,toolbar=no,scrollbars=no, top=150, left=345');
		}
		
		else if (viewname === 'BokoHaramAttacks')  {
			window.open('/smartnavxs/bokoHaramAttacks.html', 'attacks', 'height=650,width=900,location=no,toolbar=no,scrollbars=no, top=50, left=345');
		}

		else if (viewname === 'NigeriaEmployment')  {
			window.open('/smartnavxs/nigeriaEmployment.html', 'attacks', 'height=510,width=900,location=no,toolbar=no,scrollbars=no, top=50, left=345');
		}

		else  {
			var url = this.getModel().getProperty("sourceUrl", oItem.getBindingContext());
			var refView = sap.ui.view({
				viewName : "view.references." + viewname,
				type : sap.ui.core.mvc.ViewType.JS,
				viewData : {
					url : url
				}
			});


			var dialog = new sap.ui.commons.Dialog({
				title : title,
				modal : true,
				showCloseButton : false,
				//width  : "700px",
				//height : "425px"
			});		
			
			dialog.addContent(refView);
			dialog.addStyleClass("conceptDialog"); // TODO required to be on top
			dialog.addButton(new sap.ui.commons.Button({
				text : "Close",
				press : function() {
					dialog.close();
					dialog.destroy();
				}
			}));
			dialog.open();
		}
	},
	
	setActive : function() {
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
		this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
		
		this.onUpdateView(null, null, sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"));
	},
	
	setInactive : function() {
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},
	onShowButtonPressed : undefined, //showAll or showLess

	onDataLoaded : function(event, test) {
		if (!this.onShowButtonPressed || this.onShowButtonPressed == this.showAll)
			this.showLess();
		else
			this.showAll();
	},
	
	showAll : function() {
		this.getView().getModel().oData.showButton = "Show Less";
		this.forEachItem(function(index, value) {
			value.isVisible = true; 
		});
		this.onShowButtonPressed = this.showLess;
	},
	
	showLess : function() {
		var oData = this.getView().getModel().oData;
		var visibleItems = 0;
		
		if (oData.references && oData.references.length > this.showLimit)
			oData.showButton = "Show All";
		
		this.forEachItem(function(index, value) {
			value.isVisible = (visibleItems < this.showLimit) ? true : false;
			visibleItems += value.isVisible ? 1 : 0;
		}, this);
		
		this.onShowButtonPressed = this.showAll;
	},
	
	forEachItem : function(callback, obj, updateBinding) {
		var oData = this.getView().getModel().oData;
		
		if (oData.references) 
			$.each(oData.references, function(index, value) {
				callback.call(obj, index, value);
			});
		
		if (updateBinding == undefined || updateBinding)
			this.getView().getModel().updateBindings(true);
	}	
	
});