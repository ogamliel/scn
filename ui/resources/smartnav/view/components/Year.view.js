jQuery.sap.require("view.components.YearHTMLChart");
sap.ui.jsview("view.components.Year", {

	getControllerName : function() {
		return "view.components.Year";
	},

	createContent : function(oController) {
		var oLayout = new sap.ui.commons.layout.VerticalLayout({
			width: "100%"
		});
		
		var minYearTextView = new sap.ui.commons.TextView({
			text: {
		        path:"/minSelected"
		    }
		}).addStyleClass("minYear");
		
		var countTextView = new sap.ui.commons.TextView({
			text: {
		        path:"/count",
		        type: new sap.ui.model.type.Integer(constants.NUMBER_FORMAT),
		        formatter: function(bValue) {
		        	if (bValue)
		        		if (bValue == 1)
		        			return bValue + " Result";
		        		else
		        			return bValue + " Results";
					else
						return "";
		        }
		    }
		}).addStyleClass("yearCount");
		
		var maxYearTextView = new sap.ui.commons.TextView({
			text: {
		        path:"/maxSelected"
		    }
		}).addStyleClass("maxYear");
		
/*		var header = new sap.ui.commons.layout.HorizontalLayout({
			content: [minYearTextView, maxYearTextView, countTextView]
		});*/
		
		var header = new sap.ui.commons.layout.HorizontalLayout({
			content: [ countTextView]
		});
		oLayout.addContent(header);

		var html = new view.components.YearHTMLChart("smartnav_year_filter_area", {
             content : "<div id='smartnav_year_filter_area' style='position:relative;width:100%;height:74px;'></div>",
             preferDOM : true
		 });
		html.bindProperty("minYear","/minSelected");
		html.bindProperty("maxYear","/maxSelected");
		html.bindProperty("min","/minYear");
		html.bindProperty("max","/maxYear");
		html.bindProperty("years","/series");		
		oLayout.addContent(html);
		
		
		var oSlider = new sap.ui.commons.RangeSlider({
			id : 'yearSlider2',
			width : '100%',
			smallStepWidth : 1,
			stepLabels : true,
			enabled : false,
			change : function(){
	             oController.changeYears(oSlider.getValue(), oSlider.getValue2());
	        },
			liveChange : function(){
	             oController.changeLiveYears(oSlider.getValue(), oSlider.getValue2());
	        }
		});
		oSlider.bindProperty("labels","/labels");
		oSlider.bindProperty("totalUnits","/yearDistance");
		oSlider.bindProperty("min","/minYear");
		oSlider.bindProperty("max","/maxYear");
		oSlider.bindProperty("value","/minSelected");
		oSlider.bindProperty("value2","/maxSelected");
		oSlider.addStyleClass("additionalSpaceForYearSlider");
		oLayout.addContent(oSlider);

		return oLayout;
	}
});
