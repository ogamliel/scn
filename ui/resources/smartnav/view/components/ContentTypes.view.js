jQuery.sap.require("view.components.DrilldownItem");

sap.ui.jsview("view.components.ContentTypes", {

    getControllerName : function() {
        return "view.components.ContentTypes";
    },

    createContent : function(oController) {
        var oTemplate = new view.components.DrilldownItem({
            title : "{contentTypes}",
            count : "{count}",
            total : "{total}",
            selected : "{isSelected}",
            press : [ function(oEvent) {
                this.onItemSelected(oEvent.getSource().getTitle(), 'Content Type');
            }, oController ]
        });
        
        /*var searchButton = new sap.ui.commons.Link({
        	id : "CONTENTTYPE",
            text : "{/searchButton}",
            tooltip : "Search For Person Not Listed", 
            press : [ function() {
                this.onSearchButtonPressed("Person");
            }, oController ]
        });*/

        this.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/contentTypes",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, true) ]
            }
        })).addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/contentTypes",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, false),
                    new sap.ui.model.Filter("isVisible", sap.ui.model.FilterOperator.EQ, true) ]
            }
        }));
        /*.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : [ searchButton ]
        })).addContent(new sap.ui.commons.Link({
            text : "{/showButton}",
            press : [ function() {
                this.onShowButtonPressed();
            }, oController ]
        }));*/
    }
});

