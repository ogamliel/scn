jQuery.sap.require("view.ReferenceItem");
sap.ui.jsview("view.components.References", {

	getControllerName : function() {
		return "view.components.References";
	},

	createContent : function(oController) {
		return [new sap.ui.ux3.DataSet({
			showToolbar: false,
			width: "100%",
			items: {
				path: "/references",
				template: new sap.ui.ux3.DataSetItem({
					title : "{title}",
				}),
				filters: [new sap.ui.model.Filter("isVisible", sap.ui.model.FilterOperator.EQ, true)]
			},
			views: [
				new sap.ui.ux3.DataSetSimpleView({
					name: "Single Row View",
					icon: "images/icons/list.png",
					iconHovered: "images/icons/list_hover.png",
					iconSelected: "images/icons/list_hover.png",
					floating: false,
					responsive: false,
					itemMinWidth: 0,
					template: new view.ReferenceItem({
						title : new sap.ui.commons.Link({
							text : "{title}"
						}),
						xsFile    : "{xsFile}",
						viewname  : "{viewname}",
						source    : "{source}",
						sourceUrl : "{sourceUrl}",
						//source    : new sap.ui.commons.Link({
						//	text : "{source}",
						//	href : "{sourceUrl}",
						//	target : "_blank"
						//}),
						image : new sap.ui.commons.Image({
							src : "images/icons/documents_64.png",
						}),
					}),
				})
			],
			//search: oController.onFilter, 
			selectionChanged: oController.onSelect,
		}).addStyleClass("CompactList"),
		new sap.ui.commons.Link({
			text: {path: "/showButton"},
			press: function() {oController.onShowButtonPressed();}
		})
       ];
	}
});
