jQuery.sap.require("view.components.DrilldownItem");
jQuery.sap.require("view.components.DrilldownItemCategory");

sap.ui.jsview("view.components.FacetEntities", {

    getControllerName : function() {
        return "view.components.FacetEntities";
    },

    createContent : function(oController) {
        var oTemplate = new view.components.DrilldownItemCategory({
            title : "{category}",
            selected : "{isSelected}",
            styleClass : "{styleClass}",
            items : {
              path: "items",
                template:  new view.components.DrilldownItem({
                     title : "{entity}", 
                     count : "{count}", 
                     total : "{total}",
                  press : [ function(oEvent) {
                    	var parent = $('span:contains(' + oEvent.getSource().getTitle() + ')').closest('div.selected').parent();
                    	var cat = $(parent).last().children('span.drilldownCategoryTitle').text();
                    	this.onItemSelected(oEvent.getSource().getTitle(), 'entities', cat);                           
                     }, oController ]
                })
            }, 
            press : [ function(oEvent) {
            	this.onItemSelected(oEvent.getSource().getTitle(), 'categories', null);
            }, oController ]
        });
        
        this.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/facets",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true)//,
            }
        }));
    }
});

