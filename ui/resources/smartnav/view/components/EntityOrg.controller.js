jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.EntityOrg", {
	path : "orgs",
	attrName : "org",
	restCall: "/smartnavxs/entities/orgs.xsjs"
});
