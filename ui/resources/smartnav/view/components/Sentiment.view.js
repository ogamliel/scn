sap.ui.jsview("view.components.Sentiment", {

	getControllerName : function() {
		return "view.components.Sentiment";
	},

	createContent : function(oController) {
		var oLayout = new sap.ui.commons.layout.MatrixLayout({
			layoutFixed : true,
			columns : 1,
			width : "100%",
			widths : [ "100%" ]
			});

		var oGradient = new sap.ui.core.HTML( {
			width: '100%',
			content: "<div id=\"sentiment\"><div id=\"sentimentGradient\"></div></div>",
		});
		oGradient.addStyleClass("sentiment");
		
		var oSlider = new sap.ui.commons.RangeSlider({
			width: '90%',
			min: -1000,
			max: 1000,
			change : function(e){
	            oController.changeSentiment( oSlider.getValue(), oSlider.getValue2());
	        },
		});
		oSlider.addStyleClass("sentimentSlider");
		
		oSlider.bindProperty("value","/sentimentStart");
		oSlider.bindProperty("value2","/sentimentEnd");

		oLayout.createRow(oGradient);
		oLayout.createRow(oSlider);
		return oLayout;
	}
});
