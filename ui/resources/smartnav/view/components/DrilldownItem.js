sap.ui.commons.Button.extend("view.components.DrilldownItem", {
    metadata : {
        properties : {
            title : {
                type : "string",
                defaultValue : "Unknown"
            },
            count : "int",
            total : "int",
            selected : {
                type : "boolean",
                defaultValue : false
            },
            styleClass : {
                type : "string",
                defaultValue : null
            }
        }
    },

    renderer : function(oRm, oControl) {
        var oNumberFormat = sap.ui.core.format.NumberFormat.getIntegerInstance(constants.NUMBER_FORMAT);
        oRm.write("<div");
        oRm.writeControlData(oControl);
        oRm.writeStyles();
        oRm.addClass("drilldownitem");
        if (oControl.getSelected()) {
            oRm.addClass("selected");
        }
        if (oControl.getStyleClass()) {
            oRm.addClass(oControl.getStyleClass());
        }
        oRm.writeClasses();
        oRm.write(">");
        oRm.write("<span class='drilldownTitle'>");
        oRm.write(oControl.getTitle());
        oRm.write("</span>");
        oRm.write("<span class='drilldownCount'>");
        oRm.write(oNumberFormat.format(oControl.getCount()));
        oRm.write("</span>");
        oRm.write("<span class='drilldownPercent backgroundGradient' style='width:");
        oRm.write(Math.floor((oControl.getCount() * 100) / oControl.getTotal()));
        oRm.write("%'>");
        oRm.write("</span>");
        oRm.write("<div style=\"clear:both;\"></div>");
        oRm.write("</div>");
    }
});