sap.ui.core.mvc.Controller.extend("view.components.TranslateEntities", { 
		
	showLimit: 5,
	isActive: false,

	onInit : function() {
		this.getView().setModel(new sap.ui.model.json.JSONModel());
		this.getView().setModel(new sap.ui.model.json.JSONModel(), "layers");
		this.setActive();
	},

	onExit : function() {
		this.setInactive();
	},

	setActive : function() {
		if (!this.isActive) {
			this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
			this.getView().getModel("layers").attachRequestCompleted(this.onLayerDataLoaded, this);
			sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.fetchData, this);
			sap.ui.getCore().getEventBus().subscribe(constants.events.ADD_HIGHLIGHT_TYPE, this.addHighlightType, this);

			this.fetchData(null, null, sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"));
			this.isActive = true;
		}
	},

	setInactive : function() {
		if (this.isActive) {
			this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
			this.getView().getModel("layers").detachRequestCompleted(this.onLayerDataLoaded, this);
			sap.ui.getCore().getEventBus().unsubscribe(constants.events.ADD_HIGHLIGHT_TYPE, this.addHighlightType, this);
			sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.fetchData, this);
			this.isActive = false;
		}
	},

	fetchData : function(sChannelId, sEventId, oData) {
		// clean previous layer model
		var parent = this;

		$.ajax({  
			type:"POST",
        	url: "/smartnavxs/textanalysis/analyze12.xsjs",
        	async: true,
        	data: encodeURIComponent(JSON.stringify({text: oData.contentText})),
        	success: function(result){
	            if(!result.error){
	            	//var jsonRes = JSON.parse(result);
	            	var entities = result.entities; //jsonRes.entities;
	            	
	            	// filter, sort, unique, count...
	            	entities = parent.unique(parent.sort(parent.filter(entities)));
	            	
	            	var stats = parent.countEntities(entities);
					parent.getView().getModel().oData.entities = stats.entities;
					parent.onDataLoaded();
	            }
        	}
    	});
   	
	},
	
	countEntities : function (entities)  {
		this.getView().getModel().oData.tokens = {};
		var stats = [];
		var entityTypes = Object.keys(constants.entitymapping);
		for (var i=0; i<entityTypes.length; i++)  {
			var key = entityTypes[i];
			var arr = jQuery.grep(entities, function( a ) {
				if (key === "ORGANIZATION")  {
					return a.labelPath.startsWith("ORGANIZATION");
				}
				else if (key === "EMAIL")  {
					return a.labelPath === "URI/EMAIL";
				}
				else  {
					return a.labelPath === key;
				}
			});
			if (arr.length > 0)  {
				stats.push({
					type : key, //constants.entitymapping[key].title,
					count : arr.length
				});
				this.getView().getModel().oData.tokens[key] = arr; // make entity values available for highlighting
			}
		}
		return {"entities": stats};
	},
	
	unique : function(arr) {
	    var u = [];
	    arr.reduce(function (a, b) {
	        if (a["labePath"] !== b["labelPath"] && a["text"] !== b["text"]) {u.push(b);}
	        return b;
	    }, []);
	    return u;
	},
	
	sort : function (arr)  {
		arr.sort(function(a, b){
			var labelPathA=a.labelPath.toUpperCase(), labelPathB=b.labelPath.toUpperCase();
			if (labelPathA < labelPathB) {//sort string ascending
				return -1;
			}
			if (labelPathA > labelPathB)  {
				return 1;
			}
			return 0; //default return value (no sorting)
		});
		return arr;	
	},
	
	filter : function (arr)  {
		return arr.filter(function (el) {
			return el.labelPath === "PERSON" ||
				el.labelPath === "URI/EMAIL" ||
				el.labelPath === "LOCALITY" ||
				el.labelPath === "NOUN_GROUP" ||
				el.labelPath === "PARLIAMENT" ||
				el.labelPath.startsWith("ORGANIZATION");
		});		
	},
	
	onItemSelected : function(item) {
		// add/remove selected entity type to/from view context
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		if (!viewContext.entities) {
			viewContext.entities = [];
		}
		
		if ($.inArray(item, viewContext.entities) != -1) {
			viewContext.entities.splice( $.inArray(item, viewContext.entities), 1 );
			this.removeLayer(item);
		} else {
			viewContext.entities.push(item);
			var model = this.getView().getModel("layers");
			if (item === "USER_CONCEPT_SEARCH") {
				this.showLayer(item);
			} else if (model.oData[item]) {
				// add layer directly
				this.addLayer(item, model.oData[item]);
			} else {
				// layer will be added when data is loaded
				this.loadLayer(viewContext.pdfId, item);
			}
		}

		// propagate selection to  view representation without
		// triggering an update of the PDF viewer, i.e., without
		// firing an UPDATE_VIEW event over the event bus
		this.onUpdateView(null, null, viewContext);
	},
	
	onUpdateView : function(sChannelId, sEventId, oData) {
		var model = this.getView().getModel();

		if (model.oData.doi != oData.doi) {
			this.loadData(oData.doi);
		} else {
			if (oData.entities) {
				this.selectItems(oData.entities);
			}			
			if (this.onShowButtonPressed == this.showAll) {
				this.showLess();
			}
		}
	},

	addHighlightType :function(sChannelId, sEventId, oData) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var mData = this.getView().getModel().oData;

		var type = oData.type;
		var count = oData.count;

		var found = false;
		for (var i = 0; i < mData.entities.length; i++) {
			if (mData.entities[i].type == type) {
				found = true;
				break;
			}
		}

		if (!found)
			mData.entities.push({type:type, count:count});
		else
			mData.entities[i].count = count;
		if (typeof	viewContext.entities == "undefined")
			viewContext.entities = [];
		if ($.inArray(type, viewContext.entities) == -1) {
			viewContext.entities.push(type);
		}
		this.selectItems(viewContext.entities);
	},

	onDataLoaded : function() {
	    $("#translateProgress").css("display","none");

		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var oData = this.getView().getModel().oData;
		viewContext.entities = [];
		if (typeof viewContext.searchTerm != "undefined" && viewContext.searchTerm.length > 0) {
			var found = false;
			for (var i = 0; i < oData.entities.length; i++) {
				if (oData.entities[i].type == "SEARCH")  {
					found = true;
				}
			}
			if (!found)  {
				$(document).ready(function() {
					var searchCnt = $('#viewer').find('span.search').length;
					if (searchCnt === 0)  {
						oData.entities.push({type:"SEARCH", count:1});
					}
					else  {
						oData.entities.push({type:"SEARCH", count:searchCnt});
					}
				});
			}
			if (typeof	viewContext.entities == "undefined")
				viewContext.entities = [];
			if ($.inArray("SEARCH", viewContext.entities) == -1)
				viewContext.entities.push("SEARCH");
		}
		oData.showButton = undefined;
		var layersModel = this.getView().getModel("layers");

		if (typeof viewContext.searchTerm != "undefined" && viewContext.searchTerm.length > 0) {
			layersModel.oData["SEARCH"] = [[viewContext.searchTerm, false]];
			this.addLayer("SEARCH",[[viewContext.searchTerm, false]]);
		}

		this.selectItems(viewContext.entities);
		this.showLess();
	},
	
	onLayerDataLoaded : function(name) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel("layers");
		for (var layer in model.oData) {
			if ($.inArray(layer, viewContext.entities) != -1) {
				if (layer != 'SEARCH')
					this.addLayer(layer, model.oData[layer]);
			}
		}
	},	
	
	addLayer : function(name, entities) {
		sap.ui.getCore().getEventBus().publish(constants.events.TRANSLATE_ADD_HIGHLIGHT_LAYER, {
			type: highlightType.CUSTOM,
			entityType: name,
			layer: name,
			highlights:
				$.map(entities, function(value) {
					return {
						highlight : value.text,
						metadata: {
							cssClassName: constants.entitymapping[name].css, 
							normalized: value.normalizedForm,
							layer: name
						}
					};
				})
		});
		
	},
	
	showLayer : function(name) {
		sap.ui.getCore().getEventBus().publish(constants.events.SHOW_HIGHLIGHTS_OF_TYPE, { layer: name });
	},

	removeLayer : function(name) {
		sap.ui.getCore().getEventBus().publish(constants.events.TRANSLATE_REMOVE_HIGHLIGHT_LAYER, { layer: name });
	},
	
	loadLayer : function(pdfId, name) {
		var tokens = this.getView().getModel().oData.tokens;
		var model = this.getView().getModel("layers");
		model.oData[name] = tokens[name];
		this.onLayerDataLoaded(name);
	},
	
	onShowButtonPressed : undefined, //showAll or showLess
	
	showAll : function() {
		this.getView().getModel().oData.showButton = "Show Less";
		this.forEachItem(function(index, value) {
			value.isVisible = true; 
		});
		this.onShowButtonPressed = this.showLess;
	},
	
	showLess : function() {
		var oData = this.getView().getModel().oData;
		var visibleItems = 0;
		
		this.forEachItem(function(index, value) {
			visibleItems += value.isSelected ? 1 : 0;
		}, this, false);
		
		if (oData.entities && oData.entities.length > this.showLimit)
			oData.showButton = "Show All";
		
		this.forEachItem(function(index, value) {
			value.isVisible = (visibleItems < this.showLimit) ? true : false;
			visibleItems += (value.isVisible && !value.isSelected) ? 1 : 0;
		}, this);
		
		this.onShowButtonPressed = this.showAll;
	},
	
	selectItems : function(itemssToSelect) {
		if (itemssToSelect) {
			for (var i = 0; i < itemssToSelect.length; i++) {
				
				var found = false;
				this.forEachItem(function(index, value) {
					if (itemssToSelect[i] === value['type']) 
						found = true;					
				}, this, false);
				
				if (!found) {
					var obj = {	};
					obj.count = 0;
					obj.total = 0;
					obj['type'] = itemssToSelect[i];
					
					this.getView().getModel().oData.entities.push(obj);
				}
			}
		}
		
		this.forEachItem(function(index, value) {
			value.isSelected = $.inArray(value['type'], itemssToSelect) != -1 ? true : false;
			
			if (!value.isSelected && value.count == 0)
				this.getView().getModel().oData.entities.splice(index, 1);
		}, this);
	},
	
	forEachItem : function(callback, obj, updateBinding) {
		var oData = this.getView().getModel().oData;

		if (oData.entities) 
			$.each(oData.entities, function(index, value) {
				callback.call(obj, index, value);
			});
		
		if (updateBinding == undefined || updateBinding)
			this.getView().getModel().updateBindings(true);
	}	
});