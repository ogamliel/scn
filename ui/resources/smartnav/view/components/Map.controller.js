sap.ui.controller("view.components.Map", {
	path : "mapBounds",
	map : {},
	heat : {},
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		
	},
	
	setInactive : function() {
		
	},

   onInit: function() {
		this.getView().setModel(new sap.ui.model.json.JSONModel());
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.updateMap, this);
   },
   
	onAfterRendering: function(oEvent)  {
		this.map = this.createMap();
		this.getHeatMap({
			ur_lng : this.map.getBounds().getNorthEast().lng,
			ur_lat : this.map.getBounds().getNorthEast().lat,
			ll_lng : this.map.getBounds().getSouthWest().lat,
			ll_lat : this.map.getBounds().getSouthWest().lat
		});
		
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		if (viewContext.drawnItems)  {
			//console.log(viewContext.drawnItems);
		}
		else  {
			viewContext.drawnItems = [];
		}
	},
	
	updateMap : function(sChannelId, sEventId, oData)  {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var filters = viewContext.filters;
		var params = {
		      "search" : viewContext.searchTerm
		};
		
		if (filters)  {
		  if (filters.subjects) {
		         params.subjects = filters.subjects;
		  }
		  
		  if (filters.publishers) {
		         params.publishers = filters.publishers;
		  }
		  
		  if (filters.persons) {
		      params.persons = filters.persons;
		  }
		  if (filters.orgs) {
		      params.orgs = filters.orgs;
		  }
		  if (filters.localities) {
		      params.localities = filters.localities;
		  }
		  
		  if (filters.contentTypes) {
		      params.contentTypes = filters.contentTypes;
		  }
		
		  if (filters.year) {
			 params.from = filters.year.from;
		     params.to = filters.year.to;
		 }
		  
		  if (filters.sentiment)  {
			  params.sentimentStart = filters.sentiment.start;
			  params.sentimentEnd = filters.sentiment.end;
		  }
		
		  if (filters.interactionTypes)  {
			  params.interactionTypes = filters.interactionTypes;
		  }
		}
		var bounds = viewContext.filters[this.path];
		if (bounds && bounds.length > 0)  {
			params.ur_lng = bounds[0].x1;
			params.ur_lat = bounds[0].y1;
			params.ll_lng = bounds[0].x;
			params.ll_lat = bounds[0].y;
		}
		else  {
			params.ur_lng = this.map.getBounds().getNorthEast().lng;
			params.ur_lat = this.map.getBounds().getNorthEast().lat;
			params.ll_lng = this.map.getBounds().getSouthWest().lng;
			params.ll_lat = this.map.getBounds().getSouthWest().lat;
		}
		
		var parent = this;
	    parent.map.removeLayer(parent.heat);

		if (params.ur_lng != 0 && params.ur_lat != 0 && params.ll_lng != 0 && params.ll_lat != 0)  {
			$.ajax({
			    url: "/smartnavxs/map/GetHeatMap.xsjs",
			    async: true,
			    type: 'GET',
			    data: params,
			    success: function(data) {
					parent.heat = L.heatLayer(data.data, {radius: 25, blur: 15});
					parent.map.addLayer(parent.heat);
			    },
				error: function (xhr, ajaxOptions, thrownError) {
				    console.log(xhr.status);
				    console.log(thrownError);			    
				}
			});
		}
	},
	
	getHeatMap : function(params)  {
		var parent = this;
		$.ajax({
		    url: "/smartnavxs/map/GetHeatMap.xsjs",
		    async: true,
		    type: 'GET',
		    data: params,
		    success: function(data) {
				parent.heat = L.heatLayer(data.data, {radius: 15, blur: 20});
				parent.map.addLayer(parent.heat);
		    },
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);			    
			}
		});
	},
	
	createMap : function()  {
		var parent = this;
		document.getElementById('searchmap').innerHTML = "<div id='map' style='width: 238px; height: 200px;'></div>";
		var mapOptions = { 
			   zoomControl:false
		};
		var map = L.map('map', mapOptions); //.setView([2, 20], 2);
		map.fitWorld();

		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	       attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

		var drawnItems = new L.FeatureGroup();
		drawnItems.on('dblclick', function(e) { 
		    parent.map.removeLayer(parent.heat);
		    parent.heat = {};
			//console.log(e.layer) 
		});
		
		map.addLayer(drawnItems);
		var drawControl = new L.Control.Draw({
			  draw: {
				    polygon: false,
				    polyline: false,
				    rectangle: true,
				    circle: false,
			    marker: false
			  },
			    edit: {
			        featureGroup: drawnItems
			    }
		});
			
		map.addControl(drawControl);
		
		map.on('draw:created', function(e) {
		    parent.map.removeLayer(parent.heat);
		    
			var layer = e.layer;
		    drawnItems.addLayer(layer);
		    var bounds = layer.getBounds();
		    
		   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
			if (!viewContext.filters[parent.path]) {
				viewContext.filters[parent.path] = [];
			}
			
			viewContext.filters[parent.path].push(
						{'x1': bounds.getNorthEast().lng,
				    	'y1': bounds.getNorthEast().lat,
				    	'x': bounds.getSouthWest().lng,
				    	'y': bounds.getSouthWest().lat}
			);
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
		  });	
		
		map.on('draw:deletestart', function (e) {
			drawnItems.eachLayer(function (layer) {
				drawnItems.removeLayer(layer);
			});
		});

		map.on('draw:deletestop', function (e) {
		   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
			viewContext.filters[parent.path] = [];
			drawnItems.eachLayer(function (layer) {
			    var bounds = layer.getBounds();
				viewContext.filters[parent.path].push(
						{'x1': bounds.getNorthEast().lng,
				    	'y1': bounds.getNorthEast().lat,
				    	'x': bounds.getSouthWest().lng,
				    	'y': bounds.getSouthWest().lat}
				);

			});
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
		});

		map.on('draw:edited', function (e) {
		   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
			viewContext.filters[parent.path] = [];
			drawnItems.eachLayer(function (layer) {
			    var bounds = layer.getBounds();
				viewContext.filters[parent.path].push(
						{'x1': bounds.getNorthEast().lng,
				    	'y1': bounds.getNorthEast().lat,
				    	'x': bounds.getSouthWest().lng,
				    	'y': bounds.getSouthWest().lat}
				);

			});
			//viewContext.drawnItems = drawnItems;
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
		});

		map.on('dragend', function(e) { 
		    parent.map.removeLayer(parent.heat);
		    parent.heat = {};
		    
		    var bounds = map.getBounds();
		   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
			if (!viewContext.filters[parent.path]) {
				viewContext.filters[parent.path] = [];
			}
			viewContext.filters[parent.path] =
						[{'x1': bounds.getNorthEast().lng,
				    	'y1': bounds.getNorthEast().lat,
				    	'x': bounds.getSouthWest().lng,
				    	'y': bounds.getSouthWest().lat}];
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
		});

		map.on('zoomend', function(e) { 
		    parent.map.removeLayer(parent.heat);
		    parent.heat = {};
		    
		   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
		});
		
		return map;
	},
	
   onUpdateView : function(sChannelId, sEventId, oData) {
       //this.loadData(oData);
   },

   onDataLoaded : function(event) {
	   	var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel();	

   },
   
   changeMap : function (val1, val2)  {
	   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		viewContext.filters[this.path] = {};		
		viewContext.lastFilter = this.path;

		sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
   },
	
	onExit : function() {
		this.setInactive();
		//this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	}   
   
});
