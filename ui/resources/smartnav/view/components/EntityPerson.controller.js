jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.EntityPerson", {
	path : "persons",
	attrName : "person",
	restCall: "/smartnavxs/entities/persons.xsjs"
});
