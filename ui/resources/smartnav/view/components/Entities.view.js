jQuery.sap.require("view.components.DrilldownItem");
sap.ui.jsview("view.components.Entities", {

    getControllerName : function() {
        return "view.components.Entities";
    },

    createContent : function(oController) {
       var oTemplate = new view.components.DrilldownItem({
            title : {
                path: "type",
                formatter: function(t) { return constants.entitymapping[t].title; }
            },
            count : "{count}",
            total : "{count}",
            selected : "{isSelected}",
            styleClass : {
                path: "type",
                formatter: function(t) { return constants.entitymapping[t].css; }
            },
            press : [ function(oEvent) {
                var source = oEvent.getSource();
                this.onItemSelected(source.getModel().getProperty("type", source.getBindingContext()));
            }, oController ]
        });    	
    	
        this.addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/entities",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, true) ]
            }
        })).addContent(new sap.ui.commons.layout.VerticalLayout({
            width : "100%",
            content : {
                path : "/entities",
                template : oTemplate,
                templateShareable : true,
                sorter : new sap.ui.model.Sorter("count", true),
                filters : [ new sap.ui.model.Filter("isSelected", sap.ui.model.FilterOperator.EQ, false) ]
            }
        }));
    }
});
