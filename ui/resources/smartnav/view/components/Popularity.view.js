jQuery.sap.require("view.components.YearHTMLChart");
sap.ui.jsview("view.components.Popularity", {

	getControllerName : function() {
		return "view.components.Popularity";
	},

	createContent : function(oController) {
		var oLayout = new sap.ui.commons.layout.VerticalLayout({
			width: "100%"
		});
		
		var countTextView = new sap.ui.commons.TextView({
			text: {
		        path:"/count",
		        type: new sap.ui.model.type.Integer(constants.NUMBER_FORMAT),
		        formatter: function(bValue) {
		        	if (bValue)
		        		if (bValue == 1)
		        			return bValue + " Matches (not article specific)";
		        		else
		        			return bValue + " Matches (not article specific)";
					else
						return "";
		        }
		    }
		});
		oLayout.addContent(countTextView);
		
		var html = new view.components.YearHTMLChart("smartnav_popularity_area", {
            content : "<div id='smartnav_popularity_area' style='position:relative;width:100%;height:64px;'></div>",
            preferDOM : true
		 });
		html.bindProperty("minYear","/minMonth");
		html.bindProperty("maxYear","/maxMonth");
		html.bindProperty("min","/minMonth");
		html.bindProperty("max","/maxMonth");
		html.bindProperty("years", "/years");		
		oLayout.addContent(html);
		
		var oLegend = new sap.ui.commons.layout.MatrixLayout({
			columns : 2,
			width : '100%',
			widths : ['50%', '50%'] 
		}).addStyleClass("legend");
		oLayout.addContent(oLegend);
		
		var oRow = new sap.ui.commons.layout.MatrixLayoutRow();
		oLegend.addRow(oRow);
		
		oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
			content : [new sap.ui.commons.TextView().bindText("/firstMonth"),
			           new sap.ui.commons.TextView().bindText("/firstYear", function(value) {
			        	   return value ? "'" + value.toString().substring(2) : "";
			           })]
		}));
		
		oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
			content : [new sap.ui.commons.TextView().bindText("/lastMonth"),
			           new sap.ui.commons.TextView().bindText("/lastYear", function(value) {
			        	   return value ? "'" + value.toString().substring(2) : "";
			           })]
		}));
		
		return oLayout;
	}
});
