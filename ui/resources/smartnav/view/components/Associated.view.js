jQuery.sap.require("view.PublicationItem");
sap.ui.jsview("view.components.Associated", {

	getControllerName : function() {
		return "view.components.Associated";
	},

	createContent : function(oController) {
		return [new sap.ui.ux3.DataSet({
			showToolbar: false,
			width: "100%",
			items: {
				path: "/associated",
				template: new sap.ui.ux3.DataSetItem({
					title : "{title}",
				}),
				filters: [new sap.ui.model.Filter("isVisible", sap.ui.model.FilterOperator.EQ, true)]
			},
			views: [			        
				new sap.ui.ux3.DataSetSimpleView({
					name: "Single Row View",
					icon: "images/icons/list.png",
					iconHovered: "images/icons/list_hover.png",
					iconSelected: "images/icons/list_hover.png",
					floating: false,
					responsive: false,
					itemMinWidth: 0,
					template: new view.PublicationItem({
						title : new sap.ui.commons.Link({
							text : "{title}",
						}),
						pdfId : "{pdfid}",
						sitetype: "{sitetype}",
						journal : "{journal}",
						volume : "{volume}",
						firstPage : "{firstpage}",
						lastPage : "{lastpage}",
 						articleDate : "{articleDate}",
						image : new sap.ui.commons.Image({
							src : {
								path: "type",
								formatter: function(data) { 
									var parent = this.oParent;
                               	 	var pdfId = parent.getPdfId();
                              	 	if (pdfId)  {
	                               	 	if (pdfId.substring(0,4) === 'FFAD')  {
	                               	 		return data ? "images/icons/documents_48.png" : "";
	                               	 	}
	                               	 	else  {
	                               	 		return data ? "images/icons/event_sm.png" : "";
	                               	 	}
                              	 	}
								},
							}
						}),
					}).bindAggregation("authors", "authors",
							new sap.ui.commons.Label().bindProperty("text", "")),
				})
			],
			search: oController.onFilter, 
			selectionChanged: oController.onSelect,
		}).addStyleClass("CompactList"),
		new sap.ui.commons.Link({
			text: {path: "/showButton"},
			press: function() {oController.onShowButtonPressed();}
		})
       ];
	}
});
