jQuery.sap.require("view.PublicationItem");
sap.ui.jsview("view.components.Recommendations", {

	getControllerName : function() {
		return "view.components.Recommendations";
	},

	createContent : function(oController) {
		return [new sap.ui.ux3.DataSet({
			showToolbar: false,
			width: "100%",
			items: {
				path: "/recommendations",
				template: new sap.ui.ux3.DataSetItem({
					title : "{title}",
				}),
				filters: [new sap.ui.model.Filter("isVisible", sap.ui.model.FilterOperator.EQ, true)]
			},
			views: [			        
				new sap.ui.ux3.DataSetSimpleView({
					name: "Single Row View",
					icon: "images/icons/list.png",
					iconHovered: "images/icons/list_hover.png",
					iconSelected: "images/icons/list_hover.png",
					floating: false,
					responsive: false,
					itemMinWidth: 0,
					template: new view.PublicationItem({
						title : new sap.ui.commons.Link({
							text : "{title}",
						}),
						pdfId : "{pdfid}",
						sitetype: "{sitetype}",
						journal : "{journal}",
						volume : "{volume}",
						firstPage : "{firstpage}",
						lastPage : "{lastpage}",
 						articleDate : "{articleDate}",
						image : new sap.ui.commons.Image({
							src : {
								path: "type",
								formatter: function(data) { 
									var parent = this.oParent;
                               	 	var sitetype = parent.getSitetype();
                               	 	if (sitetype === 'ACLED')  {
                               	 		return "images/icons/acled.png" 
                               	 	}
                               	 	else  {
                               	 		return data ? "images/icons/" + data.toLowerCase().replace(" ", "") + ".png" : "";
                               	 	}									
									//return data ? "images/icons/" + data.toLowerCase() + ".png" : ""; 
								},
							}
						}),
					}).bindAggregation("authors", "authors",
							new sap.ui.commons.Label().bindProperty("text", "")),
				})
			],
			search: oController.onFilter, 
			selectionChanged: oController.onSelect,
		}).addStyleClass("CompactList"),
		new sap.ui.commons.Link({
			text: {path: "/showButton"},
			press: function() {oController.onShowButtonPressed();}
		})
       ];
	}
});
