sap.ui.controller("view.components.Year", {
	
	setActive : function() {
		var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData;
		this.onUpdateView(null, null, oData.viewContext);
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.YEAR_TO_MONTH, this.showMonths, this);
	},
	
	setInactive : function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
	},

   onInit: function() {
	   this.getView().setModel(new sap.ui.model.json.JSONModel());
	   this.getView().getModel().attachRequestCompleted(this.onDataLoaded, this);
   },
   
   onUpdateView : function(sChannelId, sEventId, oData) {
       this.loadData(oData);
   },

   onDataLoaded : function(event) {
	   	var oData = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
		var model = this.getView().getModel();
		
		model.setProperty("searchTerm", oData.searchTerm);
		model.setProperty("publishers", oData.filters.publishers ? jQuery.merge([], oData.filters.publishers) : undefined);
		model.setProperty("subjects", oData.filters.subjects ? jQuery.merge([], oData.filters.subjects) : undefined);
		this.calcModel();

		this.setSelection(oData.filters.year2);
		
		// modify count label when displaying month counts
		if (oData.yearToMonth > 12)  {
			var span = $('body').find('span.yearCount');
			var spanHtml = $(span).html();
			$(span).html(spanHtml + " by Month");
		}
   },
	
   showMonths : function (sChannelId, sEventId, oData)  {
	   this.loadData(sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext, oData.yearToMonth);
   },
	
	setSelection : function(year2) {
	    if (year2) {
	       var model = this.getView().getModel();
	 	   var series = model.getProperty("/years");
	 	   var newCount = 0;
	 	   for (var elem=0; elem < series.length; elem++)
	 	   {
	            if (series[elem].year >= year2.from && series[elem].year <= year2.to)
	         	   newCount += series[elem].count;
	 	   }
	 	   model.setProperty("/count",newCount);
	 	   model.setProperty("/minSelected",year2.from);
	 	   model.setProperty("/maxSelected",year2.to);
	 	   model.updateBindings(true);
		}
	},
	calcModel : function() {
		var model = this.getView().getModel();
		var series = model.getProperty("/years");
		var newCount = 0;
		var minYear = 9999;
		var maxYear = 0;
		var labels = []; 
		for (var elem=0; elem < series.length; elem++)
		{
			newCount += series[elem].count;
			if (series[elem].year < minYear)
				minYear = series[elem].year;
			if (series[elem].year > maxYear)
				maxYear = series[elem].year;
			labels.push("");
		}
		/**
		 * add artificial "empty" years for the UI:
		 */
		var range = maxYear - minYear +1;		
		if (range < 4)
		{
			// add artificial years ;o/
			var diff = 5;
			if (maxYear < 2013)
			{
				if (maxYear + 2 <= 2013)
					diff = 2;
				else
					diff = 1;
				maxYear += diff;
				minYear -= 5-diff;
			}
			else
				minYear -= diff;
			labels.push("","","","","","");
		}
		// <end>
		
		if (minYear == 1){
			labels[0] = "Jan";
		} else if  (minYear == 2){
			labels[0] = "Feb";
		} else if  (minYear == 3){
			labels[0] = "Mar";
		} else if  (minYear == 4){
			labels[0] = "Apr";
		} else if  (minYear == 5){
			labels[0] = "May";
		} else if  (minYear == 6){
			labels[0] = "Jun";
		} else if  (minYear == 7){
			labels[0] = "Jul";
		} else if  (minYear == 8){
			labels[0] = "Aug";
		} else if  (minYear == 9){
			labels[0] = "Sep";
		} else if  (minYear == 10){
			labels[0] = "Oct";
		} else if  (minYear == 11){
			labels[0] = "Nov";
		} else if  (minYear == 12){
			labels[0] = "Dec";
		} else if  (minYear <= 0){
			labels[0] = "Jan";
		} else  {
			labels[0] = minYear;
		}
		
		if (maxYear == 2){
			labels[labels.length-1] = "Feb";
		} else if (maxYear == 3){
			labels[labels.length-1] = "Mar";
		} else if (maxYear == 4){
			labels[labels.length-1] = "Apr";
		} else if (maxYear == 5){
			labels[labels.length-1] = "May";
		} else if (maxYear == 6){
			labels[labels.length-1] = "Jun";
		} else if (maxYear == 7){
			labels[labels.length-1] = "Jul";
		} else if (maxYear == 8){
			labels[labels.length-1] = "Aug";
		} else if (maxYear == 9){
			labels[labels.length-1] = "Sep";
		} else if (maxYear == 10){
			labels[labels.length-1] = "Oct";
		} else if (maxYear == 11){
			labels[labels.length-1] = "Nov";
		} else if (maxYear == 12){
			labels[labels.length-1] = "Dec";
		} else if  (maxYear <= 0){
			labels[labels.length-1] = "Dec";
		} else {
			labels[labels.length-1] = maxYear;
		}

		model.setProperty("/count",newCount);
		model.setProperty("/minYear",minYear);
		model.setProperty("/maxYear",maxYear);
		model.setProperty("/labels",labels);
		model.setProperty("/minSelected",minYear);
		model.setProperty("/maxSelected",maxYear);
		model.setProperty("/yearDistance", ((maxYear - minYear) > 4 ? 4 : (maxYear-minYear > 1) ? (maxYear-minYear) : 1));
		model.setProperty("/series",series);
	},
	
	loadData : function(oData, yearMonth) {
		oData.yearToMonth = yearMonth;
		
	   // month view
	   if (yearMonth && yearMonth > 12)  {
			this.getView().getModel().fetchData("/smartnavxs/year/byKeywordMonthSlider.xsjs",
					{
						"year": yearMonth,
						"search": oData.searchTerm,
						"publishers": oData.filters.publishers,
						"subjects": oData.filters.subjects,
						"persons": oData.filters.persons,
						"orgs": oData.filters.orgs,
						"localities": oData.filters.localities
					});
	   }
	   // year view
	   else  {
			this.getView().getModel().fetchData("/smartnavxs/year/byKeywordsForSlider.xsjs",
					{
						"search": oData.searchTerm,
						"publishers": oData.filters.publishers,
						"subjects": oData.filters.subjects,
						"persons": oData.filters.persons,
						"orgs": oData.filters.orgs,
						"localities": oData.filters.localities
					});
			
			oData.yearToMonth = 0;
	   }
	},
	
   changeYears : function(start, end) {
	   	var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
  		if (!viewContext.filters.year) 
  			viewContext.filters.year = {from:start, to:end};
  		else {
  			viewContext.filters.year.from = start;
  			viewContext.filters.year.to = end;
  		}

        // avoid self notification
  		
        sap.ui.getCore().getEventBus()
            .unsubscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this)
         //   .publish(constants.events.UPDATE_VIEW, viewContext);
        sap.ui.getCore().getEventBus()
            .subscribe(constants.events.UPDATE_VIEW, this.onUpdateView, this);
        this.setSelection(viewContext.filters.year2);
        
   },
   
   changeLiveYears : function(start, end) {
	   this.setSelection({from: start, to: end});
   },
	
	onExit : function() {
		this.setInactive();
		this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
	}   
   
});
