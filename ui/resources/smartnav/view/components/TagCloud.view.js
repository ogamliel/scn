sap.ui.jsview("view.components.TagCloud", {

	getControllerName : function() {
		return "view.components.TagCloud";
	},

	createContent : function(oController) {
		var html = new sap.ui.core.HTML("tagcloud", { width: "100%;", content : "<div class='tagcloud'/>" });
		html.addStyleClass("tagcloud");
		return html;
	}
});