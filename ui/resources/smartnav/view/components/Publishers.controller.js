jQuery.sap.require("view.components.AbstractFilterController");
view.components.AbstractFilterController.extend("view.components.Publishers", {
	path : "publishers",
	attrName : "publisher",
	restCall: "/smartnavxs/publisher/search.xsjs"
});
