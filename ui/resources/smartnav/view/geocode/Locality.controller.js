sap.ui.controller("view.geocode.Locality", {
	
	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		var locality = this.getView().getViewData().locality;
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/geocode/locality.xsjs", { locality : locality }, true, "POST");
	},
	
	onExit : function() {
		
	},
	
	onDataLoaded : function(oEvent)  {
		var model = this.getView().getModel();
		//var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		
		var data = $(model.getData().data);
		if (data[0])  {
			var lat = data[0].lat;
			var long = data[0].long;
			var locality = this.getView().getViewData().locality;
			
			var oLatlng = new google.maps.LatLng(lat,long);
			
			if (!this.initialized) {  
				this.initialized = true;  
		        this.geocoder = new google.maps.Geocoder();  
		        var mapOptions = {  
		        		center: oLatlng,  
		                zoom: 8,  
		                mapTypeId: google.maps.MapTypeId.ROADMAP  
		        };  
		        this.map = new google.maps.Map($(".map").get(0), mapOptions); 

		        var marker = new google.maps.Marker({
		            position: oLatlng,
		            title: locality
		        });
		        marker.setMap(this.map);
			}
		}
		else  {
			$(".map").html("No Geocode Data Available.");
		}
	}
	
	//onAfterRendering : function()  {
	//}
	
});	