sap.ui.jsview("view.geocode.Locality", {

	getControllerName : function() {
		return "view.geocode.Locality";
	},

/*	createContent : function(oController) {
		var htmlTemplate = sap.ui.view({
			id : "idLocalityTemplate",
			viewName : "smartnav.geocode.LocalityTemplate",
			type : sap.ui.core.mvc.ViewType.HTML
		});
		return htmlTemplate;
	}
*/
	createContent : function(oController) {
		var html = new sap.ui.core.HTML( {
			content:
				"<div id='map_canvas' class='map' style='width: 500px; height: 400px;'></div>"
		});
		return html;
	}
	
});
