sap.ui.jsview("view.SearchTermInput", {

	getControllerName : function() {
		return "view.SearchTermInput";
	},

	createContent : function(oController) {
		var searchTermInput = new sap.ui.commons.SearchField({
			width : '100%',
			searchProvider : new sap.ui.core.search.OpenSearchProvider({
				suggestType : "json",
				suggestUrl : "/smartnavxs/document/suggest.xsjs?q={searchTerms}",
				icon : "images/icons/txtonly_16x16.ico"
			}),
		});
		searchTermInput.bindProperty("value", "searchModel>/viewContext/searchTerm");
		searchTermInput.attachSearch(oController.searchTermChange, searchTermInput);
		return searchTermInput;
	}
});