sap.ui.core.Control.extend("view.ToolbarPanel", {
	metadata : {
		interfaces : [ "sap.ui.commons.ToolbarItem" ],
		aggregations : {
			"content" : {
				type : "sap.ui.core.Control",
				multiple : false
			},
		}
	},

	renderer : function(rm, ctrl) {
		rm.write("<span");
		rm.writeControlData(ctrl);
		rm.writeAttribute("class", "ToolBarPanelLayout");
		rm.write(">");
		rm.renderControl(ctrl.getContent());
		rm.write("</span>");
	},
});
