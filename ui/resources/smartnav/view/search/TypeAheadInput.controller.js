sap.ui.controller("view.search.TypeAheadInput", {

	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		model.setData({
			liveSearchTerm : "",
			liveSearchTimeout : null
		});
		this.getView().setModel(model);
		//sap.ui.getCore().getEventBus().subscribe(constants.events.CHANGE_VIEW, this.onChangeView, this);
	},
	
	onExit: function() {
		//sap.ui.getCore().getEventBus().unsubscribe(constants.events.CHANGE_VIEW, this.onChangeView, this);
	},
	
	onAfterRendering : function() {
		// attach typeahead.js library to text input
		$('.typeahead').typeahead({
		  name: 'searchterms',
		  remote: {
			  url: '/smartnavxs/document/typeahead.xsjs?q=%QUERY',
			  filter: function(result) {
				  return result[1]
			  }
		  },
		  limit: 10,
		  cache: true
		});
		
		// add UI5 styles to hidden text input
		$(".tt-hint").addClass("sapUiTf").addClass("sapUiTfBack").addClass("sapUiTfBrd").addClass("sapUiTfStd");
	},

	doSearch : function(eventData)
	{
		var model = this.getModel();
		var liveval = model.liveSearchTerm;
		var val = eventData.getParameter("newValue");
		model.liveSearchTerm = val;
		setTimeout(function() {
			sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST, {
				liveSearchTerm: liveval,
				searchTerm: val,
				filters: sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext/filters")
			});
		}, 1);
		// remove focus from inputfield (collapses suggestions)
		eventData.getSource().getDomRef().blur();
	},
	
	doLiveSearch: function(eventData) {
		var model = this.getModel();
		var val = eventData.getParameter("liveValue");
		if (model.liveSearchTerm == val) {
			return;
		}
		model.liveSearchTerm = val;
		clearTimeout(model.liveSearchTimeout);
		
		// do not trigger live search when in PDF view
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		if (viewContext.viewType == constants.viewtypes.TEXT_VIEW) {
			return;
		}
		
		model.liveSearchTimeout = setTimeout(function() {
			model.liveSearchTimeout = null;
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW,
			{
				isLiveSearch: true,
				searchTerm: model.liveSearchTerm,
				filters: viewContext.filters
			});
		}, constants.resultlist.LIVE_SEARCH_TIMER);
	}
	
});	