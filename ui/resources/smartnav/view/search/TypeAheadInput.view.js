sap.ui.jsview("view.search.TypeAheadInput", {

	getControllerName : function() {
		return "view.search.TypeAheadInput";
	},

	createContent : function(oController) {
		var text = new sap.ui.commons.TextField("idTypeAheadTextField", {
				width : '100%', 
		});
		text.addStyleClass("typeahead");
		text.bindValue("searchModel>/viewContext/searchTerm", function(data)
				{
					var myValue = text.getInputDomRef();
					if(myValue)
					{
						$.data(myValue, "ttView").setQuery(data);
					}
					return data;
				});
		text.attachChange(oController.doSearch, this);
		text.attachLiveChange(oController.doLiveSearch, this);
		
		var self = this;
		var button = new sap.ui.commons.Button({
	        icon : "images/search_springer.png",
	        press : function() {
	        	var model = self.getModel();
	        	var liveval = model.liveSearchTerm;
	        	var val = text.getValue();
	        	sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST, {
					liveSearchTerm: liveval,
					searchTerm: val,
					filters: sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext/filters")
				});
	        }
		});

		var layout = new sap.ui.commons.layout.MatrixLayout({
			width: "100%",
			columns: 3,
			widths : [ "400px", "50px", "100px"  ]
		});
		layout.createRow(text, button);
		
		$(document).ready(function(){
			$('#idTypeAheadTextField').attr("placeholder", "Search");

			$('#idTypeAheadTextField').keypress(function(event) {
				//on enter press
				
				var src = event.target || event.srcElement;
				
				if (event.keyCode == 13)
					src.blur();
			});		
		});		

		return layout;
	}
});