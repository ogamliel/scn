sap.ui.controller("view.SearchTermInput", {

	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		model.setData({
			liveSearchTerm : "",
			liveSearchTimeout : null
		});
		this.getView().setModel(model);
		
		sap.ui.getCore().getEventBus().subscribe(constants.events.CHANGE_VIEW, this.onChangeView, this);
		
		this.attachLiveSearchEvents();
	},
	
	onExit: function() {
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.CHANGE_VIEW, this.onChangeView, this);
	},
	
	onChangeView: function(sChannelId, sEventId, oData) {
		if(oData.viewType == constants.viewtypes.TEXT_VIEW)
		{
			this.detachLiveSearchEvents();
		} else
		{
			this.attachLiveSearchEvents();
		}
	},
	
	attachLiveSearchEvents : function() {
		var searchTermInput = this.getView().findElements(false)[0];
		searchTermInput.attachBrowserEvent("focusout", this.searchTermChange);
		searchTermInput.attachBrowserEvent("keyup", this.doLiveSearch);
	},
	
	detachLiveSearchEvents : function() {
		var searchTermInput = this.getView().findElements(false)[0];
		searchTermInput.detachBrowserEvent("focusout", this.searchTermChange);
		searchTermInput.detachBrowserEvent("keyup", this.doLiveSearch);
	},
	
	/* functions with searchTermInput base */ 
	searchTermChange : function(eventData)
	{
		var model = this.getParent().getModel();
		var liveval = model.liveSearchTerm;
		var val = this.getDomRef().lastChild.lastChild.value;
		model.liveSearchTerm = val;
		setTimeout(function() {
			sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST, {
				liveSearchTerm: liveval,
				searchTerm: val,
				filters: sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext/filters")
			});
		}, 1);
	},
	
	doLiveSearch: function(eventData) {
		var model = this.getParent().getModel();
		// this.getValue() does not contain the latest input!!
		var val = this.getDomRef().lastChild.lastChild.value;
		if(model.liveSearchTerm == val) return;
		
		model.liveSearchTerm = val;
		clearTimeout(model.liveSearchTimeout);
		model.liveSearchTimeout = setTimeout(function() {
			model.liveSearchTimeout = null;
			sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW,
			{
				isLiveSearch: true,
				searchTerm: model.liveSearchTerm,
				filters: sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext/filters")
			});
		}, constants.resultlist.LIVE_SEARCH_TIMER);
	},
});	