sap.ui.controller("view.Browser", {
	onInit : function()  {
		var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
		/*$.ajax({
	        url : "/smartnavxs/map/GetMapboxServer.xsjs",
	        async: false,
	        success : function (data) {
	        	globalModel.setProperty("/viewContext/mapboxServer", data.url);
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            console.log(xhr.status);
	            console.log(thrownError);
	          }
	    });*/
		$.ajax({
	        url : "/smartnavxs/map/GetMapTileServer.xsjs",
	        async: false,
	        success : function (data) {
	        	globalModel.setProperty("/viewContext/mapTileServerUrl", data.url);
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            console.log(xhr.status);
	            console.log(thrownError);
	          }
	    });
	    
		$.ajax({
			url : "/smartnavxs/textanalysis/getTAServiceProps.xsjs",
			success : function (data)  {
				globalModel.setProperty("/viewContext/xsClassicHost", data.host);
				globalModel.setProperty("/viewContext/xsClassicPort", data.port);
			},
			error : function (xhr, ajaxOptions, thrownError)  {
				console.log(xhr.status);
				console.log(thrownError);
			}
		}); 

		$.ajax({
			url : "/smartnavxs/translate/getGoogleTranslateKey.xsjs",
			success : function (data)  {
				globalModel.setProperty("/viewContext/translateKey", data.key);
			},
			error : function (xhr, ajaxOptions, thrownError)  {
				console.log(xhr.status);
				console.log(thrownError);
			}
		});    

	},
	
	getArticleYearRange : function() {
		var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
		
		// find the most recent article year and store it in the model.  Will be used in the 
		// document search (initial load)
		$.ajax({
	        url : "/smartnavxs/document/articleYearRange.xsjs?",
	        async: false,
	        success : function (data) {
	        	globalModel.setProperty("/viewContext/maxArticleYear", data.max);
	        	globalModel.setProperty("/viewContext/minArticleYear", data.min);
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            console.log(xhr.status);
	            console.log(thrownError);
	          }
	    });
	},
	
});