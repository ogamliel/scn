sap.ui.jsview("view.ComponentPanel", {
	
	getControllerName : function() {
		return "view.ComponentPanel";
	},

	createContent : function(oController) {
		this.addStyleClass("componentPanel");
		
		this.bindAggregation("content", {
			path: "components", 
			factory: function(sId, oContext) {
				var name = oContext.getProperty("name");
				var component = null;
				if (name === "Placeholder") {
					component = new view.components.Placeholder({
						popupPosition : oContext.getProperty("popupPosition")
					});
					component.bindElement("/"+oContext.sPath.split("/")[1]);
					component.bindAggregation("components", {
						path: "components", 
						factory: function(sId, oContext) {
							var name = oContext.getProperty("name");
							
							return new sap.ui.commons.Button({
								text: name,
								press: function(event) {
									oController.setActive(name);
								}
							});
						},
						filters: [new sap.ui.model.Filter("active", sap.ui.model.FilterOperator.EQ, false)]
					});
				} else {
					var title = name.split(".")[name.split(".").length-1];

					if (name.indexOf(".") == -1) {
						name = "components." + name;
					}

					component = sap.ui.view({
						id : title,
						viewName : name,
						type : sap.ui.core.mvc.ViewType.JS
					});
					
					if (name.indexOf("components") == 5) { // view.components
						var oIcon = new sap.ui.commons.Link({ text:"", enabled: true, visible:true });
						var status = oContext.getProperty("active") && true || false;
						var statustext = status && "active" || "deactive";
						oIcon.addStyleClass("componentToggleButton icon "+ statustext);
						
						var oTitle = new sap.ui.commons.TextView({text: oContext.getProperty("title")})
						
						var oHeader = new sap.ui.commons.layout.VerticalLayout({
							id : title + "Header",
							width : "100%",
							content : [oIcon, oTitle]
						}).addStyleClass("componentTitle backgroundGradient");
						
						oHeader.attachBrowserEvent("click", function(event){
							oController.setStatus(title, oContext.getProperty("active") ? false : true);
							$("#"+oIcon.sId).removeClass("deactive").removeClass("active").addClass(oContext.getProperty("active") ? "active" : "deactive");
							$("#"+title).slideToggle("slow", function(event){
								if (oContext.getProperty("active"))
									component.oController.setActive();
								else
									component.oController.setInactive();
							});
						});
						
						if (!status) {
							var cloneOnAfterRendering = component.onAfterRendering;
							component.onAfterRendering = function() {
								cloneOnAfterRendering.call(component);
								$("#"+title).hide();
							}
							component.oController.setInactive();
						} else {
							component.oController.setActive();
						}
						
						var componentBox = new sap.ui.commons.layout.VerticalLayout({
							id : title + "Panel",
							width : "100%",
							content : [oHeader, component]
						}).addStyleClass("component");
						
						return componentBox;
					}
				}

				return component;
			},
			sorter: new sap.ui.model.Sorter("order", false)
		});
	}
});