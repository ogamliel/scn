sap.ui.controller("view.references.PersonImage", {
	name : "",
	url :  "",
	
	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/references/personImage.xsjs");
	},
	
	//call : function() {},
	
	onDataLoaded : function(oEvent)  {
		var model = this.getView().getModel();
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		
		var results = $(model.getData().data);
		//$.each(results, function(i, item)  {
		//	console.log('name=' + item.name + ' url=' + item.imageUrl);
		//});
		$("#personPhoto").attr('src', results[0].imageUrl);
		//$("#source").attr('href', results[0].source);
		$("#source").attr('href', results[0].source);
		$("#source").html('Rajya Sabha');
		this.name = results[0].name;
	},
	
	personName : function(event)  {
		return this.name;
	}
});	