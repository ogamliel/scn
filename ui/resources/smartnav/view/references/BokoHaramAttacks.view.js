jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.BokoHaramAttacks", {

	getControllerName : function() {
		return "view.references.BokoHaramAttacks";
	},

	createContent : function(oController) {	
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [
		  {axis : 1, name: 'Attack Type', value: '{attack}'},
		  {axis : 2, name: 'Weapon Used', value: '{weapon}'}
		  ],
		  measures : [
		  { name : 'Total', value : '{count}'}
		  ],
		  data : {
			  path : "/data1"
		  }
		});

		var oChart = new sap.viz.ui5.Column({
		     width : "90%",
             height : "600px",
             plotArea : {
            	 'colorPalette' : d3.scale.category20().range()
             },
             title : {
               visible : true,
               text : 'Boko Haram Attacks 2009-2012'
             },
            dataset : oDataset
           });
		
	
		oChart.setModel(this.getModel());
		oChart.rerender();
		
		return oChart;
		
	}

});