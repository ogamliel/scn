jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.ReportViewer", {

	getControllerName : function() {
		return "view.references.ReportViewer";
	},

	createContent : function(oController) {
		var url = this.getViewData().url;
		
		var html = new sap.ui.core.HTML( {
			content:
				"<iframe src='" + url + "' width='1000' height='600'>"
		});
		return html;
	}

});