sap.ui.controller("view.references.TradeBalance", {

	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/references/tradeBalance.xsjs");
	},
	
	call : function() {},
	
/*	onDataLoaded : function(oEvent)  {
		var model = this.getView().getModel();
		//var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		
		var data = $(model.getData().data1);
		var country = data[0].country;
		var total = data[0].total;
		var balance = data[0].balance;
		
		//var locality = this.getView().getViewData().locality;
		
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [
		              {axis : 1, name: 'Country', value: country }
		              ],
		              measures : [
                      { name : 'Trade Sum', value : total},
                      { name : 'Trade Balance', value : balance }
                      ],
                      data : {
                    	  path : "/data1"
                      }
		});


		var oColumn = new sap.viz.ui5.Column({
			width : "90%",
			height : "400px",
			plotArea : {
				'colorPalette' : d3.scale.category20().range()
			},
			title : {
				visible : true,
				text : '2013 India Trade Balance by Country'
			},
			dataset : oDataset
       });
		oColumn.setModel(model);
		console.log($("#chart_canvas").get(0));
		oColumn.placeAt($("#chart_canvas").get(0));
	},
*/
});	