jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.BokoHaramTargets", {

	getControllerName : function() {
		return "view.references.BokoHaramTargets";
	},

	createContent : function(oController) {	
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [
		  {axis : 1, name: 'Target Type', value: '{target}'}
		  ],
		  measures : [
		  { name : 'Total', value : '{count}'}
		  ],
		  data : {
			  path : "/data1"
		  }
		});

		var oChart = new sap.viz.ui5.Pie({
		     width : "90%",
             height : "500px",
             plotArea : {
            	 'colorPalette' : d3.scale.category20().range()
             },
             title : {
               visible : true,
               text : 'Boko Haram Targets 2009-2012'
             },
            dataset : oDataset
           });
		
	
		oChart.setModel(this.getModel());
		oChart.rerender();
		
		return oChart;
		
	}

});