sap.ui.controller("view.references.BokoHaramTargets", {

	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/references/bokoHaramTargets.xsjs");
	},
	
	call : function() {},
	
});	