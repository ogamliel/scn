sap.ui.controller("view.references.ImportsByRegion", {

	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/references/importsByRegion.xsjs");
	},
	
	call : function() {}
});	