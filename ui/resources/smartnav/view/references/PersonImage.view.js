sap.ui.jsview("view.references.PersonImage", {

	getControllerName : function() {
		return "view.references.PersonImage";
	},

	createContent : function(oController) {
		var htmlTemplate = sap.ui.view({
			id : "idPersonPhotoTemplate",
			viewName : "view.references.PersonPhotoTemplate",
			type : sap.ui.core.mvc.ViewType.HTML
		});
		return htmlTemplate;
	}
});