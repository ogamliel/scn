jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.NigeriaEmployment", {

	getControllerName : function() {
		return "view.references.NigeriaEmployment";
	},

	createContent : function(oController) {	
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [
		  {axis : 1, name: 'Year', value: '{year}'},
		  {axis : 2, name: 'Gender', value: '{gender}'}
		  ],
		  measures : [
		  { name : 'Total', value : '{count}'}
		  ],
		  data : {
			  path : "/data1"
		  }
		});

		var oChart = new sap.viz.ui5.StackedColumn100({
		     width : "90%",
             height : "440px",
             plotArea : {
            	 'colorPalette' : d3.scale.category20().range()
             },
             title : {
               visible : true,
               text : '2002-2012 Employment to Population Ratio, ages 15-24'
             },
            dataset : oDataset
           });
		
	
		oChart.setModel(this.getModel());
		oChart.rerender();
		
		return oChart;
		
	}

});