jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.TradeBalance", {

	getControllerName : function() {
		return "view.references.TradeBalance";
	},

/*	createContent : function(oController) {		
		var html = new sap.ui.core.HTML("chart_canvas", {
			content:
				"<div id='chart_canvas' style='width: 500px; height: 400px;'>TEST</div>"
		});
		return html;
	}
*/
	createContent : function(oController) {	
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [
		  {axis : 1, name: 'Country', value: '{country}'}
		  ],
		  measures : [
		  { name : 'Trade Sum', value : '{total}'},
		  { name : 'Trade Balance', value : '{balance}'}
		  ],
		  data : {
			  path : "/data1"
		  }
		});

		var oColumn = new sap.viz.ui5.Column({
		     width : "90%",
             height : "400px",
             plotArea : {
            	 'colorPalette' : d3.scale.category20().range()
             },
             title : {
               visible : true,
               text : '2013 India Trade Balance by Country'
             },
            dataset : oDataset
           });
		
	
		oColumn.setModel(this.getModel());
		oColumn.rerender();
		
		return oColumn;
		
	}

});