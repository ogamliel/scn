jQuery.sap.require("sap.viz.ui5.Column");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.jsview("view.references.ImportsByRegion", {

	getControllerName : function() {
		return "view.references.ImportsByRegion";
	},

	createContent : function(oController) {
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [
			              {axis : 1, name: 'Region', value: '{region}'}
			              ],
			              measures : [
                          { name : 'Import', value : '{imp}'},
                          { name : 'Export', value : '{exp}'}
                          ],
                          data : {
                        	  path : "/data1"
                          }
		});


/*		var oColumn = new sap.viz.ui5.Column({
		     width : "100%",
             height : "300px",
             plotArea : {
            	 'colorPalette' : d3.scale.category20().range()
             },
             title : {
               visible : true,
               text : '2013 India Imports/Exports by Region'
             },
             dataset : oDataset
           });
		
		oColumn.setModel(this.getModel());
		
		return oColumn;*/
		
		var oChart = new sap.viz.ui5.Line({  
		    width : "90%",  
		    height : "400px",  
		    title : {  
		        visible : true,  
		        text : '2013 India Imports/Exports by Region'  
		    },
		    legend: {  
		        visible: true  
		    },  
		    dataset : oDataset  
		});  
		oChart.setModel(this.getModel());  
		return oChart;
	}
});