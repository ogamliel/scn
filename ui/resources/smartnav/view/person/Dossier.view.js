sap.ui.jsview("xsjs.person.Dossier", {

	getControllerName : function() {
		return "xsjs.person.Dossier";
	},

/*	createContent : function(oController) {
		var htmlTemplate = sap.ui.view({
			id : "idLocalityTemplate",
			viewName : "smartnav.geocode.LocalityTemplate",
			type : sap.ui.core.mvc.ViewType.HTML
		});
		return htmlTemplate;
	}
*/
	createContent : function(oController) {
		var html = new sap.ui.core.HTML({
			content:
				"<div class='dossier'></div>"
		});
		return html;
	}
	
});
