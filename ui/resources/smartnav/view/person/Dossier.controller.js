sap.ui.controller("view.person.Dossier", {
	
	onInit : function() {
		var model = new sap.ui.model.json.JSONModel();
		this.getView().setModel(model);
		var fullname = this.getView().getViewData().name;
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.fetchData("/smartnavxs/person/dossier.xsjs", { name : fullname }, true, "POST");
	},
	
	onExit : function() {
		
	},
	
	onDataLoaded : function(oEvent)  {
		var model = this.getView().getModel();
		
		var data = $(model.getData().data);
		if (data[0])  {
			var title = data[0].title;
			var title2 = data[0].title2;
			var name = data[0].name;
			var photo = data[0].photo;
			var source = data[0].source;
			
			var table = "<table style=\"width:600px; height=300px;\">" +
							"<tr valign=\"top\">" +
								"<td style=\"vertical-align: top;\" width=\"55%\">" +
									"<table cellspacing=\"10px\" cellpadding=\"10\" >" +
									"<tr><td style=\"font-weight: bold; padding: 5px 5px;\" width=\"50px\" valign=\"top\">Name:</td><td valign=\"top\">" + name + "</td></tr>" +
									"<tr><td style=\"font-weight: bold; padding: 5px 5px;\" valign=\"top\">Title:</td><td valign=\"top\">" + title + "</td></tr>";
									if (title2)  {
										table += "<tr><td style=\"font-weight: bold; padding: 5px 5px;\" valign=\"top\"></td><td valign=\"top\">" + title2 + "</td></tr>";
									}
								table += "<tr><td style=\"font-weight: bold; padding: 5px 5px; vertical-align: top;\" >Source:</td><td valign=\"top\">" + source + "</td></tr>" +
								"</table>" +
								"</td>" +
								"<td valign=\"top\" width=\"250px\"><img src=\"" + photo +"\"></td>" +
							"</tr>" +
						"</table>";						

/*			var table = "<img src=\"" + photo +"\">" +
					"<table cellspacing=\"10px\" cellpadding=\"10px\" >" +
					"<tr><td style=\"font-weight: bold;\" width=\"100px\">Name:</td><td>" + name + "</td></tr>" +
					"<tr><td style=\"font-weight: bold;\">Title:</td><td>" + title + "</td></tr>" +
					"<tr><td style=\"font-weight: bold;\">Photo Source:</td><td>" + source + "</td></tr>" +
					"</table>";
*/
			
			$(".dossier").html(table);
		}
		else  {
			$(".dossier").html("Dossier Not Available.");
		}

	},
	
	onAfterRendering : function()  {
	}
	
});	