sap.ui.core.Control.extend("view.ReferenceItem", {
	metadata : {
		properties : {
			xsFile : "string",
			viewname : "string",
			sourceUrl : "string",
			source : "string"
		},
		aggregations : {
			"title" : {
				type : "sap.ui.core.Control",
				multiple : false
			},
			"image" : {
				type : "sap.ui.commons.Image",
				multiple : false
			},
			//"source" : {
			//	type : "sap.ui.commons.Link",
			//	multiple : false
			//}
		}
	},

	renderer : function(rm, ctrl) {
		rm.writePublicationAttribute = function(attribute, cssclass) {
			if (attribute) {
				return this.write("<span")
				.writeAttribute("class", cssclass || "")
				.write(">")
				.write(attribute)
				.write("</span>");
			} else {
				return this;
			}
		};
		var titletext = ctrl.getTitle().getText();
		rm.write("<section");
		rm.writeControlData(ctrl);
		rm.writeAttribute("class", "PublicationItemLayout");
		rm.writeAttribute("title", titletext);
		rm.write("\"><div");
		rm.writeAttribute("class", "PublicationItemLayoutInner");
		rm.write(">");
		rm.write("<header");
		rm.writeAttribute("class", "PublicationItemLayoutTitle");
		rm.write(">");
		rm.renderControl(ctrl.getImage());
		rm.write("<h2>");
		rm.renderControl(ctrl.getTitle());
		rm.write("</h2>");
		rm.write("</header>");	
		
		rm.write("<span");
		rm.writeAttribute("class", "PublicationSource");
		rm.write(">");
		rm.writePublicationAttribute(ctrl.getSource());
		//rm.renderControl(ctrl.getSource());
		rm.write("</span></p>");
		rm.write("</div>");
		rm.write("</section>");
	},
	
	onBeforeRendering : function() {
		if (this.resizeTimer) {
			clearTimeout(this.resizeTimer);
			this.resizeTimer = null;
		}
	},

	onAfterRendering : function() {
		var $This = this.$();
		if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
			this._resize();
		} else {
			$This.addClass("PublicationItemLayoutSmall");
		}
	},

	_resize : function() {
		if (!this.getDomRef()) {
			return;
		}
		var $This = this.$();
		if ($This.outerWidth() >= 440) {
			$This.removeClass("PublicationItemLayoutSmall").addClass(
					"PublicationItemLayoutLarge");
		} else {
			$This.removeClass("PublicationItemLayoutLarge").addClass(
					"PublicationItemLayoutSmall");
		}
		setTimeout(jQuery.proxy(this._resize, this), 300);
	}
});
