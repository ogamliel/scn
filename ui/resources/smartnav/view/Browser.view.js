sap.ui.jsview("view.Browser", {

	getControllerName : function() {
		return "view.Browser";
	},

	createContent : function(oController) {

		var typeAheadInput = sap.ui.view({
			id : "idTypeAheadInput",
			viewName : "view.search.TypeAheadInput",
			type : sap.ui.core.mvc.ViewType.JS
		});

		typeAheadInput.placeAt("uiTop");

		var oComponentPanel = sap.ui.view({
			id : "idComponentPanelMain",
			viewName : "view.ComponentPanel",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : {
                "SEARCH_VIEW": {"components" : [{name:"view.table.ResultTable2", active:true, order:1}]},
                "TEXT_VIEW": {"components" : [{name:"view.viewer.TextViewer", active:true, order:1}]},
                "TRANSLATE_VIEW": {"components" : [{name:"view.viewer.TranslateViewer", active:true, order:1}]}
            }
		});
    	oComponentPanel.placeAt("uiMain");
    	
    	var oComponentPanelLeft = sap.ui.view({
			id : "idComponentPanelLeft",
			viewName : "view.ComponentPanel",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : {
				"SEARCH_VIEW" : {
					"components" : [{
						name : "view.components.Date",
						title: "Publication Date",
						active : true,
						order : 1
					},  {
						name : "view.components.Map",
						title: "Map",
						active : true,
						order: 2
					}, 
					/*{ 
						name : "Year",
						title: "Articles by Publication Year",
						active : false,
						order : 1 
					}, {
						name : "Subjects",
						title: "Source Publication",
						active : false,
						order : 1
					},*/{ 
						name : "view.components.DocumentTypes",
						title: "Document Type",
						active : false,
						order : 3
					},{ 
						name : "view.components.Publishers",
						title: "Author",
						active : false,
						order : 5
					},{
						name : "view.components.EntityPerson",
						title: "Person",
						active : false,
						order : 6
					}, {
						name : "view.components.EntityOrg",
						title: "Organization",
						active : false,
						order : 7
					}, {
						name : "view.components.EntityLocality",
						title: "Locality",
						active : false,
						order : 8
					}, {
						name : "view.components.Sentiment",
						title: "Sentiment",
						active : true,
						order : 9
					},{
						name : "view.components.TagCloud",
						title: "Tag Cloud",
						active : true,
						order : 10
					}/*,{
						name : "view.components.FileUpload",
						title: "Document Upload",
						active : true,
						order : 11
					}*/]
				},
				"TEXT_VIEW" : {
					"components" : [{
						name : "view.components.Entities",
						title: "Entities",
						active : true,
						order : 1
					}, /*{
						name : "References",
						title: "References",
						active : false,
						order : 1
					},*/ {
						name : "view.components.Recommendations",
						title: "Recommendations",
						active : false,
						order : 2
					}, {
						name : "view.components.Translations",
						title : "Translations",
						active : true,
						order : 3
					}, {
						name : "view.components.Associated",
						title: "Associated",
						active : false,
						order : 4
					}]
					/*, {
						name : "Popularity",
						title: "Entity Matches by Month",
						active : false,
						order : 1
					}*/
				},
				"TRANSLATE_VIEW" : {
					"components" : [{
						name : "view.components.TranslateEntities",
						title: "Entities",
						active : true,
						order : 1
					}]
				}				
			}
		});
		oComponentPanelLeft.placeAt("uiLeft");

		var awesomeBar = sap.ui.view({
			id : "idAwesomeBar",
			viewName : "view.awesomebar.AwesomeBar",
			type : sap.ui.core.mvc.ViewType.JS
		});
		awesomeBar.placeAt("uiAwesomeBar");
	}
});
