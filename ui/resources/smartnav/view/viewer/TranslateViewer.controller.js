sap.ui.controller("view.viewer.TranslateViewer", {
	
	highlightPromises:[],
	isTextReady:false,

	onInit : function() {
		$("#translateProgress").css("display","inline");
			    
		var model = new sap.ui.model.json.JSONModel();
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.setData({"loadTextTimeout": null});
		this.getView().setModel(model);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TRANSLATE_LITERAL_SEARCH, this.literalSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TRANSLATE_ADD_HIGHLIGHT_LAYER, this.addHighlightLayer, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TRANSLATE_REMOVE_HIGHLIGHT_LAYER, this.removeHighlightLayer, this);

		this.highlightPromises = [];

		// remove the Translate button and add the Add to Repository button
		$("#translate").remove();
		// check if translated version exists in repository before activating save button
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		var entityid = viewContext.entityid;
		var langId = viewContext.translateLangId;

		$.ajax({
		    url: "/smartnavxs/translate/checkTranslated.xsjs",
		    type: "GET",
		    data: { entityid: entityid, langId: langId },
		    success: function (data)  {
				if (data.translated === true)  {
					$( "<div id=\"translated\"><button id=\"saveBtn\" class=\"saveBtnDisabled\" disabled>Add to Repository</button></div>" ).insertBefore( "#uiLeft" );
				}
				else  {
					$( "<div id=\"translated\"><button id=\"saveBtn\" onclick=\"sap.ui.controller('view.viewer.TranslateViewer').save();\" class=\"saveBtn\">Add to Repository</button></div>" ).insertBefore( "#uiLeft" );
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);
			    // display error msg
	    		var list = $("#list");
			    list.html("<strong>Upload Error!</strong>");
			}
		});

	},
	
	onExit : function()
	{
		$("#translated").remove();

		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TRANSLATE_LITERAL_SEARCH, this.literalSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TRANSLATE_ADD_HIGHLIGHT_LAYER, this.addHighlightLayer, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TRANSLATE_REMOVE_HIGHLIGHT_LAYER, this.removeHighlightLayer, this);

		this.highlightPromises = [];
		
		var curViewModel = this.getView().getModel();
		curViewModel.detachRequestCompleted(this.onDataLoaded, this);
		if (typeof curViewModel.oData.viewer != "undefined")
			curViewModel.oData.viewer.destroy();
		
		// clean any open callout
		for (var i = 0 ; i < openCallouts.length; i++)
		{
			openCallouts[i].stop(true,true).mouseout();
		}

	},
	
	onAfterRendering : function() {
		this.translateText(sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"), true);
	},

	translateText: function(viewContext, newTextViewer) {
		this.isTextReady = false;
		
		// clean any open callout
		if (typeof openCallouts != "undefined") {
			for (var i = 0 ; i < openCallouts.length; i++)
			{
				openCallouts[i].stop(true,true).mouseout();
			}
		}

		var articleType  = viewContext.type;
		var highlights = viewContext.entities;

		var textDomContainer = $("#tMainContainer");
		if (textDomContainer.length) {
			if (textDomContainer.css("visibility") === "hidden")
				textDomContainer.css("visibility","visible");
		}

       	var imageSrc = 'images/icons/' + articleType.toLowerCase().replace(" ", "") + '.png';

		var transTitle =  this.translate(viewContext.title, viewContext.translateLangId, viewContext.translateKey);
		var transSubtitle = this.translate(viewContext.subtitle, viewContext.translateLangId, viewContext.translateKey);
		var transText = this.translate(viewContext.contentHtml, viewContext.translateLangId, viewContext.translateKey);

		// add translated text to divs
    	$("#tArticleType").html($('<img>',{id:'tArticleType', style:'vertical-align:middle;', src: imageSrc }));
    	$("#tTitle").html(transTitle);
    	$("#tSubtitle").html(transSubtitle);
        $("#tViewer").html(transText);
        
        // remove the highlights since we will re-highlight the translated document
        for(var i=0; i<highlights.length; i++)  {
        	if (highlights[i] !== "SEARCH")  {
        		$('#tViewer').unhighlight({ className: highlights[i].toLowerCase() });
        	}
        }

		this.highlightPromises = [];
	},

	translate : function (text, lang, translateKey)  {
		// break text into chunks to prevent errors for long strings
		var chunks = this.chunkString(text);
		var translatedText = "";
		var error = false;

		for (var i=0; i<chunks.length; i++)  {
			//translatedText += chunks[i];
			if (!error)  {
				$.ajax({  
					type:"POST",
		        	headers: {
		        		"X-HTTP-Method-Override":"GET",
		        		"Content-type": "application/x-www-form-urlencoded"},
		        	url: "https://www.googleapis.com/language/translate/v2",
		        	async: false,
		        	data: { key: translateKey,
		                  //source: langId,
		                  target: lang,
		                  q: chunks[i]},
		        	success: function(result){
			            if(!result.error){
			            	translatedText += result.data.translations[0].translatedText;
			            }
		        	},
		        	error: function (xhr, ajaxOptions, thrownError)  {
		        		error = true;
		        		var resp = JSON.parse(xhr.responseText);
						var errStr = resp.error.message;
						if (resp.error.errors.length > 0)  {
							errStr += "\nReason: " + resp.error.errors[0].reason;
						}
		        		var oErrDialog = new sap.ui.commons.Dialog();
						oErrDialog.setTitle("Translation Error");
						var oText = new sap.ui.commons.TextView({text: errStr});
						oErrDialog.addContent(oText);
						oErrDialog.addButton(new sap.ui.commons.Button({text: "OK", press:function(){oErrDialog.close();}}));
						oErrDialog.open();
						
						$("#translateProgress").css("display","none");
		        	}
		    	});
			}
			else  {
				$("#translateProgress").css("display","none");
				break;
			}
		}
		//console.log(translatedText)
		return translatedText;
	},
	
	chunkString : function (input)  {
		var len = 4000;
		var curr = len;
		var prev = 0;
		var output = [];
		
		while (input[curr]) {
		    if (input[curr++] == '\n') {
		        output.push(input.substring(prev,curr));
		        prev = curr;
		        curr += len;
		    }
		}
		output.push(input.substr(prev)); 
		return output;
	},
	
	addHighlightLayer : function(sChannelId, sEventId, oData) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		var type = oData.entityType;
		var highlights = oData.highlights;
		var searchTerm = viewContext.searchTerm;
		
		// check is highlight already exists
		if(this.highlightPromises.indexOf(type) === -1)  {
			// add to array
			this.highlightPromises.push(type);

			if (type !== 'SEARCH')  {
				$.each(highlights, function(key, val){
					$('#tViewer').highlight(val.highlight, {className: type.toLowerCase()});
           	    });
			}
			else  {
				$('#tViewer').highlight(searchTerm.split(" "), {className: type.toLowerCase() });
			}
		}
	},
	
	removeHighlightLayer : function(sChannelId, sEventId, oData) {
		var type = oData.layer;
		for(var i = this.highlightPromises.length-1; i >= 0; i--) {
		    if(this.highlightPromises[i] === type) {
		    	this.highlightPromises.splice(i, 1);
		    }
		}
		$('#tViewer').unhighlight({ className: type.toLowerCase() });
	},

	literalSearch : function(sChannelId, sEventId, oData) {
		sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST, { searchTerm:oData.text});
	},
	
	save : function ()  {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		var translatedText = $("#tViewer").html();
		var translatedTitle = $("#tTitle").html();

		var params = { 
			articleDate : viewContext.articleDate,
			authors : viewContext.authors.toString(),
			source : 'Translation',
			langId : viewContext.translateLangId,
			language : viewContext.translateLang,
			title : translatedTitle,
			type : viewContext.type,
			sitetype : viewContext.sitetype,
			content : encodeURIComponent(translatedText),
			entityid : viewContext.entityid
		};
		
		$.ajax({
		    url: "/smartnavxs/article/insertDocument.xsjs",
		    async: false,
		    type: "POST",
		    data: JSON.stringify(params),
		    success: function (data)  {
				$("#saveBtn").removeClass("saveBtn").addClass("saveBtnSuccess");
			},
			error: function (xhr, ajaxOptions, thrownError) {
			    console.log(xhr.status);
			    console.log(thrownError);
			    // display error msg
	    		var list = $("#list");
			    list.html("<strong>Upload Error!</strong>");
			}
		});
		
	}

});