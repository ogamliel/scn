sap.ui.jsview("view.viewer.TextViewer", {

	getControllerName : function() {
		return "view.viewer.TextViewer";
	},

	createContent : function(oController) {
		var htmlTemplate = sap.ui.view({
			id : "idTextViewTemplate",
			viewName : "view.viewer.TextViewerTemplate",
			type : sap.ui.core.mvc.ViewType.HTML
	});
	return htmlTemplate;
	}
});