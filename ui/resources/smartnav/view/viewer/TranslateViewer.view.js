sap.ui.jsview("view.viewer.TranslateViewer", {

	getControllerName : function() {
		return "view.viewer.TranslateViewer";
	},

	createContent : function(oController) {
		var htmlTemplate = sap.ui.view({
			id : "idTranslateViewTemplate",
			viewName : "view.viewer.TranslateViewerTemplate",
			type : sap.ui.core.mvc.ViewType.HTML
	});
	return htmlTemplate;
	}
});