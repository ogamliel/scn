sap.ui.controller("view.viewer.TextViewer", {
	
	onInit : function() {		
		$( "<div id=\"translate\"><button id=\"translateBtn\" onclick=\"sap.ui.controller('view.viewer.TextViewer').translate();\" class=\"translateBtn\">Translate</button>" +
			"<select id=\"translateLang\" name=\"translateLang\" class=\"lang\">").insertBefore( "#uiLeft" );
		//	.append($("<span id=\"translateProgress\" class=\"translate-progress\"><img src=\"images/spiffygif_gray.gif\">")).insertBefore( "#uiLeft" );
			

        var model = new sap.ui.model.json.JSONModel();
		model.attachRequestCompleted(this.onDataLoaded, this);
		model.setData({"loadTextTimeout": null});
		this.getView().setModel(model);
		sap.ui.getCore().getEventBus().subscribe(constants.events.RENDER_TEXT_HIGHLIGHT, this.renderHighlight, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_CONCEPT_SEARCH, this.conceptSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.SHOW_HIGHLIGHTS_OF_TYPE, this.enableHighlightsOfType, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_LITERAL_SEARCH, this.literalSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.WIKI_SEARCH, this.wikiSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_LITERAL_SEARCH_OF_CONCEPTS, this.literalConceptSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_ADD_HIGHLIGHT_LAYER, this.addHighlightLayer, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_REMOVE_HIGHLIGHT_LAYER, this.removeHighlightLayer, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW,this.updateView, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.TEXT_READY, this.textReady, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.GEOCODE_SEARCH, this.geocodeSearch, this);
		sap.ui.getCore().getEventBus().subscribe(constants.events.PERSON_DOSSIER, this.personDossier, this);

		this.highlightPromises = [];
		
	},
	
	onExit : function()
	{
		$('#translate').remove();
		
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_READY, this.textReady, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.RENDER_TEXT_HIGHLIGHT, this.renderHighlight, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_CONCEPT_SEARCH, this.conceptSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.SHOW_HIGHLIGHTS_OF_TYPE, this.enableHighlightsOfType, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_LITERAL_SEARCH, this.literalSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.WIKI_SEARCH, this.wikiSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_LITERAL_SEARCH_OF_CONCEPTS, this.literalConceptSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_ADD_HIGHLIGHT_LAYER, this.addHighlightLayer, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.TEXT_REMOVE_HIGHLIGHT_LAYER, this.removeHighlightLayer, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW,this.updateView, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.GEOCODE_SEARCH, this.geocodeSearch, this);
		sap.ui.getCore().getEventBus().unsubscribe(constants.events.PERSON_DOSSIER, this.personDossier, this);

		
		this.highlightPromises = [];
		
		var curViewModel = this.getView().getModel();
		curViewModel.detachRequestCompleted(this.onDataLoaded, this);
		if (typeof curViewModel.oData.viewer != "undefined")
			curViewModel.oData.viewer.destroy();
		
		// clean any open callout
		for (var i = 0 ; i < openCallouts.length; i++)
		{
			openCallouts[i].stop(true,true).mouseout();
		}

	},
	
	highlightPromises:[],
	isTextReady:false,
	
	textReady: function() {
		
		this.isTextReady = true;
		for (var i = 0; i < this.highlightPromises.length; i++) {
			if (typeof this.highlightPromises[i] != "undefined")
				this.addHighlights(this.highlightPromises[i][0],this.highlightPromises[i][1],this.highlightPromises[i][2],this.highlightPromises[i][3]);
		}
		this.highlightPromises = [];
		var textViewer = this.getView().getModel().getData().viewer;
		if (typeof textViewer != "undefined"){
			textViewer.executeNextSearch();
		}
	},

	onAfterRendering : function() {
		$.ajax({
            url : "/smartnavxs/translate/languages.xsjs",
            success : function (data) {
            	for(var i=0; i<data.languages.length; i++)  {
            		var lang = data.languages[i];
					$('#translateLang').append($("<option></option>").attr("value",lang.id).text(lang.name));
            	}
            	$("#translateLang").val("en");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });

		this.loadText(sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"), true);
	},
	
	updateView : function(sChannelId, sEventId, oData) {
		if (oData.viewType != constants.viewtypes.TEXT_VIEW) {
			return;
		}
		this.loadText(oData, false);
	},

	loadText: function(viewContext, newTextViewer) {
		this.isTextReady = false;
		
		// clean any open callout
		if (typeof openCallouts != "undefined") {
			for (var i = 0 ; i < openCallouts.length; i++)
			{
				openCallouts[i].stop(true,true).mouseout();
			}
		}
		
		var pdfId 		 = viewContext.pdfId;
		var searchTerm 	 = viewContext.searchTerm;
		var articleTitle = viewContext.pdfTitle;
		var authors      = viewContext.authors;
		var articleDate  = viewContext.articleDate;
		var journal      = viewContext.journal;
		var articleType  = viewContext.type;
		var sitetype     = viewContext.sitetype;
		var language     = viewContext.language;

		var textDomContainer = $("#mainContainer");
		if (textDomContainer.length) {
			if (textDomContainer.css("visibility") === "hidden")
				textDomContainer.css("visibility","visible");
			}

		$.ajax({
            url : "/smartnavxs/document/get.xsjs?pdfid=" + pdfId,
            //dataType: "text",
            success : function (data) {
            	// maintain newlines
            	//data = data.replace(/\n/g,'<br/>');
            	var content = data.content.replace(/\n/g,'<br>');
            	content = content.replace(/\r/g, '');

            	var imageSrc = 'images/icons/' + articleType.toLowerCase().replace(" ", "") + '.png';
            	if (sitetype === 'ACLED')  {
            		imageSrc = 'images/icons/acled.png'
           	 	}
           	 									

            	$("#articleType").html($('<img>',{id:'articleType', style:'vertical-align:middle;', src: imageSrc }));
            	$(".title").html(articleTitle);
//            	$(".subtitle").html(authors + " in " + sitetype + ": " + journal + ", " + articleDate + ", Language: " + language);
            	$(".subtitle").html(authors + " in " + sitetype + ", " + articleDate + ", Language: " + language);
                $("#viewer").html(content);
                $('#viewer').highlight(searchTerm.split(" "), {className: 'search' });
                $("#loadingBox").css("visibility","hidden");
                
                viewContext.langId = data.langId;
                //if (data.langId !== "en")  {
                //	$(".translate").css("visibility","visible");
                //}
          	  
                $('#viewer').unbind("mouseup", highlightedSnippet);
                $('#viewer').bind("mouseup", highlightedSnippet);
                
            }
        });
		this.highlightPromises = [];
	},

	
	onDataLoaded : function(event) {
		var model = this.getView().getModel();
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
	},
  
	addHighlightLayer : function(sChannelId, sEventId, oData) {
		var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		var pdfid = viewContext.pdfId;
		var type = oData.entityType;
		var searchTerm 	 = viewContext.searchTerm;
		
		// check is highlight already exists
		if(this.highlightPromises.indexOf(type) == -1)  {
			// add to array
			this.highlightPromises.push(type);
			
			if (type != 'SEARCH')  {
				if (type == 'ORGANIZATION')  {
					$.ajax({
			            url : "/smartnavxs/entities/entitiesByOrg.xsjs?pdfid=" + pdfid,
			            dataType: "json",
			            success : function (data) {
			    			$.each(data, function(key, val){
			    				$.each(val, function(i, org){
			    					$('#viewer').highlight(org, {className: 'organization' });
			    				});
			           	    });
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                console.log(xhr.status);
			                console.log(thrownError);
			              }
			        });
				
				}
				else if (type == 'EMAIL')  {
					$.ajax({
			            url : "/smartnavxs/entities/entitiesByEmail.xsjs?pdfid=" + pdfid,
			            dataType: "json",
			            success : function (data) {
			    			$.each(data, function(key, val){
			    				$.each(val, function(i, org){
			    					$('#viewer').highlight(org, {className: 'email' });
			    				});
			           	    });
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                console.log(xhr.status);
			                console.log(thrownError);
			              }
			        });
				
				}
				else  {
					$.ajax({
			            url : "/smartnavxs/entities/entitiesByType.xsjs?pdfid=" + pdfid + "&type=" + type,
			            dataType: "json",
			            success : function (data) {
			    			$.each(data, function(i, field){			    				
			 	                $('#viewer').highlight(field, {className: type.toLowerCase() });
			           	    });
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                console.log(xhr.status);
			                console.log(thrownError);
			              }
			        });
				}
				
			}
			else  {
				$('#viewer').highlight(searchTerm.split(" "), {className: type.toLowerCase() });
			}
		}
	},
	
	removeHighlightLayer : function(sChannelId, sEventId, oData) {
		var type = oData.layer;
		for(var i = this.highlightPromises.length - 1; i >= 0; i--) {
		    if(this.highlightPromises[i] === type) {
		    	this.highlightPromises.splice(i, 1);
		    }
		}
		$('#viewer').unhighlight({ className: type.toLowerCase() });
	},

	literalSearch : function(sChannelId, sEventId, oData) {
		sap.ui.getCore().getEventBus().publish(constants.events.OPEN_RESULTLIST, { searchTerm:oData.text});
	},
	
	geocodeSearch : function(sChannelId, sEventId, oData) {
		var locality = oData.text;
		var dialog = new sap.ui.commons.Dialog({
			title : locality,
			modal : true,
			showCloseButton : false
		});
		var localityView = sap.ui.view({
			viewName : "smartnav.geocode.Locality",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : {
				locality : locality,
				dialog : dialog
			}
		});
		dialog.addContent(localityView);
		dialog.addStyleClass("conceptDialog"); // TODO required to be on top
		dialog.addButton(new sap.ui.commons.Button({
			text : "Close",
			press : function() {
				dialog.close();
				dialog.destroy();
			}
		}));
		dialog.open();	
	},
	
	personDossier : function(sChannelId, sEventId, oData) {
		var name = oData.text;
		var dialog = new sap.ui.commons.Dialog({
			title  : name,
			modal  : true,
			showCloseButton : false,
			width  : "650px",
			height : "450px"
		});
		var personView = sap.ui.view({
			viewName : "smartnav.person.Dossier",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : {
				name : name,
				dialog : dialog
			}
		});
		dialog.addContent(personView);
		dialog.addStyleClass("conceptDialog"); // TODO required to be on top
		dialog.addButton(new sap.ui.commons.Button({
			text : "Close",
			press : function() {
				dialog.close();
				dialog.destroy();
			}
		}));
		dialog.open();	
	},
	
	translate : function(sChannelId, sEventId, oData) {
		$("#translateProgress").css("display","inline");
		var curContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");
		
        var linkContext = {
            pdfId : curContext.pdfId
        };
        
        var lang = $("#translateLang option:selected").text();
        var langId = $("#translateLang option:selected").val();

	    // Next context - some properties must be cleared or reset
        var nextContext = jQuery.extend(true, {}, curContext, linkContext, {
            viewType : constants.viewtypes.TRANSLATE_VIEW,
            desc : curContext.pdfTitle,
            prevDesc : curContext.desc,
            nextDesc : null,
            pdfState : undefined,
            pdfTitle : curContext.pdfTitle,
            authors  : curContext.authors,
            articleDate : curContext.articleDate,
            journal  : curContext.journal,
            type     : curContext.type,
            sitetype : curContext.sitetype,
            contentHtml : $("#viewer").html(),
            contentText : $("#viewer").text(), 
            subtitle : $("#subtitle").html().replace("Language:", "Original Language:"),
            title : $("#title").html(),
            entityid : curContext.entityid,
            translateLangId: langId,
            translateLang: lang
        });
        curContext.nextDesc = nextContext.title;

        // Update current state and create a new state on history stack
        window.history.replaceState(curContext, curContext.desc);
        window.history.pushState(nextContext, nextContext.desc, constants.appRoot + "/index.html?" + $.param(linkContext));
        document.title = nextContext.desc + " - " + constants.title;

		var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
        globalModel.setProperty("/viewContext", nextContext);
		sap.ui.getCore().getEventBus().publish(constants.events.CHANGE_VIEW, nextContext);
	}

});