sap.ui.controller("view.awesomebar.AwesomeBar", {

    /**
     * Called when a controller is instantiated and its View controls (if available) are already created. Can be used to
     * modify the View before it is displayed, to bind event handlers and do other one-time initialization.
     */
    onInit : function() {
        this.getView()
            .setModel(sap.ui.getCore().getModel(constants.models.SEARCH_MODEL))
            .bindElement("/viewContext");

        sap.ui.getCore().getEventBus().subscribe(constants.events.OPEN_TEXT, this.openText, this);
        sap.ui.getCore().getEventBus().subscribe(constants.events.OPEN_RESULTLIST, this.openResultList, this);
        sap.ui.getCore().getEventBus().subscribe(constants.events.OPEN_UPLOADLIST, this.openUploadList, this);
        window.onpopstate = this.onContextChange;
    },

    onExit : function() {
        sap.ui.getCore().getEventBus().unsubscribe(constants.events.OPEN_TEXT, this.openText, this);
        sap.ui.getCore().getEventBus().unsubscribe(constants.events.OPEN_RESULTLIST, this.openResultList, this);
        sap.ui.getCore().getEventBus().unsubscribe(constants.events.OPEN_UPLOADLIST, this.openUploadList, this);
        window.onpopstate = undefined;
    },

    openPDF : function(sChannelId, sEventId, oData) {
        // Current context
        var curContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");

        // Minimal context describing next state - used for deep link shown in browser bar
        var linkContext = {
            pdfId : oData.pdfId
        };

        // Next context - some properties must be cleared or reset
        var nextContext = jQuery.extend(true, {}, curContext, linkContext, {
            viewType : constants.viewtypes.TEXT_VIEW,
            desc : oData.pdfTitle,
            prevDesc : curContext.desc,
            nextDesc : null,
            pdfState : undefined,
            pdfTitle : oData.pdfTitle,
            filters : {},
            searchOffset : 0,
        });
        curContext.nextDesc = nextContext.desc;

        // Update current state and create a new state on history stack
        window.history.replaceState(curContext, curContext.desc);
        window.history.pushState(nextContext, nextContext.desc, constants.appRoot + "/index.html?" + $.param(linkContext));
        document.title = nextContext.desc + " - " + constants.title;
        this.onContextChange({
            state : nextContext
        });
    },

    openText : function(sChannelId, sEventId, oData) {
        // Current context
        var curContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");

        // Minimal context describing next state - used for deep link shown in browser bar
        var linkContext = {
            pdfId : oData.pdfId
        };

        // Next context - some properties must be cleared or reset
        var nextContext = jQuery.extend(true, {}, curContext, linkContext, {
            viewType : constants.viewtypes.TEXT_VIEW,
            desc : oData.pdfTitle,
            prevDesc : curContext.desc,
            nextDesc : null,
            pdfState : undefined,
            pdfTitle : oData.pdfTitle,
            authors  : oData.authors,
            articleDate : oData.articleDate,
            journal  : oData.journal,
            type     : oData.type,
            sitetype : oData.sitetype,
            objectType: oData.objectType,
            filters : {},
            searchOffset : 0,
            language: oData.language,
            entityid: oData.entityid
        });
        curContext.nextDesc = nextContext.desc;

        // Update current state and create a new state on history stack
        window.history.replaceState(curContext, curContext.desc);
        window.history.pushState(nextContext, nextContext.desc, constants.appRoot + "/index.html?" + $.param(linkContext));
        document.title = nextContext.desc + " - " + constants.title;
        this.onContextChange({
            state : nextContext
        });
    },

    openResultList : function(sChannelId, sEventId, oData) {
        if (oData.searchTerm == sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty(
                "/viewContext/searchTerm"))
            return;

        // Current context
        var curContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext");

        // Minimal context describing next state - used for deep link shown in browser bar
        var linkContext = {
            searchTerm : oData.searchTerm
        };

        // Next context - some properties must be cleared or reset
        var nextContext = jQuery.extend(true, {}, curContext, linkContext, {
            viewType : constants.viewtypes.SEARCH_VIEW,
            desc : "Search \"" + oData.searchTerm + "\"",
            prevDesc : curContext.desc,
            nextDesc : null,
            pdfId : undefined,
            pdfState : undefined,
            pdfTitle : undefined,
            searchOffset : 0
        });
        curContext.nextDesc = nextContext.desc;

        // Update current state and create a new state on history stack
        window.history.replaceState(curContext, curContext.desc);
        window.history.pushState(nextContext, nextContext.desc, constants.appRoot + "/index.html?" + $.param(linkContext));
        document.title = nextContext.desc + " - " + constants.title;
        this.onContextChange({
            state : nextContext,
            skipUpdate : oData.liveSearchTerm == oData.searchTerm,
        });
    },
    

    onContextChange : function(event) {
        if (!event.state)
            return;

        var viewContext = event.state;
        var withoutUpdate = event.skipUpdate || false;
        var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
        var prevViewContext = globalModel.getProperty("/viewContext");
        globalModel.setProperty("/viewContext", viewContext);
        // if(withoutUpdate) return;
        if (prevViewContext == null || prevViewContext.viewType != viewContext.viewType) {
            sap.ui.getCore().getEventBus().publish(constants.events.CHANGE_VIEW, viewContext);
        } 
        else {
            sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
        }
    },
});