sap.ui.jsview("view.awesomebar.AwesomeBar", {

    getControllerName : function() {
        return "view.awesomebar.AwesomeBar";
    },

    createContent : function(oController) {
        var layoutTemplate = new sap.ui.commons.layout.MatrixLayout({
            layoutFixed : false,
            width : '100%',
            columns : 3,
            widths : [ '40%', '20%', '40%' ],
        });

        var oRow = new sap.ui.commons.layout.MatrixLayoutRow();
        layoutTemplate.addRow(oRow);

        var oCell = new sap.ui.commons.layout.MatrixLayoutCell({
            vAlign : sap.ui.commons.layout.VAlign.Middle,
            hAlign : sap.ui.commons.layout.HAlign.Begin
        });
        oCell.addContent(new sap.ui.commons.Link({
            text : "{prevDesc}",
            enabled : {
                path : "prevDesc",
                formatter : function(v) {
                    return (!!v);
                }
            },
            press : [ window.history.back, window.history ]
        }).addStyleClass("prev icon awesomeEntry"));
        oRow.addCell(oCell);

        oCell = new sap.ui.commons.layout.MatrixLayoutCell({
            vAlign : sap.ui.commons.layout.VAlign.Middle,
            hAlign : sap.ui.commons.layout.HAlign.Center
        });
        oCell.addContent(new sap.ui.commons.Label({
            text : ""
        }));
        oRow.addCell(oCell);

        oCell = new sap.ui.commons.layout.MatrixLayoutCell({
            vAlign : sap.ui.commons.layout.VAlign.Middle,
            hAlign : sap.ui.commons.layout.HAlign.End
        });
        oCell.addContent(new sap.ui.commons.Link({
            text : "{nextDesc}",
            enabled : {
                path : "nextDesc",
                formatter : function(v) {
                    return (!!v);
                }
            },
            press : [ window.history.forward, window.history ]
        }).addStyleClass("next icon awesomeEntry"));
        oRow.addCell(oCell);

        return layoutTemplate;
    }
});