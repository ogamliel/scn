jQuery.sap.require("view.PublicationItem");
jQuery.sap.require("view.ToolbarPanel");
sap.ui.jsview("view.table.ResultTable2", {

       getControllerName : function() {
              return "view.table.ResultTable2";
       },

       createContent : function(oController) {
              var controls = [];
              
              function createTemplate() {
            	  
                     return new view.PublicationItem({
                           title : new sap.ui.commons.Link({
                                  text : "{title}",
                           }),
                           pdfId : "{pdfid}",
                           sitetype : "{sitetype}",
                           journal : "{journal}",
                           articleDate : "{articleDate}",
                           cpnyName : "{cpnyName}",
                           extractDate : "{extractDate}",
                           objectType: "{objectType}",
                           language: "{language}",
                           entityid: "{entityid}",
                           image : new sap.ui.commons.Image({
                                  src : {
                                         path: "type",
                                         formatter: function(data)  {
                                         	return data ? "images/icons/" + data.toLowerCase().replace(" ", "") + ".png" : "";
                                         }
                                  }
                           }),
                           content : new sap.ui.core.HTML({
                                  content : {
                                         path: "snippets",
                                         formatter: function(v) { return "<p>" + v + "</p>"; }
                                  }
                           }),
                     }).bindAggregation("authors", "authors", new sap.ui.commons.Label().bindProperty("text", ""));
              }
              
              var oInfoPanel = new sap.ui.commons.layout.MatrixLayout({
                     columns : 2,
                     width : '100%',
                     widths : ['80%', '20%'] 
              }).addStyleClass("InfoPanel");
              controls.push(oInfoPanel);
              
              var oRow = new sap.ui.commons.layout.MatrixLayoutRow();
              oInfoPanel.addRow(oRow);
              
              oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
                     content : [new sap.ui.commons.TextView().bindText("/count", new sap.ui.model.type.Integer(constants.NUMBER_FORMAT)),
                                new sap.ui.commons.TextView({text:"Results"}),
                                new sap.ui.commons.TextView().bindText("/searchTerm", function(value) {
                                      return value ? "for" : "";
                                }),
                                new sap.ui.commons.TextView().bindText("/searchTerm", function(value) {
                                      return value ? "'" + value + "'" : "";
                                })]
              }).addStyleClass("ResultsCount"));
              
              oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
                     content : [new sap.ui.commons.TextView().bindProperty("text", {
                                                path: "/duration", 
                                                //type: new sap.ui.model.type.Integer(constants.NUMBER_FORMAT),
                                                formatter: function(value) {
                                                       if (value)
                                                              return (parseInt(value) / 1000) + " Seconds";
                                                       else
                                                              return "";
                                                }
                                         }),
                                         new sap.ui.commons.TextView().bindProperty("visible", {
                                                path: "/duration", 
                                                formatter: function(value) {
                                                       return !value;
                                                }
                                         }).addStyleClass("Loading")]
              }).addStyleClass("DurationTime"));

              oRow = new sap.ui.commons.layout.MatrixLayoutRow();
              oInfoPanel.addRow(oRow);
              
              oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
                     colSpan : 2,
                     content : [   new sap.ui.commons.TextView().bindText({
                                                path : "/activeFilters",
                                                formatter : function(subjects) {
                                                       return subjects && subjects.length!=0 ? "within " : ""; }
                                         }),
                                   new sap.ui.commons.layout.HorizontalLayout({
                                          width : "100%",
                                          content : {
                                                path : "/activeFilters",
                                                factory : function(sId, oContext) {
                                                       return new sap.ui.commons.Link({text: oContext.getProperty("name")})
                                                             .addStyleClass("filter removableFilter")
                                                              .attachPress(function(event){
                                                                     oController.onRemoveActiveFilter(oContext.getProperty());
                                                              });
                                                }
                                          }
                                          })]
              }));
              
              oRow = new sap.ui.commons.layout.MatrixLayoutRow();
              oInfoPanel.addRow(oRow);
              
              oRow.addCell(new sap.ui.commons.layout.MatrixLayoutCell({
                     colSpan : 2,
                     content : [   new sap.ui.commons.layout.HorizontalLayout({
                                          width : "100%",
                                          content : [
                                             new sap.ui.commons.TextView().bindText({
                                                              path : "/year",
                                                              formatter : function(year) { return year  ? "from" : ""; }
                                                       }),
                                                       new sap.ui.commons.TextView().bindText({
                                                              path : "/year",
                                                              formatter : function(year) { return year  ? year.from : ""; }
                                                       }).addStyleClass("year")
                                                       .attachBrowserEvent("click", function(event){
                                                              oController.onRemoveYear();
                                                }).attachBrowserEvent("mouseenter", function(event){
                                                       $(".year").addClass("hover");
                                                }).attachBrowserEvent("mouseleave", function(event){
                                                       $(".year").removeClass("hover");
                                                }),
                                                       new sap.ui.commons.TextView().bindText({
                                                              path : "/year",
                                                              formatter : function(year) { return year  ? "to" : ""; }
                                                       }),
                                                       new sap.ui.commons.TextView().bindText({
                                                              path : "/year",
                                                              formatter : function(year) { return year  ? year.to : ""; }
                                                       }).addStyleClass("year removableFilter")
                                                       .attachBrowserEvent("click", function(event){
                                                              oController.onRemoveYear();
                                                }).attachBrowserEvent("mouseenter", function(event){
                                                       $(".year").addClass("hover");
                                                }).attachBrowserEvent("mouseleave", function(event){
                                                       $(".year").removeClass("hover");
                                                })]
                                         })]
              }));
                            
              var oPaginator = new sap.ui.commons.Paginator("paginator",
                           {
                                  numberOfPages : {
                                         path: "/count",
                                         formatter: function(count) {
                                                return Math.ceil((count || 0) / constants.resultlist.SEARCH_LIMIT);
                                         }
                                  },
                                  currentPage : {
                                         path: "searchModel>/viewContext/searchOffset",
                                         formatter: function(searchOffset) {
                                                return Math.ceil((searchOffset || 0) / constants.resultlist.SEARCH_LIMIT + 1);
                                         }
                                  },
                                  page : function(oEvent) {
                                         var page = parseInt(oEvent.getParameter("targetPage"));
                                         oController.showPage(page);
                                  }
                           });

              var oDataSet = new sap.ui.ux3.DataSet({
                     showFilter: false,
                     showSearchField: false,
                     width : "100%",
                     items : {
                           path : "/results",
                           template : new sap.ui.ux3.DataSetItem({
                                  title : "{title}",
                           templateShareable : true,
                            //iconSrc : "{image}"
                           })
                     },
                     views : [ new sap.ui.ux3.DataSetSimpleView({
                           name : "Single Row View",
                           icon : "images/icons/list.png",
                           iconHovered : "images/icons/list_hover.png",
                           iconSelected : "images/icons/list_hover.png",
                           floating : false,
                           responsive : false,
                           itemMinWidth : 0,
                           template : createTemplate(),
                           templateShareable : true,
                     })/*, new sap.ui.ux3.DataSetSimpleView({
                           name : "Floating, responsive View",
                           icon : "images/icons/tiles2.png",
                           iconHovered : "images/icons/tiles2_hover.png",
                           iconSelected : "images/icons/tiles2_hover.png",
                           floating : true,
                           responsive : true,
                           itemMinWidth : 200,
                           template : createTemplate(),
                     })*/ ],
                     search : oController.onFilter,
                     selectionChanged : oController.onSelect,
              });
              oDataSet.addToolbarItem(new view.ToolbarPanel({
                     content : oPaginator,
              }));
              controls.push(oDataSet);
              
              // again, the paginator
              controls.push(oPaginator.clone("foot"));
              
              return controls;
       }
});

