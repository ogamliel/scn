sap.ui.controller("view.table.ResultTable2", {
       onInit : function() {
           this.getView().setModel(new sap.ui.model.json.JSONModel().attachRequestCompleted(this.onDataLoaded, this));
           sap.ui.getCore().getEventBus().subscribe(constants.events.UPDATE_VIEW, this.fetchData, this);
       },

       onAfterRendering : function() {
              var globalModel = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL);
              this.fetchData(null, null, globalModel.getProperty("/viewContext"));
 	 		//$( "<div id=\"event\"><button id=\"eventBtn\" onclick=\"sap.ui.controller('view.article.EventUpload').openDialog();\" class=\"eventBtn\">Event Upload</button>").insertAfter( "#uiLeft" );
      },
       
       showPage : function(page) {
              var searchOffset = (page-1)*constants.resultlist.SEARCH_LIMIT;
              sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).setProperty("/viewContext/searchOffset", searchOffset);
              this.fetchData(null, null, sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).getProperty("/viewContext"));
       },
       
       onExit : function()
       {
			//$('#event').remove();
            this.getView().getModel().detachRequestCompleted(this.onDataLoaded, this);
            sap.ui.getCore().getEventBus().unsubscribe(constants.events.UPDATE_VIEW, this.fetchData, this);
       },
       
       fetchData : function(sChannelId, sEventId, oData) {
              var model = this.getView().getModel();
	         
	          var d = new Date();
	          var endDate = d.toISOString().slice(0,10);
	          
		  	  var s = new Date();
		      s.setMonth(s.getMonth() - 6);
		      var startDate = s.toISOString().slice(0,10);
              
              var params = {
                      "search" : oData.searchTerm,
                      "limit" : constants.resultlist.SEARCH_LIMIT,
                      "offset" : (model.getProperty("/searchTerm") === oData.searchTerm) && oData.searchOffset || 0
              };

              if (oData.filters)  {
	              if (oData.filters.subjects) {
	                     params.subjects = oData.filters.subjects;
	              }
	              
	              if (oData.filters.publishers) {
	                     params.publishers = oData.filters.publishers;
	              }
	              
	              if (oData.filters.persons) {
	                  params.persons = oData.filters.persons;
	              }
	              if (oData.filters.orgs) {
	                  params.orgs = oData.filters.orgs;
	              }
	              if (oData.filters.localities) {
	                  params.localities = oData.filters.localities;
	              }
	              
	              if (oData.filters.contentTypes) {
	                  params.contentTypes = oData.filters.contentTypes;
	              }
	
	              if (oData.filters.year) {
	            	 params.from = oData.filters.year.from;
	                 params.to = oData.filters.year.to;
	             }
	              
	              if (oData.filters.sentiment)  {
	            	  params.sentimentStart = oData.filters.sentiment.start;
	            	  params.sentimentEnd = oData.filters.sentiment.end;
	              }
	 
	              if (oData.filters.interactionTypes)  {
	            	  params.interactionTypes = oData.filters.interactionTypes;
	              }
	              
	              if (oData.filters.mapBounds)  {
	            	  params.mapBounds = JSON.stringify(oData.filters.mapBounds);
	              }
	          }

    			model.setProperty("duration", null);
	           	model.fetchData("/smartnavxs/document/search.xsjs", params, false, "POST", false);
       },
       
       onDataLoaded : function(event) {
           var model = event.getSource();
           var params = JSON.parse(event.getParameter("info"));

          if (params.offset > model.getProperty("/count"))
                 this.showPage(Math.ceil(model.getProperty("/count") / constants.resultlist.SEARCH_LIMIT))
          
          if (params.from && params.to)
              model.setProperty("/year", {from: params.from, to: params.to});

              var activeFilters = [];          
            $.each(params.subjects || [], function(index, value) {
	            activeFilters.push({name: value, type: "subjects" });
	        });
	        $.each(params.publishers || [], function(index, value) {
	            activeFilters.push({name: value, type: "publishers" });
	        });
	        $.each(params.persons || [], function(index, value) {
	            activeFilters.push({name: value, type: "persons" });
	        });
	        $.each(params.orgs || [], function(index, value) {
	            activeFilters.push({name: value, type: "orgs" });
	        });
	        $.each(params.localities || [], function(index, value) {
	            activeFilters.push({name: value, type: "localities" });
	        });
	        $.each(params.contentTypes || [], function(index, value) {
	            activeFilters.push({name: value, type: "contentTypes" });
	        });
	        $.each(params.interactionTypes || [], function(index, value) {
	            activeFilters.push({name: value, type: "interactionTypes" });
	        });
	        
	        model.setProperty("/activeFilters", activeFilters);       
	   },
       
       onSelect: function(oEvent) {
              var oItem = this.getItems()[oEvent.getParameter("newLeadSelectedIndex")];
              var pdfid = this.getModel().getProperty("pdfid", oItem.getBindingContext());
              var pdfTitle = this.getModel().getProperty("title", oItem.getBindingContext());
              var authors = this.getModel().getProperty("authors", oItem.getBindingContext());
              var articleDate = this.getModel().getProperty("articleDate", oItem.getBindingContext());
              var journal = this.getModel().getProperty("journal", oItem.getBindingContext());
              var type = this.getModel().getProperty("type", oItem.getBindingContext());
              var sitetype = this.getModel().getProperty("sitetype", oItem.getBindingContext());
              var objectType = this.getModel().getProperty("objectType", oItem.getBindingContext());
              var language = this.getModel().getProperty("language", oItem.getBindingContext());
              var entityid = this.getModel().getProperty("entityid", oItem.getBindingContext());

              setTimeout(function() {  // this nesting is necessary to asnch trigger open pdf, otherwise the onClick event is still active and locking for the item in the result list, which was removed already
                     sap.ui.getCore().getEventBus().publish(constants.events.OPEN_TEXT, { pdfId: pdfid, pdfTitle: pdfTitle, 
                           authors: authors, articleDate: articleDate, journal: journal, type : type, 
                           sitetype : sitetype, objectType: objectType, language: language, entityid: entityid });
              }, 1);
       },
              
       onFilter: function(oEvent) {
    	   
              var sQuery = oEvent.getParameter("query");
              var oBinding = this.getBinding("items");
              oBinding.filter(!sQuery ? [] : [new sap.ui.model.Filter("title", sap.ui.model.FilterOperator.Contains, sQuery)]);
              oDataSet.setLeadSelection(-1);    
       },
       
       onRemoveActiveFilter: function(activeFilter) {
              var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
              var aFilters = this.getView().getModel().oData.activeFilters;
              
              aFilters.splice( $.inArray(activeFilter, aFilters), 1);
              viewContext.filters[activeFilter.type].splice( $.inArray(activeFilter.name, viewContext.filters[activeFilter.type]), 1);
              
              sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
       },
       
       onRemoveYear: function() {
              var viewContext = sap.ui.getCore().getModel(constants.models.SEARCH_MODEL).oData.viewContext;
              
              this.getView().getModel().oData.year = undefined;
              viewContext.filters.year = undefined;
              
              sap.ui.getCore().getEventBus().publish(constants.events.UPDATE_VIEW, viewContext);
       }
});

