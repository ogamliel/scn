(function($) {
$.fn.ellipsis = function()
{
    return this.each(function()
    {
    	var el = $(this);

        if(el.css("overflow") == "hidden")
        {
        	var text = el.html();
        	var fulltext = text;
        	
        	var multiline = el.hasClass('multiline');
        	var t = $(this.cloneNode(true))
                .hide()
                .css('position', 'absolute')
                .css('overflow', 'visible')
                .width(multiline ? el.width() : 'auto')
                .height(multiline ? 'auto' : el.height());

			el.after(t);
	
			function height_calc() { 
				return  t.height() < el.height();
			
			};
			function width_calc() { return t.width() < el.width(); };

			function func(){ 
				if(multiline){
					return height_calc();
				}
				else
				{
					return width_calc();
				}
			};

			var newText = text;
			if (text.length > 250)
				text = text.substr(0,250);
			while (text.length > 30 && !func())
			{
				
				var step = text.length/20;
				if (step < 1) step = 1;
		        text = text.substr(0, text.length - step);
		        newText = text+"...";
				t.html(newText);
			}
			newText = "Search For \"<span id=\"selectedText\">"+newText+"</span><span id=\"hiddenFullText\" style=\"display:none\">"+fulltext+"</span>\"";
			t.html(newText);
			el.html(t.html());
			t.remove();
        }
    });
};
})(jQuery);
