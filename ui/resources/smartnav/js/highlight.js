/*
* jQuery Highlight plugin
*
* Based on highlight v3 by Johann Burkard
* http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
*
* Code a little bit refactored and cleaned (in my humble opinion).
* Most important changes:
*  - has an option to highlight only entire words (wordsOnly - false by default),
*  - has an option to be case sensitive (caseSensitive - false by default)
*  - highlight element tag and class names can be specified in options
*
* Usage:
*   // wrap every occurrance of text 'lorem' in content
*   // with <span class='highlight'> (default options)
*   $('#content').highlight('lorem');
*
*   // search for and highlight more terms at once
*   // so you can save some time on traversing DOM
*   $('#content').highlight(['lorem', 'ipsum']);
*   $('#content').highlight('lorem ipsum');
*
*   // search only for entire word 'lorem'
*   $('#content').highlight('lorem', { wordsOnly: true });
*
*   // don't ignore case during search of term 'lorem'
*   $('#content').highlight('lorem', { caseSensitive: true });
*
*   // wrap every occurrance of term 'ipsum' in content
*   // with <em class='important'>
*   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
*
*   // remove default highlight
*   $('#content').unhighlight();
*
*   // remove custom highlight
*   $('#content').unhighlight({ element: 'em', className: 'important' });
*
*
* Copyright (c) 2009 Bartek Szopka
*
* Licensed under MIT license.
*
*/
jQuery.sap.require("sap.ui.core.HTML");
jQuery.sap.require("sap.ui.commons.Callout");
jQuery.sap.require("sap.ui.commons.TextView");

var CALLOUT_HEIGHT = 300;
var CALLOUT_WIDTH  = 300;
var SELECTIONPANEL_WIDTH = 300;
var SELECTIONPANEL_HEIGHT = 300;
var SELECTIONPANEL_MIN_HEIGHT = 50;
var HANGER_TOP_OFFSET_IN_PANEL = 25;

jQuery.extend({
    highlight: function (node, re, nodeName, className) {
        if (node.nodeType === 3) {  // text node
        	var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                //highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);                
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                
                var contentStr = "<div>" +
                "<table class=\"calloutTable\" cellspacing=\"4\">" +
               "<tr>" +
                       "<td colspan=\"2\" class=\"tooltipTitle\">" + constants.entitymapping[className.toUpperCase()].title +"<span class=\"tooltipValue\">"+wordNode.data+"</span></td>" +
                "</tr>";
               contentStr += "<tr onclick=\"clearTooltips();  publishTextSearch('"+wordNode.data+"');\"  class=\"calloutActionLink\">" +
                       "<td><img src=\"./images/ns2_logo_search_trans.gif\"/></td>" +
                       "<td class=\"calloutAction\">Search in Catalog</td>"+
                "</tr>";
                /*if (className === 'locality')  {
                    contentStr += "<tr onclick=\"clearTooltips();sap.ui.getCore().getEventBus().publish(constants.events.GEOCODE_SEARCH, { text:'"+wordNode.data+"'});\"  class=\"calloutActionLink\">" +
                    "<td><img src=\"./images/icons/pin_icon.jpg\"/></td>" +
                    "<td class=\"calloutAction\">View Map of " + wordNode.data + "</td>"+
                    "</tr>";
                }
                if (className === 'person')  {
                    contentStr += "<tr onclick=\"clearTooltips();sap.ui.getCore().getEventBus().publish(constants.events.PERSON_DOSSIER, { text:'"+wordNode.data+"'});\"  class=\"calloutActionLink\">" +
                    "<td><img src=\"./images/icons/person.png\"/></td>" +
                    "<td class=\"calloutAction\">View <i>" + wordNode.data + "</i> Dossier</td>"+
                    "</tr>";
                }*/
                contentStr += "</table></div>";
                var html = new sap.ui.core.HTML({
                   // the static content as a long string literal
                   content   : contentStr,
                   preferDOM : false
                  });

                  var oCallout = new sap.ui.commons.Callout({content: html});
                  oCallout.addStyleClass("sap_custom_callout");
                  oCallout.addStyleClass("sap_marker_callout");
                  oCallout.addStyleClass("sap_highlight_callout");
                  oCallout.addStyleClass("sap_custom_callout1");
              
                  var oTextView = new sap.ui.commons.TextView({text: wordNode.data});
                  oTextView.addStyleClass(className);
                  oTextView.setTooltip(oCallout);
                  oTextView.placeAt(highlight);
                  wordNode.parentNode.replaceChild(highlight, wordNode);
              
                return 1;
            }
        } 
        
        // node type 1 = element
        else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName)  && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted

              // check if token not already highlight in another entity
              if (!$(node).hasClass('organization') && 
                           !$(node).hasClass('person') && 
                           !$(node).hasClass('noun_group') &&
                           !$(node).hasClass('locality') &&
                           !$(node).hasClass('search') &&
                           !$(node).hasClass('email') &&
                           !$(node).hasClass('parliament'))  {
                   for (var i = 0; i < node.childNodes.length; i++) {
                       i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
                   }
              }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options) {
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);
    
    var child = this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
    	var viewer = parent.parentNode;  	

        var span = this.firstChild.parentNode;
        var textNode = span.firstChild;

        viewer.replaceChild(textNode, parent);
        viewer.normalize();
    }).end();
    
    return child;

/*    return this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
              parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();*/
};

jQuery.fn.highlight = function (words, options) {
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
    jQuery.extend(settings, options);
    
    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i){
      return word != '';
    });
    words = jQuery.map(words, function(word, i) {
      //return word.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&");
      return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; };
    var flag = settings.caseSensitive ? "" : "i";
    
    /*var multiwords, multiwordsPattern;
    for(var i=0; i<words.length; i++)  {
    	multiwords = words[i].split(/\n/);
    }
   	multiwordsPattern = multiwords.slice();
    if (multiwordsPattern.length > 1)  {
    	for (var i=0; i<multiwordsPattern.length; i++)  {
    		multiwordsPattern[i] = "^\\s?"+multiwordsPattern[i].trim();
    		multiwords[i] = multiwords[i].trim();
    	}
    }
    var pattern = "(" + multiwordsPattern.join("|") + ")"; */
    
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function () {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};


getSelected = function(){
         var text = '';
         if(window.getSelection){
                text = window.getSelection();
         }
         else if(document.getSelection){
                text = document.getSelection();
         }
         else if(document.selection){
                text = document.selection.createRange().text;
         }
         return text;
}

convertSelection = function()  {
	if (window.getSelection) {
	  if (window.getSelection().empty) {  // Chrome
		  window.getSelection().empty();
	  } 
	  else if (window.getSelection().removeAllRanges) {  // Firefox
		  window.getSelection().removeAllRanges();
	  }
	  else if (document.selection) {  // IE?
		  document.selection.empty();
	  }
	}
    $("#searchText").css("background-color","rgba(18%, 9%, 69%, 0.2)");              
}

highlightedSnippet = function(){
       $('body').click(function (e) {
           var targetId = e.target.id;
           if (targetId != 'search' && targetId != 'mark' && targetId != 'viewer') {
              if($('#viewer').find('#searchText').length > 0)  {
                     var curText = $('#searchText').html();
                     $('#searchText').replaceWith(curText);
              }
              $("#selectionPanel").css("visibility","hidden");
              $("#searchWrapper").css("visibility","hidden");
           }
       });
       
      var range = getFirstRange();
      if (range && (range.startOffset != range.endOffset)) {
    	  
         // make sure the #searchText does not already exist.  if it does, delete it.
         if($('#viewer').find('#searchText').length > 0)  {
                var curText = $('#searchText').html();
                $('#searchText').replaceWith(curText);
         }
         
          var el = document.createElement('span');
          $(el).attr('id', 'searchText');
          
          try {
        	  
              range.surroundContents(el);
              
              // undo the highlight and apply a
              // background color to the selection
              convertSelection();
            
              var _html = "<table id=\"selectionTable\" cellspacing=\"4\"><tbody>";
              
              	_html += "<tr class=\"selectionWrapper sap_highlight_callout_action\"  id=\"searchWrapper\">" +
              		"<td><img src=\"./images/ns2_logo_search_trans.gif\"/></td>" +
              		"<td><div id=\"search\">Search Catalog for Selected Entities</div></td>" +
              		"</tr>";
      
		        _html +="<tr class=\"selectionWrapper sap_highlight_callout_action\" id=\"markWrapper\">" +
			        "<td><img src=\"./images/mark_text.png\"/></td>" +
			        "<td><div id=\"mark\">Highlight Entities in Selection</div></td>" +
			        "</tr>";
      
		        _html += "</tbody>" +
              		"</table><span id=\"textSelectionPosition\" style=\"display:none;\"></span>";
            
              var startOffset = $("#searchText").offset().top;
             
              var ac = $("#selectionPanel");
            
              if (ac.length > 0) {
                   ac.addClass("sap_custom_callout");
                   ac.html(_html); 
              } 
              else {
                    $('<div>').attr('id',"selectionPanel").addClass("sap_custom_callout").html(_html).appendTo('body');
                    ac = $("#selectionPanel");
                    ac.css("display","block");
                    ac.css("width",SELECTIONPANEL_WIDTH + 'px');
                    ac.css("max-width",SELECTIONPANEL_WIDTH + 'px');
                    ac.css("max-height",SELECTIONPANEL_HEIGHT + 'px');
                    ac.css("min-height",SELECTIONPANEL_MIN_HEIGHT + 'px');
              }
              ac.css("top",startOffset+'px');
              ac.css("left",'97px');                  

              //$("#selectionPanel").html(_html);
              $("#selectionPanel").css("visibility","visible");
              
              $("#searchWrapper").css("visibility","visible");
              $("#searchWrapper").css("display","table-row");

              $("#searchWrapper").click(
                    function()  {
                           $("#selectionPanel").css("visibility","hidden");
                           $("#searchWrapper").css("visibility","hidden");
                           $('#viewer').unbind("mouseup", highlightedSnippet);
                           
                           var selectedText = $("#searchText").html();
                           if (selectedText)  {
                                  var selectedTokens = new Array();
                                  var kids = $("#searchText").find("span.search, span.person, span.organization, span.locality, span.parliament, span.noun_group, span.email");
                                  $(kids).each(function(index)  {
                                         var token = $(this).text();
                                         if(selectedTokens.indexOf(token) == -1)  {
                                                selectedTokens.push(token);
                                         }
                                  });

                                  publishEvent(constants.events.TEXT_LITERAL_SEARCH, { text: selectedTokens.join(" ") });
                           }
                    }
              );

              $("#markWrapper").click(
                    function()  {
                           $("#selectionPanel").css("visibility","hidden");
                           $("#searchWrapper").css("visibility","hidden");

                           var paramStr = window.location.search;
                           var endPdfid = paramStr.indexOf('&');
                           var pdfid = paramStr.substring(paramStr.indexOf('=')+1);
                           if (endPdfid != -1)
                                  pdfid = paramStr.substring(1, endPdfid);
                           

                           if($('#searchText').find('.person').length == 0)  {                         
                                  $.ajax({
                                url : constants.appRoot + "/rest/entities/entitiesByType.xsjs?pdfid=" + pdfid + "&type=PERSON ",// + key,
                                dataType: "json",
                                success : function (data) {
                                         $.each(data, function(i, field){
                                          $('#searchText').highlight(field, { className: 'person' });
                                      });
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.status);
                                    console.log(thrownError);
                                }
                            });
                           }
                                                       
                           if($('#searchText').find('.organization').length == 0)  {
                               $.ajax({
                             url : constants.appRoot + "/rest/entities/entitiesByOrg.xsjs?pdfid=" + pdfid,// + key,
                             dataType: "json",
                             success : function (data) {
                                      $.each(data, function(key, val){
                                            $.each(val, function(i, org){
                                                   $('#searchText').highlight(org, {className: 'organization' });
                                            });
                                   });
                             },
                             error: function (xhr, ajaxOptions, thrownError) {
                                 console.log(xhr.status);
                                 console.log(thrownError);
                             }
                         });
                        }                          

                           if($('#searchText').find('.locality').length == 0)  {                       
                               $.ajax({
                             url : constants.appRoot + "/rest/entities/entitiesByType.xsjs?pdfid=" + pdfid + "&type=LOCALITY ",// + key,
                             dataType: "json",
                             success : function (data) {
                                   $.each(data, function(i, field){
                                       $('#searchText').highlight(field, { className: 'locality' });
                                   });
                             },
                             error: function (xhr, ajaxOptions, thrownError) {
                                 console.log(xhr.status);
                                 console.log(thrownError);
                             }
                         });
                        }

                           if($('#searchText').find('.email').length == 0)  {                       
                               $.ajax({
                             url : constants.appRoot + "/rest/entities/entitiesByEmail.xsjs?pdfid=" + pdfid,// + key,
                             dataType: "json",
                             success : function (data) {
                                   $.each(data, function(i, field){
                                       $('#searchText').highlight(field, { className: 'email' });
                                   });
                             },
                             error: function (xhr, ajaxOptions, thrownError) {
                                 console.log(xhr.status);
                                 console.log(thrownError);
                             }
                         });
                        }

                           if($('#searchText').find('.parliament').length == 0)  {                            
                               $.ajax({
                             url : constants.appRoot + "/rest/entities/entitiesByType.xsjs?pdfid=" + pdfid + "&type=PARLIAMENT ",// + key,
                             dataType: "json",
                             success : function (data) {
                                      $.each(data, function(i, field){
                                       $('#searchText').highlight(field, { className: 'parliament' });
                                   });
                             },
                             error: function (xhr, ajaxOptions, thrownError) {
                                 console.log(xhr.status);
                                 console.log(thrownError);
                             }
                         });
                        }

                           if($('#searchText').find('.noun_group').length == 0)  {                            
                                $.ajax({
	                                url : constants.appRoot + "/rest/entities/entitiesByType.xsjs?pdfid=" + pdfid + "&type=NOUN_GROUP ",// + key,
	                                dataType: "json",
	                                success : function (data) {
	                                         $.each(data, function(i, field){
	                                          $('#searchText').highlight(field, { className: 'noun_group' });
	                                      });
	                                },
	                                error: function (xhr, ajaxOptions, thrownError) {
	                                    console.log(xhr.status);
	                                    console.log(thrownError);
	                                }
                                });
                           }
   
                    }
              );
          

          
          } catch(ex) {
              if ((ex instanceof rangy.RangeException || Object.prototype.toString.call(ex) == "[object RangeException]") && ex.code == 1) {
                  console.log("Unable to surround range because range partially selects a non-text node. See DOM Level 2 Range spec for more information.\n\n" + ex);
              } else {
                  console.log("Unexpected errror: " + ex);
                  range = null;
              }
          }
      }
         
   }

function getFirstRange() {
    var sel = rangy.getSelection();
    if (!sel.getRangeAt(0).canSurroundContents()) {  
        var range = sel.getRangeAt(0);       
        // highlighted tokens at the beginning of a highlighted snippet
        if (sel.anchorNode.parentNode.id !== 'viewer')  {
        	range.setStartBefore(sel.anchorNode.parentNode.parentNode);
        }
        // highlighted tokens at the end of a highlighted snippet
        if (sel.focusNode.parentNode.id !== 'viewer')  {
        	range.setEndAfter(sel.focusNode.parentNode.parentNode);
        }       
        sel.setSingleRange(range, false);       
        return sel.rangeCount ? range : null;
    }
    else  {
        return sel.rangeCount ? sel.getRangeAt(0) : null;	
    }   
}

function clearTooltips() {
	$(".sap_custom_callout").each(function() { 
		$(this).remove();
	});
}

function publishTextSearch(data)  {
	sap.ui.getCore().getEventBus().publish(constants.events.TEXT_LITERAL_SEARCH, { text: data});
}

