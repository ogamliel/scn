var currentRequestCount = {};

sap.ui.model.Model.prototype.fetchData = function(sURL, oParameters, async, sType, bMerge, bCache, mHeaders, sProperty)
{
	var self = this;
	var key = hash(self);
	var currentCount = currentRequestCount[key];
	if(typeof currentCount == "undefined")
	{
		currentCount = 0;
	}
	currentCount++;
	currentRequestCount[key] = currentCount;
	
	jQuery.getJSON(sURL, oParameters, function(json)
	{
		var newCount = currentRequestCount[key];
		if(newCount == currentCount)
		{
			if(bMerge)
			{
				jQuery.extend(self.getData(), json);
			} else
			{
				if(typeof sProperty == "undefined")
				{
					self.setData(json);
				} else
				{
					self.setProperty(sProperty, json);
				}
			}
			self.fireRequestCompleted(
				{
					url: sURL,
					type: sType,
					async: async,
					info: JSON.stringify(oParameters)
				});
		} else
		{
			self.fireRequestFailed(
				{
					message: sURL,
					statusCode: null,
					statusText: 'Request/Answer is outdated',
					responseText: ''
				});
		}
	});
	self.fireRequestSent(
			{
				url: sURL,
				type: sType,
				async: async,
				info: JSON.stringify(oParameters)
			});
}

function hash(value) {
    return (typeof value) + ' ' + (value instanceof Object ?
        (value.__hash || (value.__hash = ++arguments.callee.current)) :
        value.toString());
}

hash.current = 0;

// extended by controller
sap.ui.base.EventProvider.prototype.arrayEquals = function(ar1, ar2) {
	if (ar1 == undefined && ar2 == undefined)
		return true;
	
	if (ar1 == undefined)
		return false;
	
	if (ar2 == undefined)
		return false;
	
	if (ar1.length != ar2.length)
		return false;
	
	for (var i=0; i<ar1.length; i++) {
		if (ar1[i] != ar2[i])
			return false;
	}
	
	return true;
}