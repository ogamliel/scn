var appRoot = function() {
	var path = document.location.pathname;
	return path.substring(0, path.lastIndexOf("/"));	
}();

var constants = {
	appRoot: appRoot,
	appName: appRoot.substring(1, appRoot.length),
	events: {
		UPDATE_VIEW: "UPDATE_VIEW",
		CHANGE_VIEW: "CHANGE_VIEW",
		OPEN_TEXT: "OPEN_TEXT",
		OPEN_RESULTLIST: "OPEN_RESULTLIST",
		//OPEN_UPLOADLIST: "OPEN_UPLOADLIST",
		RENDER_PDF_HIGHLIGHT: "RENDER_PDF_HIGHLIGHT",
		RENDER_TEXT_HIGHLIGHT: "RENDER_TEXT_HIGHLIGHT",
		PDF_LITERAL_SEARCH : "PDF_LITERAL_SEARCH",
		WIKI_SEARCH : "WIKI_SEARCH",
		PDF_CONCEPT_SEARCH : "PDF_CONCEPT_SEARCH",
		PDF_ADD_HIGHLIGHT_LAYER : "PDF_ADD_HIGHLIGHT_LAYER",
		PDF_REMOVE_HIGHLIGHT_LAYER : "PDF_REMOVE_HIGHLIGHT_LAYER",
		TEXT_CONCEPT_SEARCH : "TEXT_CONCEPT_SEARCH",
		TEXT_ADD_HIGHLIGHT_LAYER : "TEXT_ADD_HIGHLIGHT_LAYER",
		TEXT_REMOVE_HIGHLIGHT_LAYER : "TEXT_REMOVE_HIGHLIGHT_LAYER",
		TRANSLATE_ADD_HIGHLIGHT_LAYER : "TRANSLATE_ADD_HIGHLIGHT_LAYER",
		TRANSLATE_REMOVE_HIGHLIGHT_LAYER : "TRANSLATE_REMOVE_HIGHLIGHT_LAYER",
		PDF_READY: "PDF_READY_FOR_HIGHLIGHTS",
		TEXT_READY: "TEXT_READY_FOR_HIGHLIGHTS",
		PDF_LITERAL_SEARCH_OF_CONCEPTS: "PDF_LITERAL_SEARCH_OF_CONCEPTS",
		TEXT_LITERAL_SEARCH_OF_CONCEPTS: "TEXT_LITERAL_SEARCH_OF_CONCEPTS",
		TEXT_LITERAL_SEARCH: "TEXT_LITERAL_SEARCH",
		ADD_HIGHLIGHT_TYPE : "ADD_HIGHLIGHT_TYPE",
		SHOW_HIGHLIGHTS_OF_TYPE: "SHOW_HIGHLIGHTS_OF_TYPE",
		GEOCODE_SEARCH: "GEOCODE_SEARCH",
		PERSON_DOSSIER: "PERSON_DOSSIER",
		YEAR_TO_MONTH: "YEAR_TO_MONTH",
		//UPLOAD_DOCS: "UPLOAD_DOCS"
	},
	viewtypes: {
		SEARCH_VIEW: "SEARCH_VIEW",
		TEXT_VIEW: "TEXT_VIEW",
		TRANSLATE_VIEW: "TRANSLATE_VIEW"
	},
	models: {
		SEARCH_MODEL: "searchModel",
	},
	entitymapping: {
		 "NOUN_GROUP" : { css : "sap_entity_highlight_noungroup", title : "Concepts"},
		 "PERSON" : { css : "sap_entity_highlight_person", title : "Person"},
		 "LOCALITY" : { css : "sap_entity_highlight_locality", title : "Locality"},
		 "ORGANIZATION" : { css : "sap_entity_highlight_organization", title : "Organization"},
		 "PARLIAMENT" : { css : "sap_entity_highlight_parliament", title : "Parliament"},
		 "SEARCH" : { css: "sap_entity_highlight_search", title:"Search Term"},
		 "EMAIL" : { css: "sap_entity_highlight_email", title:"Email"}
	},
	pdf: {
		URL: appRoot + "/rest/document/get.xsjs"
	},
	searchFeature: {
		ng : false, // NG=true needs the new binary for HANA incl. snippet (currently not on ld9772!) 
	},
	resultlist: {
		SEARCH_LIMIT: 10,
		LIVE_SEARCH_TIMER: 200
	},
	textanalysis: {
		URL: appRoot + "/rest/textanalysis/analyze.xsjs"
	},
	NUMBER_FORMAT: {
		groupingEnabled: true,
		groupingSeparator: ",",
		decimalSeparator: ".",
		minFractionDigits: 0, 
		maxFractionDigits: 3
	},
	tagcloud: {
		COLORS: ["#1d766f", "#00665e", "#0176C3", "#a64800", "#8cadc1", "#a60400"],
	},
	trendcloud: {
		//mode : "RELATIVE",    // scales words based on occurrence relative to each other (smoothed by a log function)
	    mode : "RANK",     // scales words based on rank only - uniform scaling
	},
	title: "Smart Content Navigation on HANA | SAP NS2"
}
