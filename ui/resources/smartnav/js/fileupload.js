function getFileMetadata()  {
	var x = document.getElementById("fileUploader");
	var txt = '';
	if ('files' in x) {
        if (x.files.length == 0) {
            txt = "Select one or more files.";
        } else {
            for (var i = 0; i < x.files.length; i++) {
                var file = x.files[i];
                txt += "<br><strong>" + (i+1) + ". " + file.name + " (" + file.size + " bytes) </strong><br>";                
                txt += "<table>"
                txt += "<tr class='spaceUnder'><td valign='top'>Document Name:</td><td><input type='text' class='documentMeta' name='docName" + i + "' /></td></tr>";
                txt += "<tr class='spaceUnder'><td valign='top'>Document Author:</td><td><input type='text' class='documentMeta' name='docAuthor" + i + "'/></td></tr>";
                txt += "<tr class='spaceUnder'><td valign='top'>Document Date:</td><td><input type='text' class='documentMeta' name='docDate" + i + "'/></td></tr>";
                txt += "<tr class='spaceUnder'><td valign='top'>Document Type:</td><td><input type='text' class='documentMeta' name='docType" + i + "'/></td></tr>";
                txt += "</table>"
                
                //txt += "<div=\"event" + i+1 + " class=\"event\">Event Type: <select class=\"event\"><option value=''></option><option value='Battle No Change of Territory'>Battle No Change of Territory</option> </select></div><br>";
            }
        }
    } 
    else {
        if (x.value == "") {
            txt += "Select one or more files.";
        } else {
            txt += "The files property is not supported by your browser!";
            txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
	document.getElementById("fileGallery").innerHTML = txt;
}

function dropHandler(event)  {
	event.preventDefault();
	sap.ui.controller("view.components.FileUpload").dropHandler(event);
}

function dragoverHandler(event)  {
	event.preventDefault();
	sap.ui.controller("view.components.FileUpload").dragoverHandler(event);
}

function dragendHandler(event)  {
	event.preventDefault();
	sap.ui.controller("view.components.FileUpload").dragendHandler(event);
}

function browseUpload(event)  {
	sap.ui.controller("view.components.FileUpload").browseUpload(event);
}
