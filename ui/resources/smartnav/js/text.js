
	

var sapUI5Enabled = true;
var sapUI5EnabledTextSelectionPanel = false;
var MAX_WORDS_FOR_LITERAL_SEARCH = 3;
var textSelectionPanelForDemo = true;
var CALLOUT_HEIGHT = 300;
var CALLOUT_WIDTH  = 300;
var SELECTIONPANEL_WIDTH = 250;
var SELECTIONPANEL_HEIGHT = 160;
var SELECTIONPANEL_MIN_HEIGHT = 40;
var HANGER_TOP_OFFSET_IN_PANEL = 25;
var enableThreads = true;
var clearOverlappingHighlights = true;
var _textUrl = undefined;
var _docState = undefined;
var _textViewer = undefined;
var _searchActive = false;
var openCallouts = [];
var activeHighlightTypes = [];

function inArray(elem, arr) {
	for(var i = 0 ; i < arr.length; i++){
		if (arr[i] == elem)
			return true;
	}
	return false;
}

/**
 *	Contains all information necessary for rendering and interacting with highlights 
 *  can be either search highlight, user mark highlight, or user comment 
 */

var highlightGlobalCss = "sap_custom_highlight";
var highlightTypeToCss = {
	SEARCH 		: "sap_search_highlight",
	CONCEPT_SEARCH : "sap_concept_highlight",
	MARKER	 	: "sap_marker_highlight",
	COMMENT		: "sap_comment_highlight",
	REFERENCE 	: "sap_reference_highlight",
	TEXT_SELECTION: "sap_textselection_highlight",
	CUSTOM: "sap_custom_highlight",
	SYNONYMS: "sap_synonyms_highlight"
};

var highlightType = {
	SEARCH 	 : "SEARCH",
	MARKER	 : "MARKER",
	COMMENT	 : "COMMENT",
	REFERENCE: "REFERENCE",
	CONCEPT_SEARCH: "USER_CONCEPT_SEARCH",
	TEXT_SELECTION: "TEXT_SELECTION",
	CUSTOM: "CUSTOM",
	SYNONYMS: "SYNONYMS"
};

var HighlightRenderingNormalizer={
	flushTime: 100,
	flushTimeout: undefined,

	init:function() {
	},

	touch: function() {
		clearTimeout(HighlightRenderingNormalizer.flushTimeout);
		HighlightRenderingNormalizer.flushTimeout = setTimeout(HighlightRenderingNormalizer.flush,HighlightRenderingNormalizer.flushTime);
	},
	flush: function() {
		if (typeof _textViewer != "undefined")
			_textViewer.showAllHighlights();
	},
	destroy:function() {
	}
};

function publishEvent(event,data) {
	sap.ui.getCore().getEventBus().publish(event,data);
};

function clearTooltips() {
	console.log('clearing tooltips...');
	
	for (var i = 0 ; i < openCallouts.length; i++)
		openCallouts[i].stop(true,true).mouseout();
	
	$(".sap_custom_callout").each(function() { 
		console.log('found callout');
		$(this).mouseout();
	});
}
/**
 * Represents the state of a document so that we can recover the document
 * for navigation
 */
function DocumentState(doi, searchTerm, synonyms) {
	this.doi = doi;
	this.loadedPages = {};
	// all extracted references declarations
	this.references = [];
	this.searchTerm = searchTerm;
	this.synonyms = synonyms;
	this.renderedHighlightTypes =[];

	// contains all the highlights of the current document
	this.highlights = [];
	this.nextHighlightId = 0;

	this.addHighlight = function(highlight) {
		this.highlights.push(highlight);
		this.nextHighlightId++;
	};

	this.clearAllHighlights = function() {
		for (var i = 0 ; i < this.highlights.length; i++) {
				delete this.highlights[i];
		}
		this.highlights = [];
	};

	this.removeHighlight = function(indx) {
		this.highlights.splice(indx,1);
	};

	this.removeHighlightsOfType = function(type) {
		for (var i = 0 ; i < this.highlights.length; i++) {
			if (this.highlights[i].type & type) {
				if (typeof this.highlights[i].domContainer != 'undefined') {
					this.highlights[i].domContainer.remove();
				}
				this.highlights[i].markAsRemoved = true;
			}
			else {
				this.highlights[i].markAsRemoved = false;
			}
		}
		if (type == highlightType.REFERENCE) {
			this.references = [];
			this.referencesExtracted = false;
			this.loadedPages = {};
			this.referencesExtracted = false;
		}

		this.highlights = this.highlights.filter(function(elem) { return !elem.markAsRemoved;});
		
	};
	
	this.clear = function() {
		this.loadedPages = {};
		this.references = [];
		this.referencesExtracted = false;
		this.highlights = [];
		this.searchTerm = undefined;
		this.nextHighlightId = 0;	
	}
};

function TextViewer() {
	this.doneInit = false;
	this.workerThread = undefined;
	this.instanceNum = 0;
	this.freshText = true;

	this.init = function() {
		_textViewer = this;
		DocumentEventListener.initialize();
		textSelectionPanel.initialize();
		HighlightRenderingNormalizer.init();
		if (!sapUI5Enabled) {
			MarkerCallout.initialize();
			commentPanel.initialize();
		}
		this.doneInit = true;
		this.searchTextRequests = [];
		this.currentSearchId = 0;
	};

	this.loadText = function(url, state, fresh) {
		if (!this.doneInit)
			this.init();
		_textUrl = url;
		_searchActive = false;
		_docState = state;
		this.freshText = fresh;
		// clean any open callout
		for (var i = 0 ; i < openCallouts.length; i++)
		{
			openCallouts[i].stop(true,true).mouseout();
		}
		openCallouts = [];
		webViewerLoad(_textUrl, _docState.doi, this.freshText);
		var textDomContainer = $("#mainContainer");
		if (textDomContainer.length) {
			if (textDomContainer.css("visibility") === "hidden")
				textDomContainer.css("visibility","visible");
		}
		publishEvent(constants.events.TEXT_READY, {});
	};
	this.searchTextRequests = [];
	this.currentSearchId = 0;

	this.searchText = function(q,t,et,r,m,c) {
		if (!inArray(et,activeHighlightTypes))
			return;
		_searchActive = true;
		TextFindAndMark.initialize(q,t,et,r,m,c);
		TextFindAndMark.markAll();
	};
	
	this.executeNextSearch = function() {
		if (_searchActive)
			return false;
		if (this.currentSearchId < this.searchTextRequests.length) {
			if (typeof this.searchTextRequests[this.currentSearchId] != "undefined") {
				this.searchText(this.searchTextRequests[this.currentSearchId].query,this.searchTextRequests[this.currentSearchId].type, this.searchTextRequests[this.currentSearchId].entityType,this.searchTextRequests[this.currentSearchId].range,this.searchTextRequests[this.currentSearchId].metadata,this.searchTextRequests[this.currentSearchId].caseSensitive);
				this.currentSearchId++;
				return true;
			}
			else {
				return false;
			}
		}
	};

	this.registerSearchOperation = function(q,t,et,r,m,isCaseSensitive) {
		if (q.length <= 2 && q != q.toUpperCase())
			return;
		var alreadyActive = false;
		for (var i = 0; i < activeHighlightTypes.length; i++){
			if (activeHighlightTypes[i] == et) { 
				alreadyActive = true;
				break;
			}
		}
		if (!alreadyActive) {
			activeHighlightTypes.push(et);
		}
		_textViewer.searchTextRequests.push({type:t, entityType:et, query:q, range:r, metadata:m, caseSensitive: isCaseSensitive});
	};

	this.getMatchesCountOfType = function(et) {
		var count = 0;
		for (var key in TextFindController.allMatchesByQuery) {
			if (TextFindController.allMatchesByQuery[key].entityType == et) {
				for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].matches.length; i++) {
					count += TextFindController.allMatchesByQuery[key].matches[i].length;
				}

			}
		}
		return count;
	};

	this.getMatchesDistinctCountOfType = function(et) {
		var count = 0;
		for (var key in TextFindController.allMatchesByQuery) {
			if (TextFindController.allMatchesByQuery[key].entityType == et) {
				for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].matches.length; i++) {
					if(TextFindController.allMatchesByQuery[key].matches[i].length > 0)
					{
						count++;
						break;
					}
				}
			}
		}
		return count;
	};

	this.updateMatches = function() {
		TextFindAndMark.updatePages();
	};

	this.enableMatchesIfCalculated = function (q,st,et) {
//		var key = q.split(/ |\r\n|\n|\r/).join("_")+"____"+st+"_"+et;
		var key = q.replace("\n","_").replace("\r","__").replace("\r\n","____").replace(" ","_____")+"____"+st+"_"+et;
		if (typeof   TextFindController.allMatchesByQuery[key] == "undefined")
			return false;
		else {
			TextFindController.allMatchesByQuery[key].isActive = true;
			for (var i = 0; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
					TextFindController.allMatchesByQuery[key].isSend[i] = false;
			}
		}
		return true;
	};

	this.enableHighlightsOfType = function(et){
		for (var i = 0; i < activeHighlightTypes.length; i++){
			if (activeHighlightTypes[i] == et) { 
				return true;
			}
		}
		activeHighlightTypes.push(et);
		return false;
	};

	this.disableHighlights = function(et) {
		// et can be "SEARCH", "SYNONYMS", "CONCEPTS", "YEAR" etc
		// remove it first from the activeHighlights
		activeHighlightTypes = activeHighlightTypes.filter(function(elem){
			if (elem != et)
				return true;
			return false;
		});
		// disable the results of all the queries of type et from being send for rendering
	};

	this.destroy = function() {
		if (typeof this.workerThread != "undefined") {
			this.workerThread.terminate();
		}
		HighlightRenderingNormalizer.destroy();
		this.searchTextRequests = [];
		this.currentSearchId =0;
		DocumentEventListener.destroy();
		textSelectionPanel.destroy();
		if (!sapUI5Enabled) {
			MarkerCallout.destroy();
			commentPanel.destroy();
		}

		$("#viewer").children().each(function(){$(this).remove()});
		$(".pdfHiTags").each(
				function() {
					$(this).remove();
				}
		);
		TextFindController.clean();
		// clean any open callout
		for (var i = 0 ; i < openCallouts.length; i++)
		{
			openCallouts[i].stop(true,true).mouseout();
		}
		openCallouts = [];
		activeHighlightTypes = [];
		_textUrl = undefined;
		_docState = undefined;
		_textViewer = undefined;
		
	}
	
	this.showAllHighlights  = function() {
		$(".sap_hide_highlight").each(
				function() {
					$(this).css("visibility","visible");
					$(this).children().each(
						function() {
							$(this).css("visibility","visible");
						}
					);
				}
		);
	};

	var ReferencesExtractor = {
		referenceDeclarationPatterns : [/^\s*\[(\d*?)\]\s*/,/^\s*(\d+?)\s*\.\s*/g],
		referenceUsagePatterns: [/\[((\d+,)*\d+)\]/g, /\(((\d+,)*\d+)\)/g],
		
		findMatchingReferences: function(refUsa, refDec) {
			var validRefs = [];
			for (var i = 0 ; i < refUsa.length; i++) {
				for (var j = 0 ; j < refDec.length; j++) {
					if (refDec[j].id.toString().toUpperCase() === refUsa[i].toString().toUpperCase()) { 
						validRefs.push(refDec[j]);
						break;	
					}
				}
			}
			return validRefs;	
		},
		
		extactReferenceUsages: function(state,textTokens) {
			var tokensInPages = textTokens;
			
			for (var i = 0; i < tokensInPages.length; i++) {
				for (var j = 0; j < tokensInPages[i].length; j++) {
	
					if (tokensInPages[i][j].toUpperCase() === "References".toUpperCase()) {
						return;
					}
					var match;
	
					for (var iPdx = 0; iPdx < this.referenceUsagePatterns.length; iPdx++) {
						while((match = this.referenceUsagePatterns[iPdx].exec(tokensInPages[i][j]))) {
							
							// 1st group of match contains either \d or \d,\d,\d,..,\d
							var refsIds = match[1].split(',');
							var refs = this.findMatchingReferences(refsIds, state.references);
							if (refs.length == 0)
								continue;
	
							var position = {
								page : i+1,
								beginChild: j,
								beginOffset: match.index,
								endChild: j,
								endOffset:match.index + match[0].length
							};
							
							var info = {
								ids: refsIds,
								refers: refs
							};
							var refHighlight = new Highlight(state.nextHighlightId,state.nextHighlightId, highlightType.REFERENCE, position, {}, null, info);
							state.addHighlight(refHighlight, true);
							refHighlight.render();
						}
					}
				}			
			}
		},
		
		extractReferences: function(state, textTokens) {
			var tokensInPages = textTokens;
			var refStartPage = 0;
			var refStartToken = 0;
			for (var i = 0; i < tokensInPages.length; i++) {
				for (var j = 0; j < tokensInPages[i].length; j++) {
					if (tokensInPages[i][j].toUpperCase() === "References".toUpperCase()) {
						refStartPage = i;
						refStartToken = j+1;
					}
				}			
			}
			
			var references = [];
			var refId = 0;
	
			for (var i = refStartPage; i < tokensInPages.length; i++) {
				for (var j = refStartToken; j < tokensInPages[i].length; j++) {
					var match;
					var iPdx = 0;
					for (iPdx = 0; iPdx < this.referenceDeclarationPatterns.length; iPdx++) {
						match = this.referenceDeclarationPatterns[iPdx].exec(tokensInPages[i][j]);
						if (match){
							break;
						}
					}
					if (match) {
						if (match[1] == refId || match[1] == refId+1) {
							 references.push({
								 id: match[1],
								 title:tokensInPages[i][j].substring(match[0].length)
							 });
							 refId++;
						 }
						 // something like 2006. belongs to the previous ref if any
						 else if (refId > 0){
							 references[refId-1].title += tokensInPages[i][j];
						 }
	
					}
					else if (refId > 0){
						// consume tokens
						references[refId-1].title += tokensInPages[i][j];
					}
				}			
			}
			return references;
		},
	};
	
	/**
	 *	Event listener that handles events signaled from the PDFJS framework
	 *  currently support document initial load event, page load (re-draw) event 
	 */
	var DocumentEventListener = {
		
		events : [
	   		'documentLoaded',
	  		'pageLoaded',
	  		'textExtracted',
	  		'searchFinished',
	  		'textOfPageLoaded',
	  		'highlightsReady',
	  		'changeScale'
	   	],
	   	
	   	handleEvent :  function(e) {
	   		if (typeof e.detail.doi == "undefined")
	   			return;
	 		// ensure that this event was intended for the specific pdf
	 		var eDoi = e.detail.doi;
	 		// get Document State of this doi
	 		if (eDoi != _docState.doi)
	 			return;
	 		if (e.type === 'changeScale') {
			 	_docState.clearAllHighlights();
	 		}
	 		else if (e.type === 'textOfPageLoaded') {
	 		}
	 		if (e.type === 'searchFinished') {
	 			_searchActive = false;
	 			var searchIsNotFinished = _textViewer.executeNextSearch();
	 			if (!searchIsNotFinished) {
	 				var userCustomHighlights = _textViewer.getMatchesDistinctCountOfType(highlightType.CONCEPT_SEARCH);
	 				if (userCustomHighlights > 0 && $.inArray("USER_CONCEPT_SEARCH", activeHighlightTypes) != -1)
	 					publishEvent(constants.events.ADD_HIGHLIGHT_TYPE, {type:"USER_CONCEPT_SEARCH",count:userCustomHighlights, enabled:true});
	 			}
	 		}
	 		else if (e.type === 'highlightsReady') {
	 			var entType = e.detail.searchType;
	 			if (typeof e.detail.metadata != "undefined" && typeof e.detail.metadata.layer != "undefined")
	 			{
	 				entType = e.detail.metadata.layer;
	 			}
	 			if (!inArray(entType, activeHighlightTypes))
	 				return;

	 			var range = undefined;
	 			if (typeof e.detail.range != "undefined") {
 					range = e.detail.range;
 					if (e.detail.page < range.start.page ||
 						e.detail.page > range.end.page) {
 						return;
 					}
 				}
	 			
	 			for (var i = 0 ; i < e.detail.matches.length; i++) {
	 				// if a specific range was given, highlight only in the range
	 				if (typeof range != "undefined") {
	 					if (e.detail.page == range.start.page) {
	 						if (e.detail.matches[i].begin.divIdx < range.start.child ||
	 							e.detail.matches[i].end.divIdx < range.start.child) {
	 							continue;
	 						}
	 						
	 						else if (
	 								e.detail.matches[i].begin.divIdx == range.start.child &&
	 								e.detail.matches[i].begin.offset < range.start.offset) {
	 							continue;
	 						}
	 					}
	 					if (e.detail.page == range.end.page) {
	 						if (e.detail.matches[i].begin.divIdx > range.end.child ||
	 							e.detail.matches[i].end.divIdx > range.end.child) {
	 							continue;
	 						}
	 						else if (
								e.detail.matches[i].end.divIdx == range.end.child &&
 								e.detail.matches[i].end.offset > range.end.offset) {
	 							continue;
	 						}
	 					}
	 					
	 					
	 				}
	 				// create search highlights and add them in document state
					var position = {
						page : e.detail.page,
						beginChild : e.detail.matches[i].begin.divIdx,
						beginOffset: e.detail.matches[i].begin.offset,
						endChild: e.detail.matches[i].end.divIdx,
						endOffset: e.detail.matches[i].end.offset
					};

					var info = {
						term:e.detail.query
					};

					var highLight = new Highlight(_docState.nextHighlightId, _docState.nextHighlightId, e.detail.searchType, e.detail.entityType, position, e.detail.metadata, null, info);

					var renderHighlight = true;
				 	for (var j = 0; j < _docState.highlights.length; j++){
						if ( // skip if the same match has been send more than once
								_docState.highlights[j].entityType == highLight.entityType &&
								_docState.highlights[j].position.page == highLight.position.page &&
								_docState.highlights[j].position.beginChild == highLight.position.beginChild &&
								_docState.highlights[j].position.beginOffset == highLight.position.beginOffset &&
								_docState.highlights[j].position.endChild == highLight.position.endChild &&
								_docState.highlights[j].position.endOffset == highLight.position.endOffset
						)
						{
								renderHighlight = false;
						}

						if (clearOverlappingHighlights) {
							if( 
								(
								_docState.highlights[j].entityType == highLight.entityType &&
								_docState.highlights[j].position.page == highLight.position.page &&
								_docState.highlights[j].position.beginChild < highLight.position.beginChild &&
								_docState.highlights[j].position.endChild > highLight.position.endChild
								)
								||
								(
								_docState.highlights[j].entityType == highLight.entityType &&
								_docState.highlights[j].position.page == highLight.position.page &&
								_docState.highlights[j].position.beginChild == highLight.position.beginChild &&
								_docState.highlights[j].position.beginOffset <= highLight.position.beginOffset &&
								_docState.highlights[j].position.endChild == highLight.position.endChild &&
								_docState.highlights[j].position.endOffset >= highLight.position.endOffset
								)
							)
							{
								if(clearOverlappingHighlights) {
									renderHighlight = false;
								}
							}
							else if (
								(
								_docState.highlights[j].entityType == highLight.entityType &&
								_docState.highlights[j].position.page == highLight.position.page &&
								_docState.highlights[j].position.beginChild > highLight.position.beginChild &&
								_docState.highlights[j].position.endChild < highLight.position.endChild
								)
								||
								(
								_docState.highlights[j].entityType == highLight.entityType &&
								_docState.highlights[j].position.page == highLight.position.page &&
								_docState.highlights[j].position.beginChild ==highLight.position.beginChild &&
								_docState.highlights[j].position.beginOffset >= highLight.position.beginOffset &&
								_docState.highlights[j].position.endChild == highLight.position.endChild &&
								_docState.highlights[j].position.endOffset <= highLight.position.endOffset
								)
							)
							{
								if(clearOverlappingHighlights) {
									_docState.highlights[j].removeHighlight();
									_docState.removeHighlight(j);
									renderHighlight = true;
								}
							}
				 		}
				 	}
					if (renderHighlight) {
						_docState.addHighlight(highLight, true);
						highLight.render();
					}
				}
	 		}
	    	else if (e.type === 'pageLoaded') {
	    	}
	    	else if (e.type == 'documentLoaded') {
	    		TextExtractor.extractText();
	    	}
	    	else if (e.type == 'textExtracted') {
				publishEvent(constants.events.TEXT_READY, {});
	    	}
	  	},
	  	
	  	initialize : function() {
		  	this.handleEvent = this.handleEvent.bind(this);
	    	for (var i = 0; i < this.events.length; i++) {
	     		window.addEventListener(this.events[i], this.handleEvent);
	    	}
	    },
	    
	    destroy: function() {
	    	for (var i = 0; i < this.events.length; i++) {
	     		window.removeEventListener(this.events[i], this.handleEvent);
	    	}
	    }
	};
	
	/**
	 *	Responsible for renderring all corresponding information of a document state 
	 */
	var DocumentStateRenderer = {
		activeHighlights : [highlightType.CONCEPT_SEARCH],
		clearAllHighlightLayers: function(pageIdx) {
			$("#pageContainer"+pageIdx + " .layer").each(
				function() {
					$(this).remove();
				}
			);
		},
		
		renderPersistedHighlights: function(documentState, pageIdx) {
			for(var i = 0; i < documentState.highlights.length; i++) {
				if (documentState.highlights[i].page != pageIdx) {
					 continue;
				} 

				if (inArray(documentState.highlights[i].type ,this.activeHighlights)) {
					documentState.highlights[i].render();
				}
			}	
		},
		
		
		renderPageOfState: function(documentState, pageIdx) {
			this.clearAllHighlightLayers(pageIdx);
			this.renderPersistedHighlights(documentState, pageIdx);
			documentState.loadedPages[pageIdx-1] = true;
		},
		
		reRenderLoadedPages: function(documentState) {
			for (var pageIdx in documentState.loadedPages) {
				if(documentState.loadedPages[pageIdx]) {
					this.clearAllHighlightLayers(pageIdx);
					this.renderPersistedHighlights(documentState,pageIdx);
				}
			}
		}
	};

	
	
	/**
	 * Represents the highlight entity, all information for how to render it and interact with callbacks 
	 * @param {Object} highlightId
	 * @param {Object} logicalId
	 * @param {Object} highlightT
	 * @param {Object} highlightPos
	 * @param {Object} hoverCallback
	 */
	function Highlight(highlightId, logicalId, highlightT, highlightT2, highlightPos, highlightMetadata, hoverCallback, highlightInfo) {
		this.highlightId = highlightId;
		this.logicalId = logicalId;
		this.type = highlightT;
		this.entityType = highlightT2;
		this.position = highlightPos;
		this.page = highlightPos.page;
		this.hoverCallback = hoverCallback;
		this.info = highlightInfo;
		this.metadata = highlightMetadata;
		this.cssClassName = undefined;
		this.domElement = undefined;
		this.domContainer = undefined;
		this.markerCalloutInfo = undefined;

		var self = this;
	
		if (this.type == highlightType.MARKER) {
			this.cssClassName = highlightTypeToCss.MARKER;
		}
		else if(this.type == highlightType.REFERENCE){
			this.cssClassName = highlightTypeToCss.REFERENCE;
		}		
		else if(this.type == highlightType.SEARCH){
			this.cssClassName = highlightTypeToCss.SEARCH;
		}
		else if (this.type == highlightType.CONCEPT_SEARCH) {
			this.cssClassName = highlightTypeToCss.CONCEPT_SEARCH;
		}
		else if (this.type == highlightType.TEXT_SELECTION) {
			this.cssClassName = highlightTypeToCss.TEXT_SELECTION;
		}
		else if (this.type == highlightType.CUSTOM) {
			this.cssClassName = this.metadata.cssClassName || highlightTypeToCss.CUSTOM;
		}
		else if (this.type == highlightType.SYNONYMS) {
			this.cssClassName = this.metadata.cssClassName || highlightTypeToCss.SYNONYMS;
		}
		
		this.removeHighlight = function() {
			$("#layer"+ this.highlightId).remove();
		};

		this.isRendered = function() {
			if ($("#layer"+ this.highlightId).length == 0)
				return false;
			if ($("#layer"+ this.highlightId).find(".sap_custom_highlight > span").length == 0)
				return false;

			$("#layer"+ this.highlightId).find(".sap_custom_highlight > span").each(
					function() {
						if ($(this).css("visibility") == "hidden" ||
							$(this).css("display") == "none")
							return false;
					}
			);
			return true;
		};
		this.render = function() {
			// create the layer 
			
			var layer;
			if (typeof this.metadata != "undefined" &&
				typeof this.metadata.layer != "undefined") {
					layer= $("<div>").addClass("layer")
						.attr("layer", this.metadata.layer)
	        			.attr('id',"layer"+this.highlightId)
	        			.appendTo("#pageContainer"+this.page);
			}
			else {
				layer= $("<div>").addClass("layer")
	    			.attr('id',"layer"+this.highlightId)
	    			.appendTo("#pageContainer"+this.page);
			}

			if (this.type == highlightType.TEXT_SELECTION) {
				layer.attr("id","textSelectionLayer"+this.page);
				layer.addClass("textSelectionLayer");
			}

			this.domContainer = layer;
			layer.html("");

			var inHighlightLayerCntr = 0;
			for (var divIdx = this.position.beginChild; divIdx <= this.position.endChild; divIdx++) {
				var jQSelIdx = divIdx+1;
				var elem = $("#pageContainer"+this.position.page+" .textLayer > div:nth-child("+jQSelIdx+")").clone();
				elem.html(elem.text());
				layer.append(elem);
	
				var jsElem;
				var layersOfPage = document.getElementById("pageContainer"+this.position.page).getElementsByClassName("layer");
				if (this.type != highlightType.TEXT_SELECTION) {
					for (var iCL = 0; iCL < layersOfPage.length; iCL++) {
						if (layersOfPage[iCL].id === "layer"+this.highlightId) {
							jsElem = layersOfPage[iCL].childNodes[inHighlightLayerCntr];
							break;
						}
					}
				}
				else {
					for (var iCL = 0; iCL < layersOfPage.length; iCL++) {
						if (layersOfPage[iCL].id === "textSelectionLayer"+this.page) {
							jsElem = layersOfPage[iCL].childNodes[inHighlightLayerCntr];
							break;
						}
					}
				}

				if (typeof jsElem == "undefined" || !jsElem)
				{
					layer.remove();
					return;
				}

				var elemText = elem.text();
				var startOffset, endOffset;

				if (this.position.beginChild == this.position.endChild) {
					// if same line
					elem.html("");
					
					startOffset = this.position.beginOffset;
					endOffset = this.position.endOffset;
					// create the beginning node
					beginText(false, inHighlightLayerCntr, jsElem,startOffset, elemText);
					// highlight the span
					if (this.type == highlightType.TEXT_SELECTION)
						appendText(true, inHighlightLayerCntr,jsElem, startOffset, endOffset, elemText, " " +highlightGlobalCss);
					else
			        	appendText(false, inHighlightLayerCntr,jsElem, startOffset, endOffset, elemText, " " +highlightGlobalCss);
			        // create the ending node
			        appendText(false, inHighlightLayerCntr, jsElem,endOffset, elemText.length, elemText);
				}
				else if (divIdx == this.position.beginChild) {
					// first line of multi-line
					elem.html("");
					
					startOffset = this.position.beginOffset;
					endOffset = elemText.length;
					// create the beginning node
					beginText(false, inHighlightLayerCntr, jsElem,startOffset, elemText);
					// highlight the span
					if (this.type == highlightType.TEXT_SELECTION)
						appendText(true, inHighlightLayerCntr, jsElem, startOffset, endOffset, elemText," begin " +highlightGlobalCss);
					else
						appendText(false, inHighlightLayerCntr, jsElem, startOffset, endOffset, elemText," begin " +highlightGlobalCss);
				}
				else if (divIdx == this.position.endChild) {
					// last line of multi-line
					elem.html("");
					
					startOffset = 0;
					endOffset = this.position.endOffset;
					if (this.type == highlightType.TEXT_SELECTION)
						beginText(true,  inHighlightLayerCntr, jsElem,endOffset, elemText, " end " +highlightGlobalCss);
					else
						beginText(false, inHighlightLayerCntr, jsElem,endOffset, elemText, " end " +highlightGlobalCss);
					if (this.type == highlightType.TEXT_SELECTION) {
						appendText(true, inHighlightLayerCntr, jsElem, endOffset, elemText.length, elemText);
					}
				}
				else {
					// middle lines of multi-line
					if (this.type == highlightType.TEXT_SELECTION)
						highlightFullLine(true, inHighlightLayerCntr, jsElem, " middle " +highlightGlobalCss);
					else
						highlightFullLine(false, inHighlightLayerCntr, jsElem, " middle " +highlightGlobalCss);
				}
				inHighlightLayerCntr++;
				
				
				
				this.domElement = $("#pageContainer"+this.position.page+" #layer"+this.highlightId+" > div:nth-child("+inHighlightLayerCntr+") > ." +highlightGlobalCss);
				// create marker callout content and sizing
				if (this.type == highlightType.REFERENCE) {
					var refStrs = "";
					for (var i = 0 ; i < this.info.refers.length; i++) {
						refStrs += this.info.refers[i].title;
					}
					this.markerCalloutInfo = {
						width  : 200,
						height : 600,
						html   : refStrs
					};
				}
				else if (this.type == highlightType.SEARCH) {
					this.markerCalloutInfo = {
						width  : 400,
						height : 500,
						html   : this.info.term
					};
				}
				
				if (!sapUI5Enabled) {
					$("#pageContainer"+this.position.page+" #layer"+this.highlightId+" > div:nth-child("+inHighlightLayerCntr+") > ." +highlightGlobalCss).stop(true,true).hover(function(){
							self.onHover();
					});
				}
			 }
		};
		

		function highlightFullLine(sync, nl, elem, className) {
			elem.className = className + " sap_hide_highlight";
			elem.id = self.cssClassName+self.highlightId+"_line_"+nl;
			if (sapUI5Enabled && !sync) {
				var textContent = elem.textContent;
				elem.textContent = '';
				elem.innerHTML = "";
				elem.innerText = "";
				publishEvent(constants.events.RENDER_TEXT_HIGHLIGHT, {"container": elem.id, "type":self.type, "text":textContent, "containerClass":self.cssClassName+self.highlightId + " " + self.cssClassName, "metadata":self.metadata, "id":self.highlightId,  "calloutText":self.info.term});
			}
			else 
				elem.className = className + " " + self.cssClassName;
		};
		
	    function beginText(sync, nl, elem, to, lineText, className) {
	    	if (to == 0) 
	    		return;
		    var content = lineText.substring(0, to);
		   	var node = document.createTextNode(content);
		    
		    if (className) {
		    	if (sapUI5Enabled && !sync) {
		    		var span = document.createElement('span');
		    		span.id = self.cssClassName+self.highlightId+"_line_"+nl;
		    		span.className = className + " sap_hide_highlight";
		    		// attach it to some element in the page
		    		elem.appendChild(span);
		    		publishEvent(constants.events.RENDER_TEXT_HIGHLIGHT, {"container": span.id, "type":self.type, "text" : content, "containerClass":self.cssClassName+self.highlightId + " " + self.cssClassName,"metadata":self.metadata,"id":self.highlightId, "calloutText":self.info.term});
		    		return;
		    	}
		    	else {
		    		var span = document.createElement('span');
		    		span.className = className + " " +self.cssClassName+self.highlightId + " "+ self.cssClassName;
		    		span.id = self.cssClassName+self.highlightId+"_line_"+nl;
		    		span.appendChild(node);
		    		elem.appendChild(span);
		    		return;
		    	}
		    }
		    elem.appendChild(node);
	 	};
	
	    function appendText(sync, nl, elem,from, to, lineText, className) {
	    	if (from == to)
	    		return;
	    	var content = lineText.substring(from, to);
	      	var node = document.createTextNode(content);
	      	if (className) {
		    	if (sapUI5Enabled && !sync) {
		    		var span = document.createElement('span');
		    		span.className = className + " sap_hide_highlight";
		    		span.id = self.cssClassName+self.highlightId+"_line_"+nl;
		    		elem.appendChild(span);
		    		publishEvent(constants.events.RENDER_TEXT_HIGHLIGHT, {"container": span.id, "type":self.type, "text" : content, "containerClass":self.cssClassName+self.highlightId + " " + self.cssClassName,"metadata":self.metadata,"id":self.highlightId, "calloutText":self.info.term});
		    		return;
		    	}
		    	else {
		    		var span = document.createElement('span');
		    		span.className = className + " " +self.cssClassName+self.highlightId + " " + self.cssClassName;
		    		span.id = self.cssClassName+self.highlightId+"_line_"+nl;
		    		span.appendChild(node);
		    		elem.appendChild(span);
		    		return;
		    	}
	        }
	        elem.appendChild(node);
	    };
		
		
	    this.onHover = function() {
	    	if (sapUI5Enabled)
	    		return;
	    	
			if (this.type == highlightType.REFERENCE) {
				MarkerCallout.show(this.domElement,this.markerCalloutInfo);
			}
			else if (this.type == highlightType.SEARCH) {
				MarkerCallout.show(this.domElement,this.markerCalloutInfo);
			}
			else if (this.type == highlightType.MARKER) {
		
			}
		};
	};

	function getSelected() {
		try {
		    var selected = '';
			var selection = undefined;
			
		    if (window.getSelection()) {
		        selection = jQuery.extend(true,{},window.getSelection().getRangeAt(0));
			    selected = window.getSelection().getRangeAt(0).toString();
		    } else if (document.getSelection()) {
		    	selection = jQuery.extend(true,{},document.getSelection().getRangeAt(0));
			    selected = window.getSelection().getRangeAt(0).toString();
		    } else if (document.selection()) {
		    	selection = jQuery.extend(true,{},document.selection.createRange());
			    selected = window.getSelection().getRangeAt(0).toString();
		    }
		    return [selected,selection];
		} catch(err) {
			return [];
		}
	};
	
	var searchTokens = [];

	$(document).mouseup(function (e)
	{
		e.preventDefault();
		if (typeof _textViewer == "undefined")
			return;

		if (textSelectionPanel.clickedOnText(e.target)) {
			var userTextSelection = getSelected();
			if (userTextSelection.length != 2)
				return;
			var selectedText = userTextSelection[0];
			var selectedRange = userTextSelection[1];
			if (selectedText.toString().length > 0 ) {
				textSelectionPanel.show(userTextSelection);
			}
			else if (!sapUI5EnabledTextSelectionPanel){
				if (textSelectionPanel.isVisible())
				{
					textSelectionPanel.hide();
				}
			}
		} else if (!textSelectionPanel.findSelectionPanel(e.target) && !sapUI5EnabledTextSelectionPanel) {
			if (textSelectionPanel.isVisible())
			{
				textSelectionPanel.hide();
			}
		}
	});
			
	
	
	var textSelectionPanel = {
		jelem: $("#selectionPanel"),
		hiding: false,
		selectionText: undefined,
		selectionRange: undefined,
		currentOffset: 0,
		comment:null,
		initialize: function(){
			if (!sapUI5EnabledTextSelectionPanel) {
				
				var _html = "<img id=\"panelHanger\" src=\"./images/placeholderPopup_arrow_right_custom.png\"/><table id=\"selectionTable\" cellspacing=\"4\">"+
		        "<tbody>";
				
				_html += "<tr class=\"selectionWrapper sap_highlight_callout_action\" id=\"searchWrapper\">" +
	            "<td><img src=\"./images/ns2_logo_search_trans.gif\"/></td>" +
	            "<td><div>Search Catalog for Selected Items</div></td>" +
	            "</tr>";
	          	
				_html += "<tr class=\"selectionWrapper sap_highlight_callout_action\" id=\"searchConceptWrapper\">" +
	   			"<td><img src=\"./images/mark_concepts.png\"/></td>" +
	   			"<td><div>Mark Item(s) in Highlighted Text</div><span id=\"hiddenFullText\"></span></td>" +
	   			"</tr>";
	          	
				_html += "</tbody>" +
		        "</table><span id=\"textSelectionPosition\" style=\"display:none;\"></span>";

				
				var ac = $("#selectionPanel");
				if (ac.length > 0) {
					ac.addClass("sap_custom_callout");
					ac.html(_html);
				} else {
					$('<div>').attr('id',"selectionPanel").addClass("sap_custom_callout").html(_html)
					.appendTo('body');
					ac = $("#selectionPanel");
					ac.css("width",SELECTIONPANEL_WIDTH + 'px');
					ac.css("max-width",SELECTIONPANEL_WIDTH + 'px');
					ac.css("max-height",SELECTIONPANEL_HEIGHT + 'px');
					ac.css("min-height",SELECTIONPANEL_MIN_HEIGHT + 'px');
				}
				
		  		this.jelem = ac;
		  		ac.css("visibility","hidden");
				
				$("#searchWrapper").click(
					function(){
						if (!textSelectionPanelForDemo) {
							textSelectionPanel.hide(0);
							publishEvent(constants.events.TEXT_LITERAL_SEARCH, { text: $("#searchText").html()});
						}
						else {
							textSelectionPanel.hide(0);
							publishEvent(constants.events.TEXT_LITERAL_SEARCH_OF_CONCEPTS, { text: $("#hiddenFullText").html()});
						}
					}
				);
				
				$("#searchConceptWrapper").click(
					function() {
						var selPos =$("#textSelectionPosition").text().match(/start\((\d+?),(\d+?),(\d+?)\)-end\((\d+?),(\d+?),(\d+?)\)/)
						var range = {
							"start": {
								"page":selPos[1],
								"child":selPos[2],
								"offset":selPos[3],
							},
							"end" : {
								"page":selPos[4],
								"child":selPos[5],
								"offset":selPos[6]
							}
						};
						if (!textSelectionPanelForDemo)
							textSelectionPanel.hide(0);
						publishEvent(constants.events.TEXT_CONCEPT_SEARCH, { text: $("#hiddenFullText").html(), callback: _textViewer.searchText, range:range});
					}
						
				);

				$("#searchGoogleWrapper").click(
					function() {
						if (textSelectionPanelForDemo) {
							textSelectionPanel.hide(0);
						}
						window.open(
								"http://www.google.com/search?q="+$("#hiddenFullText").html(),
								'_blank'
						);
					}
				);

				$("#markWrapper").click(
					function(){
						textSelectionPanel.hide(0);
						textSelectionPanel.addHighlight(highlightType.MARKER, true);
					}
				);
			}
		},
		findPageOfUserSelection: function(node){
			var curNode = node;
			if (curNode.className == undefined && curNode.id == undefined) {
				if (curNode.parentNode)
					return this.findPageOfUserSelection(curNode.parentNode);
			} else if (curNode.className !== "page" && curNode.id.indexOf("pageContainer") != 0) {
				if (curNode.parentNode)
					return this.findPageOfUserSelection(curNode.parentNode);
			} else {
				return parseInt(curNode.id.substr("pageContainer".length));
			}
		},
		
		findDivOfUserSelection: function(node, isStartContainer){
			var curNode = node;
			if (isStartContainer) {
				if ($(curNode).find(".textLayerDiv").length == 0) {
					return $(curNode).parents(".textLayerDiv").first()[0];
				}
				else {
					return $(curNode).find(".textLayerDiv").first()[0];
				}
			}
			else {
				if ($(curNode).find(".textLayerDiv").length == 0) {
					return $(curNode).parents(".textLayerDiv").first()[0];
				}
				else {
					return $(curNode).find(".textLayerDiv").last()[0];
				}
			}
		},

		findSelectionPanel: function(node){
			var curNode = node;
			if (!curNode)
				return false;
			if (curNode.className == undefined && curNode.id == undefined) {
				if (curNode.parentNode)
					return this.findSelectionPanel(curNode.parentNode);
				else
					return false;
			} else if (curNode.id.indexOf("selectionPanel") != 0) {
				if (curNode.parentNode)
					return this.findSelectionPanel(curNode.parentNode);
				else
					return false;
			} else {
				return true;
			}
		},
		calculateOffsetInLine: function(node, curNode) {
			var currentOffset = 0;
			if (node == curNode)
				return currentOffset;
			for (var i = 0; i < curNode.childNodes.length; i++) {
				if (node == curNode.childNodes[i]) {
					return currentOffset;
				}
				else if (curNode.childNodes[i].nodeType == 1) {
					currentOffset += this.calculateOffsetInLine(node, curNode.childNodes[i]);
					return currentOffset;
				} 
				else if (curNode.childNodes[i].nodeType == 3) {
					currentOffset += curNode.childNodes[i].length;
				}
			}
			return currentOffset;
		},
	
		addHighlight: function(type,persist){
			var startPage 	= this.findPageOfUserSelection(this.selectionRange.startContainer);
			var endPage		= this.findPageOfUserSelection(this.selectionRange.endContainer);
			var startDiv 	= this.findDivOfUserSelection(this.selectionRange.startContainer, true);
			var endDiv		= this.findDivOfUserSelection(this.selectionRange.endContainer, false);
			var startOffset = 0;
			if (typeof startDiv == "undefined" || typeof endDiv == "undefined") {
				this.hide(0);
				return false;
			}
			if (this.selectionRange.startContainer.className != "page")
				startOffset = this.selectionRange.startOffset + this.calculateOffsetInLine(this.selectionRange.startContainer, startDiv);
			var endOffset 	= endDiv.innerHTML.length;
			if (this.selectionRange.endContainer.className != "page")
				endOffset = this.selectionRange.endOffset + this.calculateOffsetInLine(this.selectionRange.endContainer, endDiv);
			var startDivIndex,endDivIndex;

			for (var ii = 0 ; ii < startDiv.parentNode.children.length; ii++){
				if (startDiv.parentNode.children[ii] === startDiv) {
					startDivIndex = ii;
					break;
				}
			}
			for (var ij = ii ; ij < endDiv.parentNode.children.length; ij++){
				if (endDiv.parentNode.children[ij] === endDiv) {
					endDivIndex = ij;
					break;
				}
			}	
			if (type == highlightType.TEXT_SELECTION) {
				$("#textSelectionPosition").html("start("+startPage+","+startDivIndex+","+startOffset+")-end("+endPage+","+endDivIndex+","+endOffset+")");
			}
			var curStartDivChild, curEndDivChild, curStartDivOffset, curEndDivOffset;
			var logicalId = _docState.nextHighlightId;
	
			for (var pageIdx = startPage; pageIdx <= endPage; pageIdx++) {
				if (pageIdx == startPage && pageIdx < endPage) {
					// if first page of a multi-page comment
					curStartDivChild = startDivIndex;
					curEndDivChild = $("#pageContainer"+pageIdx+" .textLayer > div").length-1;
					curStartDivOffset = startOffset;
					curEndDivOffset = $("#pageContainer"+pageIdx+" .textLayer > div:nth-child("+curEndDivChild+")").text().length;
				}
				else if (pageIdx == endPage && startPage < endPage) {
					// if last page of a multi-page comment
					curStartDivChild = 0;
					curEndDivChild = endDivIndex;
					curStartDivOffset = 0;
					curEndDivOffset = endOffset;
				}
				else if (startPage == endPage) {
					// single page
					curStartDivChild = startDivIndex;
					curEndDivChild = endDivIndex;
					curStartDivOffset = startOffset;
					curEndDivOffset = endOffset;
				}
				else {
					curStartDivChild = 0;
					curStartDivOffset = 0;
					curEndDivChild = $("#pageContainer"+pageIdx+" .textLayer > div").length-1;
					curEndDivOffset = $("#pageContainer"+pageIdx+" .textLayer > div:nth-child("+curEndDivChild+")").text().length;
				}
				
				var highlightinfo = {};
				if (type == highlightType.COMMENT) {
					info["comment"] = this.comment;
				}
				var position = { 
					 page : pageIdx,
					 beginChild : curStartDivChild,
					 beginOffset: curStartDivOffset,
					 endChild: curEndDivChild,
					 endOffset: curEndDivOffset
				};

				var metadata = {};

				var highLight = new Highlight(_docState.nextHighlightId, logicalId, type, type, position, metadata, null);
				highLight.render(true);
				if (persist) {
					_docState.addHighlight(highLight);
				}
			}
			return true;
		},
	
		show: function(userSelection, event){
			if(userSelection) {
				this.selectionRange = userSelection[1];
				// remove any previous textSelection layers
				$(".textSelectionLayer").each(function(){
					$(this).remove();
				});

				var ret = this.addHighlight(highlightType.TEXT_SELECTION, false);
				if (!ret)
					return;
				var selectedtext = "";
				var prevTop = -1;
				var curTop  = -1;
				var threshold = 3;

				$(".textSelectionLayer .sap_textselection_highlight").each(
					function() {
						curTop = parseInt($(this).offset().top);
						if (Math.abs(curTop -prevTop) < threshold || prevTop == -1)
						{
							// same line
							selectedtext += $(this).text();
						}
						else {
							// new line
							selectedtext += ("\n" + $(this).text());
						}
						prevTop = curTop;
					}
				);
				this.selectionText = selectedtext;
			}
			
			this.hiding = false;
	
			var ac = $("#selectionPanel");
			this.jelem = ac;

			var dockElem = $(".textSelectionLayer").first().find("." +highlightGlobalCss).first();
			if (typeof HANGER_TOP_OFFSET_IN_PANEL == "undefined") {
				HANGER_TOP_OFFSET_IN_PANEL = ((this.jelem.outerHeight() - 6)/2 - $("#panelHanger").height()/2);
			}
			var offset_top  = $(".textSelectionLayer").children().first().offset().top - HANGER_TOP_OFFSET_IN_PANEL;
			if (dockElem.offset().top < $("#viewerContainer").offset().top)
				offset_top = $("#viewerContainer").offset().top - HANGER_TOP_OFFSET_IN_PANEL;

			var offset_left = dockElem.offset().left;
			var elem_width  = dockElem.width();

			if (offset_top < 0)
				offset_top = $("#viewerContainer").offset().top;
			else if (offset_top + (this.jelem.height() + 10) >= $(document).height())
				offset_top = $(document).height() -  (this.jelem.height() + 10);
			ac.css("top",offset_top+'px');

			$("#panelHanger").css("top", HANGER_TOP_OFFSET_IN_PANEL-5 + 'px');

			var offset_right 	= ($(document).width()- offset_left);
			
			$("#panelHanger").css("left", (this.jelem.width() +20) + 'px');
			$("#panelHanger").css("transform","rotate(0deg)");
			$("#panelHanger").css("-ms-transform", "rotate(0deg)");
			$("#panelHanger").css("-webkit-transform","rotate(0deg)");

			var left = offset_left - this.jelem.width() - 40;
			if (left < 0) left = 0;
			ac.css("left",left + 'px');
			this.jelem.css("display","block");
			this.jelem.css("visibility","visible");
			$("#hiddenFullText").html(this.selectionText);
			if (textSelectionPanelForDemo) {
				$("#searchWrapper").css("visibility","visible");
				$("#searchWrapper").css("display","table-row");
			}
			else {
				if (this.selectionText.split(/[ \r\n]/).filter(function(elem) { return !(elem == "");}).length <= MAX_WORDS_FOR_LITERAL_SEARCH) {
					$("#searchText").html(this.selectionText);
					$("#searchWrapper").css("visibility","visible");
					$("#searchWrapper").css("display","table-row");
				}
				else {
					$("#searchText").html("");
					$("#searchWrapper").css("visibility","hidden");
					$("#searchWrapper").css("display","none");
				}
			}
		},
		clickedOnText : function(e) {
			var pageParent = this.findPageOfUserSelection(e);
			if (typeof pageParent != "undefined") {
				return true;
			}
			return false;
		},
		clickedOnPanel: function(e){

		},
		
		isVisible: function() {
			return (this.jelem.css("visibility") == "visible") ? true:false;
		},
	
		
		hide: function(delay){
			if (typeof delay === "undefined") {
				delay = 300;
			}
			if (this.isVisible()) {
				this.hiding = true;
				var that = this;
				setTimeout(function(){
					if (that.hiding == false)
						return;
					// remove any previous textSelection layers
					$(".textSelectionLayer").each(function(){
						$(this).remove();
					});
					that.jelem.stop(true,true).fadeOut(delay,"linear" , function(){
						if (that.hiding) {
							that.jelem.css("visibility","hidden");
							that.hiding = false;
						}
						else {
							that.show();
						}
					});
				}, delay);
			}
		},
		destroy: function() {
			// 	keep non-div selectionPanels
			$("div#selectionPanel").remove();
			$("#selectionPanel").remove();
		}
	};
};

if (!sapUI5Enabled) {
	var MarkerCallout = {
		jelem: $("#callout"),
		jMarkerElem : undefined,
		jparElem : undefined,
		pageElem: undefined,
		divElemInTextLayer:undefined,
		hideTimeout: undefined,
		hiding:false,
		content:undefined,
		willShow:false,
		
		applyDefaultCss: function() {
			this.jelem.css("z-index",'10001');
			this.jelem.css("width",CALLOUT_WIDTH + 'px');
			this.jelem.css("max-width",CALLOUT_WIDTH + 'px');
			this.jelem.css("height",CALLOUT_HEIGHT + 'px');
			this.jelem.css("max-height",CALLOUT_HEIGHT + 'px');
			this.jelem.html("Default Content");
		},
		
		initialize: function(){
			$('<div>').attr('id',"callout")
			  .html("")
			  .appendTo('body');
	  		var ac = $("#callout");
	  		this.jelem = ac;
	  		ac.css("visibility","hidden");
			ac.css("position","absolute");
			this.applyDefaultCss();
			ac.css("top","0px");
			ac.css("left","0px");
			var that = this;
			ac.hover(
				function(){
					that.stopHiding();
				},
				function(){
					that.hide(false);
				}
			);
		
		},
		
		
		show: function(elem, markerContent){
			var that = this;
			this.willShow = true;
			if (markerContent) {
				this.jelem.css("width",markerContent.width+"px");
				this.jelem.css("height",markerContent.height+"px");
				this.jelem.html(markerContent.html);
			}
			else {
				this.applyDefaultCss();
			}
			this.stopHiding();
			
			
			this.jMarkerElem = elem;
	 		this.jparElem = elem.parent();
			var curElem = elem;
			while(!curElem.hasClass('page')) {
				curElem = curElem.parent();
			}
			this.pageElem = curElem;
			this.pageElem.children().each(
				function(i) {
					if ($(this).hasClass("textLayer")) {
						$(this).children().each(
							function(j) {
								if ($(this).text() == that.jparElem.text() || $(this).text() == that.jMarkerElem.text()) {
									that.divElemInTextLayer = $(this);
									that.divElemInTextLayer.css("cursor","pointer");
								}				
							}
						);
					}	
				}
			);
			
			var offset = elem.offset();
			var offset_right 	= ($(window).width()- offset.left);
			var offset_bottom 	= ($(window).height()- offset.top);
			
			// decide where to place callout left, right top bottom of the marker
			var ac = $("#callout");
			if ( ( offset_bottom >= offset.top || offset_bottom > this.jelem.height() + 20)  && offset_right > offset.left) {
				//place it bottom- left
				var left = offset.left - this.jelem.width();
				if (left < 0) left = 0;
				ac.css("top",offset.top + 'px');
				ac.css("left",left + 'px');
				
			} else if (( offset_bottom >= offset.top || offset_bottom > this.jelem.height() + 20 )  && offset_right <= offset.left) {
				//place it bottom- right
				ac.css("top",offset.top + 'px');
				ac.css("left",(offset.left + elem.width())+ 'px');
							
			} else if (( offset_bottom < offset.top || offset_top > this.jelem.height() + 20 ) && offset_right > offset.left) {
				//place it top -left
				var top = offset.top - this.jelem.height();
				if (top < 0) top = 0;
				var left = offset.left - this.jelem.width();
				if (left < 0) left = 0;
				
				ac.css("top",top + 'px');
				ac.css("left",left +'px');
				
			} else {
				// place it top -right
				var top = offset.top - this.jelem.height();
				if (top < 0) top = 0;
			
				ac.css("top",top + 'px');
				ac.css("left",(offset.left + elem.width()) + 'px');
				// find element in text layer 
				
			}
			ac.css("display","block");
			ac.css("visibility","visible");
		},
		
		isVisible: function() {
			return (this.jelem.css("visibility") == "visible") ? true:false;
		},
		
		isCloseToMarker: function(){
			return this.isOnCallout() ||  this.jparElem.hasClass('hovered') || this.jMarkerElem.hasClass('hovered');
		},
		
		isOnCallout: function(){
			return this.jelem.hasClass('hovered') ? true:false;
		},
		
		isCalloutOfElem: function(elem) {
			if (elem === this.divElemInTextLayer || elem == this.divElemInTextLayer.parent()) {
			}
		},
		
		stopHiding: function(){
			this.hiding = false;
			if (this.hideTimeout)
				clearTimeout(this.hideTimeout);	
		},
		
		hide: function(force){
			var that = this;
			if (this.isVisible() && !force) {
				this.hiding = true;
				this.hideTimeout = setTimeout(
					function(){
						if (that.hiding == false)
							return;
						that.jelem.stop(true,true).fadeOut(600,"linear" , function(){
							if (that.hiding) {
								that.divElemInTextLayer.css("cursor","text");
								that.willShow = false;
								that.jelem.css("visibility","hidden");
								that.hiding = false;
							}
							else {
								that.stopHiding();
							}
						});
					},800);
			}
			else if (this.isVisible() && force){
				this.jelem.stop(true,true).fadeOut(600,"linear" , function(){
					that.jelem.css("visibility","hidden");
				});
			}
		},
		destroy: function() {
			this.jelem.remove();
		}
	};
}

var TextViewerUtil = {
	repairExtractedText : function(text) {
		// join wrapped words 
		text = text.replace(new RegExp("[-]\n", "g"), "");
		// replace remaining linebreaks with a single space  
		text = text.replace(new RegExp("\n", "g"), " ");
		return text;
	},

	/**
	 * Returns a list of highlight->term records present in the text.
	 * 
	 * Parameter:
	 * str - original text with linebreaks ('\n') and dashes in wrapped words
	 * terms - terms present in the original text
	 */
	convertTermsToHighlights : function(str, terms) {
		var highlights = [];
		// match each term in string either full or in parts at line breaks
		for (var i = 0; i < terms.length; i++) {
			var term = terms[i];
			for (var strP = 0; strP < str.length; strP++) {
				var parts = [];
				var currentPartStart = 0;
				for (termP = 0; termP < term.length; termP++) {
					var termC = term.charAt(termP);
					var strC = str.charAt(strP + termP);
					if (termC != strC)  {
						if ((strC == "\n") && (termC == " ")) {
							// remember match part
							parts.push(term.substring(currentPartStart, termP));
							currentPartStart = termP + 1;
							continue;
						} else if ((strC == "-") && (strP + termP < str.length) && (str.charAt(strP + termP + 1) == "\n")) {
							// remember match part
							parts.push(term.substring(currentPartStart, termP) + "-");						
							currentPartStart = termP;
							termP -= 1; // one step back
							strP += 2; // consume "-\n"
							continue;
						}
						// no match
						parts = [];
						currentPartStart = 0;
						break;
					} else if (termP == term.length -1) {
						// matched last part
						parts.push(term.substring(currentPartStart, termP + 1));
						currentPartStart = termP + 1;
					} else {
						// continue scan
					}
				}
				// add parts
				for (var j = 0; j < parts.length; j++) {
					highlights.push({ metadata : { term : terms[i], layer : "USER_CONCEPT_SEARCH" }, highlight : parts[j]})
				}
			}
		}

		// remove duplicates from found highlight terms
		var cmp = function(a, b) {
			if (a.term > b.term) {
				return 1;
			} else if (a.term < b.term) {
				return -1;
			} else {
				return (a.highlight > b.highlight) ? 1 : ((a.highlight < b.highlight) ? -1 : 0);  
			}
		}
		highlights.sort(cmp);
		for (var i = 1; i < highlights.length; ) {
			if (cmp(highlights[i-1], highlights[i]) === 0) {
				highlights.splice(i, 1);
			} else {
				i++;
			}
		}

		return highlights;
	}	
}
