function JSONscriptRequest(text, fullUrl) {
	this.searchTerm = text;
    // REST request path
    this.fullUrl = fullUrl; 
    // Get the DOM location to put the script tag
    this.headLoc = document.getElementsByTagName("head").item(0);
    // Generate a unique script tag id
    this.scriptId = 'JscriptId' + JSONscriptRequest.scriptCounter++;
}

// Static script ID counter
JSONscriptRequest.scriptCounter = 1;

// buildScriptTag method
//
JSONscriptRequest.prototype.buildScriptTag = function () {

    // Create the script tag
    this.scriptObj = document.createElement("script");
    
    // Add script object attributes
    this.scriptObj.setAttribute("type", "text/javascript");
    this.scriptObj.setAttribute("charset", "utf-8");
    this.scriptObj.setAttribute("src", this.fullUrl);
    this.scriptObj.setAttribute("id", this.scriptId);
}
 
// removeScriptTag method
// 
JSONscriptRequest.prototype.removeScriptTag = function () {
    // Destroy the script tag
    this.headLoc.removeChild(this.scriptObj);  
}

// addScriptTag method
//
JSONscriptRequest.prototype.addScriptTag = function () {
    // Create the script tag
    this.headLoc.appendChild(this.scriptObj);
}

// If we could not find an article that matches our original search term, we do a wikipedia full text search
// to provide the user with alternatives
function freetextSearch () {
	var req = 'http://en.wikipedia.org/w/api.php?format=json&callback=getWikiTip&action=query&list=search&srsearch=' +
				bObj.searchTerm + '&srprop=size&srinfo=totalhits';
	bObj = new JSONscriptRequest(bObj.searchTerm, req); 
	bObj.buildScriptTag(); 
	bObj.addScriptTag();
}

var oDialog1 = undefined;

// Define the callback function
function getWikiTip(jsonData) {
	
	// Remove the script tag we added to execute this function
	bObj.removeScriptTag();
	
	var openDialog = true;
	
	if (typeof oDialog1 == "undefined") {
		oDialog1 = new sap.ui.commons.Dialog();
		oDialog1.setModal(true);
		oDialog1.addStyleClass("wikidialog");
		oDialog1.addButton(new sap.ui.commons.Button({text: "OK", press:function(){oDialog1.close();}}));
	} else {
		oDialog1.removeAllContent();	
	}
	
	var text = "";
	
	// If we have done a freetext wikipedia search then there is a jsonData.query object
	if (jsonData.query) {
		text = "<div>No article found for <b>" + bObj.searchTerm +
				"</b>.<br>Fulltext search returned the following articles:</div><br>";
		var noResultHTML = new sap.ui.core.HTML({
			content: text,
			preferDOM : false
		});
		oDialog1.addContent(noResultHTML);
		
		// Add links to the articles found in the wikipedia freetext search
		var oLayout = new sap.ui.commons.layout.VerticalLayout();
		for ( var searchResults = 0; searchResults < jsonData.query.search.length; searchResults++) {
			var title = jsonData.query.search[searchResults].title;
			var link = new sap.ui.commons.Link({
				text: title,
				press: function () {
					var req = 'http://en.wikipedia.org/w/api.php?format=json&callback=getWikiTip&action=parse&page='+
						this.getText()+'&prop=text&section=0';
					bObj = new JSONscriptRequest(this.getText(), req); 
					bObj.buildScriptTag(); 
					bObj.addScriptTag();
				}});
			oLayout.addContent(link);
		}
		oDialog1.setTitle("Alternative Search Terms");
		oDialog1.setWidth("400px");
		oDialog1.addContent(oLayout);
	} else {
		oDialog1.setWidth("600px");
		// If we get no results for our search wikipedia returns an error. Present the user with some other options
		if (jsonData.error) {
			openDialog = false;
			freetextSearch();
		// If this is a 'may refer to page' page, let the user know the search term is too general
		} else if (jsonData.parse.text['*'].search("<b>" + jsonData.parse.title + "</b> may refer to:") > -1){
			text = "<div>Please select a more specific search term.</div>";
			oDialog1.setTitle("Search Term '" + jsonData.parse.title + "' Too General");
		} else {		
			oDialog1.setTitle(jsonData.parse.title);
			text = jsonData.parse.text['*'];
			// Make the links point to english wikipedia and open in a blank page
			text = text.replace(/a href=\"\/wiki/g, "a target=\"_blank\" href=\"http://en.wikipedia.org/wiki");
			// Remove all the metadata boxes (things like - this article is a stub, unfinished etc -
			// at the top of the wikipedia page
			text = text.replace(/<table class="metadata[\s\S]*?<\/table>/,"");
				
			// Make a list of all images taller that 100 pixels - smaller images are icons that we do
			// not want to display
	        var imagesRegex = /<img.*?src="\/\/(upload.+?)".+?height="(.+?)"/g;	        
	        var images = [];
	        while (imagesMatch = imagesRegex.exec(text)) {
				if (imagesMatch[2] > 100) {
		        	var image = new Object();
		        	image.src = "http://" + imagesMatch[1];
		        	image.height = imagesMatch[2];
		        	images.push(image);
				}
	        }      
	        
	        // If we have one image, center it at the top and center of the page,
	        // if we have more than one then add them to a carousel
	        if (images) {
		        if (images.length == 1) {
		        	var singleImage = new sap.ui.commons.Image({
						src : images[0].src					
					});
		        	singleImage.addStyleClass("wikisingleimage");
		        	oDialog1.addContent(singleImage);
		        } else if (images.length > 1) {
					var oCarousel = new sap.ui.commons.Carousel();
			        oCarousel.setWidth("100%");
			        oCarousel.setOrientation("horizontal");
			        oCarousel.addStyleClass("wikicarousel");
			
			        oCarousel.setVisibleItems(2);
			
					for ( var int = 0; int < images.length; int++) {
			        	var carouselImage = new sap.ui.commons.Image({
							src : images[int].src					
						});
			        	carouselImage.addStyleClass("wikisingleimage");
						oCarousel.addContent(carouselImage);
					}
					oDialog1.addContent(oCarousel);
				}
	        }
	
	        // Remove any cite error text from our result
			var idx = text.search(/<strong class="error">Cite error:/);
			if (idx > 0) 
				text = text.substring(0, idx);
			
			// If this is a redirect page, do the redirect automatically
			var titleRegex = /^<ol>\s<li>REDIRECT.*title="(.+)"/i;
			var redirectTitle = titleRegex.exec(text);	
			if (redirectTitle && redirectTitle.length == 2) { 	
				openDialog = false;
				var req = 'http://en.wikipedia.org/w/api.php?format=json&callback=getWikiTip&action=parse&page='+
						redirectTitle[1]+'&prop=text&section=0';
				bObj = new JSONscriptRequest(bObj.searchTerm, req); 
				bObj.buildScriptTag(); 
				bObj.addScriptTag();	
			}			
		}
		
		var html1 = new sap.ui.core.HTML({
			content: text,
			preferDOM : false
		});

		oDialog1.addContent(html1);
	}
	
	if (openDialog) {
		oDialog1.open();
	}
    
}
