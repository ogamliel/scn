 function webViewerLoad(url, doi, _newText, params){
	 console.log("webViewerLoad:  url=[" + url + "] doi=[" + doi + "] newText=[" + _newText + "] params=[" + params + "]");
	 TextView.doi = doi;
	 TextView.initialize();
	 newText = _newText;
	 var params = TextView.parseQueryString(document.location.search.substring(1));


  var mainContainer = document.getElementById('mainContainer');
  var outerContainer = document.getElementById('outerContainer');
  mainContainer.addEventListener('transitionend', function(e) {
    if (e.target == mainContainer) {
      var event = document.createEvent('UIEvents');
      event.initUIEvent('resize', false, false, window, 0);
      window.dispatchEvent(event);
      outerContainer.classList.remove('sidebarMoving');
    }
  }, true);


  TextView.open(url, 0);
};



  var TextView = {
		  doi: null,
		  container: null,
		  initialized: false,
		  textDocument: null,

		  // called once when the document is loaded
		  initialize: function textViewInitialize() {
			console.log("TextView initialize");
		    var self = this;
		    var container = this.container = document.getElementById('viewerContainer');
		    this.initialized = true;
		  },
		  
		  open: function textViewOpen(url) {
			console.log("opening text");
			$.ajax({
	            url : url,
	            dataType: "text",
	            success : function (data) {
	            	//$(".title").html(articleTitle);
	            	//$(".subtitle").html(authors + " in " + journal + ", " + articleDate);
	                $("#viewer").html(data);
	               // $('#viewer').highlight(searchTerm.split(" "), {className: 'search' });
	               // $("#loadingBox").css("visibility","hidden");
	            }
			});

		    var loadingBox = document.getElementById('loadingBox');
		    loadingBox.setAttribute('hidden', 'true');
		    var loadingIndicator = document.getElementById('loading');
		    loadingIndicator.textContent = '';

		  },
		  
/*		  load: function textViewLoad(pdfDocument) {

			    var loadingBox = document.getElementById('loadingBox');
			    loadingBox.setAttribute('hidden', 'true');
			    var loadingIndicator = document.getElementById('loading');
			    loadingIndicator.textContent = '';

			    var container = document.getElementById('viewer');
			    while (container.hasChildNodes())
			      container.removeChild(container.lastChild);


			      var event = document.createEvent('CustomEvent');
			      event.initCustomEvent('documentload', true, true, {});
			      window.dispatchEvent(event);

			  },
*/			  
			  
		  // Helper function to parse query string (e.g. ?param1=value&parm2=...).
		  parseQueryString: function textViewParseQueryString(query) {
		    var parts = query.split('&');
		    var params = {};
		    for (var i = 0, ii = parts.length; i < parts.length; ++i) {
		      var param = parts[i].split('=');
		      var key = param[0];
		      var value = param.length > 1 ? param[1] : null;
		      params[unescape(key)] = unescape(value);
		    }
		    return params;
		  }

  };

  
  var TextFindController = {  
		  startedTextExtraction: false,

		  extractTextPromises: [],

		  // If active, find results will be highlighted.
		  active: false,

		  // Stores the text for each page.
		  pageContents: [],

		  pageMatches: [],

		  allMatchesByQuery : {},
		  // Currently selected match.
		  selected: {
		    pageIdx: -1,
		    matchIdx: -1
		  },

		  // Where find algorithm currently is in the document.
		  offset: {
		    pageIdx: null,
		    matchIdx: null
		  },

		  resumePageIdx: null,

		  resumeCallback: null,

		  state: null,

		  dirtyMatch: false,

		  findTimeout: null,

		  initialized: false,
		  clean: function() {
			this.allMatchesByQuery = {};
			this.startedTextExtraction = false;
			this.extractTextPromises = [];
			this.active = false;
			this.pageContents = [];
			this.pageMatches = [];

			this.selected = {
				pageIdx: -1,
				matchIdx: -1
			};

			this.offset = {
				pageIdx: null,
				matchIdx: null
			};

			this.resumePageIdx= null;
			this.resumeCallback = null;
			this.state = null;
			this.dirtyMatch = false;

			this.findTimeout = null;
		    var events = [
		                  'find',
		                  'findagain',
		                  'findhighlightallchange',
		                  'findAllAndHighlight',
		                  'findcasesensitivitychange',
		                  'markUserSelection',
		                  'rawTextExtraction'
		                ];


		    for (var i = 0; i < events.length; i++) {
		    	window.removeEventListener(events[i], this.handleEvent);
		    }
		    this.initialized = false;
		  },

		  initialize: function() {
		  	if (this.state)
		  		this.state.query = '';
		  	if (this.initialized)
		  		return;
		  	// Springer added event (findAllAndHighlight)  for distinguishing it from find bar 
		    var events = [
		      'find',
		      'findagain',
		      'findhighlightallchange',
		      'findAllAndHighlight',
		      'findcasesensitivitychange',
		      'markUserSelection',
		      'rawTextExtraction'
		    ];

		    this.handleEvent = this.handleEvent.bind(this);

		    for (var i = 0; i < events.length; i++) {
		      window.addEventListener(events[i], this.handleEvent);
		    }
		    this.initialized = true;
		  },

		  calcFindMatch: function(pageIndex) {
		    var pageContent = this.pageContents[pageIndex];
		    if (!this.state) {
				var event = document.createEvent('CustomEvent');
		    	event.initCustomEvent('searchFinished', true, true, {
		  			doi: TextView.doi
		  		});
		    	window.dispatchEvent(event);
		    	return;
		    }
		    var query = this.state.query;
		    query = query.trim();
		    var caseSensitive = this.state.caseSensitive;
		    var queryLen = query.length;

		    if (queryLen === 0) {
		      // Do nothing the matches should be wiped out already.
		      return;
		    }

		    if (!caseSensitive) {
		      pageContent = pageContent.toLowerCase();
		      query = query.toLowerCase();
		    }

		    var matches = [];

		    var matchIdx = -queryLen;
		   	while (true) {
		      matchIdx = pageContent.indexOf(query, matchIdx + queryLen);
		      if (matchIdx === -1) {
		      	break;
		      }

		      matches.push(matchIdx);
		    }
		    this.pageMatches[pageIndex] = matches;
		    if (customHighlight) {
		    	var actualLength = 0;
		    	for (var i = 0 ; i < this.pageMatches.length; i++) {
		    		if(typeof this.pageMatches[i] != "undefined")
		    			actualLength++;
		        }
		    	if(actualLength == TextView.pages.length) {
		    		var sendArrayStatus = [];
		    		for (var i = 0; i < this.pageMatches.length; i++)
		    			sendArrayStatus[i] = false;
		    		var keyen = TextFindController.state.query.replace("\n","_").replace("\r","__").replace("\r\n","____").replace(" ","_____")+"____"+TextFindController.state.searchType+"_"+TextFindController.state.entityType;
		    		//var keyen = TextFindController.state.query.split(/ |\r\n|\n|\r/).join("_")+"____"+TextFindController.state.searchType+"_"+TextFindController.state.entityType;
		    		TextFindController.allMatchesByQuery[keyen] ={
			    			matches:this.pageMatches,
			    			type :TextFindController.state.searchType,
			    			entityType: TextFindController.state.entityType,
			    			range:TextFindController.state.range,
			    			metadata:TextFindController.state.metadata,
			    			query: TextFindController.state.query,
			    			isSend:sendArrayStatus,
			    			isActive : true
			    	};

		    		var event = document.createEvent('CustomEvent');
			    	event.initCustomEvent('searchFinished', true, true, {
			  			doi: TextView.doi,
			    		query: TextFindController.state.query,
			    		searchType: TextFindController.state.searchType,
			    		entityType: TextFindController.state.entityType,
			    		range: TextFindController.state.range
			    	});
			    	window.dispatchEvent(event);
			    	//todo :fix this properly
			    	for (var i = 0 ; i < TextView.pages.length; i++)
			    		this.updatePage(i,true);
			    }
		    }
		    this.updatePage(pageIndex, true);
		    if (this.resumePageIdx === pageIndex) {
		      var callback = this.resumeCallback;
		      this.resumePageIdx = null;
		      this.resumeCallback = null;
		      callback();
		    }

		  },
		  extractRawText: function() {
			var textInPage = [];
			var tokenTextInPage = [];
			var extractTextProm = [];
		    for (var i = 0, ii = TextView.textDocument.numPages; i < ii; i++) {
		      	extractTextProm.push(new TextJS.Promise());
		    }

		    function extractPageRawText(pageIndex) {
		        TextView.pages[pageIndex].getTextContent().then(
		        function textRawContentResolved(data) {
		          var str = '';
				  var tokens = [];
		          for (var i = 0; i < data.bidiTexts.length; i++) {
		            str += data.bidiTexts[i].str;
		            tokens.push(data.bidiTexts[i].str);
		          }
		          tokenTextInPage.push(tokens);
				  textInPage.push(str);
		          extractTextProm[pageIndex].resolve(pageIndex);
		          if ((pageIndex + 1) < TextView.pages.length)
		            extractPageRawText(pageIndex + 1);
		          else {
		          		// text extraction is finished send event 
		          		var event = document.createEvent('CustomEvent');
		    			event.initCustomEvent('textExtracted', true, true, {
		  					doi: TextView.doi,
		    				text: textInPage,
		    				tokenizedText: tokenTextInPage
		    			});
		    			window.dispatchEvent(event);
		          }            
		        }
		      );
		    }
		    extractPageRawText(0);
		  },
		  
		  extractText: function() {
		    if (this.startedTextExtraction) {
		      return;
		    }
		    this.startedTextExtraction = true;

		    this.pageContents = [];
		    for (var i = 0, ii = TextView.textDocument.numPages; i < ii; i++) {
		      this.extractTextPromises.push(new TextJS.Promise());
		    }

		    var self = this;
		    function extractPageText(pageIndex) {
		      TextView.pages[pageIndex].getTextContent().then(
		        function textContentResolved(data) {
		          // Build the find string.
		          var bidiTexts = data.bidiTexts;
		          var str = '';

		          for (var i = 0; i < bidiTexts.length; i++) {
		            str += bidiTexts[i].str;
		          }

		          // Store the pageContent as a string.
		          self.pageContents.push(str);
		          self.extractTextPromises[pageIndex].resolve(pageIndex);
		          if ((pageIndex + 1) < TextView.pages.length)
		            extractPageText(pageIndex + 1);
		            
		        }
		      );
		    }
		    extractPageText(0);
		    return this.extractTextPromise;
		  },

		  handleEvent: function(e) {
		    customHighlight = true;
		  	markUserSelection = false;
		  	
		  	if (e.type === 'markUserSelection') {
				markUserSelection = true;
		  	}
		  	if (e.type === 'findAllAndHighlight') {
		  		customHighlight = true;
		  	}
		  	if (e.type == 'find' || e.type == 'findagain' || e.type == ' findcasesensitivitychange') {
		  		customHighlight = false;
		  		
		  	}
		    if (this.state === null || e.type !== 'findagain' || e.type === 'markUserSelection'|| e.type === 'findAllAndHighlight') {
		      this.dirtyMatch = true;
		    }
		    this.state = e.detail;
		    this.updateUIState(FindStates.FIND_PENDING);

		    this.extractText();

		    clearTimeout(this.findTimeout);
		    if (e.type === 'find') {
		      // Only trigger the find action after 250ms of silence.
		      this.findTimeout = setTimeout(this.nextMatch.bind(this), 250);
		    } else {
		      this.nextMatch();
		    }
		  },

		  updatePage: function(idx, notify) {
		    var page = TextView.pages[idx];
		    if (this.selected.pageIdx === idx) {
		      // If the page is selected, scroll the page into view, which triggers
		      // rendering the page, which adds the textLayer. Once the textLayer is
		      // build, it will scroll onto the selected match.
		      if(!markUserSelection && !customHighlight)
		      	page.scrollIntoView();
		    }
			
		    if (page.textLayer) {
		      if(typeof notify == "undefined")
		      	notify =false;
		      page.textLayer.updateMatches(notify);
		    }
		  },

		  matchOnCurrentPage: function(){
		    var pages = TextView.pages;
		    var previous = this.state.findPrevious;
		    var numPages = TextView.pages.length;
		    
		  	this.active = true;
			if(this.dirtyMatch) {
		  		// Need to recalculate the matches, reset everything.
		      	this.dirtyMatch = false;
			    this.selected.pageIdx = this.selected.matchIdx = -1;
			    this.offset.pageIdx = previous ? numPages - 1 : 0;
			    this.offset.matchIdx = null;
			    this.hadMatch = false;
			    this.resumeCallback = null;
			    this.resumePageIdx = null;
			    this.pageMatches = [];
			    var self = this;
				
			  	for (var i = 0; i < numPages; i++) {
		        	// Wipe out any previous highlighted matches.
		        	this.updatePage(i,false);

		        	// As soon as the text is extracted start finding the matches.
		        	this.extractTextPromises[i].onData(function(pageIdx) {
		        		// Use a timeout since all the pages may already be extracted and we
		    	    	// want to start highlighting before finding all the matches.
		        	  	setTimeout(function() {
		            		self.calcFindMatch(pageIdx);
		          		});
		        	});
		      	}
			}

			// If there's no query there's no point in searching.
		   	if (this.state.query === '') {
		      	this.updateUIState(FindStates.FIND_FOUND);
		      	return;
		    }

		    // If we're waiting on a page, we return since we can't do anything else.
		    if (this.resumeCallback) {
		      	return;
		    }
		    var offset = this.offset;
		    // If there's already a matchIdx that means we are iterating through a
		    // page's matches.
		    if (offset.matchIdx !== null) {
		      var numPageMatches = this.pageMatches[offset.pageIdx].length;
		      if ((!previous && offset.matchIdx + 1 < numPageMatches) ||
		          (previous && offset.matchIdx > 0)) {
		        // The simple case, we just have advance the matchIdx to select the next
		        // match on the page.
		        this.hadMatch = true;
		        offset.matchIdx = previous ? offset.matchIdx - 1 : offset.matchIdx + 1;
		        this.updateMatch(true);
		        return;
		      }
		      // We went beyond the current page's matches, so we advance to the next
		      // page.
		      this.advanceOffsetPage(previous);
		    }
		    // Start searching through the page.
		  },
		  
		  nextMatch: function() {
		    var pages = TextView.pages;
		    var previous = this.state.findPrevious;
		    var numPages = TextView.pages.length;

		    this.active = true;

		    if (this.dirtyMatch) {
		      // Need to recalculate the matches, reset everything.
		      this.dirtyMatch = false;
		      this.selected.pageIdx = this.selected.matchIdx = -1;
		      this.offset.pageIdx = previous ? numPages - 1 : 0;
		      this.offset.matchIdx = null;
		      this.hadMatch = false;
		      this.resumeCallback = null;
		      this.resumePageIdx = null;
		      this.pageMatches = [];
		      var self = this;

		      for (var i = 0; i < numPages; i++) {
		        // Wipe out any previous highlighted matches.
		        this.updatePage(i,false);

		        // As soon as the text is extracted start finding the matches.
		        this.extractTextPromises[i].onData(function(pageIdx) {
		          // Use a timeout since all the pages may already be extracted and we
		          // want to start highlighting before finding all the matches.
		          setTimeout(function() {
		            self.calcFindMatch(pageIdx);
		          });
		        });
		      }
		    }

		    // If there's no query there's no point in searching.
		    if (this.state.query === '') {
		      this.updateUIState(FindStates.FIND_FOUND);
		      return;
		    }

		    // If we're waiting on a page, we return since we can't do anything else.
		    if (this.resumeCallback) {
		      return;
		    }

		    var offset = this.offset;
		    // If there's already a matchIdx that means we are iterating through a
		    // page's matches.
		    if (offset.matchIdx !== null) {
		      var numPageMatches = this.pageMatches[offset.pageIdx].length;
		      if ((!previous && offset.matchIdx + 1 < numPageMatches) ||
		          (previous && offset.matchIdx > 0)) {
		        // The simple case, we just have advance the matchIdx to select the next
		        // match on the page.
		        this.hadMatch = true;
		        offset.matchIdx = previous ? offset.matchIdx - 1 : offset.matchIdx + 1;
		        this.updateMatch(true);
		        return;
		      }
		      // We went beyond the current page's matches, so we advance to the next
		      // page.
		      this.advanceOffsetPage(previous);
		    }
		    // Start searching through the page.
		    this.nextPageMatch();
		  },

		  nextPageMatch: function() {
		    if (this.resumePageIdx !== null)
		      console.error('There can only be one pending page.');

		    var matchesReady = function(matches) {
		      if (typeof matches === "undefined")
		    	  matches = [];
		      var offset = this.offset;
		      var numMatches = matches.length;
		      var previous = this.state.findPrevious;
		      if (numMatches) {
		        // There were matches for the page, so initialize the matchIdx.
		        this.hadMatch = true;
		        offset.matchIdx = previous ? numMatches - 1 : 0;
		        this.updateMatch(true);
		      } else {
		        // No matches attempt to search the next page.
		        this.advanceOffsetPage(previous);
		        if (offset.wrapped) {
		          offset.matchIdx = null;
		          if (!this.hadMatch) {
		            // No point in wrapping there were no matches.
		            this.updateMatch(true);
		            return;
		          }
		        }
		        // Search the next page.
		        this.nextPageMatch();
		      }
		    }.bind(this);

		    var pageIdx = this.offset.pageIdx;
		    var pageMatches = this.pageMatches;

		    if (!pageMatches[pageIdx]) {
		      // The matches aren't ready setup a callback so we can be notified,
		      // when they are ready.
		      this.resumeCallback = function() {
		        matchesReady(pageMatches[pageIdx]);
		      };
		      this.resumePageIdx = pageIdx;
		      return;
		    }
		    // The matches are finished already.
		    matchesReady(pageMatches[pageIdx]);
		  },

		  advanceOffsetPage: function(previous) {
		    var offset = this.offset;
		    var numPages = this.extractTextPromises.length;
		    offset.pageIdx = previous ? offset.pageIdx - 1 : offset.pageIdx + 1;
		    offset.matchIdx = null;
		    if (offset.pageIdx >= numPages || offset.pageIdx < 0) {
		      offset.pageIdx = previous ? numPages - 1 : 0;
		      offset.wrapped = true;
		      return;
		    }
		  },

		  updateMatch: function(found) {
		    var state = FindStates.FIND_NOTFOUND;
		    var wrapped = this.offset.wrapped;
		    this.offset.wrapped = false;
		    if (found) {
		      var previousPage = this.selected.pageIdx;
		      this.selected.pageIdx = this.offset.pageIdx;
		      this.selected.matchIdx = this.offset.matchIdx;
		      state = wrapped ? FindStates.FIND_WRAPPED : FindStates.FIND_FOUND;
		      // Update the currently selected page to wipe out any selected matches.
		      if (previousPage !== -1 && previousPage !== this.selected.pageIdx) {
		        this.updatePage(previousPage,true);
		      }
		    }
		    this.updateUIState(state, this.state.findPrevious);
		    if (this.selected.pageIdx !== -1) {
		      this.updatePage(this.selected.pageIdx,true);
		    }
		  },

		  updateUIState: function(state, previous) {
		    if (TextView.supportsIntegratedFind) {
		      FirefoxCom.request('updateFindControlState',
		                         {result: state, findPrevious: previous});
		      return;
		    }
		    TextFindBar.updateUIState(state, previous);
		  }
		};
		/*Springer Extention End*/



		/*Springer Extention Start*/

		var TextExtractor = {
			extractText: function() {
				TextFindController.extractRawText();
			}
		};

		/*Springer Extention End*/

		/*Springer Extention Start*/

		/**
		 *	Controlls finding and marking all the tokens , words and adds hover capabilities 
		 * created for sap springer
		 */

		var TextFindAndMark = {
		  initialize: function(q,t,et,r,m,c) {
		  	this.query = q;
		  	this.type = t;
		  	this.entityType = et;
		  	this.range = r;
		  	this.metadata = m;
		  	this.caseSensitive = c;
		    var self = this;
			this.pageMatches = [];

			this.selected = {
				pageIdx: -1,
				matchIdx: -1
			};

			this.offset = {
				pageIdx: null,
				matchIdx: null
			};
		    this.active = false;
			this.resumePageIdx= null;
			this.resumeCallback = null;
			this.state = null;
			this.dirtyMatch = false;
		  },
		  enableIfMarkersArePreCalculated: function(type) {
			  var preCalced = false;
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type) {
					  preCalced = true;
					  TextFindController.allMatchesByQuery[key].isActive = true;
					  for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
						  TextFindController.allMatchesByQuery[key].isSend[i] = false;
					  }
				  }
			  }
			  return preCalced;
		  },
		  markersOfTypeAndQueryAreCalculated: function(query, type) {
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type && TextFindController.allMatchesByQuery[key].query == query) {
					  return true;
				  }
			  }
			  return false;
		  },

		  markersOfTypeAreCalculated: function(type) {
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type) {
					  return true;
				  }
			  }
			  return false;
		  },

		  enableAllMarkersOfType : function(type) {
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type){
					  TextFindController.allMatchesByQuery[key].isActive = true;
					  // send again to Visible pages
					  for (var i = 0; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
						  // for each page check if highlights for this type have been created if yes, no reason to calculate them again
						  if($("#pageContainer"+(i+1)+" [layer='" + type + "']").length == 0)
							  TextFindController.allMatchesByQuery[key].isSend[i] = false;
					  }
				  }
			  } 
			  this.updatePages();
		  },
		  enableMarkersOfTypeAndQuery : function(query, type) {
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type && TextFindController.allMatchesByQuery[key].query == query) {
					  TextFindController.allMatchesByQuery[key].isActive = true;
					  for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
						  TextFindController.allMatchesByQuery[key].isSend[i] = false;
					  }
				  }
			  } 
			  this.updatePages();
		  },
		  disableMarkers : function(type) {
			  for (var key in TextFindController.allMatchesByQuery) {
				  if (TextFindController.allMatchesByQuery[key].entityType == type) {
					  TextFindController.allMatchesByQuery[key].isActive = false;
					  for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
						  TextFindController.allMatchesByQuery[key].isSend[i] = true;
					  }
				  }
			  }
		  },
		  enableAllMarkers: function() {
			  if (typeof TextFindController.allMatchesByQuery == "undefined")
				  return;
			  for (var key in TextFindController.allMatchesByQuery) {
				  TextFindController.allMatchesByQuery[key].isActive = true;
				  for (var i = 0 ; i < TextFindController.allMatchesByQuery[key].isSend.length; i++) {
					  TextFindController.allMatchesByQuery[key].isSend[i] = false;
				  }
			  }
		  },
		  
		  updatePages : function() {
			  for (var i = 0 ; i < TextView.pages.length; i++)
				  TextFindController.updatePage(i,true);
		  },

		  markAll: function() {
			var event = document.createEvent('CustomEvent');
		    event.initCustomEvent('findAllAndHighlight', true, true, {
		      query: this.query,
		      caseSensitive:this.caseSensitive,
		      highlightAll: true,
		      findPrevious: false,
		      searchType: this.type,
		      entityType: this.entityType,
		      range: this.range,
		      metadata: this.metadata
		    });

		    return window.dispatchEvent(event);
		  }
		};


		var TextFindAndMarkUserSelection= {
		  initialize: function(q,r) {
		  	this.query = q;
		  	this.range = r;
		    var self = this;
		  },

		  markSelection: function() {
		    var event = document.createEvent('CustomEvent');
		    event.initCustomEvent('markUserSelection', true, true, {
		      query: this.query,
		      caseSensitive: false,
		      highlightAll: true,
		      findPrevious: false,
		      range:this.range
		    });
		    return window.dispatchEvent(event);
		  }
		};
		/*Springer Extention End*/

