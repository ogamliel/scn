var ref = $.request.parameters.get("ref");

var response = {
       references : []
};

if (ref) {
       var sql =
       " select x.reference_key,x.title,x.xs_file_name,x.viewname,x.source,x.source_url from ( " +
              " select COUNT(c.ta_token) CHECK ,b.reference_Key,b.title, b.xs_file_name, b.viewname, b.source, b.source_url "+
                     " from USG_REFERENCE_MASTER_TOKEN c , USG_REFERENCE_MASTER b "+
                     " where b.reference_key = c.reference_key" +
                     " and exists (select a.ta_token from " +
                     "\"TA_FTI_ARTICLE\""+
                     " a where  (lower(a.ta_token) LIKE " + 
                     "'%'"   +
                     " || C.TA_TOKEN || " +
                     "'%'"  +
                     " or c.ta_token LIKE " +
                     "'%'" +
                     "|| a.TA_TOKEN || "+
                     "'%'" +
                     "  ) and  a.article_key = ?)"+
                     " GROUP BY B.tITLE,B.REFERENCE_KEY,B.XS_FILE_NAME,B.VIEWNAME,B.SOURCE,B.SOURCE_URL "+
                     "  ) x "+
                     " where x.Check > 2";
                     
       
       var conn = $.db.getConnection();
       var pstmt = conn.prepareStatement(sql);
       pstmt.setString(1, ref);
       
       var result = [];
       var rs = pstmt.executeQuery();
       while (rs.next()) {
           result.push({
               refid: rs.getString(1),
               title: rs.getString(2),
               xsFile : rs.getString(3),
               viewname : rs.getString(4),
               source : rs.getString(5),
               sourceUrl : rs.getString(6)
           });
       }
       response.references = result;

       rs.close();
       pstmt.close();
       conn.close();
}

$.response.setBody(JSON.stringify(response));
