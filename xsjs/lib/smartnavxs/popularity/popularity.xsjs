var pdfid = $.request.parameters.get("pdfid");
/* kaj added section */
var search = $.request.parameters.get("search");
var searchQuery = search || ''; 

/* kaj added section */

var response = {
       accesscounts : []
};

function fetchresult(ps, rowmapper) {
       var rs = ps.executeQuery();
       var res = [];
       while (rs.next()) {
              res.push(rowmapper(rs));
       }
       rs.close();
       return res;
}


/* kaj added section */
if (searchQuery > '') {
       var searchParam = search.split(/AND|OR/i)[0].trim().toLowerCase();
}
/*end  kaj added section */


//if (pdfid > '') {  
/*     if (pdfid && parseInt(pdfid, 10)) {      */ /* kaj commented out */
       var conn;
       
       try {
              conn = $.db.getConnection();
              var pstmt = null;
              var mapper = null;
              
              /* kaj added section */
              if (searchQuery > '') {
                     var searchParam = search.split(/AND|OR/i)[0].trim().toLowerCase();
              //     pstmt = conn.prepareStatement("SELECT 1,article_year, month(article_date), count(ta_token)  " +
                     pstmt = conn.prepareStatement("SELECT 1,year(article_date), month(article_date), count(ta_token)  " +
                                                                     " FROM XML_CONTENT _TOKEN_MAP_CTRS2 " +
                                  " WHERE  contains(TA_TOKEN,?) " +
                                  " and Year(a.article_date) < YEAR(Current_date) "+
                                  " group by 1,year(a.article_date) , month(a.article_date) " +
                                  " group by 1,year(article_date) , month(article_date) " +
                                  " order by 1,2 desc, 3 desc"+
                                  " limit 36");
                     //            " limit 36");
                     
                     pstmt.setString(1, searchQuery); /* kaj added */
                     
              } else {
              /*end  kaj added section */
//            pstmt = conn.prepareStatement("SELECT 1,a.article_year, month(a.article_date), count(a.ta_token)  " +
              pstmt = conn.prepareStatement("SELECT '1',year(a.article_date), month(a.article_date), count(a.ta_token)  " +
                           " FROM XML_CONTENT _TOKEN_MAP_CTRS2 a " +
                           " WHERE exists (select 'X' from XML_CONTENT _TOKEN_MAP_CTRS2 b "+
                           " where b.article_key = ?" +
                           " and a.ta_token = b.ta_token)"+
                           " and Year(a.article_date) < YEAR(Current_date) "+
                           " group by 1,year(a.article_date) , month(a.article_date) " +
                           " order by 1,2 desc, 3 desc"+
                           " limit 36");
       //                   " limit 36");
              
                pstmt.setString(1, pdfid); /* kaj added */
       }
       //     pstmt.setInt(1, parseInt(pdfid, 24));
              
              
              
              var rs = pstmt.executeQuery();
              var res = [];
              while (rs.next()) {        
                     res.push({
                           pdfid : rs.getString(1),
                           year : rs.getInteger(2),
                           month : rs.getInteger(3),
                           count : rs.getInteger(4)
                     });  
              }
              rs.close();
              response.accesscounts = res;
              pstmt.close();
       }catch (ex) {
              $.trace.error("Unable to establish connection or query db. " + ex.toString());
              ex.source = fileName;
              throw ex;
       } finally {
              if (conn) {
              conn.close();
          }
       }
//}

$.response.setBody(JSON.stringify(response));
