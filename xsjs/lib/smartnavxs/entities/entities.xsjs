$.import("smartnavxs.lib", "filterGenerator");

function query(conn, pdfid, types) {
	var inClause =  $.smartnavxs.lib.filterGenerator.getInClause('ta_type', types);
	var sql = 
		"select distinct  " +
		"	IFNULL(ta_type, ' ') as type, " +
		"	IFNULL(ta_token, ' ') as token, " +
		"	IFNULL(ta_normalized, ' ') as normalized " +
		"from " +
		"	\"TA_FTI_ARTICLE\" " +
		"where  article_key = ? " + inClause.clause +
		" and (ta_type not like 'ORG%' or ta_type != 'TerrorismOrganizations' or ta_type != 'URI/EMAIL') " +
		"   and unicode(ta_token) <> 0 ";
	var pstmt = conn.prepareStatement(sql);
	pstmt.setString(1, pdfid);
	$.smartnavxs.lib.filterGenerator.applyParameters(pstmt, inClause.parameters, 2);
	return pstmt;
}

function buildResult(rs) {
	var map = {};
	if (rs.next()) {
		do {
			var type = rs.getString(1);
			map[type] = map[type] || [];
			map[type].push([rs.getString(2), rs.getString(3)]);
		} while (rs.next());
	}
	return map;
}

var pdfid = $.request.parameters.get("pdfid") || '';
var types = $.request.parameters.get("types");

var conn = $.db.getConnection();
var pstmt = query(conn, pdfid, types);
var rs = pstmt.executeQuery();
var result = buildResult(rs);
rs.close();
pstmt.close();
conn.close();

$.response.setBody(JSON.stringify(result));