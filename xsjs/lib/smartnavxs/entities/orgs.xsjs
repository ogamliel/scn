$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
	var result = [];
	var total = 0;
	
	if (rs.next()) {
	    do {
	        result.push({
	        	org: rs.getString(1),
	            count: rs.getInteger(2)
	        });
	        total += rs.getInteger(2);
	    } while (rs.next());
	}
	
	return {"data": result, "total": total};
}

var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var subjects = $.request.parameters.get("subjects");
var publishers = $.request.parameters.get("publishers");
var localities = $.request.parameters.get("localities");
var persons = $.request.parameters.get("persons");
var contentTypes = $.request.parameters.get("contentTypes");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");
var interactionTypes = $.request.parameters.get("interactionTypes");

var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(from, to, publishers, subjects, 
		persons, null, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes);
if(search) { additionalWhere.clause += " AND contains(c.article_content, ?) "; additionalWhere.parameters.push(search); };

var sql = "select t.ta_token,count(*) as ct " +
"from XML_CONTENT c, TA_FTI_ARTICLE t " +
"where c.ARTICLE_KEY=t.ARTICLE_KEY " +
"and (t.TA_TYPE like 'ORG%' or t.TA_TYPE = 'TerrorismOrganizations') " +
additionalWhere.clause + 
" group by t.ta_token " +
" order by ct desc " +
"limit 20 ";

var conn = $.db.getConnection();
var pstmt = conn.prepareStatement(sql);
var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);
var rs = pstmt.executeQuery();
var result = buildResult(rs);

rs.close();
pstmt.close();
conn.close();

var data = result.data;
var total = result.total;

// set total field in all results
for (var i = 0; i < data.length; ++i) {
	data[i].total = total;
}

$.response.setBody(JSON.stringify({orgs: data}));