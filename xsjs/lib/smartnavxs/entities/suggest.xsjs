function buildResult(rs) {
	var result = [];
	
	if (rs.next()) {
	    do {
	        result.push({
	        	token: rs.getString(1),
	        });
	    } while (rs.next());
	}
	
	return {"data": result};
}

var type = $.request.parameters.get("type");
var token = $.request.parameters.get("token");

var sql = "select distinct ta_token " +
"from TA_FTI_ARTICLE t ";
if (type.toUpperCase() === 'ORGANIZATION')
	sql += "where (TA_TYPE like 'ORG%' or TA_TYPE = 'TerrorismOrganizations') ";
else
	sql += "where TA_TYPE like '" + type.toUpperCase() + "' ";
sql += "and CONTAINS(ta_token,'" + token + "',fuzzy(0.7)) " +
"order by ta_token ";

var conn = $.db.getConnection();
var pstmt = conn.prepareStatement(sql);
var rs = pstmt.executeQuery();
var result = buildResult(rs);

rs.close();
pstmt.close();
conn.close();

var data = result.data;

$.response.setBody(JSON.stringify({tokens: data}));