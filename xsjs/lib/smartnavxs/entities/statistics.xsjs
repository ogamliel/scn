function query(conn, pdfid) {
	var sql =
		"select (case WHEN ta_type like 'ORG%' or ta_type = 'TerrorismOrganizations' THEN ('ORGANIZATION') " +
                 "WHEN ta_type = 'URI/EMAIL' THEN ('EMAIL') ELSE ta_type END) AS type, count(distinct ta_token) as count  " +
"from \"TA_FTI_ARTICLE\" " +
"where article_key = ? " +
"and (ta_type in ('LOCALITY', 'NOUN_GROUP', 'PERSON', 'PARLIAMENT') or ta_type like 'ORG%' or ta_type = 'TerrorismOrganizations' or ta_type = 'URI/EMAIL') " +
"group by  ta_type " +
"order by count desc";
	
	var pstmt = conn.prepareStatement(sql);
	pstmt.setString(1, pdfid);
	return pstmt;
}

function buildResult(rs) {
	var result = [];
	
	if (rs.next()) {
		var orgTotal = 0;
		var emailTotal = 0;
		do {
			var type = rs.getString(1);
			var cnt = rs.getInteger(2);

			if (type != 'ORGANIZATION' && type !== 'EMAIL')  {
				result.push({
					type : type,
					count : cnt,
				});
			}
			// sum the organization counts
			else if (type === 'ORGANIZATION') {
				orgTotal = orgTotal + cnt;
			}
			else if (type === 'EMAIL')  {
				emailTotal = emailTotal + cnt;
			}
		} while (rs.next());
		
		// add Organization and count
		if (orgTotal > 0)  {
			result.push({
				type : 'ORGANIZATION',
				count : orgTotal,
			});
		}
		
		// add email and count
		if (emailTotal > 0)  {
			result.push({
				type : 'EMAIL',
				count : emailTotal,
			});
		}

	}
	return result;
}

var pdfid = $.request.parameters.get("pdfid") || '';

var conn = $.db.getConnection();
var pstmt = query(conn, pdfid);
var rs = pstmt.executeQuery();
var result = buildResult(rs);
rs.close();
pstmt.close();
conn.close();

$.response.setBody(JSON.stringify({entities: result}));