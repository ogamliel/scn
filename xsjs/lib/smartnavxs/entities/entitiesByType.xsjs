function query(conn, pdfid, type) {
	var sql = 
		"select distinct  " +
		"	ta_token as token, length(ta_token) " +
		"from " +
		"	\"TA_FTI_ARTICLE\" " +
		"where  article_key = ? " +
		"   and ta_type = ? " +
		"   and unicode(ta_token) <> 0 " +
		" order by length(ta_token) desc ";
	var pstmt = conn.prepareStatement(sql);
	pstmt.setString(1, pdfid);
	pstmt.setString(2, type);
	return pstmt;
}

function buildResult(rs) {
	var tokens = [];
	while (rs.next()) {
		tokens.push(rs.getString(1));
	}
	return tokens;
}

var pdfid = $.request.parameters.get("pdfid") || '';
var type = $.request.parameters.get("type");

var conn = $.db.getConnection();
var pstmt = query(conn, pdfid, type);
var rs = pstmt.executeQuery();
var result = buildResult(rs);
rs.close();
pstmt.close();
conn.close();

$.response.setBody(JSON.stringify(result));