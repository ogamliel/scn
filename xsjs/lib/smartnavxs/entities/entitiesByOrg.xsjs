
function query(conn, pdfid, type) {
	var sql = 
		"select 'ORGANIZATION', ta_token, length(ta_token) as token " +
		"from \"TA_FTI_ARTICLE\" " +
		"where  article_key = ? " +
		"   and (ta_type like 'ORG%' or ta_type = 'TerrorismOrganizations') " +
		"   and unicode(ta_token) <> 0 " +
		"   order by length(ta_token) desc ";
	var pstmt = conn.prepareStatement(sql);
	pstmt.setString(1, pdfid);
	return pstmt;
}

function buildResult(rs) {
	var map = {};
	if (rs.next()) {
		do {
			var type = rs.getString(1);
			map[type] = map[type] || [];
			map[type].push([rs.getString(2)]);
		} while (rs.next());
	}
	return map;
}

var pdfid = $.request.parameters.get("pdfid") || '';

var conn = $.db.getConnection();
var pstmt = query(conn, pdfid);
var rs = pstmt.executeQuery();
var result = buildResult(rs);
rs.close();
pstmt.close();
conn.close();

$.response.setBody(JSON.stringify(result));