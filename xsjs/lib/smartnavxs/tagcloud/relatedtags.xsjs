$.import("smartnavxs.lib", "filterGenerator");

var SQL_DUMP_WIDTH = 60;
var screenFactor = 900;
var maxHitListLength = 25;
var search = $.request.parameters.get("search"); 

function formatLastFilter(filterName) {
	if (filterName === 'orgs')  {
		return ' like \'ORG%\' ';
	}
	else if (filterName === 'localities')  {
		return ' = \'LOCALITY\' ';
	}
	else if (filterName === 'persons')  {
		return ' = \'PERSON\' ';
	}
	else  {
		return '';
	}
}

function show_sql(isql){
	var lengthme = isql.length;
	var portions = 0;
	$.trace.info("SQLLENGTH: ="+lengthme);

	while (portions * SQL_DUMP_WIDTH < lengthme ){
        $.trace.info("SQL: "+isql.substring(portions*SQL_DUMP_WIDTH,portions*SQL_DUMP_WIDTH+SQL_DUMP_WIDTH));
		portions += 1;
	}
}

function sqlFallbackQuery(conn, from, to, publishers, searchQuery, lastFilter, sentimentStart, sentimentEnd, interactionTypes) {
	var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(from, to, publishers, null, null, null, null, null, sentimentStart, sentimentEnd, interactionTypes);
	if (additionalWhere.clause === "") {
		/*var sql1 = 
			"select * from (  "+  
	        " select  "+
			" lower(a.TA_TOKEN), sum(a.TA_COUNTER) as cnt"+ 
			" from XML_CONTENT _TOKEN_MAP_CTRS2 a, USG_TAG_TA_TYPES c "+
			" WHERE a.TA_COUNTER > 20 AND a.TA_TYPE = C.TA_TYPE"+
            " and a.article_date <= Current_date "+
            " and a.article_date >= (ADD_MONTHS(Add_years(current_date,-1),6))"+
	        " group by lower(a.TA_TOKEN)  "+
	        " ORDER BY cnt desc "+
	        " LIMIT 25 "+
	        "  with hint(OLAP_PARALLEL_AGGREGATION)"+
	        " )";*/
		
		var sql1 = "select * from ( " +
	        "select  " +
			"lower(a.TA_TOKEN), sum(a.TA_COUNTER) as cnt " + 
			"from TA_FTI_ARTICLE a, XML_CONTENT b, TAG_TA_TYPES c " +
			"WHERE a.TA_COUNTER > 20 AND a.TA_TYPE = C.TA_TYPE AND a.article_key = b.article_key ";
		if (lastFilter)  {
			sql1 += "and a.TA_TYPE " + lastFilter;
		}
		sql1 += "group by lower(a.TA_TOKEN) " +
	        "ORDER BY cnt desc " +
	        "LIMIT 25 " +
	        "with hint(OLAP_PARALLEL_AGGREGATION))";
		var pstmt = conn.prepareStatement(sql1);
		return pstmt;	  
	} 
	else {
		var sql = 
			"select * from (  "+  
			        " select  "+
					" lower(a.TA_TOKEN), sum(a.TA_COUNTER) as cnt"+ 
					" from TA_FTI_ARTICLE a, XML_CONTENT b, TAG_TA_TYPES c "+
					" WHERE a.TA_COUNTER > 20 AND a.TA_TYPE = C.TA_TYPE and a.article_key = b.article_key"+
					 " AND "+
					 additionalWhere.clause.substring(4);
						if (lastFilter)  {
							sql += "and a.TA_TYPE " + lastFilter;
						}
					sql += " group by lower(a.TA_TOKEN)  "+
			        " ORDER BY cnt desc "+
			        " LIMIT 25 "+
			        "  with hint(OLAP_PARALLEL_AGGREGATION)"+
			        " )";
		var pstmt = conn.prepareStatement(sql);
		$.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);
		return pstmt;
	}
}

function sqlSearchBackQuery(conn, from, to, publishers, searchQuery, lastFilter, sentimentStart, sentimentEnd, interactionTypes) {
	var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(from, to, publishers, null, null, null, null, null, sentimentStart, sentimentEnd, interactionTypes);
	if (additionalWhere.clause === "") {
		var sql1 = 
			"select * from (  "+  
	        " select  "+
			" lower(a.TA_TOKEN), sum(a.TA_COUNTER) as cnt"+ 
			" from TA_FTI_ARTICLE a,  TAG_TA_TYPES c "+
			" WHERE a.TA_COUNTER > 20 AND a.TA_TYPE = C.TA_TYPE  ";
			if (lastFilter)  {
				sql1 += "and a.TA_TYPE " + lastFilter;
			}
            //  " and b.article_date <= Current_date "+
            //" and b.article_date >= (ADD_MONTHS(Add_years(current_date,-1),6))"+
            sql1 += " and exists (select 'x' from XML_CONTENT b where "+
            "       a.article_key = b.article_key "+
            "       and contains(b.article_content,?) )"+
	        " group by lower(a.TA_TOKEN)  "+
	        " ORDER BY cnt desc "+     
	        " LIMIT 25 "+
	        "  with hint(OLAP_PARALLEL_AGGREGATION)"+
	        " )";
		var pstmt = conn.prepareStatement(sql1);
		var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1); 	
		pstmt.setString(index++, searchQuery);
		return pstmt;	  
	} 
	else {
		var sql = 
		"select * from (  "+  
		        " select  "+
				" lower(a.TA_TOKEN), sum(a.TA_COUNTER) as cnt"+ 
				" from TA_FTI_ARTICLE a, XML_CONTENT b, TAG_TA_TYPES c "+
				" WHERE a.TA_COUNTER > 20 AND a.TA_TYPE = C.TA_TYPE and a.article_key = b.article_key ";
				if (lastFilter)  {
					sql += " and a.TA_TYPE " + lastFilter;
				}
				sql += " AND "+
				additionalWhere.clause.substring(4) +
				  " and exists (select 'x' from XML_CONTENT b where "+
		            "       a.article_key = b.article_key "+
		            "       and contains(b.article_content,?) )"+
		        " group by lower(a.TA_TOKEN)  "+
		        " ORDER BY cnt desc "+
		        " LIMIT 25 "+
		        "  with hint(OLAP_PARALLEL_AGGREGATION)"+
		        " )";
		var pstmt = conn.prepareStatement(sql);
		var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);
		pstmt.setString(index++, searchQuery);
		return pstmt;
	}
}

function isNotOnTagFilterList(ttag){
	/* For single strings */
	var ret = ( ttag.search(/^1 rp$/) == -1 ) &&
			  ( ttag.search(/^fig\.$/) == -1  ) &&
              ( ttag.search(/^her$/) == -1 ) &&
              ( ttag.search(/^und$/)  == -1 ) &&
              ( ttag.search(/^ill patients$/) == -1 )  &&
              ( ttag.search(/^The$/) == -1 ) &&
              ( ttag.search(/^Using$/) == -1 ) &&
              ( ttag.search(/^Die$/) == -1 ) &&
              ( ttag.search(/^punto$/) == -1 ) &&
              ( ttag.search(/^ueber$/) == -1 ) &&
              ( ttag.search(/^about$/) == -1 ) &&
              ( ttag.search(/^das$/) == -1 ) &&
              ( ttag.search(/^Der$/) == -1 ); 
	
	return ret;
}

function isNotOnTagExceptionList(ttag){
	/* for patterns */
    var ret =  ( ttag.search(/^et\s+al\.?$/) == -1 ) &&
	           ( ttag.search(/^\d+ kap\.$/) == -1  ) &&
	           ( ttag.search(/^\d+. aufl\.$/) == -1 ) &&
	           ( ttag.search(/^\d+ abs\.$/)  == -1 ) &&
	           ( ttag.search(/^no\.\s?$/i) == -1 )  &&
	           ( ttag.search(/^\d+ rn\.$/i) == -1 ) &&
	           ( ttag.search(/^no\.\s?$/i) == -1 ) &&
	           ( ttag.search(/^i+\s?\.$/i) == -1 ) &&
	           ( ttag.search(/^\d+ nr\.$/i) == -1 );    
     
    return ret;
}

function isGoodTitleWordList(ttag, len){
    var ret = false;
	   
    ret = (  ( len > 5) && ( ttag.search(/ium$/i)       >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ality$/i)     >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ility$/i)     >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ion$/i)       >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ties$/i)      >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/or$/i)        >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ence$/i)      >= 0 ) ) ||
	      (  ( len > 6) && ( ttag.search(/ics$/i)       >= 0 ) ) ||
	      (  ( len > 5) && ( ttag.search(/ism$/i)       >= 0 ) );  
    
 
	
	if (ret == false && ttag[0].toUpperCase() == ttag[0]){
		ret = true;
	}
		
    return ret;    
}

function doesNotStartWithRomanNumber(tag){
	
	return ( tag.search(/^i\./) <  0 ) &&
	      ( tag.search(/^ii\./) <  0 ) &&
	      ( tag.search(/^iii./) <  0 ) &&
	      ( tag.search(/^iv./) <  0 ) &&
	      ( tag.search(/^v./) <  0 ) &&
	      ( tag.search(/^vi./) <  0 ) &&
	      ( tag.search(/^vii./) <  0 ) &&
	      ( tag.search(/^viii\./) <  0 ) &&
	      ( tag.search(/^ix\./) <  0 ) &&
	      ( tag.search(/^x\./) <  0 ) &&
	      ( tag.search(/^xi\./) <  0 ) &&
	      ( tag.search(/^xii\./) <  0 ) &&
	      ( tag.search(/^xiii\./) <  0 ) &&
	      ( tag.search(/^xiv\./) <  0 ) &&
	      ( tag.search(/^xv./) <  0 ) &&
	      ( tag.search(/^xvi\./) <  0 ) &&
	      ( tag.search(/^xvii\./) <  0 ) &&
	      ( tag.search(/^xviii\./) <  0 ) &&
	      ( tag.search(/^xix\./) <  0 ) &&
	      ( tag.search(/^xx\./) <  0 ) &&
	      ( tag.search(/^xxi./) <  0 ) &&
	      ( tag.search(/^xxii\./) <  0 ) &&
	      ( tag.search(/^xxiii\./) <  0 ) 
}

function getResult(rs, isDouble) {
	var list = [];
	var limit = 11;
	var success = 0;
	var value = 0;
	var maxValue = 0;
	while (rs.next()  && (success < limit )) {
		var tag = rs.getString(1);
		var tale = tag.length;
		var count = (isDouble) ? rs.getDouble(2) : rs.getInteger(2);
		if ( (tale > 2)  && 
			( isNotOnTagFilterList(tag) == true )  && 
			( isNotOnTagExceptionList(tag) == true ) ) {	
			
				value = 100 - (6 * success);
				maxValue = Math.ceil(screenFactor/tale);
				value = Math.min(value, maxValue);
			
				list.push({
					"tag" : tag.toLowerCase(),
					"count" : value
				});	
				success = success + 1;
			} 
	}
	return list;
}

function sortAl(a,b){
	return (b.count - a.count);
}

function getDisplayList(pairT){
	var index = 0;
	var listS = [];
	var value = 0;
	var maxValue = 0;
	var mele = pairT.length;
	
	pairT.sort(sortAl);
	

	while ( index < pairT.length){
	   var tag = pairT[index].tag;
	   var tale = tag.length;
	   var value = 100 - (6 * index);
	   var maxValue = Math.ceil(screenFactor/tale);
	 
	   value = Math.min(value, maxValue);
	   
	   listS.push({
		"tag" : tag,
		"count" : value   
	     });
	//   $.trace.info("Tagcloud: igD "+tag);
	 
	    index = index + 1;
	}
	
	//$.trace.info("Tagcloud: gD  out"+mele);
	
	return listS;
}

function getAndAggegrateSimilarResults(rs) {
	var pairT = [];
	
	while ( rs.next()  && pairT.length < maxHitListLength ) {
		var tag = rs.getString(1).toLowerCase();
		var count = rs.getInteger(2);
		addNewHit(tag, count,  pairT);
	}
	return getDisplayList(pairT) ;	
}

function commonStr(lhs, lhsLength, rhs) {
    var tem1 = lhs;
    var s = lhsLength;

    while(s && rhs.indexOf(tem1) == -1) {
        tem1 = tem1.substring(0, --s);
    }
    return tem1;
}

function checkSimpleSimilarity(testArr, existingObject, dimension){	
      var testv = testArr[dimension];	
	  var testLength = testv.length;
	  var newObject = new Object();
	  newObject.trouvee = false;
	  newObject.base = commonStr(testv , testLength, (existingObject.base)[dimension] );
	  var newBaseLength = newObject.base.length;
	  
	  if (newBaseLength > 2){
		  var oldMaxi = (existingObject.maxi)[dimension];
		  newObject.maxi = Math.max (oldMaxi, testLength);
		   
		 if ( newObject.maxi - newBaseLength < 4){
			 
			 var oldMini =(existingObject.mini)[dimension];
			 
			 if (testLength < oldMini) {
				 if  (oldMaxi - testLength < 3){
					 
					 newObject.trouvee = true;
					 newObject.mini = testLength;
					
				 }
			 } else {
				 if( newObject.maxi - oldMini < 3 ){
					 newObject.trouvee = true;
					 newObject.mini = oldMini;
					 
				 }
			 }

		 }
	  }
	  
	  return newObject;	
}

function checkArraySimilarity(testArr, count, existingObject){
	 var isSimilar = true;
     var testArrLength = testArr.length;
     var list = [];     
     
     if (testArrLength == existingObject.width ){
    	 var index = 0;    	 
    	 while ( (index < testArrLength) && (isSimilar == true )){
    	    var interCheck =  checkSimpleSimilarity(testArr, existingObject, index);
    		isSimilar = interCheck.trouvee;
    		list[index] = interCheck;
    		index++
    	 }
    	 
    	 if (isSimilar == true){
    		 index = 0;
    		 
    		 while (index < testArrLength){
    			 (existingObject.base)[index] = (list[index]).base;
    			 (existingObject.maxi)[index] = (list[index]).maxi;
    			 (existingObject.mini)[index] = (list[index]).mini;
    			 index++
    		 }  		 
    		 existingObject.count = existingObject.count + count; 
    	 }   		 
    	 
     } else {
    	 isSimilar = false;
     }
     
    return isSimilar;
 }

function addNewHit(testw, count, pairRefT){	
	var index = 0;
	var trouvee = false;
	var ret = 0;
	var check = [];
		
	
	testw =testw.toLowerCase().replace(/^\(/,"").replace(/\)$/,"").replace(/,$/,"").replace(/\.$/,""); 
	check = testw.split(" ");
	
	if (check.length >= 2){
		if (check[0].search(/^\d+\.?\d*$/) >= 0){
			check = check.slice(1);
			check.slice(1)
		
		}
	}
	
	var testAsString = check.join(" ");
	var len = testAsString.length;
		
	if (( len> 3 ) && 
	    ( isNotOnTagFilterList(testAsString) == true) && 
	    ( isNotOnTagExceptionList(testAsString) == true) &&
	    ( doesNotStartWithRomanNumber(testAsString) == true) ){
		    ret = 1;	
	    	
	    	//First check, if testw is similar to an existing entry from the hit list
			// loop over the hit list   
	    	while ((index < pairRefT.length) && (trouvee == false )){
	    		 
	    		trouvee =	checkArraySimilarity(check, count, pairRefT[index]);
	    		index++;
	    		
	    	 } // end of loop over already found terms (hitlist)
	    	  
	    	 if (trouvee == false){
	    		 var lea = [];
	    		 index = 0;
	    		 
	    		 while (index < check.length){
	    			 lea[index] = check[index].length;
	    			 index++;
	    		 }
	    		  
	    	
	    		 pairRefT.push({
	    				"tag" : testAsString,
	    				"count" : count, 
	    				"width" : check.length,
	    				"base" : check,
	    				"maxi" : lea,
	    				"mini" : lea
	    			     }) ;                  
	    	    } 	        	  
		      
	   } // ends three conditions
	return ret ;
}

function addNewHits(wordsRef,  pairRefT){
	var le = wordsRef.length;
	var index = 0;
	var foundRet = 0;
	
	while (index < le){
		var testw =(wordsRef[index]).toString(); 
		var len = testw.length ;
		
	    if ( isGoodTitleWordList(testw, len) == true) {
	    	foundRet = foundRet + addNewHit(testw, 1,  pairRefT);
	    }
	   index = index + 1;
	} // end of loop over title words
	
	return foundRet;
}


function getTitleResult(rs) {
	var limit = 300;
	var listS = [];
	var pairT = [];
	var success = 0;	
	
	while (rs.next()  && (pairT.length < maxHitListLength ) && ( success < limit)) {
		var title = rs.getString(1);
		success = success + 1;			
	    var words = title.split(" ");
	    var found = addNewHits(words, pairT);
	}
	
	var listS = getDisplayList(pairT);
   
   if ((listS.length == 0) && (success > 0)){
	   listS = getShandyResults(1);
   }

	return listS;
}


var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var subjects = $.request.parameters.get("subjects");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");
var interactionTypes = $.request.parameters.get("interactionTypes");
var lastFilter = $.request.parameters.get("lastFilter");

if (lastFilter)  { lastFilter = this.formatLastFilter(lastFilter); }

var list = [];
var conn = $.db.getConnection();
//var schema = "FFA";

var searchQuery = search || ''; 

try {
    //var sstmt = conn.prepareStatement("SET SCHEMA "+schema);
    //sstmt.execute();
	if (searchQuery > '') {
		var searchParam = search.split(/AND|OR/i)[0].trim().toLowerCase();
		var pstmt1 = sqlSearchBackQuery(conn, from, to, subjects, searchQuery, lastFilter, sentimentStart, sentimentEnd, interactionTypes);
		var rs1 = pstmt1.executeQuery();
		list = getAndAggegrateSimilarResults(rs1);
		rs1.close();
		pstmt1.close();
	} 	
	else { 	  
		var pstmt1 = sqlFallbackQuery(conn, from, to, subjects, searchQuery, lastFilter, sentimentStart, sentimentEnd,  interactionTypes);
		var rs1 = pstmt1.executeQuery();
		list = getAndAggegrateSimilarResults(rs1);
		rs1.close();
		pstmt1.close();
	}

	conn.close();
} 
catch (e){
	if (conn != null){
		conn.close();
	}
	$.trace.info("Tagcloud: Exception"+e);
	list = [];
}

$.response.setBody(JSON.stringify(list));