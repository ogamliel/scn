$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
       var result = [];
       if (rs.next()) {
              do {
                     result.push({
                        //   year : rs.getInteger(1),
                    	//     date : rs.getDate(1),
                    	     year : rs.getDate(1),
                           count : rs.getInteger(2)
                     });

              } while (rs.next());
       }
       return result;
}

var search = $.request.parameters.get("search");
var subjects = $.request.parameters.get("subjects");
var publishers = $.request.parameters.get("publishers");

var conn = $.db.getConnection();

var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(null, null, subjects, publishers);
if(search) { additionalWhere.clause += " AND contains(m.article_content, ?) "; additionalWhere.parameters.push(search); };


var sql = "SELECT article_date, count(*) as count  " + 
"FROM XML_CONTENT  m WHERE m.ARTICLE_TYPE like '%' and year(article_date) between 2010 and 2018 " + additionalWhere.clause + 
" GROUP BY article_date " +
"ORDER BY count DESC LIMIT 20 " +
"with hint (OLAP_PARALLEL_AGGREGATION)";

var pstmt = conn.prepareStatement(sql);

$.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);

var rs = pstmt.executeQuery();
var result = buildResult(rs);
rs.close();
pstmt.close();
conn.close();

if (result.length == 0) {
       result = [{ count : 0, year : 1840}, { count : 0, year : 2016 }];
}

$.response.setBody(JSON.stringify({ years : result }));

