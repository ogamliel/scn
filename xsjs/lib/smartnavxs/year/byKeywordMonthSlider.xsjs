$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
       var resultSlider = [];
       if (rs.next()) {
              do {
            	  resultSlider.push({
                           year : rs.getInteger(1),
                           count : rs.getInteger(2)
                     });

              } while (rs.next());
       }
       return resultSlider;
}

var search = $.request.parameters.get("search");
var subjects = $.request.parameters.get("subjects");
var publishers = $.request.parameters.get("publishers");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");
var monthYear = $.request.parameters.get("year");
var conn = $.db.getConnection();
var contentTypes = $.request.parameters.get("contentTypes");

var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(null, null, publishers, subjects, persons, orgs, localities, contentTypes);
if(search) { additionalWhere.clause += " AND contains(c.article_content, ?) "; additionalWhere.parameters.push(search); };


var sql = "SELECT month(article_date), count(*) as count  " + 
"FROM XML_CONTENT c WHERE c.ARTICLE_TYPE like '%' AND year(article_date) = " + monthYear + " " + 
additionalWhere.clause   + 
" GROUP BY month(article_date) " +
"ORDER BY month(article_date) ";
//"with hint (OLAP_PARALLEL_AGGREGATION)";

var pstmt = conn.prepareStatement(sql);

$.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);

var rs = pstmt.executeQuery();
var resultSlider = buildResult(rs);
rs.close();
pstmt.close(); 
conn.close();

if (resultSlider.length == 0) {
	resultSlider = [{ count : 0, year : 1840}, { count : 0, year : 2014 }];
}

$.response.setBody(JSON.stringify({ years : resultSlider }));
