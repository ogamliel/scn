var conn = $.hdb.getConnection();
var sql = 'select VALUE from PROPERTIES where PROPERTY_NAME = \'MAP_TILE_SERVER_URL\'';
var rs = conn.executeQuery(sql);
var server, currentRow, itr=rs.getIterator();
if (itr.next())  {
	currentRow = itr.value();
	server = currentRow["VALUE"];
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({url: server}));
$.response.status = $.net.http.OK;
