$.import("smartnavxs.lib", "filterGenerator");

var ll_lat = $.request.parameters.get("ll_lat");
var ll_lng = $.request.parameters.get("ll_lng");
var ur_lat = $.request.parameters.get("ur_lat");
var ur_lng = $.request.parameters.get("ur_lng");
var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var subjects = $.request.parameters.get("subjects");
var publishers = $.request.parameters.get("publishers");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");
var contentTypes = $.request.parameters.get("contentTypes");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");
var interactionTypes = $.request.parameters.get("interactionTypes");

var searchQuery = search || '';
var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhereForMap(from, to, publishers, 
			subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, null);

var conn = $.db.getConnection();
var sql = 'select ST_ClusterCentroid().ST_ASWKT() AS clustercenter, ST_ClusterCentroid().ST_X() as lon, ST_ClusterCentroid().ST_Y() as lat, count(*) as counter ' + 
'from "DOCUMENTS" where (new ST_POLYGON(\'POLYGON((' + ll_lng + ' ' + ll_lat + ', ' +  ur_lng + ' ' + ll_lat + ', ' +
ur_lng + ' ' + ur_lat + ', ' +  ll_lng + ' ' +  ur_lat + ', ' + ll_lng + ' ' + ll_lat + '))\',1000004326).ST_Intersects("DOC_GEO_POINT")) = 1 ';

if (additionalWhere.clause)  {
	sql += additionalWhere.clause;
	if (searchQuery)  {
		sql += ' AND contains(DOC_CONTENT, ?) ';
	}
}
else if (searchQuery)  {
	sql += ' AND contains(DOC_CONTENT, ?) '; 
}
sql +=' GROUP CLUSTER BY "DOC_GEO_POINT" USING KMEANS CLUSTERS 1024';

var pstmt = conn.prepareStatement(sql);
var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);

if(searchQuery)  {
	pstmt.setString(index++, searchQuery);
}
var rs = pstmt.executeQuery();
var result = [];

while(rs.next()){
	result.push([rs.getReal(3), rs.getReal(2), rs.getString(4)]);
}

rs.close();
pstmt.close();
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({max: 25, data: result}));
$.response.status = $.net.http.OK;

