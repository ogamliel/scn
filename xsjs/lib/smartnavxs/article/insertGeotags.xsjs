var data = $.request.body.asString();
var obj = JSON.parse(data);
var geotags = obj.geotags;
var docId = obj.docId;

var exists = 99;
var conn = $.hdb.getConnection();
if (geotags)  {
	geotags.forEach(function(tag) {
	    var disjunct = tag.Disjunct;
	    var conjunct = disjunct[0].Conjunct[0];
	    var geo = conjunct.Dot[0].key;
	    var textExtent = tag.TextExtent[0].key;

	    // only insert place (P) or Streets, highways, roads, or railroad (R)
	    if (conjunct.key.Class === 'P' || conjunct.key.Class === 'R')  {
	    	var procInsertGeotags = conn.loadProcedure('FFA', 'ADD_DOC_GEOTAG');
	    	var result = procInsertGeotags(docId, textExtent.Anchor.toString(),  parseFloat(tag.key.Confidence), parseFloat(disjunct[0].key.Weight),
	    			conjunct.key.Name, conjunct.key.Class, (conjunct.key.Type) ? conjunct.key.Type : null, (conjunct.key.CountryName) ? conjunct.key.CountryName : null, 
	    			(conjunct.key.ProvinceName) ? conjunct.key.ProvinceName : null, 'Point(' + geo.Longitude + ' ' + geo.Latitude +')');
	    	// value of output parameter 'TAG_ALREADY_EXISTS'
	    	if (result)  {
	    		exists = result['TAG_ALREADY_EXISTS'];
	    	}
	    	conn.commit();
	    }
	});
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({"exists": exists}));
$.response.status = $.net.http.OK;
