var req = $.request.body.asString();
var data = JSON.parse(req);
var content = decodeURIComponent(data.contents);
content = content.replace(/\uFFFD/g, '');
var title = (content) ? content.substring(0, 100) : "FFA Document";

var conn = $.db.getConnection();

function getNextId()  {
	var hdbConn = $.hdb.getConnection();
	var stmt = 'select CAST(SYSUUID AS VARCHAR) "SYSUUID" FROM DUMMY';
	var res = hdbConn.executeQuery(stmt);
	var row = res[0];
	var uuid = row['SYSUUID'];
	hdbConn.close();
	return uuid;
}
var nextId = getNextId();
var pstmt = conn.prepareStatement('INSERT INTO "DOCUMENTS" (DOC_ID, ENTITYID, DOC_CONTENT, EVENT_ENTITYID, DOC_NAME, DOC_TYPE, DOC_UPLOAD_DATE, ' + 
		'DOC_SOURCE, DOC_AUTHOR_NAME, DOC_DATE, DOC_GEO_POINT) VALUES(?,?,?,?,?, \'Document\', CURRENT_TIMESTAMP, \'Manual Upload\', \'Analyst\', CURRENT_TIMESTAMP, new ST_POINT(' + 
		data.lng + ', ' + data.lat + '))' );
pstmt.setString(1,nextId);  
pstmt.setString(2,nextId);  
pstmt.setString(3,content);  
pstmt.setString(4,data.eventId);  
pstmt.setString(5, title);  

pstmt.execute();
conn.commit();
pstmt.close();

pstmt = conn.prepareStatement('CALL ADD_DOCUMENT_TO_FFA_GRAPH(\'' + nextId + '\')');
pstmt.execute();
conn.commit();
pstmt.close();

conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({id: nextId}));
$.response.status = $.net.http.OK;
