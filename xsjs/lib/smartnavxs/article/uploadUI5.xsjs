var data = $.request.entities[0].body.asString();
var eventId = $.request.parameters.get("eventId");
//var data = $.request.parameters.get("content");
var result = [];
var conn = $.db.getConnection();

function getNextId()  {
	var nextId = 0;
	var stmt = 'select max(DOC_ID) from "DOCUMENTS"';
	var pstmt = conn.prepareStatement(stmt);
	var rs = pstmt.executeQuery();
	if (rs.next())  {
		nextId = rs.getInteger(1)+1;
	}
	rs.close();
	return nextId;
}

var nextId = getNextId();
//var pstmt = conn.prepareStatement('INSERT INTO "SDS_DATA"."BS_GRAPHS" (DOC_ID, DOC_BEEBO_TEXT) VALUES(' + nextId + ',\'' + data.replace(/'/g, "''") + '\')' );
var pstmt = conn.prepareStatement('INSERT INTO "DOCUMENTS" (DOC_ID, DOC_BEEBO_TEXT, EVENT_ENTITYID, DOC_NAME, DOC_TYPE, DOC_UPLOAD_DATE) ' +
		'VALUES(?,?,?, \'FFA Document ' + nextId + '\', \'FFA\', CURRENT_TIMESTAMP)' );
pstmt.setString(1,nextId.toString());  
pstmt.setString(2,data);  
pstmt.setString(3,eventId);  

//pstmt.execute();
pstmt.close();
conn.commit();
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({id: nextId, data: data}));
$.response.status = $.net.http.OK;
