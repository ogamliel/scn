var entities = $.request.entities;
var fileBody = $.request.entities[0].body;
var fileBodyStr = $.request.entities[0].body.asString();
var file = $.util.codec.encodeBase64($.request.entities[0].body.asArrayBuffer());
var text = '';
var conn = $.db.getConnection();

function getPdfText()  {
	$.post("http://localhost:3000/textract?buff=" + file, function(data) {
		  return data;
	});
}

function getNextId()  {
	var nextId = 0;
	var stmt = 'select max(DOC_ID) from "DOCUMENTS"';
	var pstmt = conn.prepareStatement(stmt);
	var rs = pstmt.executeQuery();
	if (rs.next())  {
		nextId = rs.getInteger(1)+1;
	}
	rs.close();
	return nextId;
}

text = getPdfText();

var nextId = getNextId();
var pstmt = conn.prepareStatement('INSERT INTO "DOCUMENTS" (DOC_ID, DOC_CONTENT) VALUES(' + nextId + ',' + fileBody + ')' );
//var pstmt = conn.prepareStatement('INSERT INTO "SDS_DATA"."BS_DOCS" (DOC_ID, DOC_CONTENT) VALUES(?,?)' );
pstmt.setString(1,nextId.toString());  
pstmt.setString(2,file);  
//pstmt.execute();
pstmt.close();
conn.commit();
conn.close();

$.response.contentType = "application/json";
//$.response.setBody(JSON.stringify({id: nextId, data: data}));
$.response.status = $.net.http.OK;
