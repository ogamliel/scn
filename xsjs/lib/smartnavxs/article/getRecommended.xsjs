$.import("smartnavxs","smartnav");

var keys = $.request.parameters.get("keys");

var results = [], currentRow;
var conn = $.hdb.getConnection();
var response = {
		recommendations : []
	};

var sql = 'select ARTICLE_KEY, ARTICLE_AUTHOR, ARTICLE_TITLE, SOURCE_NAME, SITETYPE, ARTICLE_DATE, ARTICLE_TYPE from "' + $.smartnavxs.smartnav.smartnav.getSchema() + 
'"."XML_CONTENT" where ARTICLE_KEY IN (' + keys + ')';
var rs = conn.executeQuery(sql);
var itr = rs.getIterator();
while (itr.next())  {
	currentRow = itr.value();
	results.push({
		pdfid: currentRow["ARTICLE_KEY"], 
		authors: (currentRow["ARTICLE_AUTHOR"] || '').split('|'), 
		title: currentRow["ARTICLE_TITLE"],
		journal: currentRow["SOURCE_NAME"],
		sitetype: (currentRow["SITETYPE"] || ''), 
		articleDate: currentRow["ARTICLE_DATE"],
		type: currentRow["ARTICLE_TYPE"]
	});
}

conn.close();

response.recommendations = results;

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(response));
$.response.status = $.net.http.OK;
