var data = $.request.body.asString();
var params = JSON.parse(data);

var content = decodeURIComponent(params.content);
content = content.replace(/\uFFFD/g, '');

var docDate = params.articleDate;
var translationOfEntityid = params.entityid || null;
var authors = params.authors || 'UNKNOWN';
var source = params.source || 'Manual Upload';
var langId = params.langId || 'UNKNOWN';
var lang = params.language || 'UNKNOWN';
var title = params.title || content.substring(0, 100);
var type = params.type || 'NEWS';

var nextId;
var conn = $.db.getConnection();

function getNextId()  {
	var hdbConn = $.hdb.getConnection();
	var stmt = 'select CAST(SYSUUID AS VARCHAR) "SYSUUID" FROM DUMMY';
	var result = hdbConn.executeQuery(stmt);
	var row = result[0];
	var uuid = row['SYSUUID'];
	hdbConn.close();
	return uuid;
}

if (content)  {
	nextId = getNextId();
	var sql = 'INSERT INTO "DOCUMENTS" (DOC_ID, DOC_CONTENT, DOC_NAME, DOC_AUTHOR_NAME, DOC_SOURCE, DOC_LANGUAGE_ID, ' +
				'DOC_LANGUAGE_DISPLAY_NAME, DOC_TYPE, TRANSLATION_OF_ENTITYID, ENTITYID, DOC_UPLOAD_DATE, DOC_DATE) ' +
				'VALUES (?,?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP, ';

	// if document date is null, use CURRENT_TIMESTAMP			
	if (docDate)  {
		sql += '\'' + docDate + '\' ';	
	} 
	else  {
		sql += ' CURRENT_TIMESTAMP ';
	}
	sql += ')';
	var pstmt = conn.prepareStatement(sql);
	pstmt.setString(1,nextId);  
	pstmt.setString(2,content);  
	pstmt.setString(3,title);  
	pstmt.setString(4,authors);  
	pstmt.setString(5,source);  
	pstmt.setString(6,langId);  
	pstmt.setString(7,lang);
	pstmt.setString(8,type);
	pstmt.setString(9,translationOfEntityid);
	pstmt.setString(10,nextId);
	
	pstmt.execute();
	conn.commit();
	pstmt.close();
	conn.close();
}

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({id: nextId}));
$.response.status = $.net.http.OK;
