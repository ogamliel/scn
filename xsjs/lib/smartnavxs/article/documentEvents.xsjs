var id = $.request.parameters.get("id");

var results = [];
var conn = $.hdb.getConnection();
var response = {
		associated : []
	};

var sql = 'select ARTICLE_KEY, ARTICLE_AUTHOR, ARTICLE_TITLE, SOURCE_NAME, SITETYPE, ARTICLE_DATE, ARTICLE_TYPE, OBJECT_TYPE from "XML_CONTENT" where ARTICLE_KEY IN (' +
	'select EVENT_ENTITYID from "DOCUMENTS" where ENTITYID = \'' + id + '\')';

var rs = conn.executeQuery(sql);
var currentRow, itr = rs.getIterator();
while (itr.next())  {
	currentRow = itr.value();
	results.push({
		pdfid: currentRow["ARTICLE_KEY"], 
		authors: (currentRow["ARTICLE_AUTHOR"] || '').split('|'), 
		title: currentRow["ARTICLE_TITLE"],
		journal: currentRow["SOURCE_NAME"],
		sitetype: (currentRow["SITETYPE"] || ''), 
		articleDate: currentRow["ARTICLE_DATE"],
		type: currentRow["ARTICLE_TYPE"],
		objectType: currentRow["OBJECT_TYPE"]
	});
}

conn.close();

response.associated = results;

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(response));
$.response.status = $.net.http.OK;
