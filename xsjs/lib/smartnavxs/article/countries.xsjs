var conn = $.hdb.getConnection();
var rs = conn.executeQuery('select ISO3_COUNTRY_CODE, COUNTRY_NAME from "COUNTRIES" order by COUNTRY_NAME');
var result = [], currentRow, itr=rs.getIterator();

while (itr.next())  {
	currentRow = itr.value();
	result.push({
		id: currentRow["ISO3_COUNTRY_CODE"],
		name: currentRow["COUNTRY_NAME"]
	});
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({countries: result}));
$.response.status = $.net.http.OK;
