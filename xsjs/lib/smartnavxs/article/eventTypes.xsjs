var conn = $.hdb.getConnection();
var rs = conn.executeQuery('select EVENT_TYPE_CODE, EVENT_TYPE from "EVENT_TYPE_CODES"');
var result = [], currentRow, itr=rs.getIterator();

while (itr.next())  {
	currentRow = itr.value();
	result.push({
		id: currentRow["EVENT_TYPE_CODE"],
		name: currentRow["EVENT_TYPE"]
	});
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({eventtypes: result}));
$.response.status = $.net.http.OK;
