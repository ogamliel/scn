//var data = $.request.body.asString();
var type = $.request.parameters.get("type");
var typeId = $.request.parameters.get("typeId");
var actor1 = $.request.parameters.get("actor1");
var actor2 = $.request.parameters.get("actor2");
var date = $.request.parameters.get("date");
var countryIso3 = $.request.parameters.get("countryIso3");
var country = $.request.parameters.get("country");
var location = $.request.parameters.get("location");
var lat = $.request.parameters.get("lat");
var lng = $.request.parameters.get("lng");
var fatalities = $.request.parameters.get("fatalities");
var comments = decodeURIComponent($.request.parameters.get("comments"));
var interactionId = $.request.parameters.get("interactionId");

var conn = $.db.getConnection();

function getNextId()  {
	var hdbConn = $.hdb.getConnection();
	var stmt = 'select CAST(SYSUUID AS VARCHAR) "SYSUUID" FROM DUMMY';
	var result = hdbConn.executeQuery(stmt);
	var row = result[0];
	var uuid = row['SYSUUID'];
	hdbConn.close();
	return uuid;
}

var nextId = getNextId();
var stmt = 'insert into "EVENT" (EVENT_ID, COUNTRY_ISO3, COUNTRY_NAME, EVENT_DATE, EVENT_YEAR, EVENT_TYPE, ' +
	'ACTOR_1, ACTOR_2, WHERE_LOCATION, SOURCE, FATALITIES, COMMENTS, GEO_LOCATION, INTERACTION, ENTITYID) values ' + 
	'(\'' + nextId + '\', \'' + countryIso3 + '\', \'' + country + '\', \'' + date + '\', \'' + date.substring(0,4) + '\', \'' + type + '\', \'' +
	actor1 + '\', \'' + actor2 + '\', \'' + location + '\', \'FFA Event\', ' + fatalities + ', \'' + comments.replace(/'/g, "''") + 
	'\', new ST_POINT(\'POINT(' + lng + ' ' + lat + ')\'), ' +  interactionId + ', \'' + nextId + '\')';
var pstmt = conn.prepareStatement(stmt);
pstmt.execute();
pstmt.close();
conn.commit();

stmt = 'CALL ADD_EVENT_TO_FFA_GRAPH(\'' + nextId + '\')';
pstmt = conn.prepareStatement(stmt);
pstmt.execute();
pstmt.close();
conn.commit();

conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({eventId: nextId, eventNum : nextId}));
$.response.status = $.net.http.OK;
