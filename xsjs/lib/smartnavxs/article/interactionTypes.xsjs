var conn = $.hdb.getConnection();
var rs = conn.executeQuery('select INTERACTION_CODE, INTERACTION from "EVENT_INTERACTION_CODES"');
var result = [], currentRow, itr=rs.getIterator();

while (itr.next())  {
	currentRow = itr.value();
	result.push({
		id: currentRow["INTERACTION_CODE"],
		name: currentRow["INTERACTION"]
	});
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({interactiontypes: result}));
$.response.status = $.net.http.OK;
