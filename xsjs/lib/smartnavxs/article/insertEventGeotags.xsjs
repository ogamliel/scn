var docId = $.request.parameters.get("docId");
var lat = $.request.parameters.get("lat");
var lng = $.request.parameters.get("lng");
var country = $.request.parameters.get("country");
var location = $.request.parameters.get("location");

var conn = $.db.getConnection();

var stmt = 'insert into "DOCUMENTS_GEOTAGS" (DOC_ID, ANCHOR_NAME, CONJUNCT_NAME, CONFIDENCE, COUNTRY_NAME, GEOLOCATION) values (\'' + docId + '\', \'' + location + 
	'\', \'' + location + '\', 1, \'' + country + '\', new ST_POINT(' + lng + ', ' + lat + '))';

var pstmt = conn.prepareStatement(stmt);
var inserted = pstmt.execute();

pstmt.close();
conn.commit();
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({inserted: inserted}));
$.response.status = $.net.http.OK;
