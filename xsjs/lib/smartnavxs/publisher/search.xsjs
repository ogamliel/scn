$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
	var result = [];
	var total = 0;
	
	if (rs.next()) {
	    do {
	        result.push({
	        	publisher: rs.getString(1) || 'Unknown',
	            count: rs.getInteger(2)
	        });
	        total += rs.getInteger(2);
	    } while (rs.next());
	}
	
	return {"data": result, "total": total};
}

var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var subjects = $.request.parameters.get("subjects");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");
var contentTypes = $.request.parameters.get("contentTypes");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");
var interactionTypes = $.request.parameters.get("interactionTypes");

var conn = $.db.getConnection();

var pstmt = $.smartnavxs.lib.filterGenerator.queryCount(conn, "article_author", search, from, to, null, subjects, 
		persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes);
var rs = pstmt.executeQuery();
var result = buildResult(rs);

rs.close();
pstmt.close();
conn.close();

var data = result.data;
var total = result.total;

// set total field in all results
for (var i = 0; i < data.length; ++i) {
	data[i].total = total;
}

$.response.setBody(JSON.stringify({publishers: data}));