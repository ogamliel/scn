$.import("xsjs.smartnav.lib", "filterGenerator");

function buildResult(rs) {
	var result = [];
	var total = 0;

	if (rs.next()) {
	    do {
	        result.push({
	            subject: rs.getString(1),
	            count: rs.getInteger(2)
	        });
	        total += rs.getInteger(2);
	    } while (rs.next());
	}
	
	return {"data": result, "total": total};
}

var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var publishers = $.request.parameters.get("publishers");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");

var conn = $.db.getConnection();

var pstmt = $.xsjs.smartnav.lib.filterGenerator.queryCount(conn, "source_name", search, from, to, publishers, null, persons, orgs, localities);

var rs = pstmt.executeQuery();
var result = buildResult(rs);

rs.close();
pstmt.close();
conn.close();

var data = result.data;
var total = result.total;

// set total field in all results
for (var i = 0; i < data.length; ++i) {
	data[i].total = total;
}

$.response.setBody(JSON.stringify({subjects: data}));