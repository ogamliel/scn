$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs, sitetype) {
       var result = [];
       var subjects = [];
       var total = 0;
       var catTotal = 0;
       var itemTotal = 0;
       var category = '';
       var styleClass = 'notSelected';

       if (rs.next()) {
          
    	   do {   
    		   var nextCategory = (rs.getString(1) || 'Unknown');
    	   
    		   if (nextCategory !== category)  {
    			   // loop through the sitetypes(subject) to reset the categories that were previously selected
    			   if (sitetype)  {
    				   if (Array.isArray(sitetype)) {
    					   for (var i = 0; i < sitetype.length; i++)  {
    						   if(sitetype[i].indexOf(category) != -1)  {
    							   styleClass = 'selected';
    							   break;
    						   }
    					   }
    				   }
    				   else  {
    					   if (sitetype.indexOf(category) != -1)
    						   styleClass = 'selected';				   
    				   }
    			   }
                
                	if (category != '')  {
	                     result.push({
	                           category : category,
	                           catTotal : catTotal,
	                           isSelected : (styleClass === 'selected') ? true : false,
	                           styleClass : styleClass,
	                           items : subjects
	                     });
                	}
                     // reset values
                     category = nextCategory;
                     catTotal = 0;
                     itemTotal = 0;
                     subjects = [];
                     styleClass = 'notSelected';
              }
              
              itemTotal += rs.getInteger(3);
              
               subjects.push({
                   subject: rs.getString(2),
                   count: rs.getInteger(3),
                   total: itemTotal
              });
               
              catTotal += 1;
               
           } while (rs.next());
       
			// push the last result		
    	   
    	   // loop through the sitetypes(subject) to reset the categories that were previously selected
		   if (sitetype)  {
			   if (Array.isArray(sitetype)) {
				   for (var i = 0; i < sitetype.length; i++)  {
					   if(sitetype[i].indexOf(category) != -1)  {
						   styleClass = 'selected';
						   break;
					   }
				   }
			   }
			   else  {
				   if (sitetype.indexOf(category) != -1)
					   styleClass = 'selected';				   
			   }
		   }

               
            result.push({
            	category : category,
                catTotal : catTotal,
                isSelected : (styleClass === 'selected') ? true : false,
                styleClass : styleClass,
                items : subjects
            });
      }
       
       return {"data": result, "catTotal": catTotal, "total": itemTotal};
}

var search = $.request.parameters.get("search");
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var publishers = $.request.parameters.get("publishers");
var sitetype = $.request.parameters.get("sitetype");
var subjects = $.request.parameters.get("subjects");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");
var contentTypes = $.request.parameters.get("contentTypes");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");

var additionalWhere = $.FFA.smartnav.rest.lib.filterGenerator.getAdditionalWhere(from, to, publishers, null, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd);
if(search) { additionalWhere.clause += " AND contains(c.article_content, ?) "; additionalWhere.parameters.push(search); };

var sql = "SELECT sitetype, source_name, count(*) as count  " + 
"FROM XML_CONTENT  c " + 
"WHERE 1=1 " + additionalWhere.clause + 
(" GROUP BY sitetype, source_name ") + 
"ORDER BY sitetype, count DESC ";
"with hint (OLAP_PARALLEL_AGGREGATION)";

var conn = $.db.getConnection();
var pstmt = conn.prepareStatement(sql);
$.FFA.smartnav.rest.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);

var rs = pstmt.executeQuery();
var result = buildResult(rs, subjects);

rs.close();
pstmt.close();
conn.close();

var data = result.data;
var total = result.total;

// set total field in all results
for (var i = 0; i < data.length; ++i) {
       data[i].total = total;
}

$.response.setBody(JSON.stringify({subjects: data}));


