// Implementation of the "OpenSearch" protocol for type-ahead searches
// see http://www.opensearch.org/
// Response format: http://www.opensearch.org/Specifications/OpenSearch/Extensions/Suggestions/1.1#Response_format

var suggestions = 10;
var query = $.request.parameters.get("q");
var terms = [];
var descriptions = [];


query = query.toLowerCase() + '%';
var sql = 'select top ? "TA_TOKEN", sum("COUNT") AS "TOTAL" from "$TA_FTI_TMP002"' +
'where "TA_TOKEN" like ?' +
'group by "TA_TOKEN"' +
'order by "TOTAL" desc';

var conn = $.db.getConnection();
var pstmt = conn.prepareStatement(sql);

pstmt.setInt(1, suggestions);
pstmt.setString(2, query);

var rs = pstmt.executeQuery();
while (rs.next()) {
	terms.push(rs.getString(1));
	descriptions.push(rs.getInteger(2) + " hits");
}

rs.close();
pstmt.close();

conn.close();

var output = [
	query, terms, descriptions
];

$.response.setBody(JSON.stringify(output));
