var schema = $.request.parameters.get('schema');
var table = $.request.parameters.get('table');
var docColumn = $.request.parameters.get('docColumn');
var id = $.request.parameters.get('id');
var idColumn = $.request.parameters.get('idColumn');

var includeDoc = $.request.parameters.get('includeDoc');
var minScore = $.request.parameters.get('minScore');
var maxResults = $.request.parameters.get('maxResults');

function getDocumentsRelatedToDocument()  {

	var WhereClause, topRows, SelectFields, stmt;
	
	// If a minimum score is provided, set the minimum score, otherwise do not set a minimum
	
	if (minScore)
		WhereClause = 'where SCORE > ' + minScore;
	else
		WhereClause = '';

	// If a maximum number of rows is specified, set the maximum rows, otherwise default to 20
	
	if (maxResults)
		topRows = maxResults;
	else
		topRows = 20;
	
	// If includeDoc is set to Y, include the Document column, otherwise dont.
	
	if (includeDoc == 'Y')
		SelectFields = idColumn + ', ' + docColumn;
	else
		SelectFields = idColumn;
	
	var stmt = 'select ' + SelectFields + ', RANK, TOTAL_TERM_COUNT, SCORE FROM TM_GET_RELATED_DOCUMENTS (' +
	           'DOCUMENT IN FULLTEXT INDEX WHERE ' + idColumn + ' = \'' + id + '\' ' +
	           'SEARCH "' + docColumn + '" FROM "' + table + '" ' +
	           'RETURN TOP ' + topRows + ' "' + idColumn + '", "' + docColumn + '"' +
	           ') AS T ' + WhereClause;
console.log(stmt)
	var conn = $.hdb.getConnection();
	var rs = conn.executeQuery(stmt);
	var itr = rs.getIterator();
	var row;
	
	var result = [];

	while (itr.next())  {
		row = itr.value();
		if (includeDoc == 'Y')
			result.push({
				ID: row[idColumn],
				Document: row[docColumn],
				Rank: row['RANK'],
				TermCount: row['TOTAL_TERM_COUNT'],
				Score: row['SCORE']
			});
		else
			result.push({
				ID: row[idColumn],
				Rank: row['RANK'],
				TermCount: row['TOTAL_TERM_COUNT'],
				Score: row['SCORE']
			});
	}
	return result;
}

var result = getDocumentsRelatedToDocument();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({results: result, totalHits: result.length}));
$.response.status = $.net.http.OK;

