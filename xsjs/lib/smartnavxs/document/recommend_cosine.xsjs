 var ref = $.request.parameters.get("ref");
 var corr = $.request.parameters.get("corr");  // unused
var signific = $.request.parameters.get("signific");

var response = {
	recommendations : [] + ref
};
           
    var sql =
    	"select distinct c.article_key, d.article_type, ifnull(d.article_author,'Unknown')," +
    	" d.article_title, d.source_name,d.article_date,'X','X','X','X','X',d.sitetype from " +
    	" (select top 25 b.article_key,count(*) from" + 
    	"  (select article_key, ta_type, ta_token, count(*) CNT from  "+
    	"    XML_CONTENT_TOKEN_MAP_CTRS2" +
    	"   where article_key = ?" + 
    	" group by article_key, ta_type, ta_token ) a," + 
    	" (select article_key, ta_type, ta_token, count(*) CNT from" + 
    	"    XML_CONTENT_TOKEN_MAP_CTRS2" +
        "  	where article_key <> ?" + 
    	" group by article_key, ta_type, ta_token ) b" +
    	" where a.ta_token = b.ta_token" +
    	" group by b.article_key" +
    	" order by 2 desc "+
    	" with hint(OLAP_PARALLEL_AGGREGATION) ) c ,"+
    	" XML_CONTENT_TOKEN_MAP_CTRS2 d"+
    	" where c.article_key = d.article_key" +
    	" and d.article_key <> ?";

	var conn = $.db.getConnection();
	var pstmt = conn.prepareStatement(sql);
	var psi = 0;
	var vSiteType = '';
	  
	pstmt.setString(1, ref);
	pstmt.setString(2, ref);
	pstmt.setString(3, ref);
	
	var result = [];
	var rs = pstmt.executeQuery();
	while (rs.next()) {
	   
		result.push({
	    	pdfid: rs.getString(1), 
	        authors: (rs.getString(3) || '').split('|'),
	        title: rs.getString(4),
	       journal: rs.getString(5),
	       sitetype: (rs.getString(12) || 'Unknown'), 
	        articleDate: rs.getString(6),
		    volume: rs.getString(7),
	        firstpage: rs.getString(8),
	        lastpage: rs.getString(9), 
	        type: rs.getString(2), 
            correlation: rs.getString(10) 
	    });
	}
	response.recommendations = result;

	rs.close();
	pstmt.close();
	conn.close();

$.response.setBody(JSON.stringify(response));

/*
 
 var ref = $.request.parameters.get("ref");

var response = {
	recommendations : [] + ref
};
           
var sql =
    	"select distinct c.article_key, d.article_type, ifnull(d.article_author,'Unknown')," +
    	" d.article_title, d.source_name,d.article_date,d.sitetype from " +
    	" (select top 25 b.article_key,count(*) from" + 
    	"  (select article_key, ta_type, ta_token, count(*) CNT from  "+
    	"    FFA.XML_CONTENT _TOKEN_MAP_CTRS2" +
    	"   where article_key = ?" + 
    	" group by article_key, ta_type, ta_token ) a," + 
    	" (select article_key, ta_type, ta_token, count(*) CNT from" + 
    	"    FFA.XML_CONTENT _TOKEN_MAP_CTRS2" +
        "  	where article_key <> ?" + 
    	" group by article_key, ta_type, ta_token ) b" +
    	" where a.ta_token = b.ta_token" +
    	" group by b.article_key" +
    	" order by 2 desc "+
    	" with hint(OLAP_PARALLEL_AGGREGATION) ) c ,"+
    	" FFA.XML_CONTENT _TOKEN_MAP_CTRS2 d"+
    	" where c.article_key = d.article_key" +
    	" and d.article_key <> ?";

	var conn = $.db.getConnection();
	var pstmt = conn.prepareStatement(sql);
	var psi = 0;
	var vSiteType = '';
	  
	pstmt.setString(1, ref);
	pstmt.setString(2, ref);
	pstmt.setString(3, ref);
	
	var result = [];
	var rs = pstmt.executeQuery();
	while (rs.next()) {	   
		result.push({
			pdfid: rs.getString(1),
	        type: rs.getString(2), 
	        authors: (rs.getString(3) || '').split('|'),
	        title: rs.getString(4),
	        journal: rs.getString(5),
	        articleDate: rs.getString(6),
	        sitetype: (rs.getString(7) || 'Unknown')
	    });
	}
	response.recommendations = result;

	rs.close();
	pstmt.close();
	conn.close();

$.response.setBody(JSON.stringify(response));

*/