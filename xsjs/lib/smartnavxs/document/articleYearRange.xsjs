var result = {};
var conn = $.hdb.getConnection();

var query = 'SELECT max(article_year) as "MAX", min(article_year) as "MIN" FROM XML_CONTENT ';
var rs = conn.executeQuery(query);
var currentRow, itr = rs.getIterator();

if (itr.next()) {
	currentRow = itr.value();
	result.max = currentRow["MAX"];
	result.min = currentRow["MIN"];
}
conn.close();

$.response.contentType = "text/json";
$.response.status = $.net.http.OK;
$.response.setBody(JSON.stringify(result));
