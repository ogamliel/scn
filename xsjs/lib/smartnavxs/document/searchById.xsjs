$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
	var result = [];
	while (rs.next()) {
	    result.push({
	        pdfid: rs.getString(1),
	        rank: rs.getDouble(2),
	        title: rs.getString(3),
	        snippets: rs.getString(4),
	        year: rs.getString(5),
	        journal: rs.getString(6),
			authors : (rs.getString(7) || 'Unknown').split('|'),
			articleDate : rs.getString(8),
			type : rs.getString(9),
			cpnyName : rs.getString(10),
			extractDate: rs.getString(11),
			sitetype: (rs.getString(12) || 'Unknown'),
			objectType: rs.getString(13),
			language: (rs.getString(14)) ? rs.getString(14) + " (" + rs.getString(15) + ")" : 'Unknown'
	    });
	}
	return result;
}

function addSnippets(conn, result, searchQuery, orderByClause, limit, snippetfunction, parameters, formatter)
{
	if(result.length > 0)
	{
		if(!formatter) formatter = function(data) { return data; };
		if(!parameters) parameters = "with parameters ('SNIPPET_LENGTH' = '290', 'SNIPPET_COUNT' = '1')";
		if(!snippetfunction) snippetfunction = "'... ' || replace(substr_after(substring(SNIPPETS(ARTICLE_CONTENT), LENGTH(SNIPPETS(ARTICLE_CONTENT)) / 2, 500), ' ') || SUBSTR_BEFORE(SUBSTRING(LENGTH(SNIPPETS(ARTICLE_CONTENT)) / 2 + 1, 20), ' '), 'BOOKMARKS:', '') ||' ...' as snippet";
		
		var pdfids = [];
		for (var i = 0; i < result.length; i++) {
			pdfids.push(result[i].pdfid)
		}
		var pdfidInClause = $.smartnavxs.lib.filterGenerator.getInClause("ARTICLE_KEY", pdfids);

		var sql =
			"SELECT " + 
			snippetfunction +
		    " FROM XML_CONTENT  c " +
		    " WHERE contains(c.ARTICLE_CONTENT, ?) " +
		    pdfidInClause.clause +
		    orderByClause +
			" LIMIT ? " +
			parameters;
		var pstmt = conn.prepareStatement(sql);		

		pstmt.setString(1, searchQuery);
	
		var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, pdfidInClause.parameters, 2);
		pstmt.setInt(index++, limit);
		
		var start = new Date();
		var rs = pstmt.executeQuery();
		var end = new Date();
		var time = (end-start);
		
		var i = 0;
		while (rs.next()) {
			if (result[i])  {
				result[i].snippets = formatter(rs.getString(1));
				i++;
			}
		}
		
		rs.close();
		pstmt.close();
		
		return time;
	}	
	return 0;
}

function search(conn, limit, offset, ids) {
	var docidInClause = $.smartnavxs.lib.filterGenerator.getInClause("ARTICLE_KEY", ids);
	var sql =
        "SELECT " +
        "     md.ARTICLE_KEY, " +
        "     1 as RANK, " +
        "     md.ARTICLE_TITLE, " +
        "     '' as SNIPPETS, " +
        "    md.ARTICLE_YEAR, " +
        "     md. SOURCE_NAME, " +
        "     md.ARTICLE_AUTHOR , " + 
      "    md.ARTICLE_DATE, " +
      "    md.ARTICLE_TYPE, " +
      "    md.COMPANY_NAME, " +
      "    md.EXTRACTED_DATE, " +
      "    md.SITETYPE, " +
      "   md.object_type, " +
      "   md.language_name, " +
        "   md.language_id " +
		" FROM " + 
        "       (SELECT c.ARTICLE_KEY, ARTICLE_TITLE, " +
        "			ARTICLE_AUTHOR, SOURCE_NAME, " +
        "			ARTICLE_YEAR, ARTICLE_DATE, ARTICLE_TYPE, COMPANY_NAME, EXTRACTED_DATE, SITETYPE, OBJECT_TYPE, LANGUAGE_NAME, LANGUAGE_ID from XML_CONTENT  c " + 
        "WHERE 1=1 " +
		docidInClause.clause +
        " ORDER BY ARTICLE_KEY desc LIMIT ? OFFSET ?) md " +
        " ORDER BY md.ARTICLE_KEY ";

	var pstmt = conn.prepareStatement(sql);
	var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, docidInClause.parameters, 1);
	pstmt.setInt(index++, limit && parseInt(limit) || 10);
	pstmt.setInt(index++, offset && parseInt(offset) || 0);

	var start = new Date();
	var rs = pstmt.executeQuery();
	var end = new Date();
	var time = end-start;
	var result = buildResult(rs);
	
	var searchString = '';
	for (var i = 0; i < result.length; i++) {
		if (result[i].title != null)  { 
			searchString += ' OR ' + result[i].title.split(' ')[0];
		}
	}
	
	time += addSnippets(conn, result, '%', " ORDER BY ARTICLE_KEY ", limit);
	rs.close();
	pstmt.close();

	return {"data" : result, "duration" : time};
}


var scriptStart = new Date();

var limit = parseInt($.request.parameters.get("limit") || 10);
var offset = parseInt($.request.parameters.get("offset") || 0);
var ids = $.request.parameters.get("ids");
var count = $.request.parameters.get("count");

var conn = $.db.getConnection();
var result = search(conn, limit, offset, ids);
var output = { "count": count === 0 ? 0 : count, "results" : result.data, "duration": (new Date()-scriptStart) };
conn.close();

$.response.setBody(JSON.stringify(output));
