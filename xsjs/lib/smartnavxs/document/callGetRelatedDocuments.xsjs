$.import("smartnavxs","smartnav");

var entityid = $.request.parameters.get('id');

 function callGetRelatedDocuments()  {
	var result;
	if (entityid)  {
		var hdbConn = $.hdb.getConnection();
		var proc = hdbConn.loadProcedure($.smartnavxs.smartnav.smartnav.getSchema(), 'get_related_documents');
		result = proc(entityid);
		hdbConn.close();
	}
	return result;
 }
 
function getRelatedDocuments()  {
	var results = [], currentRow;
	var conn = $.hdb.getConnection();
	
	var sql = "select ENTITYID, RANK, TOTAL_TERM_COUNT, SCORE FROM TM_GET_RELATED_DOCUMENTS (DOCUMENT IN FULLTEXT INDEX " +
	"where ENTITYID = '" + entityid + "' SEARCH \"DOC_CONTENT\" FROM \"" + $.smartnavxs.smartnav.smartnav.getSchema() + "\".\"DOCUMENTS\" RETURN TOP 10 \"ENTITYID\", \"DOC_CONTENT\") AS T";
	var rs = conn.executeQuery(sql);
	var itr = rs.getIterator();
	while (itr.next())  {
		currentRow = itr.value();
		results.push(currentRow["ENTITYID"]);
	}

	conn.close();
	return results;
}
 
var result = getRelatedDocuments();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(result));
$.response.status = $.net.http.OK;
