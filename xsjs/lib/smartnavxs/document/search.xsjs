$.import("smartnavxs.lib", "filterGenerator");

function buildResult(rs) {
	var result = [];
	while (rs.next()) {
	    result.push({
	        pdfid: rs.getString(1),
	        rank: (rs.getDouble(2) === 'NaN') ? 0 : rs.getDouble(2),
	        title: rs.getString(3),
	        snippets: rs.getString(4),
	        year: rs.getString(5),
	        journal: rs.getString(6),
			authors : (rs.getString(7) || 'Unknown').split('|'),
			articleDate : rs.getString(8),
			type : rs.getString(9),
			cpnyName : rs.getString(10),
			extractDate: rs.getString(11),
			sitetype: (rs.getString(12) || 'Unknown'),
			objectType: rs.getString(13),
			language: (rs.getString(14)) ? rs.getString(14) + " (" + rs.getString(15) + ")" : 'Unknown',
			entityid: rs.getString(16)
	    });
	}
	return result;
}

function subjectsInClause(subjects) {
	var inClause = "";
	if (subjects && Array.isArray(subjects)) {
		for (var i = 0; i < subjects.length; i++) {
			inClause += "?";
			inClause += (i != subjects.length-1 ) ? "," : "";
		}
	} else {
		inClause = "?";
	}
	return inClause;
}

function addSnippets(conn, result, searchQuery, orderByClause, limit, snippetfunction, parameters, formatter)
{
	if(result.length > 0)
	{
		if(!formatter) formatter = function(data) { return data; };
		if(!parameters) parameters = "with parameters ('SNIPPET_LENGTH' = '290', 'SNIPPET_COUNT' = '1')";
		if(!snippetfunction) snippetfunction = "'... ' || replace(substr_after(substring(SNIPPETS(ARTICLE_CONTENT), LENGTH(SNIPPETS(ARTICLE_CONTENT)) / 2, 500), ' ') || SUBSTR_BEFORE(SUBSTRING(LENGTH(SNIPPETS(ARTICLE_CONTENT)) / 2 + 1, 20), ' '), 'BOOKMARKS:', '') ||' ...' as snippet";
		
		var pdfids = [];
		for (var i = 0; i < result.length; i++) {
			pdfids.push(result[i].pdfid)
		}
		var pdfidInClause = $.smartnavxs.lib.filterGenerator.getInClause("ARTICLE_KEY", pdfids);

		var sql =
			"SELECT " + 
			snippetfunction +
		    " FROM XML_CONTENT  c " +
		    " WHERE contains(c.ARTICLE_CONTENT, ?) " +
		    pdfidInClause.clause +
		    orderByClause +
			" LIMIT ? " +
			parameters;
		var pstmt = conn.prepareStatement(sql);		
	
		pstmt.setString(1, searchQuery);
	
		var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, pdfidInClause.parameters, 2);
		
		pstmt.setInt(index++, limit);
		
		var start = new Date();
		var rs = pstmt.executeQuery();
		var end = new Date();
		var time = (end-start);
		
		var i = 0;
		while (rs.next()) {
			if (result[i])  {
				result[i].snippets = formatter(rs.getString(1));
				i++;
			}
		}
		
		rs.close();
		pstmt.close();
		
		return time;
	}	
	return 0;
}

function fulltextSearch(conn, searchQuery, limit, offset, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, 
		interactionTypes, mapBounds) {
	var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(from, to, publishers, 
			subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
	
	var sql = (additionalWhere.clause) &&
	    /* FILTERED version */	
	    "SELECT " +
	    "   IFNULL(md.ARTICLE_KEY,  ' '), " +
	    "   SCORE,  " +
	    "   IFNULL(md.ARTICLE_TITLE, ' ')," +
	    //"   SNIPPETS(ARTICLE_CONTENT), " +
	    "   IFNULL(md.ARTICLE_TITLE, ' '), " +
	    "   md.ARTICLE_YEAR, " +
        "   IFNULL(md.source_name, ' '), " + 
        "   IFNULL(md.article_author, 'Unknown'), " + 
        "   md.ARTICLE_DATE, " + 
        "   IFNULL(md.ARTICLE_TYPE, ' '), " + 
        "   IFNULL(md.company_name,  ' ')," + 
        "   md.extracted_date, " +
        "   IFNULL(md.sitetype,  ' ')," +
        "   IFNULL(md.object_type, ' '), " +
        "   IFNULL(md.language_name,  ' ')," +
        "   IFNULL(md.language_id, ' '), " +
        "   md.entityid " +
        " FROM " + 
//        "       (SELECT c.ARTICLE_KEY, score() as \"SCORE\", ARTICLE_TITLE, " +
        "       (SELECT c.ARTICLE_KEY, 1 as \"SCORE\", ARTICLE_TITLE, " +
        "			ARTICLE_AUTHOR, SOURCE_NAME, " +
        "			ARTICLE_YEAR, ARTICLE_DATE, ARTICLE_TYPE, COMPANY_NAME, EXTRACTED_DATE, SITETYPE, OBJECT_TYPE, LANGUAGE_NAME, LANGUAGE_ID, ENTITYID from XML_CONTENT  c " + 
        " WHERE 1=1 " +
        additionalWhere.clause +
        " AND contains(c.ARTICLE_CONTENT, ?) " +
//        " ORDER BY score() desc LIMIT ? OFFSET ?) md " +
        " ORDER BY \"SCORE\" desc LIMIT ? OFFSET ?) md " +
       " ORDER BY md.EXTRACTED_DATE desc " ||
        
	    /* UNFILTERED version */
        "select " + 
        "   IFNULL(md.ARTICLE_KEY, ' '),  " + 
        "   SCORE, " + 
        "   IFNULL(md.ARTICLE_TITLE, ' '),  " + 
        "   IFNULL(md.ARTICLE_TITLE,  ' '), " + 
        "   md.ARTICLE_YEAR, " + 
        "   IFNULL(md.source_name, ' '),  " + 
        "   IFNULL(md.article_author, ' '),  " + 
        "   md.ARTICLE_DATE, " + 
        "   IFNULL(md.ARTICLE_TYPE, ' '),  " + 
        "   IFNULL(md.company_name,  ' '), " + 
        "   md.extracted_date, " +
        "   IFNULL(md.sitetype, ' '),  " +
        "   IFNULL(md.object_type, ' '),  " +
        "   IFNULL(md.language_name,  ' '), " +
        "   IFNULL(md.language_id, ' '),  " +
        "   md.entityid " +
//        "   FROM (SELECT c.ARTICLE_KEY, score() as \"SCORE\", ARTICLE_TITLE, " +
        "   FROM (SELECT c.ARTICLE_KEY, 1 as \"SCORE\", ARTICLE_TITLE, " +
        "		ARTICLE_AUTHOR, SOURCE_NAME, " +
        "		ARTICLE_YEAR, ARTICLE_DATE, ARTICLE_TYPE, COMPANY_NAME, EXTRACTED_DATE, SITETYPE, OBJECT_TYPE, LANGUAGE_NAME, LANGUAGE_ID, ENTITYID " +
        "       from XML_CONTENT  c WHERE contains(c.ARTICLE_CONTENT, ?) " + 
        " 		ORDER BY score() desc LIMIT ? OFFSET ?) md " +
        " ORDER BY md.EXTRACTED_DATE desc, md.ARTICLE_TITLE ";

	var pstmt = conn.prepareStatement(sql);

	var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);
	
	pstmt.setString(index++, searchQuery);
	pstmt.setInt(index++, limit && parseInt(limit) || 20);
	pstmt.setInt(index++, offset && parseInt(offset) || 0);

	var start = new Date();
	var rs = pstmt.executeQuery();
	var end = new Date();
	var time = end-start;
	var result = buildResult(rs);
	rs.close();
	pstmt.close();
	
	//time += addSnippets(conn, result, searchQuery, " ORDER BY score() DESC ", limit);
	time += addSnippets(conn, result, searchQuery, " ORDER BY EXTRACTED_DATE desc, ARTICLE_TITLE ", limit); 
	return {"data" : result, "duration" : time};
}

function filterSearch(conn, limit, offset, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds) {
	var additionalWhere = $.smartnavxs.lib.filterGenerator.getAdditionalWhere(from, to, publishers, 
			subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
	var sql =
        "SELECT " +
        "     IFNULL(md.ARTICLE_KEY, ' '), " +
        "     1 as RANK, " +
        "     IFNULL(md.ARTICLE_TITLE,  ' ')," +
        "     '' as SNIPPETS, " +
        "   md.ARTICLE_YEAR," +
        "     IFNULL(md. SOURCE_NAME,  ' ')," +
        "     IFNULL(md.ARTICLE_AUTHOR,  ' ')," + 
      "    md.ARTICLE_DATE, " +
      "    IFNULL(md.ARTICLE_TYPE,  ' ')," +
      "   IFNULL( md.COMPANY_NAME,  ' ')," +
      "    md.EXTRACTED_DATE, " +
      "    IFNULL(md.SITETYPE,  ' ')," +
      "   IFNULL(md.object_type,  ' ')," +
      "   IFNULL(md.language_name,  ' ')," +
        "   IFNULL(md.language_id,  ' ')," +
        "   md.entityid " +
		" FROM " + 
        "       (SELECT c.ARTICLE_KEY, ARTICLE_TITLE, " +
        "			ARTICLE_AUTHOR, SOURCE_NAME, " +
        "			ARTICLE_YEAR, ARTICLE_DATE, ARTICLE_TYPE, COMPANY_NAME, EXTRACTED_DATE, SITETYPE, OBJECT_TYPE, LANGUAGE_NAME, LANGUAGE_ID, ENTITYID from XML_CONTENT  c " + 
        " WHERE 1=1 " +
        (additionalWhere.clause ||  " ") +
        " ORDER BY EXTRACTED_DATE desc, ARTICLE_TITLE LIMIT ? OFFSET ?) md " +
        " ORDER BY md.EXTRACTED_DATE desc, ARTICLE_TITLE";

	var pstmt = conn.prepareStatement(sql);
	var index = $.smartnavxs.lib.filterGenerator.applyParameters(pstmt, additionalWhere.parameters, 1);
		
	pstmt.setInt(index++, limit && parseInt(limit) || 10);
	pstmt.setInt(index++, offset && parseInt(offset) || 0);

	var start = new Date();
	var rs = pstmt.executeQuery();
	var end = new Date();
	var time = end-start;
	var result = buildResult(rs);
	
	var searchString = '';
	for (var i = 0; i < result.length; i++) {
		if (result[i].title != null)  { 
			searchString += ' OR ' + result[i].title.split(' ')[0];
		}
	}
	
	time += addSnippets(conn, result, '%', " ORDER BY EXTRACTED_DATE desc, ARTICLE_TITLE ", limit);
	rs.close();
	pstmt.close();

	return {"data" : result, "duration" : time};
}


var scriptStart = new Date();

var search = $.request.parameters.get("search");
var limit = parseInt($.request.parameters.get("limit") || 10);
var offset = parseInt($.request.parameters.get("offset") || 0);
var from = $.request.parameters.get("from");
var to = $.request.parameters.get("to");
var subjects = $.request.parameters.get("subjects");
var publishers = $.request.parameters.get("publishers");
var persons = $.request.parameters.get("persons");
var orgs = $.request.parameters.get("orgs");
var localities = $.request.parameters.get("localities");
var contentTypes = $.request.parameters.get("contentTypes");
var sentimentStart = $.request.parameters.get("sentimentStart");
var sentimentEnd = $.request.parameters.get("sentimentEnd");
var interactionTypes = $.request.parameters.get("interactionTypes");
var mapBounds = $.request.parameters.get("mapBounds");
if (mapBounds)  {
	mapBounds = JSON.parse(mapBounds);
}
var params = $.request.parameters;

var searchQuery = search || '';
var conn = $.db.getConnection();

var count = $.smartnavxs.lib.filterGenerator.getCount(conn, null, search, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
var result = [];

if (searchQuery) {
	result = fulltextSearch(conn, searchQuery, limit, offset, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
} else {
	result = filterSearch(conn, limit, offset, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
}
var output = { "count": count == 0 ? 0 : count, "results" : result.data, "duration": (new Date()-scriptStart), "searchTerm": searchQuery }

conn.close();

$.response.setBody(JSON.stringify(output));
