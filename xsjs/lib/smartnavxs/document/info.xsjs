function buildResult(rs) {
	var result = {};
	
	if (rs.next()) {
	    result = {
	        pdfid: rs.getString(1),
	        type: rs.getString(2),
	        title: rs.getString(3),
	        publisher_name: rs.getString(4),
	        year: rs.getString(5),
	        authors : rs.getString(6)
	    };
	}
	
	return result;
}

function queryForPdfInfo(conn, pdfId) {
	var sql = 
		"SELECT  " + 
		"	c.article_key,  " +
		"	c.article_type,  " +
		"	c.article_title,  " +
		"	c.source_name,  " + 
		"	c.article_year,  " + 
		"	c.article_author  " + 
		"FROM XML_CONTENT c " + 
		"where c.article_key = ? ";
		
	var pstmt = conn.prepareStatement(sql);

	pstmt.setString(1, pdfId);

	return pstmt;
}

var pdfId = $.request.parameters.get("pdfId");

var result = {};

if (pdfId) {
	var conn = $.db.getConnection();

	var pstmt = queryForPdfInfo(conn, pdfId);
	var rs = pstmt.executeQuery();
	result = buildResult(rs);
	
	rs.close();
	pstmt.close();
	conn.close();
}


$.response.setBody(JSON.stringify(result));
