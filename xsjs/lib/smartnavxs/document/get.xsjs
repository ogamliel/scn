var pdfid = $.request.parameters.get("pdfid") || '';
//var body = null;
var body = ' ';
var langId = '';

if (pdfid) {
	var conn = $.hdb.getConnection();
//	var query = "SELECT replace(cast(highlighted(article_content) as varchar),BINTOSTR(HEXTOBIN('0A')),'<BR>') as \"CONTENT\", language_id as \"LANG\" FROM XML_CONTENT WHERE contains(article_content, '%') and article_key = '" + pdfid + "'";
	var query = "SELECT highlighted(article_content) as \"CONTENT\", language_id as \"LANG\" FROM XML_CONTENT WHERE contains(article_content, '%') and article_key = '" + pdfid + "'";
	var rs = conn.executeQuery(query);
	var itr = rs.getIterator();
	if (itr.next()) {
		var currentRow = itr.value();
		body = currentRow["CONTENT"];
		langId = currentRow["LANG"];
	}
	conn.close();
}

$.response.status = $.net.http.OK;
$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({content: body.toString(), langId: langId }));
