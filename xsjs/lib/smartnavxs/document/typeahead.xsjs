var suggestions = 10;
var query = $.request.parameters.get("q");
var terms = [];
var descriptions = [];

var output = [];

if (query) {
	query = query.toLowerCase() + '%';
	var sql = 'select top ' + suggestions + ' "TA_TOKEN", sum("TA_COUNTER") AS "TOTAL" from "TA_FTI_ARTICLE" ' +
	' where lower(TA_TOKEN) like lower(\'' + query + '\')' +
	' group by "TA_TOKEN"' +
	' order by "TOTAL" desc';

	var conn = $.hdb.getConnection();
	var rs = conn.executeQuery(sql);
	var currentRow, itr = rs.getIterator();

	while (itr.next()) {
		currentRow = itr.value();
		terms.push(currentRow["TA_TOKEN"]);
		descriptions.push(currentRow["TOTAL"] + " hits");
	}

	conn.close();

	output = [ query, terms, descriptions ];
}

$.response.contentType = "application/json; charset=utf-8";
$.response.setBody(JSON.stringify(output));