var translationOfEntityid = $.request.parameters.get("entityid");
var conn = $.hdb.getConnection();

var response = {
	translations : []
};

var sql =
	"select distinct article_key, article_type, article_author, article_title, source_name, article_date, object_type, sitetype, language_name, language_id, entityid from XML_CONTENT " +
	"where TRANSLATION_OF_ENTITYID = '" + translationOfEntityid + "' ";
var rs = conn.executeQuery(sql);
var result = [], currentRow, itr=rs.getIterator();
while (itr.next()) {
	currentRow = itr.value();
	result.push({
    	pdfid: currentRow["ARTICLE_KEY"], 
        type: currentRow["ARTICLE_TYPE"], 
        authors: (currentRow["ARTICLE_AUTHOR"] || '').split('|'),
        title: currentRow["ARTICLE_TITLE"],
    	journal: currentRow["SOURCE_NAME"],
        articleDate: currentRow["ARTICLE_DATE"],
        objectType: currentRow["OBJECT_TYPE"],
    	sitetype: (currentRow["SITETYPE"] || 'Unknown'), 
        language: (currentRow["LANGUAGE_NAME"]) || 'Unknown',
		entityid: currentRow["ENTITYID"]
    });
}
response.translations = result;
conn.close();

$.response.setBody(JSON.stringify(response));
