var conn = $.hdb.getConnection();

var response = {
	languages : []
};

var sql = "select LANGUAGE_CODE, LANGUAGE_NAME from \"SYS\".\"M_TEXT_ANALYSIS_LANGUAGES\" ";
var rs = conn.executeQuery(sql);
var result = [], currentRow, itr=rs.getIterator();
while (itr.next()) {
	currentRow = itr.value();
	result.push({
    	id: currentRow["LANGUAGE_CODE"], 
        name: currentRow["LANGUAGE_NAME"]
    });
}
response.languages = result;

conn.close();

$.response.status = $.net.http.OK;
$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(response));
