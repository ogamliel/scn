var conn = $.hdb.getConnection();
var sql = "select VALUE from \"PROPERTIES\" where PROPERTY_NAME = 'GOOGLE_TRANSLATE_KEY' ";
var rs = conn.executeQuery(sql);
var result, currentRow, itr=rs.getIterator();
if (itr.next()) {
	currentRow = itr.value();
	result = currentRow["VALUE"];
}
conn.close();

$.response.status = $.net.http.OK;
$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({key: result}));
