var translationOfEntityid = $.request.parameters.get("entityid");
var langId = $.request.parameters.get("langId");
var conn = $.hdb.getConnection();
var sql = 'select ENTITYID from "XML_CONTENT" where TRANSLATION_OF_ENTITYID = \'' + translationOfEntityid + '\' and LANGUAGE_ID = \'' + langId + '\'';
var rs = conn.executeQuery(sql);
var translated = false, currentRow, itr=rs.getIterator();
while (itr.next())  {
	currentRow = itr.value();
	translated = true;
}
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify({"translated" : translated }));
$.response.status = $.net.http.OK;
