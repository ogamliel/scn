function getAdditionalWhere(from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds)
{
     var clause = '';
     var parameters = [];
     if(from && to)
     {
      //   clause += " AND ARTICLE_YEAR between ? and ? ";
         clause += " AND ARTICLE_DATE between TO_DATE(?) and TO_DATE(?) ";
         parameters.push(from);
         parameters.push(to);
     }

     if(subjects)
     {
		 clause += ' AND (';
		 if (Array.isArray(subjects)) {
	    	 for (var i = 0; i < subjects.length; i++) {
	    		 var site = parseCategory(subjects[i]);
	    		 var source = parseItem(subjects[i]);
	    		 (i>0) ? clause += ' OR ' : '';
	    		 clause += getSiteSourceClause(site, source);
	    	 }
		 }
		 else  {
			 var site = parseCategory(subjects);
			 var source = parseItem(subjects);
			 clause += getSiteSourceClause(site, source);
		 }
		 clause += ')';
     }
     
     if (contentTypes)  {
    	 clause += ' AND (';
		 if (Array.isArray(contentTypes)) {
	    	 for (var i = 0; i < contentTypes.length; i++) {
	    		 var objectType = parseCategory(contentTypes[i]);
	    		 var articleType = parseItem(contentTypes[i]);
	    		 (i>0) ? clause += ' OR ' : '';
	    		 clause += "(OBJECT_TYPE='" + objectType + "' AND ARTICLE_TYPE='" + articleType +"') ";
	    	 }
		 }
		 else  {
			 var objectType = parseCategory(contentTypes);
			 var articleType = parseItem(contentTypes);
    		 clause += "(OBJECT_TYPE='" + objectType + "' AND ARTICLE_TYPE='" + articleType +"') ";
		 }
		 clause += ')';
     }
     
     if (interactionTypes)  {
    	 clause += ' AND (';
		 if (Array.isArray(interactionTypes)) {
	    	 for (var i = 0; i < interactionTypes.length; i++) {
	    		 var interactionType = parseItem(interactionTypes[i]);
	    		 (i>0) ? clause += ' OR ' : '';
	    		 clause += "(INTERACTION ='" + interactionType + "') ";
	    	 }
		 }
		 else  {
			 var interactionType = parseItem(interactionTypes);
    		 clause += "(INTERACTION ='" + interactionType + "') ";
		 }
		 clause += ')';
     }

     if (sentimentStart && sentimentEnd)  {
    	 clause += ' AND (';
    	 clause += 'DOC_SENTIMENT_SCORE between ' + sentimentStart + ' AND ' + sentimentEnd;
    	 clause += ')';
     }
     
     if (mapBounds)  {
    	 if (Array.isArray(mapBounds)) {
	    	 for (var i=0; i<mapBounds.length; i++)  {
	    		 clause += ' AND (new ST_POLYGON(\'POLYGON((' + mapBounds[i].x + ' ' + mapBounds[i].y + ', ' +  mapBounds[i].x1 + ' ' + mapBounds[i].y + ', ' +  
	    		 mapBounds[i].x1 + ' ' + mapBounds[i].y1 + ', ' +  mapBounds[i].x + ' ' +  mapBounds[i].y1 + ', ' + mapBounds[i].x + ' ' + mapBounds[i].y + 
	    		 '))\').ST_Intersects("GEOLOCATION")) = 1';
	    	 }
    	 }
    	 else  {
    		clause += ' AND (new ST_POLYGON(\'POLYGON((' + mapBounds.x + ' ' + mapBounds.y + ', ' +  mapBounds.x1 + ' ' + mapBounds.y + ', ' +  
    		mapBounds.x1 + ' ' + mapBounds.y1 + ', ' +  mapBounds.x + ' ' +  mapBounds.y1 + ', ' + mapBounds.x + ' ' + mapBounds.y + '))\').ST_Intersects("GEOLOCATION")) = 1';
    	 }
     }

     if (persons || orgs || localities)  {
    	 clause += ' AND (';
    	 var addAnd = false;
 
	     if(persons)  {
	    	 var c = getEntityClause(persons, 'PERSON', addAnd, 't2');
	    	 if (c)  {
	    		 clause += c;
	    		 addAnd = true;
	    	 }
	     }		 
	
	     if(orgs)  {
	    	 var c = getEntityClause(orgs, 'ORGANIZATION', addAnd, 't3');
	    	 if (c)  {
	    		 clause += c;
	    		 addAnd = true;
	    	 }
	     }		 
	
	     if(localities)  {
	    	 var c = getEntityClause(localities, 'LOCALITY', addAnd, 't4');
	    	 if (c)  {
	    		 clause += c;
	    		 addAnd = true;
	    	 }
	     }	
	     
	     clause += ')';
     }


     if(publishers)
     {
    	 var unkIdx = publishers.indexOf('Unknown');
         if (unkIdx != -1)  {
                clause += ' and (ARTICLE_AUTHOR is null or ARTICLE_AUTHOR = \'\') ';
         }
         else  {
              var inClause = getInClause("ARTICLE_AUTHOR", publishers);
              clause += inClause.clause;
              parameters = parameters.concat(inClause.parameters);                  
         }
     }
          
     return { clause: clause, parameters: parameters };
}

function getAdditionalWhereForMap(from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds)  {
	var result = { clause: '', parameters: []};

	if (publishers ||sentimentStart)  {
		if (publishers)  {
	    	 var unkIdx = publishers.indexOf('Unknown');
	         if (unkIdx != -1)  {
	                result.clause += ' and (DOC_AUTHOR_NAME is null or ARTICLE_AUTHOR = \'\') ';
	         }
	         else  {
	              var inClause = getInClause("DOC_AUTHOR_NAME", publishers);
	              result.clause += inClause.clause;
	              result.parameters = inClause.parameters;
	         }
		}
		
		if (sentimentStart && sentimentEnd)  {
	    	 result.clause += ' AND (';
	    	 result.clause += 'DOC_SENTIMENT_SCORE between ' + sentimentStart + ' AND ' + sentimentEnd;
	    	 result.clause += ')';
		}
	}
	else  {
		var res = getAdditionalWhere(from, to, null, subjects, persons, orgs, localities, contentTypes, null, null, interactionTypes, mapBounds);
		if (res.clause)  {
			result.clause += ' AND DOC_ID IN (select ARTICLE_KEY from "XML_CONTENT" c where 1=1 ' + res.clause + ') ';
			result.parameters.push.apply(result.parameters, res.parameters);
		}
	}
	return result;
}

function getEntityClause(items, entityType, addAnd, alias)  {
	var clause = '';
	var oEntity = { type: entityType, tokens:[] };
	if (Array.isArray(items)) {
		 for (var i = 0; i < items.length; i++) {
			 var token = parseItem(items[i]);    		 
			 oEntity.tokens.push(token);
		 } 
	}
	else  {
		 var token = parseItem(items);
		 oEntity.tokens.push(token);			 
	}
	
	clause += (addAnd) ? ' AND EXISTS ' : ' EXISTS ';
	var thisAddAnd = false;
	if (oEntity.tokens.length > 0)  {
		 if (thisAddAnd) {
			 clause += ' AND EXISTS ';
			 clause += getTypeTokenExistsClause(oEntity, alias);
		 }
		 else  {
			 clause += getTypeTokenExistsClause(oEntity, alias);
			 thisAddAnd = true;
		 }
	}
	
	return clause;
}

function getTypeTokenClause(oEntity)  {
	var clause = "(t.TA_TYPE = '" + oEntity.type.toUpperCase() + "' AND t.TA_TOKEN IN ( ";
	
	if (oEntity.type.toUpperCase() === 'ORGANIZATION')  {
		clause = "(TA_GROUP = 'ORGANIZATION' AND TA_TOKEN IN ( ";
	}
	for (var i=0; i<oEntity.tokens.length; i++)  {
		(i>0) ? clause += " ," : " ";
		clause += "'" + oEntity.tokens[i] + "' ";
	}
	clause += ")) ";	
	
	return clause;
}

function getTypeTokenExistsClause(oEntity, alias)  {

	var clause = "(select distinct 'X' from TA_FTI_ARTICLE " + alias + " ";

	if (oEntity.type.toUpperCase() === 'ORGANIZATION' ) {
		//clause += "where (" + alias + ".TA_TYPE like 'ORG%' or " + alias + ".TA_TYPE = 'TerrorismOrganizations') ";
		clause += "where (contains (" + alias + ".TA_TYPE,'Organization') or " + alias + ".TA_TYPE = 'TerrorismOrganizations') ";
		clause += "and " + alias + ".TA_TOKEN  IN (" + getEntityTokens(oEntity.tokens) + ") ";	
	}
	else  {
		clause += "where " + alias + ".TA_TYPE = '" + oEntity.type.toUpperCase();
		clause += "' AND " + alias + ".TA_TOKEN IN ( " + getEntityTokens(oEntity.tokens) + ") ";
	}
	clause += "and c.article_key = " + alias + ".article_key) ";

	return clause;
}

function getEntityTokens(tokens)  {
	var clause = "";
	for (var i=0; i<tokens.length; i++)  {
		(i>0) ? clause += " ," : " ";
		clause += "'" + tokens[i] + "' ";
	}
	return clause;
}

function parseCategory(val)  {
	 var category = val.substring(0, val.indexOf(':'));
	 return category;
}
function parseItem(val)  {
	 var item = val.substring(val.indexOf(':')+2);
	 return item;
}

function getSiteSourceClause(site, source)  {
	var ssClause = "";
	 if (site === 'Unknown')  {
		 ssClause = "(SITETYPE is null AND SOURCE_NAME='" + source +"') ";
	 }
	 else  {
		 ssClause += "(SITETYPE='" + site + "' AND SOURCE_NAME='" + source +"') ";
	 }
	return ssClause;
}

function getInClause(colName, list)
{
     var clause = '';
     var parameters = [];
     if(list)
     {
        clause += " AND " + colName + " IN (";
         if (Array.isArray(list)) {
             for (var i = 0; i < list.length; i++) {
                 parameters.push(list[i] || "");
                 clause += (i == 0 ? "?" : ",?"); 
             }
         } else {
             parameters.push(list || "");
             clause += "?";
         }
         clause += ") ";
     }
     return { clause: clause, parameters: parameters };
}

function getNotInClause(colName, list)
{
     var clause = '';
     var parameters = [];
     if(list)
     {
         clause += " AND " + colName + " NOT IN (";
         if (Array.isArray(list)) {
             for (var i = 0; i < list.length; i++) {
                 parameters.push(list[i] || "");
                 clause += (i == 0 ? "?" : ",?"); 
             }
         } else {
             parameters.push(list || "");
             clause += "?";
         }
         clause += ") ";
     }
     return { clause: clause, parameters: parameters };
}

function applyParameters(pstmt, parameters, startIndex, lowercase)
{
    if(typeof startIndex == "undefined")  startIndex = 1;
    if(typeof lowercase == "undefined") lowercase = false;
    for(var i = 0; i < parameters.length; i++)  {
        var value = parameters[i];
        if(typeof value === "number") {
            pstmt.setInt(startIndex + i, value);
        } 
        else  {
            pstmt.setString(startIndex + i, lowercase ? value.toLowerCase() : value);
        }
    }
    //return the new startIndex
    return startIndex + parameters.length;
}


function queryCount(conn, groupByColumn, searchQuery, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds) {  
                var searchString = searchQuery;
                
                var additionalWhere = getAdditionalWhere(from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, sentimentEnd, interactionTypes, mapBounds);
               
                if(searchString) { additionalWhere.clause += " AND contains(c.article_content, ?) "; additionalWhere.parameters.push(searchString); }
           
                var sql = "SELECT " + 
                (groupByColumn ? groupByColumn + ", " : "") + 
                "              count(*) as cnt  " + 
                "FROM XML_CONTENT  c " +
                "WHERE 1=1 " + additionalWhere.clause + 
                (groupByColumn ? " GROUP BY " + groupByColumn + " " : " ") + 
                "ORDER BY cnt DESC LIMIT 100";
                //"with hint (OLAP_PARALLEL_AGGREGATION)";

                var pstmt = conn.prepareStatement(sql);              
                applyParameters(pstmt, additionalWhere.parameters, 1);               
                return pstmt;
}

function getCount(conn, groupByColumn, searchQuery, from, to, publishers, subjects, persons, orgs, localities, contentTypes, sentimentStart, 
		sentimentEnd, interactionTypes, mapBounds) {       
                var pstmt = queryCount(conn, groupByColumn, searchQuery, from, to, publishers, subjects, persons, orgs, localities, contentTypes, 
                		sentimentStart, sentimentEnd, interactionTypes, mapBounds);
                
                var index = groupByColumn ? 2 : 1;
                var count = 0;

                var rs = pstmt.executeQuery();
                while (rs.next())  {
                	count += rs.getInteger(index);
                }
                rs.close();
                pstmt.close();

                return count;
}                              

