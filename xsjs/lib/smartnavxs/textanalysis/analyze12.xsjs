var body = decodeURIComponent($.request.body.asString());
var oText = JSON.parse(body);

var ta = new $.text.analysis.Session({
	//configuration:'EXTRACTION_CORE'
    configuration: 'ndextractioncorepublicsector12'
});

// Call the analyze method. Explicitly set the language, although the default is English anyway
var result = ta.analyze({inputDocumentText: oText.text, language: 'en'});

// Send the results back
$.response.contentType = 'application/json';
$.response.setBody(JSON.stringify(result));
