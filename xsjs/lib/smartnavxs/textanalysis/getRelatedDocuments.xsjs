
var conn = $.hdb.getConnection();

var tm = new $.text.mining.Session({
    referenceTable: "DOCUMENTS",
    referenceColumn: "DOC_CONTENT",
    connection: conn
});

var result = tm.getRelatedDocuments ({
    top: 16,
    //inputDocumentIDs: "1E0FA257E233B887E11800000A015864",
    inputDocumentText: "Boko",
    includeColumns: ["DOC_CONTENT", "ENTITYID"]
});

console.log(result); 

// Send the results back
$.response.contentType = 'text/json';
$.response.setBody(JSON.stringify(result));