var conn = $.db.getConnection();
var result = {};

var stmt = 'select PROPERTY_NAME, VALUE from "PROPERTIES" where PROPERTY_NAME in (\'XS_TA_SERVICE_HOST\', \'XS_TA_SERVICE_PORT\') ';
var pstmt = conn.prepareStatement(stmt);
var rs = pstmt.executeQuery();
var result;
while(rs.next())  {
	if (rs.getString(1) === 'XS_TA_SERVICE_HOST')  {
		result.host = rs.getString(2);
	}
	else  {
		result.port = rs.getString(2);		
	}
}
rs.close();
conn.close();

$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(result));
$.response.status = $.net.http.OK;