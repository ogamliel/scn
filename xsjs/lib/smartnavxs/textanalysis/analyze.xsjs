var text = $.request.parameters.get("text") || "";
// e.g., indexConfigFC.xml indexConfigNER.xml
var config = $.request.parameters.get("config") || "indexConfigNER.xml";
//e.g. "text/html"
var mimeType = $.request.parameters.get("mimeType") || null;
//array of returned token types,e.g., [$.db.textanalysis.WT_VERB,$.db.textanalysis.WT_NOUN];
var filter = $.request.parameters.get("filter") || [];
//e.g. "de"
var language = $.request.parameters.get("language") || "en";

var res = {};
if ($.db.textanalysis) {
	// we're on XS engine
	res = $.db.textanalysis.analyse(text,filter,language ,mimeType,config)
} else {
	// we're on jXS: return fake answer
	var entities = [];
	var tokens = [];
	var words = text.replace(/(\.|,|\?|\!)/g, ""). split(' ');
	for (var i = 0; i < words.length; ++i) {
		entities.push({
			"paragraph": -1,
			"sentence": -1,
			"offset": -1,
			"text": words[i],
			"normForm": "",
			"labelPath": "FAKE",
			"label": "FAKE"
		});
		tokens.push({
			"term": words[i],
			"nterm": words[i].toLowerCase(),
			"itype": 0,
			"stype": "unknown",
			"stems": [ ],
			"offset": -1,
			"paragraph": -1,
			"sentence": -1			
		});
	}
	
	res = {
			"namedEntities" : entities,
			"tokens" : tokens
	};
} 
$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(res));