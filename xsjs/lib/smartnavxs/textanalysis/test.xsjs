//var body = decodeURIComponent($.request.body.asString());
//var oText = JSON.parse(body);
var oText = "New York, New York is great.";

var ta = new $.text.analysis.Session({
    configuration: 'sap.hana.ta.config::EXTRACTION_CORE_PUBLIC_SECTOR.hdbtextconfig'
    //configuration: 'CUSTOM_TA.TA.config::ND_EXTRACTION_CORE_PUBLIC_SECTOR12'
});


// Call the analyze method. Explicitly set the language, although the default is English anyway
var result = ta.analyze({inputDocumentText: oText, language: 'en'});

// Send the results back
$.response.contentType = 'application/json';
$.response.setBody(JSON.stringify(result));
