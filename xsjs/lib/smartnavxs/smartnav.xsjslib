/**
 * @fileOverview Reusable smartnav functions.
 * @author Michelle Esler
 * @version 1.0
 */

/**
 * smartnav Namespace.
 * @namespace
 */
var smartnav = {
	getSchema : function()  {
		var conn = $.hdb.getConnection();
		var rs = conn.executeQuery('SELECT CURRENT_SCHEMA FROM DUMMY;');
		var itr = rs.getIterator();
		var schema;
	
		if (itr.next())  {
			var currentRow = itr.value();
			schema = currentRow["CURRENT_SCHEMA"];
		}
		conn.close();
		return schema;
	}

};
	