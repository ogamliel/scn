var sql = "select title, name, photo_url, source, title2 " +
	   "from \"MEMBER_PICS\" " +
	   "where CONTAINS(name,?,fuzzy(0.7)) "; 

var name = $.request.parameters.get("name");

var data = [];
var conn = $.db.getConnection();
var pstmt = conn.prepareStatement(sql);
pstmt.setString(1, name);
var rs = pstmt.executeQuery();

while (rs.next()) {
	data.push({
		title : rs.getString(1),
		name : rs.getString(2),
		photo : rs.getString(3),
		source : rs.getString(4),
		title2 : rs.getString(5)
	});
}

rs.close();
pstmt.close();
var result = { "data" : data };
$.response.setBody(JSON.stringify(result));	    	
    