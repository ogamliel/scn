var locality = $.request.parameters.get("locality");

var sql = "select LAT_VAL, LONG_VAL from \"USG_TOKEN_LAT_LONG\" " +
"where TA_TYPE='LOCALITY' and TA_TOKEN = '" + locality + "'";

var conn = $.hdb.getConnection();
var rs = conn.executeQuery(sql);
var data = [], currentRow, itr=rs.getIterator();

while (itr.next()) {
	currentRow = itr.value();
	data.push({
		lat : currentRow["LAT_VAL"],
		long : currentRow["LONG_VAL"]
	});
}
conn.close();

var result = { "data" : data };
$.response.setBody(JSON.stringify(result));
